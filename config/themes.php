<?php

return array(

    'default' => 'carlamp',

    'path' => base_path('themes'),

    'cache' => [
        'enabled' => false,
        'key' => 'pingpong.themes',
        'lifetime' => 86400,
    ],

);
