<?php

return [
    'api_user' => env('ESPUTNIK_USERNAME', null),
    'api_password' => env('ESPUTNIK_PASSWORD', null),
];