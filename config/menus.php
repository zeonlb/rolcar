<?php

return array(

    'styles' => array(
        #Admin main navigation presenter
        'admin-navigation' => 'Modules\Admin\Presenters\MainNavigation',
        'cms.main.navigation' => 'Modules\Cms\Presenters\Menu\MainNavigation',
        'cms.footer.navigation' => 'Modules\Cms\Presenters\Menu\FooterNavigation',
        'navbar' => 'Pingpong\Menus\Presenters\Bootstrap\NavbarPresenter',
        'navbar-right' => 'Pingpong\Menus\Presenters\Bootstrap\NavbarRightPresenter',
        'nav-pills' => 'Pingpong\Menus\Presenters\Bootstrap\NavPillsPresenter',
        'nav-tab' => 'Pingpong\Menus\Presenters\Bootstrap\NavTabPresenter',
        'sidebar' => 'Pingpong\Menus\Presenters\Bootstrap\SidebarMenuPresenter',
        'navmenu' => 'Pingpong\Menus\Presenters\Bootstrap\NavMenuPresenter',
    ),

    'ordering' => true,

);
