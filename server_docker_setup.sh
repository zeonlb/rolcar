#!/bin/bash
sudo docker-compose exec php composer install &&

sudo docker-compose exec php rm -rf /var/www/public/themes &&
sudo docker-compose exec php php /var/www/artisan  theme:asset &&

sudo docker-compose exec php rm -rf  /var/www/public/modules &&
sudo docker-compose exec php php  /var/www/artisan module:asset &&
sudo docker-compose exec php composer install &&

sudo docker-compose exec php chgrp -R www-data storage bootstrap/cache public themes/rolcar/views/layouts &&
sudo docker-compose exec php chmod -R ug+rwx storage bootstrap/cache public themes/rolcar/views/layouts

