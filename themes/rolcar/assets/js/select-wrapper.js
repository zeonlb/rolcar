

function SelectWrapper() {
    this.init = function() {
        $('.select-wrapper').unbind('hover');
        $('.car-option-container').unbind('click');
        //    SLIDER
        $('.select-wrapper').hover(
            function(){
                if (!$(this).find('.dropdown').attr('sel')) {
                    $(this).find('.dropdown').show();
                    var arrowImage = $(this).find('.select-arrow');
                    if (arrowImage.length) {
                        arrowImage.attr('src', arrowImage.attr('src').replace('select-arrow-btn.png', 'select-arrow-btn-hover.png') );
                        arrowImage.attr('src', arrowImage.attr('src').replace('select-arrow-yellow-btn.png', 'select-arrow-yellow-btn-hover.png') );
                    }
                }
            },
            function(){
                $(this).find('.dropdown').hide();
                var arrowImage = $(this).find('.select-arrow');
                if (arrowImage.length)
                    arrowImage.attr('src', arrowImage.attr('src').replace('select-arrow-btn-hover.png', 'select-arrow-btn.png') );
            }
        );
        //  ON CHOOSE
        $('.car-option-container').on('click', '.select-wrapper .dropdown li',
            function(){
                if ($(this).attr('value')) {
                    var wrapper = $(this).parent().parent();

                    var arrowImage = wrapper.find('.select-arrow');
                    wrapper.attr('defaultValue', wrapper.find('span').text());
                    $(this).parent().attr('sel', $(this).text()).attr('val', $(this).attr('value')).hide();
                    wrapper.find('.chosen-value>span').html($(this).text());
                    if (arrowImage.length) {
                        arrowImage.addClass('close-btn').attr('src','/themes/carlamp/images/select-close-btn.png')
                    }
                    if (wrapper.attr('callback'))
                    {
                        $.filterCatalog[wrapper.attr('callback')]($(this).attr('value'));
                    }
                }

            }
        )
        //    CLOSE BTN
        $('body').on('click', '.select-wrapper .close-btn',
            function(){
                var wrapper = $(this).parent().parent();
                wrapper.find('.dropdown').removeAttr('sel').removeAttr('val').show();
                wrapper.find('span').text(wrapper.attr('defaultValue'));
                var arrowImage = wrapper.find('.select-arrow');
                if (arrowImage.length) {
                    arrowImage.removeClass('close-btn').attr('src', '/themes/carlamp/images/select-arrow-btn-hover.png');
                }
            }
        );

    };
};
