var selectWrapper = new SelectWrapper();
String.prototype.ucFirst = function() {
    var str = this;
    if(str.length) {
        str = str.charAt(0).toUpperCase() + str.slice(1);
    }
    return str;
};
var loader = new FilterLoader();
$('#filter-contact').mask("(999) 999-99-99");
var filterParams = $.cookie('chfp');
var url = window.location.href.split('/');
var request = new Request();
// if (filterParams && !url[3]) {
if (filterParams) {
    loader.start();
    var filterData = JSON.parse(filterParams);
    if (filterData.step) {
        switch (filterData.step) {
            case 1:
                if (filterData.params.id) {
                    step1(filterData.params.id);
                }
                break;
            case 2:
                if (filterData.params) {
                    step2(filterData.params, filterData.data);
                }
                break;
        }
    }
}
var typeId = request.get('type');
var markId = request.get('mark');
if (typeId) {
    step1(typeId, markId);
}


loader.finish();
function addResetBtn(){
    var btnText = 'Сбросить автомобиль &nbsp;&nbsp;';
    var w = window.innerWidth;
    if (w < 600) {
        btnText = ''
    }
    $('.car-option-container .container').append('' +
        '<button type="button" class="filter-reset btn btn-default btn-sm" aria-label="Сбросить фильтр">' +
        btnText+'<span class="glyphicon glyphicon-refresh" aria-hidden="true"></span>' +
        '</button>'
    );
}

$('.first-step-option').on('click', function () {
    var id = $(this).attr('data-tid');
    var elId = $(this).attr('id');
    step1(id);

});
$('body').on('click', '.filter-reset',function () {
    $.cookie('chfp', '');
    window.location.reload()
});
function step1(id, mark) {
    $.ajax({
        url: '/api/car/filter/step1',
        type: "POST",
        data: {"id": id},
        beforeSend: function () {
            loader.start();
        },
        success: function (response) {
            if (response.length) {
                $('.car-option-container').html(response);
                selectWrapper.init();
                $.filterCatalog.transportId = id;
                $.filterSearch.init('mark');
                $.cookie('chfp', JSON.stringify({"step": 1, "params": {"id": id}}));
                addResetBtn();
                if (mark) {
                    $('.dropdown[data-source="mark"] li:contains("'+mark.ucFirst()+'")').trigger('click');
                }
                loader.finish(300);
            }
        }
    });
}

function step2(params, data) {
    $.ajax({
        url: '/api/car/filter/step2',
        type: "POST",
        data: params,
        beforeSend: function () {
            loader.start();
        },
        success: function (response) {
            if (response.length) {
                $('.car-option-container').html(response);
                if (data.checkSizes['it'].length == 0) {
                    $('body').find('.navig[source="inside"]').addClass('disabled');
                }

                if (data.checkSizes['bt'].length == 0) {
                    console.log($('.navig[source="back"]'))
                    $('body').find('.navig[source="back"]').addClass('disabled');
                }
                if (data.checkSizes['ft'].length == 0) {
                    $('body').find('.navig[source="front"]').addClass('disabled');
                }

                $.filterCatalog.markId = data.markId;
                $.filterCatalog.modelId = data.modelId;
                $.filterCatalog.transportId = data.transportId;
                $.filterCatalog.modificationId = data.modificationId;
                $.filterCatalog.selectedYear = data.selectedYear;
                $.filterCatalog.selectedNames = data.selectedNames;
                $.filterCatalog.selectedType = data.selectedType;
                $.filterCatalog.motorId = data.motorId;
                addResetBtn();
                loader.finish(5000);
            }

        }
    });
}

$('body').on('click', '.schema-navigation .nav-container .navig', function (e) {
    e.preventDefault();
    if ($(this).hasClass("disabled")) {
        return false;
    }
    var enabledValues = ['front', 'inside', 'back'];
    var activeNav = $('.schema-navigation .nav-container .navig.active');
    var source = $(this).attr('source');
    if (source == 'nav-left') {
        var prev = activeNav.parent().prev().find('.navig');
        if (prev && prev.attr('source')) {
            source = prev.attr('source');
        }
    }
    if (source == 'nav-right') {
        var next = activeNav.parent().next().find('.navig');
        if (next && next.attr('source')) {
            source = next.attr('source');
        }
    }
    if (enabledValues.indexOf(source) >= 0) {
        $.filterCatalog.changeSide(source);
    }
});
$('body').on('click', '.chosen-value i', function () {
    if ($.filterCatalog.transportId) {
        $.ajax({
            url: '/api/car/filter/step1',
            type: "POST",
            data: {"id": $.filterCatalog.transportId},
            beforeSend: function () {
                loader.start();
            },
            error: function () {
                loader.finish();
            },
            success: function (response) {
                if (response.length) {
                    $('.car-option-container').html(response);
                    selectWrapper.init();
                    $.filterCatalog.transportId = $.filterCatalog.transportId;
                    $.filterSearch.init('mark');
                    $.cookie('chfp', JSON.stringify({"step": 1, "params": {"id": $.filterCatalog.transportId}}))
                    addResetBtn();
                    loader.finish();
                }

            }
        });
    }
});
$('body').on('click', '#filter-question-modal-select li', function () {
    var questAns = $(this).attr('value');
    var params = {"motorId": $.filterCatalog.motorId, lampType: $(this).parent().attr('nid'), questionAns: questAns};
    if (questAns) {
        $.ajax({
            url: '/api/car/filter/step3',
            type: "POST",
            data: params,
            success: function (res) {
                if (res.success) {
                    // $.cookie('chfp', JSON.stringify({"step": 3, "params": params}));
                    window.location.href = res.message;
                } else if(!res.success && res.message == 'LED'){
                    loader.finish();
                    $('.led-tech-info-modal-dialog').show();
                } else {
                    loader.finish();
                    alert(res.message);
                }
            },
            error: function (res) {
                loader.finish();
                alert("Произошла ошибка в фильтре");
            }
        });
    }
});

$('#filter-question-modal').on('click', '.filter-question-block', function () {
    var toggeled = $(this).attr('toggeled');
    if (!toggeled) {
        $('.filter-modal-body').css({"height": 420});
        $('.filter-modal-body').addClass('filter-modal-body-changed');
        $(this).attr('toggeled', 'on');
        $('.filter-form-add-phone').show()
    } else {
        $('.filter-modal-body').css({"height": 343});
        $('.filter-modal-body').removeClass('filter-modal-body-changed');
        $(this).removeAttr('toggeled');
        $('.filter-form-add-phone').hide()
    }


});

$('body').on('click', '.filter-contact-btn', function (e) {
    e.preventDefault();
    var phone = $(this).parent().find('#filter-contact').val();
    var message = 'Chosen car params: ' + $.filterCatalog.selectedNames + ' | ' + $.filterCatalog.selectedYear;
    if (phone) {
        $.ajax({
            url: '/api/v1/form-request',
            type: "POST",
            data: {"phone": phone, "pages_type": message, "source": "Carlamp", "form_name": "Технология" },
            success: function (res) {
		ga('send', 'event', 'Teshnologia', 'Send');
    		fbq('track', 'Lead');
                alert("Спасибо за заявку. В бижайшее время с вами свяжется менеджер.");
                window.location.reload();
            },
            beforeSend: function () {
                loader.start();
            },
            complete: function () {
                loader.finish();
            },
            error: function (res) {
                alert("Произошла ошибка в фильтре");
            }
        });
    } else {
        alert('Вы не ввели номер телефона');
    }

    if (!toggeled) {
        $('.filter-modal-body').css({"height": 398});
        $('.filter-modal-body').addClass('filter-modal-body-changed');
        $(this).attr('toggeled', 'on');
        $('.filter-form-add-phone').show()
    } else {
        $('.filter-modal-body').css({"height": 343});
        $('.filter-modal-body').removeClass('filter-modal-body-changed');
        $(this).removeAttr('toggeled');
        $('.filter-form-add-phone').hide()
    }


});

$('body').on('click', '.schema-lamp .description-block p', function () {
    if ($.filterCatalog.motorId) {
        var nid = $(this).parent().attr('nid');
        $.ajax({
            url: '/api/car/filter/find-questions',
            type: "POST",
            data: {"motorId": $.filterCatalog.motorId, lampType: nid},
            beforeSend: function () {
                loader.start();
            },
            complete: function () {
                loader.finish();
            },
            success: function (res) {
                if (res.length > 1) {
                    selectWrapper.init();
                    var html = '';
                    for (var i in res) {
                        html += '<li value="' + res[i]['tsokol'] + '">' + res[i]['question']['message'] + '</li>';
                    }
                    $('#filter-question-modal-select').html(html);
                    $('#filter-question-modal-select').attr('nid', nid);
                    $('.filter-question-modal-dialog').show();
                } else {
                    $.ajax({
                        url: '/api/car/filter/step3',
                        type: "POST",
                        data: {"motorId": $.filterCatalog.motorId, lampType: nid},
                        success: function (res) {
                            if (res.success) {
                                window.location.href = res.message;
                            } else if(!res.success && res.message == 'LED'){
                                loader.finish();
                                $('.led-tech-info-modal-dialog').show();
                            } else {
                                loader.finish();
                                alert(res.message);
                            }
                        },
                        error: function (res) {
                            loader.finish();
                            alert('Ошибка '+res);
                        }
                    });
                }
            },
            error: function (res) {
                alert("Произошла ошибка в фильтре");
            }
        });
    }
});


$.filterCatalog = {
    selectedType: null,
    transportId: null,
    modificationId: null,
    modelId: null,
    markId: null,
    motorId: null,
    selectedYear: null,
    selectedNames: null,

    model: function (id) {
        $.filterCatalog.markId = id;
        $.ajax({
            url: '/api/car/model/find-by-mark-id',
            type: "POST",
            data: {"markId": id, 'transportId': $.filterCatalog.transportId},
            beforeSend: function () {
                loader.start();
            },
            complete: function () {
                loader.finish();
            },
            success: function (response) {
                if (response.length) {
                    var modelSelect = $('.select-wrapper .model');
                    modelSelect.find('li').remove();
                    for (var i in response) {
                        modelSelect.append("<li value='" + response[i].id + "'>" + response[i].name + "</li>");
                    }
                    modelSelect.show().find('.dropdown').focus();
                    selectWrapper.init();
                    $.filterSearch.init('model');
                }

            }
        })
    },
    modification: function (year) {
        $.filterCatalog.selectedYear = year;
        $.ajax({
            url: '/api/car/modification/find-by-model-id-and-year',
            type: "POST",
            data: {"modelId": $.filterCatalog.modelId, "year": year},
            beforeSend: function () {
                loader.start();
            },
            success: function (response) {
                var modificationSelect = $('.select-wrapper .modification');
                modificationSelect.find('li').remove();
                if (response.length) {
                    for (var i in response) {
                        modificationSelect.append("<li value='" + response[i].id + "'>" + response[i].name + " (" + response[i].type.name + ")</li>");
                    }
                    modificationSelect.show().find('.dropdown').focus();
                    selectWrapper.init();
                    $.filterSearch.init('modification');
                }
                loader.finish();
            },
            error: function () {
                loader.finish();
                alert("Произошла ошибка в фильтре");
            }
        })
    },
    "year": function (id) {
        $.filterCatalog.modelId = id;

        $.ajax({
            url: '/api/car/motor/find-by-model-id-return-years',
            type: "POST",
            data: {"modelId": id},
            beforeSend: function () {
                loader.start();
            },
            error: function () {
                loader.finish();
            },
            success: function (response) {
                var yearSelect = $('.select-wrapper .year');
                yearSelect.find('li').remove();
                if (response.length) {
                    for (var i in response) {
                        yearSelect.append("<li value='" + response[i] + "'>" + response[i] + "</li>");
                    }
                    yearSelect.show();
                    selectWrapper.init();
                    $.filterSearch.init('year');
                }
                loader.finish();

            }
        })

    },
    "motor": function (id) {
        $.filterCatalog.modificationId = id;
        $.ajax({
            url: '/api/car/motor/find-by-modification-id',
            type: "POST",
            data: {"modificationId": id, "year": this.selectedYear},
            beforeSend: function () {
                loader.start();
            },
            error: function () {
                loader.finish();
            },
            success: function (response) {
                var motorSelect = $('.select-wrapper .motor');
                motorSelect.find('li').remove();
                if (response.length) {
                    for (var i in  response) {
                        var html = "<li value='" + response[i].id + "'>" + response[i].motor;
                        if (response[i].power)      {
                            html += "(" + Math.ceil(response[i].power / 1.3596) + " Kw)";
                        }
                        html +=  "</li>";
                        motorSelect.append(html);
                    }
                    if (response.length == 1) {
                        motorSelect.find('li').trigger('click');
                    }else {
                        motorSelect.parent().removeClass('hidden');
                        motorSelect.show().find('.dropdown').focus();
                        selectWrapper.init();
                        $.filterSearch.init('motor');
                    }
                }
                loader.finish();

            }
        })
    },
    finish: function (id) {
        $.filterCatalog.motorId = id;
        if ($.filterCatalog.markId && $.filterCatalog.modelId && $.filterCatalog.modificationId &&
            $.filterCatalog.transportId && $.filterCatalog.selectedYear
        ) {
            $.filterCatalog.selectedNames = $('.chosen-value span').map(function () {
                if ($(this).text().indexOf("Выберите") == -1 && $(this).text().length > 0) {
                    console.log($(this).text())
                    console.log($(this).text().replace(/\(.+\)/, '').trim())
                    return $(this).text().replace(/\(.+\)/, '').trim()
                }
            }).get().join('|');
            $.ajax({
                url: '/api/car/filter/check-enabled-sides',
                type: "POST",
                data: {
                    transportId: $.filterCatalog.transportId,
                    motorId: $.filterCatalog.motorId,
                },
                beforeSend: function () {
                    loader.start();
                },
                error: function () {
                    loader.finish();
                },
                success: function (response) {
                    var data = response;

                    if (response['it'].length == 0) {
                        $('body').find('.navig[source="inside"]').addClass('disabled');
                    } else {
                        active = 'inside';
                    }

                    if (response['bt'].length == 0) {
                        console.log($('.navig[source="back"]'))
                        $('body').find('.navig[source="back"]').addClass('disabled');
                    } else {
                        active = 'back';
                    }
                    if (response['ft'].length == 0) {
                        $('body').find('.navig[source="front"]').addClass('disabled');
                    } else {
                        active = 'front';
                    }
                    var params  = {
                        transportId: $.filterCatalog.transportId,
                        motorId: $.filterCatalog.motorId,
                        names: $.filterCatalog.selectedNames,
                        side: active
                    };
                    $.ajax({
                        url: '/api/car/filter/step2',
                        type: "POST",
                        data: params,
                        success: function (response) {
                            if (response.length) {
                                $('.car-option-container').html(response);
                                if (data['it'].length == 0) {
                                    $('body').find('.navig[source="inside"]').addClass('disabled');
                                }

                                if (data['bt'].length == 0) {
                                    console.log($('.navig[source="back"]'))
                                    $('body').find('.navig[source="back"]').addClass('disabled');
                                }
                                if (data['ft'].length == 0) {
                                    $('body').find('.navig[source="front"]').addClass('disabled');
                                }
                                var dt = {};
                                $('body').scrollTop(0);
                                dt.markId = $.filterCatalog.markId;
                                dt.modelId = $.filterCatalog.modelId;
                                dt.transportId = $.filterCatalog.transportId;
                                dt.modificationId = $.filterCatalog.modificationId;
                                dt.selectedYear = $.filterCatalog.selectedYear;
                                dt.selectedNames = $.filterCatalog.selectedNames;
                                dt.selectedType = $.filterCatalog.selectedType;
                                dt.motorId = $.filterCatalog.motorId;
                                dt.checkSizes = data;
                                $.cookie('chfp', JSON.stringify({"step": 2, "params": params, "data":dt}));
                                addResetBtn()
                            }
                            loader.finish();
                        },
                        error: function () {
                            loader.finish();
                            alert('Произошла ошибка !');
                        }
                    });

                },
                error: function () {
                    loader.finish();
                    alert('Произошла ошибка !');
                }
            });

        } else {
            alert('Вы выбрали не все поля');
        }

    },
    changeSide: function (side) {
        if ($.filterCatalog.markId && $.filterCatalog.modelId && $.filterCatalog.modificationId &&
            $.filterCatalog.transportId && $.filterCatalog.selectedYear && $.filterCatalog.selectedNames
        ) {
            $.ajax({
                url: '/api/car/filter/step2',
                type: "POST",
                data: {
                    transportId: $.filterCatalog.transportId,
                    motorId: $.filterCatalog.motorId,
                    names: $.filterCatalog.selectedNames,
                    side: side
                },
                beforeSend: function () {
                    loader.start();
                },
                success: function (response) {
                    if (response.length) {
                        $('.car-option-container').html(response);
                    }
                    loader.finish();
                    addResetBtn()

                },
                error: function () {
                    loader.finish();
                }
            });
        }

    },
    yearGenerator: function (start, finish) {
        var result = [];
        var count = finish - start;
        result.push(start * 1);
        for (var i = 0; i < count; i++) {
            start++;
            result.push(start);
        }
        return result;
    }
}


$.filterSearch = {
    data: [],

    init: function (id) {
        $.filterSearch.data[id] = [];
        $('.filter-search-input[data-id="' + id + '"]').parent().find('li').each(
            function () {
                $.filterSearch.data[id].push({id: $(this).val(), name: $(this).html()});
            }
        )
        $('.dropdown[data-source="' + id + '"] .filter-search-input').keyup(function (e) {
            $.filterSearch.search(id);
        });
        // if ($(document).width() < 800) {
            if ($('.select-wrapper .dropdown:visible li:last').length > 0) {
                var rowpos = $('.dropdown[data-source="' + id + '"] .filter-search-input').parent().parent().position()
                $('body').scrollTop(rowpos.top+30)
            // }
        }
    },
    search: function (id) {
        var search = $('.dropdown[data-source="' + id + '"] .filter-search-input').val();
        var reg = new RegExp(search, 'gi');
        var ul = $('.dropdown[data-source="' + id + '"]');
        $.filterSearch.clear(ul);
        for (var i in $.filterSearch.data[id]) {
            var name = $.filterSearch.data[id][i].name;
            var eid = $.filterSearch.data[id][i].id;
            if (name.match(reg)) {
                $.filterSearch.addHtml(eid, name, ul);
            }
        }
    },
    addHtml: function (id, name, ul) {
        ul.append('<li class="name" value="' + id + '">' + name + '</li>');
    },
    clear: function (ul) {
        ul.find('li').remove();
    }


}


function Request (){
    this.get = function (name) {
        if(name=(new RegExp('[?&]'+encodeURIComponent(name)+'=([^&]*)')).exec(location.search)) {
            return decodeURIComponent(name[1]);
        }
    }
}

function FilterLoader() {
    this.select = '.filter-loader';
    this.template = '<div class="filter-loader"><img src="/themes/carlamp/images/filter-loader.gif" alt="Загрузка..."></div>';
    this.start = function () {
        if ($('.filter-loader').length == 0) {
            $('.car-option-container').prepend(this.template);
        }
        $('.car-option-container').show();
    }
    this.finish = function (delay) {
        if (!delay) {
            delay = 200;
        }
        $(this.select).fadeOut(delay);
    }
}

