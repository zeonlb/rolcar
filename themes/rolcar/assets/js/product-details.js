$(document).ready(function () {
    //CARUSEL THUMB
    var right, left = 0;
    var thumbsCount = $('.photo-img-thumb').length;
    var thumpPos = 0;
    $('#thumb-navigation-right').on('click', function (e) {
        e.preventDefault();
        var realPos = thumpPos + 3;
        if (realPos < thumbsCount) {
            thumpPos += 3
            left = left - 250;
            $('.thumbs-container').animate({"left": left});
        }
    })
    $('#thumb-navigation-left').on('click', function (e) {
        e.preventDefault();
        var realPos = thumpPos - 3;
        if (realPos >= 0) {
            thumpPos -= 3
            left = left + 250;
            $('.thumbs-container').animate({"left": left});
        }
    });


    //GALLERY INIT
    if (jQuery().magnificPopup) {
        $('.photo-img-thumb, .main-photo').magnificPopup({
            delegate: 'a',
            type: 'image',
            tLoading: 'Loading image #%curr%...',
            mainClass: 'mfp-img-mobile',
            gallery: {
                enabled: true,
                navigateByImgClick: true,
                preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
            },
            image: {
                tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
                titleSrc: function (item) {
                    return item.el.attr('title');
                }
            }
        });
    }


    //TABS
    $('.nav-tabs li').on('click', function (e) {
        e.preventDefault();
        $('.tabs').hide();
        $('.nav-tabs li').removeClass('active');
        $(this).addClass('active');
        var tab = $(this).attr('data-tab');
        $("#" + tab).show();
        if ("tab3" === tab) {
            var a = $('.product-dt-video-desc-container iframe');
            $('.product-dt-video-desc-container iframe').remove();
            $('.product-dt-video-desc-container').html(a);
        }
    });
    $('#learn-more-comments').on('click', function (e) {
        e.preventDefault();
        $('.nav-tabs li[data-tab="tab4"]').trigger('click');
    })


    //CARUSEL CROSS SALE
    var cRight, cLeft = 0;
    var cporoductsCount = $('.cross-sales-list .product').length;
    var cPos = 0;
    $('#cross-sales-right').on('click', function (e) {
        e.preventDefault();
        var realPos = cPos + 4;
        if (realPos < cporoductsCount) {
            cPos += 4
            cLeft = cLeft - 618;
            $('.cross-sales-list').animate({"left": cLeft});
        }
    })
    $('#cross-sales-left').on('click', function (e) {
        e.preventDefault();
        var realPos = cPos - 4;
        if (realPos >= 0) {
            cPos -= 4
            cLeft = cLeft + 618;
            $('.cross-sales-list').animate({"left": cLeft});
        }
    });

    //CARUSEL YOU SEEN
    var seenCRight, seenCLeft = 0;
    var sporoductsCount = $('.seen-list .product').length;
    var seenCPos = 0;
    $('#seen-list-right').on('click', function (e) {
        e.preventDefault();
        var realPos = seenCPos + 4;
        if (realPos < sporoductsCount) {
            seenCPos += 4
            seenCLeft = seenCLeft - 618;
            $('.seen-list').animate({"left": seenCLeft});
        }
    })
    $('#seen-list-left').on('click', function (e) {
        e.preventDefault();
        var realPos = seenCPos - 4;
        if (realPos >= 0) {
            seenCPos -= 4
            seenCLeft = seenCLeft + 618;
            $('.seen-list').animate({"left": seenCLeft});
        }
    });


    // COMMENTS MORE
    $('.btn-comment-more').on('click', function (e) {
        e.preventDefault();
        var offset = $('.comment-list-block:last').attr('oc') * 1;
        var total = $('.comment-list-block:last').attr('tc') * 1;
        if (offset >= total) {
            $(this).parent().parent().hide();
        } else {
            $.get('/product-comments/' + offset, function (resp) {
                $('.comment-list').append(resp);
                $('.comment-list-block').slideDown(200);
                var offset = $('.comment-list-block:last').attr('oc');
                var total = $('.comment-list-block:last').attr('tc');
                if (offset >= total) {
                    $('.btn-comment-more').parent().parent().hide();
                }
            })
        }
    });

})