    $(function(){
        $('.main-top-menu').children("li").children("a").click(function(){
            $(this).toggleClass("active").next(".submenu-1").slideToggle();
        });
        $('.ul_submenu-1 .menu_open-1').click(function(){
            $(this).toggleClass("active").next().next(".submenu-2").slideToggle();
            $(this).next().toggleClass("active");
        });
        $('.ul_submenu-1').children("li").children("a").click(function(){
            $(this).toggleClass("active").next(".submenu-2").slideToggle();
            $(this).prev(".menu_open-1").toggleClass("active");
        });
        $(".span_close_menu").click(function(){
            $("#top-navigation").removeClass("menu-opened");
            $("body").removeClass("body_ov-hidden");
        });
        $('.menu_btn').click(function(){
            $("#top-navigation").toggleClass("menu-opened");
            $("body").toggleClass("body_ov-hidden");
        });
        $(".span_find").click(function(){
            $(this).toggleClass("active");
            $(".box_input_find").slideToggle();
        });
        $(".show_contacts").click(function(){
            $(".box_contacts .contacts").slideToggle();
        });
        $(".footer_top .box_left h3").click(function(){
            $(this).next().slideToggle();
        });
        $(".link-myModal-carlamp").click(function(){
            $("#myModal-carlamp").toggleClass("active");
        });
        $("#myModal-carlamp .close").click(function(){
            $("#myModal-carlamp").removeClass("active");
        });
        $(".link-one-click-modal").click(function(){
            $("#one-click-modal").toggleClass("active");
        });
        $("#one-click-modal .close").click(function(){
            $("#one-click-modal").removeClass("active");
        });
        $(".link-one-click-modal-success").click(function(){
            $("#one-click-modal-success").toggleClass("active");
        });
        $("#one-click-modal-success .close").click(function(){
            $("#one-click-modal-success").removeClass("active");
        });
        $(".box_card_delivery h2").click(function(){
            $(this).toggleClass("active");
            $(this).next(".box").slideToggle();
        });
    });