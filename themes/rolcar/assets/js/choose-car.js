$(document).ready(function(){
//    HOVER
    $('.choose-option .btn-option').hover(
        function(){
            $(this).parent().parent().find('.transport-hover-light').show();
        },
        function(){
            $(this).parent().parent().find('.transport-hover-light').hide();
        }
    )

    $(".main-slider-content").on('keyup','.filter-search-input', function (e) {
        if (e.keyCode == 13) {
           $(this).parent().find('li:first').trigger('click')
        }
        if (e.keyCode == 40) {
           $(this).parent().find('li:first').trigger('focus')
        }
    });

});

