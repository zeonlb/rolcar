@extends('rolcar::layouts.master_custom')

@section('content')
    @include("rolcar::shop.partials.catalog.marks")
    @include("rolcar::partials.bread_crumbs")
    <div>
        <div class="container">
            <div class="catalog-header-list" class="col-md-12">
                <h2>Года  {{$model->mark->name}} {{$model->name}}:</h2>
                @foreach($yearsPack as $years)
                    <ul class="content-catalog-list">
                        @foreach($years as $year)
                            <li><a href="{{url('catalog/modifications',['year' => $year, 'modelId' => $model->id])}}">{{$year}}</a></li>
                        @endforeach
                    </ul>
                @endforeach
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@endsection
@section('style')
    <link rel="stylesheet" href="{{Theme::asset('css/catalog.min.css')}}"/>
@endsection
@section('javascript')
@endsection