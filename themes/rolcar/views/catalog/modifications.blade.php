@extends('rolcar::layouts.master_custom')

@section('content')
    @include("rolcar::partials.bread_crumbs")
    <div>
        <div class="container">
            <div class="catalog-header-list" class="col-md-12">
                <h1>@if ($pageInfo->meta->seo_h1) {{$pageInfo->meta->seo_h1}}@else Модификации  {{$model->mark->name}} {{$model->name}}@endif:</h1>
                @foreach($modificationsPack as $modifications)
                    <ul class="content-catalog-list">
                        @foreach($modifications as $modification)
                            @if ($modification->motors->count())
                                <li><a href="{{route('catalog.motors',['id' => $modification->id])}}#catalog-breadcrumb">[Марка] {{$modification->model->mark->name}} -> [Модель]  {{$modification->model->name}} -> [Модификация] {{$modification->type->name}} {{$modification->name}}</a></li>
                            @endif
                        @endforeach
                    </ul>
                @endforeach
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@endsection
@section('style')
    <link rel="stylesheet" href="{{Theme::asset('css/catalog.min.css')}}"/>
@endsection
@section('javascript')
@endsection