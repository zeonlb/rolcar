@extends('rolcar::layouts.master_custom')

@section('content')
    @include("rolcar::partials.bread_crumbs")
    <div>
        <div class="container">
            <div class="catalog-header-list" class="col-md-12">
                <h1>@if ($pageInfo->meta->seo_h1) {{$pageInfo->meta->seo_h1}}@else Двигатели  {{$modification->model->mark->name}} {{$modification->model->name}} {{$modification->name}} @endif:</h1>
                @foreach($motorsPack as $motors)
                    <ul class="content-catalog-list">
                        @foreach($motors as $motor)
                            <li><a href="{{route('catalog.lamps',['id' => $motor->id])}}#catalog-breadcrumb">
                                    [Марка] {{$motor->modification->model->mark->name}} -> [Модель]  {{$motor->modification->model->name}} -> [Модификация] {{$motor->modification->name}} -> [Двигатель] {{$motor->motor}} @if($motor->power)({{$motor->power}})@endif -> [Год выпуска] с {{$motor->year_start}} по @if($motor->year_end != 0) {{$motor->year_end}} @else н/в @endif</a>
                            </li>
                        @endforeach
                    </ul>
                @endforeach
                <div class="clearfix"></div>
                <br/>
                <br/>
            </div>
        </div>
    </div>
@endsection
@section('style')
    <link rel="stylesheet" href="{{Theme::asset('css/catalog.min.css')}}"/>
@endsection
@section('javascript')
@endsection