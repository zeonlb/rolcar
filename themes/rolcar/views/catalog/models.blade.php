@extends('rolcar::layouts.master_custom')

@section('content')

    @include("rolcar::partials.bread_crumbs")

    <div>
        <div id="catalog-model-content" class="catalog-content container">
            @if ($pageInfo->meta && $pageInfo->meta->seo_h1)
                <h1>{{$pageInfo->meta->seo_h1}}</h1>
            @endif
                <ul class="catalog-list model">
                @foreach($models as $model)
                    <li>
                        <a href="{{route('catalog.modifications', ['id' => $model->id])}}">
                                    <span class="image-wrapper">
                                        @if ($model->img)
                                            <img src="/{{$model->img}}" alt="">
                                        @else
                                            <img src="{{Theme::asset('images/f90ee1336c1071935c732fead13ee1a1.png')}}" alt="">
                                        @endif
                                    </span>
                            {{$model->name}}
                        </a>
                    </li>
                @endforeach
            </ul>

            <br/>
            <br/>
        </div>

    </div>
@endsection

@section('style')
    <link rel="stylesheet" href="{{Theme::asset('css/catalog.min.css')}}"/>
@endsection
@section('javascript')
@endsection