@extends('rolcar::layouts.master_custom')

@section('content')
    @include("rolcar::partials.bread_crumbs")
    <div>
        <div class="container">
            <h1><h1>@if ($pageInfo->meta->seo_h1) {{$pageInfo->meta->seo_h1}}@else Тип лампы @endif:</h1></h1>
            <div class="catalog-header-list" class="col-md-12">
                @if($lamps['ft'])
                    <div class="header-h2">Передний свет:</div>
                    @foreach($lamps['ft'] as $lamp)
                        <ul class="content-catalog-list">
                             <li><a href="{{route('product.index', ['category' => $category, 'naznachenie' => $lamp])}}">{{trans('car::lamps.'.$lamp)}} </a></li>
                        </ul>
                    @endforeach
                @endif
                @if($lamps['bt'])
                <div class="header-h2">Задний свет:</div>
                    @foreach($lamps['bt'] as $lamp)
                        <ul class="content-catalog-list">
                                <li><a href="{{route('product.index', ['category' => $category, 'naznachenie' => $lamp])}}">{{trans('car::lamps.'.$lamp)}} </a></li>
                        </ul>
                    @endforeach
                @endif
                @if($lamps['it'])
                    <div class="header-h2">Свет в салоне:</div>
                    @foreach($lamps['it'] as $lamp)
                        <ul class="content-catalog-list">
                                <li><a href="{{route('product.index', ['category' => $category, 'naznachenie' => $lamp])}}">{{trans('car::lamps.'.$lamp)}} </a></li>
                        </ul>
                    @endforeach
                @endif
                @if (empty($lamps['ft']) && empty($lamps['bt']) && empty($lamps['it']))
                        <div class="header-h2">Нет ламп для вашей модели</div>
                    @endif
                <div class="clearfix"></div>
                <br/>
                <br/>
            </div>
        </div>
    </div>
@endsection
@section('style')
    <link rel="stylesheet" href="{{Theme::asset('css/catalog.min.css')}}"/>
@endsection
@section('javascript')
@endsection