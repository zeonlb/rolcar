@extends('rolcar::layouts.master_custom')

@section('content')

    @include("rolcar::partials.bread_crumbs")
    <div  class="car-catalog-content">
        <div class="container">
            <div class="catalog-header-list" class="col-md-12">
                <h3>Марки:</h3>
                <ul class="catalog-list mark">
                @foreach($marksPack as $marks)
                        @foreach($marks as $mark)
                            <li>
                                <a href="{{route('catalog.models', ['id' => $mark->id, 'transport_id' => $transportId])}}#catalog-breadcrumb">
                                    
                                    <span class="image-wrapper">
                                        @if ($mark->img)
                                        <img src="/{{$mark->img}}" alt="">
                                        @endif
                                    </span>
                                    {{$mark->name}}
                                </a>
                            </li>
                        @endforeach
                @endforeach
                </ul>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@endsection

@section('style')
    <link rel="stylesheet" href="{{Theme::asset('css/catalog.min.css')}}"/>
@endsection
@section('javascript')
@endsection