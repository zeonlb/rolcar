@extends('rolcar::layouts.master_custom')

@section('content')
    <div  class="main-slider-content car-option-container">
        <div class="container">
            <div id="sku-container" class="col-md-12">
                <h1>Укажите код (артикул):</h1>

                <form action="{{route('sku.search')}}" method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                <div class="text-center">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="(например: NF600)"/>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="hbtn btn-gold">Найти <i class="fa fa-search"></i></button>
                    </div>
                </div>
                </form>
            </div>
        </div>
        <div class="clear"></div>
    </div>
    <div>
        <div class="container">
            <div id="catalog-breadcrumb" class="breadcrumb-container">
                <div class="container">
                    <ol class="breadcrumb">
                        <li><a  href="{{url('/')}}">Главная</a></li>
                        <li class="active">Поиск товара по артикулу</li>
                    </ol>
                </div>
            </div>
            <div class="">
                <div class="sku-header-list">
                    @foreach($productPack as $brandName => $brands)
                    <h2>На данный момент {{strtoupper($brandName)}} есть в наличии:</h2>
                    <ul class="content-catalog-list">
                            @foreach($brands as $product)
                                <li><a href="{{route('product.show', ['code' => $product->code])}}">{{strtoupper($product->sku)}}</a></li>
                            @endforeach
                    </ul>
                    @endforeach
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('style')
@endsection
@section('javascript')
@endsection