@extends('rolcar::layouts.master_custom')
@section('content')
    @include('partials.bread_crumbs')
    <div>
        <div class="container content-box blog_content">
            <h1 class="content-header">{{$article->title}}</h1>
            <div class="post-date">
                <p>{{ Carbon\Carbon::parse($article->created_at)->format('d-m-Y') }}</p>
            </div>
            <div class="blog-content col-md-12">
                {!! $article->content !!}
                <div>
                    <ul class="social-sharing">
                        <li><a href="https://www.facebook.com?u={{Request::url()}}"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
                        <li><a href="https://plus.google.com?url={{Request::url()}}"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a></li>
                        <li><a href="https://twitter.com?url={{Request::url()}}"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
@endsection
@section('style')
    <link rel="stylesheet" href="{{Theme::asset('css/blog.min.css')}}">
@endsection