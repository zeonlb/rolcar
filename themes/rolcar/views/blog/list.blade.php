@extends('rolcar::layouts.master_custom')
@section('content')
    @include('partials.bread_crumbs')
    <div>
        <div class="container content-box">
            <h1 class="content-header">Блог</h1>
            <div class="blog-content col-md-12">
                @if ($articles)
                    @foreach($articles as $article)
                        <div class="col-xs-12 col-sm-6 col-md-4">
                            <div class="blog-post">
                                <a href="{{route('blog.view', $article->code)}}">
                                    <div class="blog-img-block">
                                        @if ($article->img)
                                        <img width="210" class="product-thumb"
                                        src="{{ \Modules\Cms\Core\Images\ThumbBuilder::resize($article->img , 210, 'auto') }}"
                                        alt="{{$article->title}}" />
                                            @endif
                                    </div>
                                </a>
                                <div class="post-date">
                                    <p>{{ Carbon\Carbon::parse($article->created_at)->format('d-m-Y') }}</p>
                                </div>
                                <div><h3><a href="{{route('blog.view', $article->code)}}">{{$article->title}}</a></h3></div>

                                <div class="short-block">
                                    {!! $article->short_content !!}
                                </div>
                                <div class="bv-btn text-left">
                                    <a  class="btn btn-gold" href="{{route('blog.view', $article->code)}}">Подробне
                                    </a>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    @endforeach
                        <div class="clearfix"></div>
                        <div class="col-md-12 col-xs-12">
                            {!! $articles->appends(['filter' => \Illuminate\Support\Facades\Input::get('filter')])->render() !!}
                        </div>
                @else
                    <h3>Нет ни одной интересной статьи :)</h3>
                @endif
            </div>

        </div>
        <div class="clearfix"></div>
    </div>
    <br clear="both" />
@endsection
@section('style')
    <link rel="stylesheet" href="{{Theme::asset('css/blog.min.css')}}">
@endsection