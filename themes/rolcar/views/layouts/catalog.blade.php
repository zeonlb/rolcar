<!DOCTYPE html>
<html  xmlns="http://www.w3.org/1999/xhtml"
       lang="ru"
       xml:lang="ru">
<head>
    
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    @if (isset($pageInfo) && $pageInfo->meta && $pageInfo->meta->meta_title)
        <title>{{$pageInfo->meta->meta_title}}</title>
    @else
        <title>rolcar.com.ua</title>
    @endif
    <meta name="csrf-token" content="{{csrf_token()}}"/>
    <link rel="shortcut icon" href="{{Theme::asset('images/favicon.ico')}}" type="image/x-icon">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{Theme::asset('vendor/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{Theme::asset('css/style.css')}}"/>
    <link rel="stylesheet" href="{{Theme::asset('css/main-choose.css')}}"/>
    <link rel="stylesheet" href="{{Theme::asset('css/shop.min.css')}}"/>
    @yield('style')

    <link rel="stylesheet" href="{{Theme::asset('css/rolcar_main.css')}}"/>
    <link rel="stylesheet" href="{{Theme::asset('css/all.css')}}"/>
<!--- <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"> -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    @if (isset($pageInfo) && $pageInfo->meta && $pageInfo->meta->meta_title)
        <meta name="description" content="{{ $pageInfo->meta->meta_description }}">
    @endif
    @if (isset($pageInfo) && $pageInfo->meta && false === $pageInfo->meta->meta_robot_index)
        <meta name="robots" content="noindex,follow">
    @endif
    @yield('headers')

    <script data-ad-client="ca03" async src="pagead/js/adsbygoogle.js">
    </script>
    <link rel="stylesheet" href="{{Theme::asset('css/product.css')}}"/>
</head>
<body>
 @include('rolcar::partials.remarketing')
<!--Main header-->
    <header class="main_header">
        @inject('menu', 'Modules\Cms\Services\NavigationService')
        <div class="container">
            <!--Top navigation-->
            <nav id="top-navigation">
                <div class="burger menu_btn">
                    <span>меню</span>
                </div>
                <div class="li_logo">
                    <a class="logo" href="/">Логотип</a>
                </div>
                <ul class="main-top-menu">
                    <span class="span_close_menu"></span>
                    @each('partials.navigation.categories', $menu->getMenu('main_menu'), 'category', 'partials.navigation.categories_nothing')
                </ul>
                <ul id="main-top-contact-menu">
                    <li class="box_find">
                        <span class="span_find"><span class="span_close"></span></span>
                        <div class="box_input_find">
                            <div class="box_inner">
                                <form action="{{route('shop.search')}}" method="GET">
                                    <input type="search" name="search" class="input_find" placeholder="Введите название лампы или артикул...">
                                    <input type="submit" class="input_submit">
                                </form>
                            </div>
                            <div class="box_contacts">
                                <span class="show_contacts"></span>
                                <div class="contacts">
                                    <div class="box_contacts_inner">
                                        <img class="phone_black" src="{{Theme::asset('images/phone_black.svg')}}" alt="phone_black">
                                        <ul class="">
                                            <li class="li-1">
                                                <h3>Розничный отдел</h3>
                                            </li>
                                            <li class="li-2">
                                                <p class="kievstar">+38 (067) 000-10-00</p>
                                            </li>
                                            <li>
                                                <p class="vodafone">+38 (095) 000-75-00</p>
                                            </li>
                                            <li>
                                                <p class="life">+38 (093) 000-00-00</p>
                                            </li>
                                            <li class="li-1">
                                                <h3>ОПТОВЫЙ отдел</h3>
                                            </li>
                                            <li>
                                                <p class="kievstar">+38 (067) 000-00-00</p>
                                            </li>
                                            <li>
                                                <p class="vodafone">+38 (095) 000-00-00</p>
                                            </li>
                                            <li>
                                                <p class="life">+38 (093) 000-00-00</p>
                                            </li>
                                            <li>
                                                <h3>Техническая поддержка</h3>
                                            </li>
                                            <li>
                                                <p class="kievstar">+38 (067) 000-00-00</p>
                                            </li>
                                            <li>
                                                <a class="address" href="#" target="_blank">Наш адрес</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li id="recall">
                        <button class="hbtn btn-black recall-me-button" data-toggle="modal" data-target="#recall-modal">Перезвоните мне</button>
                    </li>
                    <li id="mini-cart-block">
                            @inject('shop', 'Modules\Shop\Services\ShopService')
                            <?php
                            $count = 0;
                            if ($cartData  = $shop->getCartData())
                                foreach($cartData  as $cart) {
                                    $count += $cart['count'];
                                }
                            ?>
                            <button class="hbtn btn-gold basket">Корзина</button>
                            <span class="box_quantity">{{$count}}</span>
                    </li>
                </ul>
            </nav>
            <!--Top navigation end-->
        </div>
    </header>
    <!--Main header end-->
    <section class="main-content">
    @include('rolcar::partials.flash_message')
    @yield('content')
    
    <!--Footer content-->
        <div id="footer-seo-block">
            <div class="container">
                @if(isset($pageInfo) && $pageInfo->meta && $pageInfo->meta->seo_text)
                    <div class="footer-info-block">
                        {!!$pageInfo->meta->seo_text!!}
                    </div>
                    <div id="footer-social-block" class="col-md-12">
                        Поделиться:    <img src="{{Theme::asset('images/footer_social.png')}}" alt=""/>
                    </div>
                @elseif (isset($pageSettings->footer_seo_text) && $pageSettings->footer_seo_text)
                    <div class="footer-info-block">
                        {!!$pageSettings->footer_seo_text!!}
                    </div>
                    <div id="footer-social-block" class="col-md-12">
                        Поделиться:    <img src="{{Theme::asset('images/footer_social.png')}}" alt=""/>
                    </div>
                @endif
            </div>
        </div>   
        <!--End Footer content-->
        <footer class="main_footer">
            <div class="footer_top">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-6 col-md-9 col-sm-8 box_left">
                            <div class="col-sm-6 col-md-3">
                                <div class="box_content">
                                    <h3>О КОМПАНИИ</h3>
                                    <ul class="menu_footer-1">
                                        @each('partials.navigation.categories', $menu->getMenu('menu_footer-1'), 'category', 'partials.navigation.categories_nothing')
                                    </ul>
                                </div>
                                <div class="box_content">
                                    <h3>ПОДБОР</h3>
                                    <ul class="menu_footer-2">
                                        @each('partials.navigation.categories', $menu->getMenu('menu_footer-2'), 'category', 'partials.navigation.categories_nothing')
                                    </ul>
                                </div>
                                <div class="box_content">
                                    <h3>КАТАЛОГ</h3>
                                    <ul class="menu_footer-3">
                                        @each('partials.navigation.categories', $menu->getMenu('menu_footer-3'), 'category', 'partials.navigation.categories_nothing')
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <div class="box_content">
                                    <h3>Розничным покупателям</h3>
                                    <ul class="menu_footer-4">
                                        @each('partials.navigation.categories', $menu->getMenu('menu_footer-4'), 'category', 'partials.navigation.categories_nothing')
                                    </ul>
                                </div>
                                <div class="box_content">
                                    <h3>Оптовым покупателям</h3>
                                    <ul class="menu_footer-5">
                                        @each('partials.navigation.categories', $menu->getMenu('menu_footer-5'), 'category', 'partials.navigation.categories_nothing')
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <div class="box_content">
                                    <h3>ГОЛОВНОЕ</h3>
                                    <ul class="menu_footer-6">
                                        @each('partials.navigation.categories', $menu->getMenu('menu_footer-6'), 'category', 'partials.navigation.categories_nothing')
                                    </ul>
                                </div>
                                <div class="box_content">
                                    <h3>Вспомогательное</h3>
                                    <ul class="menu_footer-7">
                                        @each('partials.navigation.categories', $menu->getMenu('menu_footer-7'), 'category', 'partials.navigation.categories_nothing')
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3 box_last">
                                <div class="box_content">
                                    <h3>Дополнительное оборудование</h3>
                                    <ul class="menu_footer-8">
                                        @each('partials.navigation.categories', $menu->getMenu('menu_footer-8'), 'category', 'partials.navigation.categories_nothing')
                                    </ul>
                                </div>
                                <div class="box_content box_mini_left">
                                    <h3>Присоединяйтесь</h3>
                                    <ul class="ul_social">
                                        <li>
                                            <a class="link_youtube" href="https://www.youtube.com" target="_blank">youtube</a>
                                        </li>
                                        <li>
                                            <a class="link_facebook" href="https://www.facebook.com" target="_blank">facebook</a>
                                        </li>
                                        <li>
                                            <a class="link_instagram" href="https://www.instagram.com" target="_blank">instagram</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="box_content box_schedule box_mini_right">
                                    <h3>График работы</h3>
                                    <ul>
                                        <li>
                                            <p>Пн-пт: <span>09:00-18:00</span></p>
                                        </li>
                                        <li>
                                            <p>Сб: <span>10:00-16:00</span></p>
                                        </li>
                                        <li>
                                            <p>Вскр: выходной</p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-4 col-md-3 pull-right box_right">
                            <div class="box_content">
                                <h3>Розничный отдел</h3>
                                <ul>
                                    <li>
                                        <p class="kievstar">+38 <span>(067)</span> 000-10-00</p>
                                    </li>
                                    <li>
                                        <p class="vodafone">+38 <span>(095)</span> 000-75-00</p>
                                    </li>
                                    <li>
                                        <p class="life">+38 <span>(093)</span> 000-00-00</p>
                                    </li>
                                </ul>
                            </div>
                            <div class="box_content">
                                <h3>ОПТОВЫЙ отдел</h3>
                                <ul>
                                    <li>
                                        <p class="kievstar">+38 <span>(067)</span> 000-00-00</p>
                                    </li>
                                    <li>
                                        <p class="vodafone">+38 <span>(095)</span> 000-00-00</p>
                                    </li>
                                    <li>
                                        <p class="life">+38 <span>(093)</span> 000-00-00</p>
                                    </li>
                                </ul>
                            </div>
                            <div class="box_content">
                                <h3>Техническая поддержка</h3>
                                <ul>
                                    <li>
                                        <p class="kievstar">+38 <span>(067)</span> 000-00-00</p>
                                    </li>
                                </ul>
                            </div>
                            <div class="box_content box_address">
                                <a href="#">Наш адрес</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer_bottom">
                <div class="container">
                    <p>rolcar©</p>
                </div>
            </div>
        </footer>
        <div class="mobile_basket_call_back">
            <button class="hbtn btn-gold basket" data-toggle="modal" data-target="#shop-cart-modal">Корзина</button>
            <button class="hbtn btn-black recall-me-button" data-toggle="modal" data-target="#recall-modal">Перезвоните мне</button>
        </div>
    </section>
@include('rolcar::partials.modal_partner')
@include('rolcar::partials.recall-modal')
@include('rolcar::partials.filter_question')
@include('rolcar::partials.led_tech_modal')
@include('rolcar::shop.checkout.shop_cart_modal')
@include('rolcar::partials.led_tech_form_modal')


<script src="{{Theme::asset('js/jquery-1.12.0.min.js')}}" type="text/javascript"></script>
<script src="{{Theme::asset('js/jquery.maskedinput.min.js')}}" type="text/javascript"></script>
<script src="{{Theme::asset('js/underscore-min.js')}}" type="text/javascript"></script>
<script src="{{Theme::asset('js/jquery.cookie.js')}}" type="text/javascript"></script>

@yield('javascript')


<script type="text/javascript" src="{{Module::asset('shop:js/front/cart.js')}}"></script>
<script src="{{Theme::asset('js/main.js')}}"></script>
<script src="{{Theme::asset('js/choose-car.js')}}"></script>
<script src="{{Theme::asset('js/select-wrapper.js')}}"></script>
<script src="{{Theme::asset('js/index_filter.js')}}"></script>
<script src="{{Theme::asset('js/list.min.js')}}"></script>

<link rel="stylesheet" href="{{Theme::asset('css/order.css')}}"/>
<script type="text/javascript" src="{{Module::asset('shop:js/front/order.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    })

</script>
<script>
    window.onload = function() {
    document.querySelector('.js-slideout-toggle').addEventListener('click', function() {
        slideout.toggle();
    });

    document.querySelector('.menu').addEventListener('click', function(eve) {
        if (eve.target.nodeName === 'A') { slideout.close(); }
    });
    };
</script>
<script src="{{Theme::asset('js/my_main.js')}}"></script>
</body>
</html>
