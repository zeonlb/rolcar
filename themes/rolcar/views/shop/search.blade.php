@extends('rolcar::layouts.master_custom')
@section('content')
    <?php
    $fCategory = request('category', false);
    ?>
    <!-- Main Container  -->
    <div class="main-container container">
        @include('partials.bread_crumbs')
        <div class="row">
            <!--Left Part Start -->
            <aside class="col-sm-0 col-md-3" id="column-left">
                @if (count($categories) > 0)
                <div class="module latest-product titleLine">
                    <h3 class="modtitle">Категории </h3>
                    <div class="modcontent ">
                        <form class="type_2">
                            <div class="table_layout filter-shopby">
                                <div class="table_row">
                                    <!-- - - - - - - - - - - - - - SUB CATEGORY - - - - - - - - - - - - - - - - -->

                                        <div class="table_cell">
                                            <fieldset>
                                                <ul class="checkboxes_list category-search">
                                                    @foreach($categories as $category)
                                                        <li class="product-filter">
                                                            @if ($fCategory === $category['code'])
                                                                <p class="active">{{$category['name']}}
                                                                    ({{$category['count']}})</p>
                                                            @else
                                                                <a   href="{{route('shop.search', ['search' => request('search'), 'category' => $category['code']])}}">{{$category['name']}}
                                                                    ({{$category['count']}})</a>
                                                            @endif

                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </fieldset>
                                        </div>
                                </div><!--/ .table_layout -->
                            </div>
                        </form>
                    </div>

                </div>
                @endif
            </aside>
            <!--Middle Part Start-->
            <div id="content" class="col-md-9 col-sm-12">
                <div class="products-category">
                    <!-- Filters -->
                    <div class="product-filter filters-panel">
                        <h1>Результаты поиска по запросу "{{request('search')}}":</h1>
                    </div>
                    <!-- //end Filters -->
                    <!--changed listings-->
                    <div class="products-list row grid">
                        <?php $pCount = 3; ?>
                        @foreach($products as $product)
                            <?php
                            if ($fCategory && !$product->categories->where('code', $fCategory)->count()) {
                                continue;
                            }
                            ?>
                            @include('rolcar::shop.partials.product')
                        @endforeach


                        @if (!$products->count())
                            <h3>&nbsp;&nbsp;&nbsp;&nbsp;По вашему запросу ничего найдено</h3>
                            @endif
                    </div>
                    <!--// End Changed listings-->
                    <!-- Filters -->
                    <div class="product-filter product-filter-bottom filters-panel">
                        <div class="row">
                            <div class="col-md-2 hidden-sm hidden-xs">
                            </div>
                            <div class="short-by-show text-center col-md-7 col-sm-8 col-xs-12">
                            </div>

                        </div>
                    </div>
                    <!-- //end Filters -->
                </div>
                @if ($pageInfo->meta && $pageInfo->meta->seo_footer_text)
                    <div class="col-md-12 col-sm-12 col-xs-12 seo-text-block">
                        {!! $pageInfo->meta->seo_footer_text !!}
                    </div>
                @endif
            </div>
        </div>
        <!--Middle Part End-->
    </div>
    <!--Middle Part End-->
    </div>
    <!-- //Main Container -->
@endsection


@section('javascript')
    {{--// GA--}}
    <script>
        $(function () {
            var ancProducts = [];
            @if (@$products)
            @foreach($products as $key => $product)
            ancProducts.push({
                'id': '{{$product->sku}}',
                'name': '{{$product->name}}',
                'brand': '{{$product->brand->name}}',
                'list': 'Search results',
                'position': '{{$key + 1}}'
            })
            @endforeach
            dataLayer.push({
                "event": "productImpression",
                "ecommerce": {
                    "currencyCode": "UAH", // Код валюты в формате ISO 4217
                    "impressions": ancProducts
                }
            })
            @endif

            fbq('track', 'Search');
        });
    </script>
@endsection

@section('headers')
    <meta name="robots" content="noindex, nofollow" />
@endsection
