<div class="col-xs-12 col-sm-4 col-md-3 col-lg-2">
    <div class="product" data-productKey="{{$product->id}}">
        <?php
        $linkAttrs = ['code' => $product->code];
        if (isset($typeLamp)) {
            $linkAttrs['lt'] = $typeLamp;
        }
        if (isset($categoryId)) {
            $linkAttrs['ck'] = $categoryId;
        }
        if (!isset($widthImg)) {
            $widthImg = 183;
        }
        ?>
        @if ($product->is_present)
            <img class="product_present_icon" src="{{Theme::asset('images/present.svg')}}">
        @endif
        <a href="{{route('product.show', $linkAttrs)}}" title="Купить {{$product->name}}">
            @if($product->icon->count())
                @foreach($product->icon as $icon)
                    <img class="stoke-lent-image product-icon-{{$icon->id}}" src="/{{$icon->path}}"/>
                @endforeach
            @endif

            @if($product->discount && $product->discount->isActive())
            <!--STOKE LENT-->
                @if($product->icon->count() == 0)
                    <img class="stoke-lent-image" src="{{Theme::asset('images/action-lent.png')}}"/>
                @endif
            <!-- /STOKE LENT-->
            @endif

            <div class="product-photo">
                @if ($product->getThumb())
                    <img width="100%" class="product-thumb"
                         src="{{ \Modules\Cms\Core\Images\ThumbBuilder::resize($product->getThumb() , $widthImg, 'auto') }}"
                         alt="{{$product->title}}"/>
                @endif
            </div>
        </a>
        <div class="product-description">
            <a href="{{route('product.show', $linkAttrs)}}" title="Купить {{$product->name}}">{{$product->name}}</a>
        </div>
        <?php
        $rating = ceil($product->comments->filter(function ($value) {
            return $value->rating > 0;
        })->avg('rating'));
        ?>
        <div class="box-rating">
            <div class="box-stars">
                @for ($i = 1; $i <= 5; $i++)
                    <img class="img-1"
                         @if ($rating >= $i)
                         src="{{Theme::asset('images/product_details/sct-1_star-2.png')}}"
                         @else
                         src="{{Theme::asset('images/product_details/sct-1_star-1.png')}}"
                            @endif
                    />
                @endfor
            </div>
            <div class="box-comment-link">
                <a href="{{route('product.show', ['code' => $product->code])}}"
                   class="p-1 link-to-comment">({{$product->comments->where('processed', 1)->where('parent_id', null)->count()}}
                    отзывов)</a>
            </div>
        </div>
        <div class="box_bottom">
            <div class="box_left">
                <div class="price-block">
                    <?php $processor = (new Modules\Shop\Utils\Price\Processor)->calculate($product) ?>

                    @if($product->discount && $product->discount->isActive() && $processor->originPrice() > $processor->actualPrice())
                        <span class="price-original">{{$processor->originPrice()}} <small>₴</small></span>
                    @endif
                    @if($product->discount && $product->discount->isActive() && $processor->originPrice() > $processor->actualPrice())
                        <span class="price-new">{{$processor->actualPrice()}} <small>₴</small></span>
                    @else
                        <span class="price"><?= $processor->actualPrice()?> <small>₴</small></span>
                    @endif
                </div>
            </div>
            <div class="box_right">
                <div class="product-more-link">
                    <p>
                        <img src="{{Theme::asset('images/product-info.jpg')}}" title="Подробнее {{$product->name}}"
                             alt="Подробнее {{$product->name}}"/>
                        <a href="{{route('product.show', $linkAttrs)}}">подробнее</a>
                    </p>
                </div>
                <div class="product-buy-btn">
                    @if ($product->pre_order_date || $product->count > 0)
                        @if ($product->pre_order_date && $count = $product->getPreOrderDiffHours() > 0)
                            <a href="{{route('product.show', $linkAttrs)}}" title="Купить {{$product->name}}"
                               class="hbtn btn-buy">Предзаказ</a>
                        @else
                            <?php
                            $form = Modules\Shop\Utils\Checkout\Front\CheckoutCartFormBuilder::make(array(), $product->id);
                            ?>
                            {!! form($form) !!}
                        @endif
                    @else
                        <a href="{{route('product.show', $linkAttrs)}}" title="Купить {{$product->name}}"
                           class="hbtn btn-buy-disabled">
                            Нет в наличии
                        </a>
                    @endif
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>

