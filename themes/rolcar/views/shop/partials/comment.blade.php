<div class="product-review-block">
    <div class="review-name"><b>{{$comment->name}}</b></div>
    <div class="review-date">{{convertDate($comment->created_at)}}</div>
    <div class="clear"></div>
    <div class="review-text">
        
        <p>{{$comment->comment}}</p>
    </div>
    @if ($comment->advantages)
        <div class="review-text">
            <p class="comment-info"><b>Достоинства</b></p>
            <p>{{$comment->advantages}}</p>
        </div>
    @endif
    @if ($comment->disadvantages)
        <div class="review-text">
            <p class="comment-info"><b>Недостатки</b></p>
            <p>{{$comment->disadvantages}}</p>
        </div>
    @endif
    @if ($comment->photos->count() > 0 || $comment->video_link)
        <div class="comment-block">
            <div class="comment-container col-md-12">
                @foreach($comment->photos as $key => $photo)
                    <div class=" col-md-2 comment-img-thumb">
                        <a class="video gallery-item" href="/{!! $photo->path !!}">
                            <img title="{{$product->name}}" alt="{{$product->name}}"
                                 src="{{\Modules\Cms\Core\Images\ThumbBuilder::resize($photo->path, 60, 60)}}"/>
                        </a>
                    </div>
                @endforeach
                    @if ($comment->video_link)
                        <div class="col-md-2 comment-video-thumb">
                            <a class="mfp-iframe" href="{{$comment->video_link}}">
                                <div class="dz-image">
                                    <img data-dz-thumbnail="" alt="dl_5_lamp.png" height="60"
                                         src="//img.youtube.com/vi/{{$comment->getYoutubeCode()}}/0.jpg"
                                         id="Oval-2">
                                    <i class="fa fa-youtube-play " aria-hidden="true"></i>
                                </div>
                            </a>
                        </div>
                    @endif

                <br clear="both">
            </div>
        </div>

    @endif


    @if ($comment->answers)
        @foreach($comment->answers as $answ)
            <div class="product-review-block comment-manager-answer-block col-md-12">
                <div class="col-md-1 text-right">
                    <i class="fa fa-reply " aria-hidden="true"> &nbsp;</i>
                </div>
                <div class="col-md-11">
                    <div class="review-name">Менеджер</div>
                    <div class="review-date">{{convertDate($answ->created_at)}}</div>
                    <div class="clear"></div>
                    <div class="review-text">
                        
                        <p>{{$answ->comment}}</p>
                    </div>
                </div>

            </div>
        @endforeach
    @endif
</div>
<hr/>
<br>