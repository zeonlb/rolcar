<div class="product-preview-block">
    <!--MAIN PHOTO-->
    <div class="product-photo-block">
        <div class="main-photo">
            @if ($product->photos->count() > 0)
            <img  src="{{ ThumbBuilder::resize($product->photos[0]->path, 'auto', 199) }}" title="{{$product->name}}" alt="{{$product->name}}"/>
            @endif
        </div>
    </div>
    <div class="inline-block">
        <p class="attributes-short-desc">Артикул: {{$product->sku}} </p>
        <h4 class="product-price-block">{{$product->price}} <small>грн</small></h4>
        <div class="buy-block">
            {!! form($form) !!}
        </div>
        <hr />
        @include('rolcar::shop.partials.socials')
    </div>
</div>