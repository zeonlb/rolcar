<div oc="{{$offsetComments}}" tc="{{$totalComments}}" class="col-md-12 comment-list-block comment-content"
     @if(isset($hidden)) style="display: none" @endif>
    <div class="col-md-2  photo-img-thumb">
        <a  target="_blank"
           href="{{route('product.show', $comment->product->code)}}">
            <img src="{{ \Modules\Cms\Core\Images\ThumbBuilder::resize($comment->product->getThumb() , 'auto', 180) }}"
                 alt="{{$comment->product->name}}"/>
        </a>
    </div>

        <div class="col-md-5"><a class="comment-content-header"
                                 href="{{route('product.show', $comment->product->code)}}">{{$comment->product->name}}</a>
        </div>
        <div class="col-md-5 comment-learn-more"><a href="{{route('product.show', $comment->product->code)}}">Все отзывы
                об этой модели</a></div>
        <br clear="both"/>
        <div class="comment-text">{{$comment->comment}}</div>
    <br clear="both"/>
</div>
