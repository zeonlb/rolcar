<div id="shop-comment-video-modal" class="comment-modal modal-wrapper">
    <div class="modal-dialog shop-comment-video-modal">
        <div class="modal-body">
            <div class="modal-header">
                <div>Загрузить видео с youtube</div>
                <div class="col-md-12">
                    <p>Вставьте ссылку на видеоролик из Youtube</p>
                    <input type="text" id="comment-video-input" class="form-control" name="comment[video]">
                </div>
                <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-content">
                <div class="cart-modal-content">
                    <button type="submit" id="comment-save-video" class="btn btn-gold">СОХРАНИТЬ</button>
                </div>
            </div>
        </div>
    </div>
</div>

