<div class="box_reviews">
    <h3>Отзывы покупателей</h3>

    @foreach ( $commentsArray as $item )

        <div class="box_content">
            <div class="box_top">
                <div class="box_left">
                    <a href="/product/{{$item['product']->code}} ">
                        <img src="/{{ $item['thumb'] }}" width="82px" alt="img-1">
                    </a>
                </div>
                <div class="box_right">
                    <p class="text_name">{{$item['comment']['name']}}</p>
                    <p class="text_date">{!! date("d.m.Y", strtotime($item['comment']['created_at'])) !!}</p>
                    <a class="title_product" href="/product/{{$item['product']->code}} " target="_blank">{{ $item['product']->name }}</a>
                </div>
            </div>
            <div class="box_bottom">
                <p class="text_review">
                    {{ $item['comment']['comment'] }}
                    <a href="/product/{{$item['product']->code}}" class="more_info" target="_blank">Ещё</a>
                </p>
            </div>
        </div>

    @endforeach
</div>