<div class="col-xs-6 col-sm-4 col-md-3">
    <div class="product">
        <a class="category-link" href="{{route('product.index', [$category->code])}}">
            <div class="catalog-photo">
                @if ($category->img)
                    <span class="c-img-helper"></span>
                    <img width="180" class="product-thumb"
                         src="{{ \Modules\Cms\Core\Images\ThumbBuilder::resize($category->img , 180, 'auto') }}"
                         alt="{{$category->title}}"/>
                @endif
            </div>
        </a>
        <div class="tcategory-link">
            <a href="{{route('product.index', [$category->code])}}">{{$category->name}}</a>
        </div>
    </div>
    <div class="clearfix"></div>
</div>