<div  class="car-catalog-content">
    <div class="container">
        <div class="catalog-header-list" class="col-md-12">
            <h3>Марки:</h3>
            @foreach($marksPack as $marks)
                <ul class="catalog-list">
                    @foreach($marks as $mark)
                        <li><a @if ($mark->id == $markId) class="active" @endif href="{{route('catalog.models', ['id' => $mark->id, 'transport_id' => $transportId])}}">{{$mark->name}}</a></li>
                    @endforeach
                </ul>
            @endforeach
            <div class="clearfix"></div>
        </div>
    </div>
</div>