<div id="shop-comment-modal" class="comment-modal modal-wrapper">
    <div class="modal-dialog shop-comment-modal-dialog">
        <div class="modal-body">
            <div class="modal-header">
                <div>Загрузить фото</div>
                <div class="col-md-12">
                    <p class="comment-photo-desc">Загрузите фотографии для своего отзыва.</p>
                    <div class="dropzone dz-clickable" id="myDrop">
                        <div class="dz-default dz-message" data-dz-message="">
                            <span>Перетащите ваш файл сюда или нажмите кнопку</span><br /> <br />
                            <button class="btn btn-default">Загрузить</button>
                        </div>
                    </div>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                </div>
                <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-content">
                <div class="cart-modal-content">

                </div>
            </div>
        </div>
    </div>
</div>

