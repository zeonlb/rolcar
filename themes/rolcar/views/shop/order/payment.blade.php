@extends('rolcar::layouts.master_custom')
@section('content')
    <div class="text-center">
        <h1>Вы успешно оформили заказ №{{$payment->order->invoice}}</h1>
        <br />
        <h3>Сумма заказа: <b>{{$payment->order->total_price}} {{$payment->order->currency}} грн</b></h3>
        {!! $form !!}
    </div>
@stop
@section('javascript')
@stop