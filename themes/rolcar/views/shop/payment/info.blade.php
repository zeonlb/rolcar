@extends('rolcar::layouts.master_custom')
@section('content')
    <!-- Main Container  -->
    <div class="main-container container">
{{--        @include('default::partials.bread_crumbs')--}}
        <div class="row">
            <!--Middle Part Start-->
            <div id="content" class="col-md-12 col-sm-8 novelties-romax shares">
                <section class="thank-you-page-successfully container">
                    <h2>{{$payment->order->first_name}}, спасибо за ваш заказ <span class="weight-bold">№{{$payment->order->invoice}}</span> принят!</h2>
                    <div class="row">
                        <div class="box-left col-md-6">
                            <div class="box box-1">
                                <p class=".p-1">{{$message}}</p>
                            </div>
                            <div class="box box-2">
                                <p class="p-2">Информация о заказе:</p>
                                <p class="p-3">Получатель: {{$payment->order->first_name}}</p>
                                @if($payment->order->delivery_type)
                                <p  class="p-4">Тип доставки:  {{trans('shop::basic.delivery_'.$payment->order->delivery_type)}}</p>
                                @endif
                                <p class="p-4">Адрес доставки: {{$deliveryAddress}}</p>
                                <p class="p-5">Тел: {{$payment->order->phone}}</p>
                            </div>
                            <div class="box box-3">
                                <p class="p-6">Номер вашего заказа: <span class="weight-bold">№{{$payment->order->invoice}}</span></p>
                                <p class="p-7">Сумма заказа: <span class="weight-bold">{{number_format($payment->order->total_price , 2)}} грн</span></p>
                            </div>
                        </div>
                        <div class="box-right col-md-6">
                            <div class="box box-1">
                                <p class="p-1">Если у вас возникли вопросы относительно заказа или нашей работы, пожалуйста, позвоните или напишите нам:</p>
                            </div>
                            <div class="box box-2">
                                <p class="p-2">(067) 000 80 06</p>
                                <p class="p-4">(095) 000-49-22</p>
                            </div>
                            <div class="box box-3">
                                <p class="p-5">rx@gmail.com</p>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!--Middle Part End-->
    </div>
    <!--Middle Part End-->
@endsection

@section('style')
    <link rel="stylesheet" href="{{Theme::asset('css/payment.css')}}">
@stop
@section('javascript')

@stop