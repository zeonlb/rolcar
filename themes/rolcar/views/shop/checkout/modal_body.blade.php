<div class="modal-shop-cart-list">
    @if ($cartsCollection)
        <div class="order-list">
            @foreach($cartsCollection as $key => $cart)
                <div class="col-md-12 cart-product-block" data-productKey="{{$cart['product']->id}}">
                    <div class="photo-img-thumb">
                        <div pid="{{$cart['uniqueCode']}}" class="delete-from-cart order-navigation">
                            <img src="{{Theme::asset('images/close-order.png')}}" alt="Remove order from list"/>
                        </div>
                        <a target="_blank" href="#">
                            <img src="{{ \Modules\Cms\Core\Images\ThumbBuilder::resize($cart['product']->getThumb() , 'auto', 50) }}"
                                 alt="{{$cart['product']->name}}"/>
                        </a>
                    </div>
                    <div class="order-content">
                        <p class="product-name">{{$cart['product']->name}}</p>
                        <p class="price">{{$cart['product']->price_actual}} грн@if($cart['product']->price_for)/{{trans('shop::product.'.$cart['product']->price_for)}}@endif</p>
                    </div>
                    @if (isset($selectedOptions[$cart['uniqueCode']]) && $selectedOptions[$cart['uniqueCode']])
                        <div class="selected-options">
                            @foreach($selectedOptions[$cart['uniqueCode']] as $attrKey => $attrValue)
                                {{$attributes[$attrKey]->name}}: {{$attrValue}} <br/>
                            @endforeach
                        </div>
                    @endif
                    <div class="order-count-picker">
                        <div pid="{{$cart['uniqueCode']}}" ptype="minus" class="count-manipulate"><span>-</span></div>
                        <input pid="{{$cart['uniqueCode']}}" type="text" class="count-value"
                               value="{{$cart['count']}}"/>
                        <div pid="{{$cart['uniqueCode']}}" ptype="plus" class="count-manipulate"><span>+</span></div>
                    </div>
                    <div class="order-price">{{(new Modules\Shop\Utils\Price\Processor)->calculate($cart['product'])->actualPrice()}}
                        <small>грн</small>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="text-right total-price-block">
            <p>Итого заказ на сумму:</p>
            <p class="price"> {{$orderCalculator->getTotalSum()}}<small>грн</small></p>
        </div>
        <div class="col-md-12 footer-cart-modal">
            <div class="col-md-6 text-left">
                <a href="#" class="text-right">
                    <button class="btn btn-gold continue-shopping"><span class="fa fa-shopping-cart">  </span>&nbsp;&nbsp; Продолжить покупки  &nbsp;&nbsp;&nbsp;</button>
                </a>
            </div>
            <div class="col-md-6  text-right">
                <a href="{{route('shop.checkout.list')}}" onclick="ga('send', 'pageview', '/oformit_zakaz'); return true;" class="text-right">
                    <button class="btn btn-buy"><span class="fa fa-shopping-cart">  </span> &nbsp;&nbsp;Оформить заказ</button>
                </a>
            </div>
        </div>
    @else
        <div class="header-h3">У вас нет товаров в корзине.</div>
    @endif
    <div class="clearfix"></div>
        @if (session('ga_script'))
            <script type="text/javascript">
                $(function(){
                    {!! Session::pull('ga_script', '') !!}
                })
            </script>
        @endif
        @if (session('ga_add_to_cart_script'))
            <script type="text/javascript">
                $(function(){
                    {!! Session::pull('ga_add_to_cart_script', '') !!}
                });
            </script>
        @endif
        @if (session('fb_add_to_cart_script'))
            <script type="text/javascript">
                $(function(){
                    {!! Session::pull('fb_add_to_cart_script', '') !!}
                });
            </script>
        @endif

        @if (session('ga_delete_from_cart_script'))
            <script type="text/javascript">
                $(function(){
                    {!! Session::pull('ga_delete_from_cart_script', '') !!}
                });
            </script>
        @endif
        @if (session('fb_delete_from_cart_script'))
            <script type="text/javascript">
                $(function(){
                    {!! Session::pull('fb_delete_from_cart_script', '') !!}
                });
            </script>
        @endif
</div>
