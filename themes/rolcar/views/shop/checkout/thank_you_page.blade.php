@extends('rolcar::layouts.master_custom')
@section('content')
<div class="thank_page">
    <div class="box_content">
        <div class="box-1">
            <div class="box_left">
                <div class="box_title">
                    <h3>Спасибо за ваш заказ №{{$order->invoice}},<br> принят</h3>
                </div>
                <div class="box_text">
                    <p>{!! $message !!}</p>
                </div>
            </div>
            <div class="box_right">
                <div class="box">
                    <div class="box_title">
                        <h3>У вас возникли вопросы по заказу?<br> Позвоните нам!</h3>
                    </div>
                    <div class="box_contacts">
                        <ul>
                            <li>
                                <p class="kievstar">+38 <span>(067)</span> 000-10-00</p>
                            </li>
                            <li>
                                <p class="vodafone">+38 <span>(095)</span> 000-75-00</p>
                            </li>
                            <li>
                                <p class="life">+38 <span>(093)</span> 000-27-00</p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-2">
            <h3>Мы ценим, что Вы выбрали нас, в связи с этим мы дарим <span> Вам промо-код на - 15% скидки на 2-й Заказ!<br>Промо-код действует в течении 30 дней с даты этого заказа!</span> Чтобы воспользоваться промо-кодом укажите его оператору по телефону или в соответствующем поле в заказе через сайт!</h3>
            <div class="promo_cod">
                <p>Промо-код "2020"</p>
            </div>
        </div>
<!--         <div class="box-3">
            <h3>А еще у нас есть отличные акционные предложения</h3>
            <div class="box_links">
                <a href="#" class="link link-1">
                    <img src="/themes/rolcar/images/thank_page_img-1.jpg" alt="thank_page_img">
                </a>
                <a href="#" class="link link-2">
                    <img src="/themes/rolcar/images/thank_page_img-2.jpg" alt="thank_page_img">
                </a>
                <a href="#" class="link link-3">
                    <img src="/themes/rolcar/images/thank_page_img-3.jpg" alt="thank_page_img">
                </a>
            </div>
        </div> -->
    </div>
</div>

@endsection

@section('style')
<link rel="stylesheet" href="{{Theme::asset('css/thank-page.css')}}">
@endsection

@section('javascript')
<script>
    (function(d, s){
        var js = d.createElement(s);
        window.WepsterButtonsInit = {
            hash: ''};
        js.src = 'share-tools/buttons/init.js';
        document.getElementsByTagName('head')[0].appendChild(js);
    }(document, 'script'));
</script>
<script>
	    window.dataLayer = window.dataLayer || [];
            dataLayer.push({
             'ecommerce': {
               'currencyCode': 'UAH',
               'purchase': {
                 'actionField': {
                   'id': '{{$order->invoice}}',
                   'affiliation': 'rolcar.com.ua',
                   'revenue': '{{$order->total_price}}',
                   'tax': '',
                   'shipping': '',
                   'coupon': ''
                 },
                 'products': [
                   @foreach ($products as $product)
                    {  
                     'name': '{{$product["name"]}}',
                     'id': '{{$product["id"]}}',
                     'price': '{{$product["price"]}}',
                     'brand': '{{$product["brand"]}}',
                     'quantity': '{{$product["quantity"]}}',
                     'coupon': ''
                    },
                   @endforeach
                  ]
               }
             },
             'event': 'gtm-ee-event',
             'gtm-ee-event-category': 'Enhanced Ecommerce',
             'gtm-ee-event-action': 'Purchase',
             'gtm-ee-event-non-interaction': 'False',
            });
</script>
<script>
fbq('track', 'Purchase', {
	contents: [
		@foreach ($products as $product)
			{ id: '{{$product["id"]}}', quantity: '{{$product["quantity"]}}' },
		@endforeach
		],
		content_type: 'product',
		value: '{{$order->total_price}}',
		currency: 'UAH'
});
</script>

@endsection
