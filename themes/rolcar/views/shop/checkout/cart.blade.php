@extends('rolcar::layouts.master_custom')
@section('content')
<div>
<style type="text/css">
    body {
        background: #f1f3f9 !important;
        font-family: "Helvetica Neue",Helvetica,Arial,sans-serif !important;
    }
    #footer-seo-block, .upper-header-banner {
        display: none;
    }
    .d-container{
        max-width: 1200px;
        display: grid;
        column-gap: 25px;
        grid-template-columns: 4fr 3fr;
        grid-template-areas:
            "text text"
            "content cart"
            "content .";
        margin: 0 auto;
        padding: 30px 10px;
    }
    .d-content-header{
        grid-area: text;
    }
    .d-content-header-text{
        font-size: 26px;
        letter-spacing: 1px;
    }
    .d-content{
        grid-area: content;
        display: block;
        background-color: #fff;
        padding: 20px;
        box-shadow: 0px 2px 4px 0px #00000017;
    }
    .d-cart{
        grid-area: cart;
        display: block;
        background-color: #fff;
        border: 4px solid transparent;
        padding: 20px;
        border-radius: 5px;
        border-image-source: linear-gradient(to right, #fcc82d 0%, #db7905 100%);
        border-image-slice: 7;
    }
    h2.d-cart-header {
        font-size: 18px;
        font-weight: 100;
    }
    .d-order-block{
        width: 100%;

    }
    .d-photo-text{
        display: flex;
    }
    .d-photo-block{
        float: left;
        position: relative;
    }
    .d-order-navigation {
        position: absolute;
        top: -9px;
        right: -72px;
    }
    .d-order-navigation span {
        margin-left: 3px;
        color: #ff1d75;
        text-decoration: underline;
        font-size: 13px;
    }
    .d-order-navigation span,.d-order-navigation img{
        float: left;
        cursor: pointer;
    }
    .d-order-content {
        display: inline-flex;
        min-width: 230px;
        max-width: 400px;
        overflow: hidden;
    }
    .d-order-content a {
        color: #4d4d4d;
        padding: 10px 6px;
        text-decoration: none;
        font-size: 14px;
        font-weight: 600;
    }
    .d-order-count-picker {
        display: flex;
        margin-left: 57px;
        justify-content: space-between;
        text-align: center;
        align-items: center;
        align-content: center;
    }
    .count-manipulate {
        display: flex !important;
        align-items: center !important;
        justify-content: center !important;
        width: 30px !important;
        height: 30px !important;
        background-color: #fcc82d !important;
        font-size: 30px !important;
        color: #FFFFFF !important;
        cursor: pointer !important;
        margin: 0px 10px !important;
    }
    .count-manipulate span {
        position: relative !important;
        left: 0 !important;
        top: 0 !important;
    }
    .d-order-price {
        margin-left: 57px;
        display: flex;
        margin-bottom: -10px;
        margin-top: 10px;
    }
    .total-price-sum {
        color: #000000;
        font-size: 42px;
        font-weight: 700;
        width: auto !important;
    }
    .order-inputs-form {
        max-width: 100% !important;
    }
    input::placeholder{
        color: #a7a7a7;
        font-style: italic;
    }
    .default-input {
        margin-bottom: 15px;
        border: 1px solid #babfe0 !important;
        width: 100% !important;
        height: 40px !important;
        padding: 5px 12px !important;
        font-size: 14px !important;
        color: #000 !important;
    }
    .form-control {
        width: 100% !important;
        height: 40px !important;
        color: #000 !important;
        border: 1px solid #babfe0 !important;
        box-shadow: none !important;
        border-radius: 0px !important;
    }
    .order-inputs-form-last{
        width: 100%;
        float: left;
    }
    .d-col{
        display: flex;
        justify-content: space-between;
        align-items: baseline;
    }
    .total-sum-block{
        width: 100%;
        float: left;
    }
    .total-sum-block h5 {
        font-size: 20px !important;
        border-bottom: none !important;
        text-transform: none !important;
        font-weight: 600 !important;
        margin: 0 !important;
        margin-bottom: 10px !important;
    }
    textarea.form-control {
        height: auto !important;
    }
    .btn-default2 {
        width: 100%;
        color: #fff;
        border: 1px solid #962c10 !important;
        background-image: linear-gradient(to right, #db7805 0%, #cc4d27 100%);
    }
    .btn-default2:hover {
        border: 1px solid #f9c60c !important;
        background-image: linear-gradient(to right, #efbd31 0%, #dea247 100%);
    }
    .submit-order-btn {
        float: none !important;
        display: flex !important;
        justify-content: center !important;
        text-decoration: none;
    }
    .submit-order-btn button {
        text-transform: none !important;
        font-size: 18px !important;
        font-weight: normal !important;
    }
    .b_autocomplete {
        display:  block;
        position: relative;
    }
    #searchSettlementsDiv .scroll-element {
        width: 100%;
        height: auto;
        overflow-x: hidden;
        overflow-y: auto;
        display: none;
        position: absolute;
        z-index: 20;
        top: 42px;
        left: 0px;
        background: #fff;
        border: 1px solid #e1e1e1;
        box-shadow: 0px 3px 6px #dadada;
    }
    #searchSettlementsDiv2 .scroll-element {
        width: 100%;
        height: auto;
        overflow-x: hidden;
        overflow-y: auto;
        display: none;
        position: absolute;
        z-index: 20;
        top: 42px;
        left: 0px;
        background: #fff;
        border: 1px solid #e1e1e1;
        box-shadow: 0px 3px 6px #dadada;
    }
    #searchSettlementsDiv .scroll-element {
        max-height: 500px;
        min-height: 100px;
    }
    #searchSettlementsDiv2 .scroll-element {
        max-height: 500px;
        min-height: 100px;
    }
    #o_cities {
        width: 100%;
        margin: 0;
        padding: 10px;
    }
    .b_autocomplete ul li {
        display: list-item;
        float: none;
        padding: 10px 0 6px 10px;
        font-size: 13px;
        line-height: 18px;
        cursor: pointer;
        word-wrap: break-word;
        list-style-type: none;
    }
    .b_autocomplete ul li:hover, .b_autocomplete ul li.hover {
        color: #f23c32;
    }
    .active{
        color: #f23c32;
        font-weight: bold;
    }
    html input[disabled] {
        cursor: default;
        background: #f3f3f3;
        border: 1px solid #e2e2e2 !important;
    }
                .custom-select-wrapper {
            position: relative;
            user-select: none;
            width: 100%;
    }
    .custom-select {
        position: relative;
        display: flex;
        flex-direction: column;
        border-width: 0 1px 0 1px;
        border-style: solid;
        border-color: #babfe0;
    }
    .custom-select__trigger {
        position: relative;
        display: flex;
        align-items: center;
        justify-content: space-between;
        padding: 0 22px 0 50px;
        font-size: 14px;
        font-weight: 100;
        color: #000000;
        height: 40px;
        line-height: 60px;
        background: #ffffff;
        cursor: pointer;
        border-width: 1px 0 1px 0;
        border-style: solid;
        border-color: #babfe0;
    }
    .custom-options {
        position: absolute;
        display: block;
        top: 100%;
        left: 0;
        right: 0;
        border-top: 0;
        background: #fff;
        transition: all 0.3s;
        opacity: 0;
        visibility: hidden;
        pointer-events: none;
        z-index: 2;
        box-shadow: 0px 6px 8px 1px #c3c3c3;
    }
        .custom-select.open .custom-options {
            opacity: 1;
            visibility: visible;
            pointer-events: all;
    }
        .custom-option {
        position: relative;
        display: block;
        padding: 0 22px 0 50px;
        font-size: 14px;
        font-weight: 300;
        color: #3b3b3b;
        line-height: 40px;
        cursor: pointer;
        transition: all 0.3s;
    }
    .custom-option.selected {
        color: #000000;
        background-color: #fff5e9;
    }
    .arrow {
            position: relative;
            height: 10px;
            width: 10px;
    }
    .arrow::before, .arrow::after {
        content: "";
        position: absolute;
        bottom: 0px;
        width: 0.1rem;
        height: 100%;
        transition: all 0.5s;
    }
        .arrow::before {
            left: 3px;
            transform: rotate(45deg);
            background-color: #394a6d;
    }
        .arrow::after {
            left: -4px;
            transform: rotate(-45deg);
            background-color: #394a6d;
    }
        .open .arrow::before {
            left: 3px;
            transform: rotate(-45deg);
    }
        .open .arrow::after {
            left: -4px;
            transform: rotate(45deg);
    }
    .nalichnie::before {
        content: '';
        top: 50%;
        left: 20px;
        transform: translateY(-50%);
        position: absolute;
        background-image: url('/themes/rolcar/images/cart-icons.png');
        vertical-align: middle;
        background-position: 0 0px;
        width: 20px;
        height: 20px;
    }
    .privat24::before {
        content: '';
        top: 50%;
        left: 20px;
        transform: translateY(-50%);
        position: absolute;
        background-image: url('/themes/rolcar/images/cart-icons.png');
        vertical-align: middle;
        background-position: -80px -20px;
        width: 20px;
        height: 20px;
    }
    .visa_master::before {
        content: '';
        top: 50%;
        left: 20px;
        transform: translateY(-50%);
        position: absolute;
        background-image: url('/themes/rolcar/images/cart-icons.png');
        vertical-align: middle;
        background-position: -20px -60px;
        width: 20px;
        height: 20px;
    }
    .part_in_installments::before {
        content: '';
        top: 50%;
        left: 20px;
        transform: translateY(-50%);
        position: absolute;
        background-image: url('/themes/rolcar/images/cart-icons.png');
        vertical-align: middle;
        background-position: -80px -60px;
        width: 20px;
        height: 20px;
    }
    .beznal::before {
        content: '';
        top: 50%;
        left: 20px;
        transform: translateY(-50%);
        position: absolute;
        background-image: url('/themes/rolcar/images/cart-icons.png');
        vertical-align: middle;
        background-position: 0 -20px;
        width: 20px;
        height: 20px;
    }
    .instant_installments::before {
        content: '';
        top: 50%;
        left: 20px;
        transform: translateY(-50%);
        position: absolute;
        background-image: url('/themes/rolcar/images/cart-icons.png');
        vertical-align: middle;
        background-position: -60px 0;
        width: 20px;
        height: 20px;
    }
    .s_np::before {
        content: '';
        top: 50%;
        left: 20px;
        transform: translateY(-50%);
        position: absolute;
        background-image: url('/themes/rolcar/images/cart-icons.png');
        vertical-align: middle;
        background-position: -80px -40px;
        width: 20px;
        height: 20px;
    }
    .s_shop::before {
        content: '';
        top: 50%;
        left: 20px;
        transform: translateY(-50%);
        position: absolute;
        background-image: url('/themes/rolcar/images/cart-icons.png');
        vertical-align: middle;
        background-position: -20px -20px;
        width: 20px;
        height: 20px;
    }
    .k_np::before {
        content: '';
        top: 50%;
        left: 20px;
        transform: translateY(-50%);
        position: absolute;
        background-image: url('/themes/rolcar/images/cart-icons.png');
        vertical-align: middle;
        background-position: -80px -40px;
        width: 20px;
        height: 20px;
    }
    .demonstration {
        display: flex;
        text-align: center;
        justify-content: space-evenly;
        margin-top: 20px;
        border: 1px solid #ce5224;
        margin-bottom: 60px;
        padding: 10px;
        box-shadow: 0px 3px 4px 0px #e0e0e0;
    }
    .ppvblok {
        display: flex;
        text-align: center;
        align-items: center;
    }
    .ppvtext{
        font-size: 12px;
        color: #a7a7a7;
    }
    .ppvblok div {
        display: grid;
        text-align: start;
        padding-left: 8px;
    }
    .ppvspan {
        font-weight: bold;
        line-height: 14px;
        color: #325c79;
    }
    .fa {
        font-size: 30px;
        color: #46acb1;
    }
    option:disabled {
        color: #d2d2d2 !important;
    }
    #payment_parts .select2-container .select2-selection--single .select2-selection__rendered {
        padding-left: 10px;
    }
    #select2-payment_part-results .select2-results__option {
        padding: 10px 10px;
    }
    .form-group .select2-container .select2-selection--single .select2-selection__rendered {
        padding-left: 10px;
    }
    @media (max-width: 1200px){
        .d-container{
            max-width: 600px;
            grid-template-columns: auto;
            grid-template-areas:
                "text"
                "cart"
                "content";
        }
    }
    @media (max-width: 490px){
        .d-cart {
            padding: 5px;
        }
        .visa_master::before {
            left: 5px;
        }
        .custom-select__trigger {
            padding: 0 5px 0 30px;
            font-size: 12px;
            line-height: 10px;
        }
        .custom-option {
            padding: 0 5px 0 30px;
            font-size: 11px;
            line-height: 38px;
        }
        .nalichnie::before {
            left: 5px;
        }
        .privat24::before {
            left: 5px;
        }
        .visa_master::before {
            left: 5px;
        }
        .part_in_installments::before {
            left: 5px;
        }
        .beznal::before {
            left: 5px;
        }
        .instant_installments::before {
            left: 5px;
        }
        .demonstration {
            margin-top: 5px;
            margin-bottom: 40px;
            padding: 10px;
        }
        .fa {
            font-size: 18px;
        }
        .ppvblok div {
            padding-left: 5px;
        }
        .ppvtext {
            font-size: 10px;
            padding-bottom: 5px;
        }
        .ppvspan {
            line-height: 10px;
            font-size: 12px;
        }
    }
</style>
<div class="d-container">
	<div class="d-content-header">
		<h1 class="d-content-header-text">Оформление заказа</h1>
	</div>
	@if ($cartsCollection)
		<div class="d-cart">
			<h2 class="d-cart-header">Ваш заказ</h2>
			<hr>
			@foreach($cartsCollection as $key => $cart)
				<div id="uniqcode-{{$cart['uniqueCode']}}" data-productKey="{{$cart['product']->id}}" class="box">
					<div class="d-order-block">
						<div class="d-photo-text">
							<div class="d-photo-block">
								<div pid="{{$cart['uniqueCode']}}" class="delete-from-cart d-order-navigation">
									<img src="{{Theme::asset('images/close-order.png')}}" alt="Remove order from list"/>
									<span>удалить</span>
								</div>
								<a target="_blank" href="#">
									<img src="{{ \Modules\Cms\Core\Images\ThumbBuilder::resize($cart['product']->getThumb() , 'auto', 50) }}"
										alt="{{$cart['product']->name}}"/>
								</a>
							</div>
							<div class="d-order-content">
								<a target="_blank" href="#">{{$cart['product']->name}}</a>
							</div>
						</div>
						<!-- <div class="selected-options">
							@if (isset($selectedOptions[$cart['uniqueCode']]) && $selectedOptions[$cart['uniqueCode']])
								@foreach($selectedOptions[$cart['uniqueCode']] as $attrKey => $attrValue)
									{{$attributes[$attrKey]->name}}: {{$attrValue}} <br/>
								@endforeach
							@endif
						</div> -->
						<div class="d-order-price cart-item-sum">
							<div style="font-size: 14px;color: #5b5b5b;">Цена:</div>
							<div style="display: flex; align-items: center;">
								<span style="font-weight: 600;color: #5b5b5b;margin-left: 10px;margin-right: 3px;">{{(new Modules\Shop\Utils\Price\Processor)->calculate($cart['product'])->actualPrice()}}</span>
								<small>грн</small>
							</div>
						</div>
						<div class="d-order-count-picker">
							<span style="font-size: 14px;color: #5b5b5b;">Количество:</span>
							<div style="display: flex; align-items: center;">
								<div pid="{{$cart['uniqueCode']}}" ptype="minus" class="link-1 count-manipulate button-plus"><span>-</span></div>
								<div class="box-number">
									<input pid="{{$cart['uniqueCode']}}" type="text" class="input-box-number count-value"
									value="{{$cart['count']}}"/>
								</div>
								<div pid="{{$cart['uniqueCode']}}" ptype="plus" class="link-2 count-manipulate button-minus"><span>+</span></div>
							</div>
						</div>
					</div>
				</div>
				<hr>
			@endforeach
			<div style="display: flex;justify-content: space-between;align-items: center;margin-bottom: 30px;">
				<h5 style="font-size: 24px;margin: 0;">К оплате</h5>
				<div class="total-price-sum">
					<span>{{$orderCalculator->getTotalSum()}}</span>
					<small>грн</small>
				</div>
			</div>
		</div>
		<div class="d-content">
			<form action="{{ route('shop.order.create') }}" method="POST" autocomplete="off">
				<div class="col-md-12">
					<div class="order-inputs-form">
						<h2 style="font-size: 16px;">Получатель:</h2>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<div class="form-default-group">
							<label for="dname" style="font-size: 12px;color: #a0a0a0;letter-spacing: 0.42px;">Имя получателя<span style="color: red;font-size: 18px;">*</span></label>
							<input onchange="gaCheckout(2)" required="" class="default-input" placeholder="Иванов" name="first_name" type="text" id="dname"/>
						</div>
						<div class="form-default-group">
							<label for="dphone" style="font-size: 12px;color: #a0a0a0;letter-spacing: 0.42px;">Телефон получателя в формате (0хх) хх-хх-хх<span style="color: red;font-size: 18px;">*</span></label>
							<input required="" class="phone default-input" name="phone"
								placeholder="Телефон:  (___)___-__-__" type="text" id="dphone"/>
							<!-- <i>Введите ваш номер телефона в формате (099) 999-99-99</i> -->
						</div>
						<div class="form-default-group">
							<label for="demail" style="font-size: 12px;color: #a0a0a0;letter-spacing: 0.42px;">Email для получения квитанции (не обязательно для заполнения)</label>
							<input class="default-input" placeholder="ivanov@pochta.com" name="email" type="email" id="demail"/>
						</div>
					</div>
				</div>
				<div class="col-md-12">
                    <div class="order-inputs-form">
                        <h2 style="font-size: 16px;">Способ оплаты:</h2>
                        <div class="custom-select-wrapper">
                            <select class="form-control" id="payment" name="payment">
                                <!-- <option value="" disabled="" selected="" hidden="">Выберите способ оплаты...</option> -->
                                <option value="nalichnie">Наложенный платеж Новой Почты</option>
                                <option value="privat24">Приват 24</option>
                                <option value="visa_master">Оплата картой Visa/Mastercard - LiqPay</option>
                                <option value="instant_installments">Мгновенная рассрочка - ПриватБанк</option>
                                <option value="part_in_installments">Оплата частями</option>
                                <option value="beznal">Безналичными</option>
                            </select>
                        </div>
                        <div id="payment_parts" style="display:none">
                            <select class="form-control" id="payment_part" name="payment_part_count">
                                <option value="2">Разделить на 2 платежа</option>
                                <option value="3">Разделить на 3 платежа</option>
                                <option value="4">Разделить на 4 платежа</option>
                                <option value="5">Разделить на 5 платежей</option>
                            </select>
                        </div>
                        <h2 style="font-size: 16px;">Способ доставки:</h2>
                        <div class="form-group">
                            <select class="form-control" id="delivery" name="delivery" required="">
                                <option value="" disabled="" selected="" hidden="">Выберите способ доставки</option>
                                <option value="s_np">Новая почта</option>
                                <option value="s_shop">Самовывоз из магазина (Одесса)</option>
                                <!-- <option value="k_np">Курьером Новой Почты</option> -->
                            </select>
                        </div>
                        <div id="delivery-block-s_np" class="delivery-info-block">
                            <div class="form-group col-md-10 b_autocomplete s_activated" id="searchSettlementsDiv" data-custom-filter="true">
                                <ins class="o_input_pointer"></ins>
                                <input id="oCityFilter" class="default-input" type="text" name="s_np_city" placeholder="Введите насел. пункт" value="" autocomplete="off">
                                <div class="scroll-element" style="display: none;">
                                    <ul class="list drop-down-ul map" id="o_cities"></ul>
                                </div>
                                <ins class="o_input_arrow" id="oCityArrow"></ins>
                            </div>
                            <div class="form-group col-md-10 b_autocomplete s_activated" id="searchSettlementsDiv2" data-custom-filter="true">
                                <select id="o_warehouses" class="default-input" name="s_np_warehouse" disabled="" required=""></select>
                            </div>
                        </div>
                        <div id="delivery-block-s_shop" class="delivery-info-block">
                            <input class="default-input" type="text" name="s_shop_city" value="Пр. Небесной Сотни 2А, Маг. 23-25Н" readonly="">
                        </div>
                    </div>
                </div>
				<div class="col-md-12">
					<div class="order-inputs-form-last">
						<h2 style="font-size: 16px;">Промокод:</h2>
						<div class="form-default-group">
							<input  class="default-input" type="text" name="promocode" placeholder="Введите промокод, если он у Вас есть">
						</div>
						<h2 style="font-size: 16px;">Коментарий:</h2>
						<div class="form-default-group">
							<textarea class="form-control" name="description" placeholder="Уточнения к заказу"></textarea>
						</div>
					</div>
					<div class="total-sum-block d-col">
						<h5>К оплате</h5>
						<div class=total-price-sum>
							<span>{{$orderCalculator->getTotalSum()}}</span>
							<small>грн</small>
						</div>
					</div>
					<div class="total-sum-block">
						<a class="submit-order-btn" onclick="ga('send', 'pageview', '/Podtverdit_Zakaz'); return true;" href="#">
							<button type="submit" class="btn btn-default2">Подтвердить заказ</button>
						</a>
					</div>
				</div>
			</form>
		</div>
	@else
		<h3>У вас нет товаров в корзине.</h3>
	@endif
</div>

<div class="clearfix"></div>
</div>
@endsection
@section('javascript')
    @if (!Request::ajax() && session('ga_script'))
        <script type="text/javascript">
            $(function () {
                {!! Session::pull('ga_script', '') !!}
            })
        </script>
    @endif
    @if (!Request::ajax() && session('ga_delete_from_cart_script'))
        <script type="text/javascript">
            $(function () {
                {!! Session::pull('ga_delete_from_cart_script', '') !!}
            })
        </script>
    @endif
    @if (!Request::ajax() && session('fb_delete_from_cart_script'))
        <script type="text/javascript">
            $(function () {
                {!! Session::pull('fb_delete_from_cart_script', '') !!}
            })
        </script>
    @endif
	<script type="text/javascript" src="https://ppcalc.privatbank.ua/pp_calculator/resources/js/calculator.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    
    <script>
        fbq('track', 'InitiateCheckout', {
          value: '{{$orderCalculator->getTotalSum()}}',
          currency: 'UAH',
        });
        window.dataLayer = window.dataLayer || [];
        dataLayer.push({
         'ecommerce': {
           'currencyCode': 'UAH',
           'checkout': {
         'actionField': {'step': 2},
             'products': [
            @foreach($cartsCollection as $key => $cart)
             {
               'name': '{{$cart['product']->name}}',
               'id': '{{$cart['product']->sku}}',
               'price': '{{(new Modules\Shop\Utils\Price\Processor)->calculate($cart['product'])->actualPrice()}}',
               'brand': '{{$cart['product']->brand->name}}',
               'quantity': {{$cart['count']}}
             },
             @endforeach
             ]
           }
         },
         'event': 'gtm-ee-event',
         'gtm-ee-event-category': 'Enhanced Ecommerce',
         'gtm-ee-event-action': 'Checkout Step 2',
         'gtm-ee-event-non-interaction': 'False',
        });

/*--------Назначение элементам списка css класса с иконками--------НАЧАЛО------*/
        function formatState (state) {
            if (!state.id) {
                return state.text;
            }
            var $state = $('<span><span  class="custom-option"></span></span>');
            $state.find("span").text(state.text);
            $state.find("span").addClass(state.element.value.toLowerCase());
            return $state;
        };
/*--------Назначение элементам списка css класса с иконками--------КОНЕЦ-------*/
/*--------По готовности страницы цепляем Умный select к спискам----НАЧАЛО------*/
        $(document).ready(function() {
            $('#payment').select2({
                templateSelection: formatState,
                templateResult: formatState,
                minimumResultsForSearch: Infinity
            });
            $('#payment_part').select2({
                minimumResultsForSearch: Infinity
            });
            $("#payment").change(function() {
                var pay = $("#payment").val(); 
                if( pay == 'part_in_installments'){
                    $('#payment_parts').show(300);
                    $('#payment_part option[value="4"]').attr("disabled", true);
                    $('#payment_part option[value="5"]').attr("disabled", true);
                }else if ( pay == 'instant_installments' ){
                    $('#payment_parts').show(300);
                    $('#payment_part option[value="4"]').attr("disabled", false);
                    $('#payment_part option[value="5"]').attr("disabled", false);
                }else{
                    $('#payment_parts').hide(300);
                };
            });
            $('#delivery').select2({
                templateSelection: formatState,
                templateResult: formatState,
                minimumResultsForSearch: Infinity
            });
        });
/*--------По готовности страницы цепляем Умный select к спискам----КОНЕЦ------*/
/*--------Блок расчета частей оплаты при помощи calculator.js------НАЧАЛО-----*/
		// $('#payment_part').change({
		// 	calculateppv();
		// });

		// function calculateppv() {
		// 	if(document.getElementById("payment").value == 'part_in_installments'){
		// 		resCalc = PP_CALCULATOR.calculatePhys((Number(inputppv.value)-1), 956);
		// 		srok.textContent = Number(inputppv.value)-1 + ' мес.';
		// 		kol.textContent = resCalc.payCount;
		// 		sum.textContent = resCalc.ppValue + ' ₴';
		// 	};
		// 	if(document.getElementById("payment").value == 'instant_installments'){
		// 		resCalc = PP_CALCULATOR.calculatePhys((Number(inputppv.value)-1), 956);
		// 		srok2.textContent = Number(inputppv.value)-1 + ' мес.';
		// 		kol2.textContent = resCalc.payCount;
		// 		sum2.textContent = resCalc.ipValue + ' ₴';
        // 	};
/*--------Блок расчета частей оплаты при помощи calculator.js-------КОНЕЦ------*/
/*--------------------------Блок Nova Poshta------------------------НАЧАЛО-----*/
        var $citySelect = $("#oCityFilter");
        var $warSelect = $("#o_warehouses");
        $('#delivery').on('change', function() {
            $('.delivery-info-block').hide();
            $('#delivery-block-' + this.value).show();
        });
        /*---Обработка нажатия клавиш в Input городов, вызов списка городов----*/
		$citySelect.keyup(function(){
            if ($citySelect.val().length > 1) {
				setTimeout(function(){
                    searchSettlements($citySelect.val());
                }, 300);
            }
			if ($citySelect.val() == "") {
                $('#o_cities .active').removeClass('active');
			}
        });
        /*---Обработка клика в Input городов,удаление и вызов списка городов---*/
		$citySelect.click(function () {
			var $active = $('#o_cities .active');
			if ($active.length) {
				$(this).closest('.b_autocomplete').data('previous', $active);
			} else {
				fillMainCities();
			}
            $warSelect.val('');
			$warSelect.prop('disabled', true);
			$citySelect.val('');
        });
        /*---Список главных городов, показывается по умолчанию-----------------*/
		function fillMainCities() {
			$ulElem = $('#o_cities');
			let myHeaders = new Headers();
			myHeaders.append("Content-Type", "application/json");
			myHeaders.append("Cookie", "PHPSESSID=be6fb6c4efa6a5c1868bc0879ec4467d; YIICSRFTOKEN=001b8ee94404bba60977e6477284d32b7e10afd0s%3A88%3A%22TXVXUVllWVZwZlI5R3JQMEg5M2RmX0J2aGM2MDNiZTlJp55FRogMevcGpAglE_0rqbQVufAqDLXIGBmhsEEL6Q%3D%3D%22%3B");
			let raw = JSON.stringify({"modelName":"Address","calledMethod":"searchSettlements","methodProperties":{"CityName":"","Limit":5}});
			let requestOptions = {
			  method: 'POST',
			  headers: myHeaders,
			  body: raw,
			  redirect: 'follow'
			};
            fetch("https://api.novaposhta.ua/v2.0/json/Address/searchSettlements/", requestOptions)
            .then(response => response.json())
            .then(result => {
                if (result) {
                    var json = result.data[0].Addresses;
                    var content = '';
                    $.each(json, function(ref, json) {
                        if(json.Warehouses > 0 ){
                        content += '<li data-city-ref="'+json.Ref+'" id="li'+json.Ref+'" onclick="applyCity(\''+json.Ref+'\', `'+json.Present+'`)"><span>'+json.Present+'</span></li>';
                        }
                    });
                    $ulElem.html(content);
                    $('#searchSettlementsDiv .scroll-element').show();
                }
            })
            .catch(error => console.log('error', error));
		}
        /*--- Список согласно введеных символов названия населенного пункта---*/
		function searchSettlements(CityName) {
			$ulElem = $('#o_cities');
			let myHeaders = new Headers();
			myHeaders.append("Content-Type", "application/json");
			myHeaders.append("Cookie", "PHPSESSID=; YIICSRFTOKEN=");
			let raw = JSON.stringify({"modelName":"Address","calledMethod":"searchSettlements","methodProperties":{"CityName":CityName,"Limit":5}});
			let requestOptions = {
			  method: 'POST',
			  headers: myHeaders,
			  body: raw,
			  redirect: 'follow'
			};
			fetch("https://api.novaposhta.ua/v2.0/json/Address/searchSettlements/", requestOptions)
            .then(response => response.json())
            .then(result => {
                if (result) {
                    var json = result.data[0].Addresses;
                    var content = '';
                    $.each(json, function(ref, json) {
                        if(json.Warehouses > 0 ){
                        content += '<li data-city-ref="'+json.Ref+'" id="li'+json.Ref+'" onclick="applyCity(\''+json.Ref+'\', `'+json.Present+'`)"><span>'+json.Present+'</span></li>';
                        }
                    });
                    $ulElem.html(content);
                    $('#searchSettlementsDiv .scroll-element').show();
                }
			})
            .catch(error => console.log('error', error));
		}
        /*---Список отделений в конкретном населенном пункте -----------------*/
		function warehousesList(CityRef) {
            $ulElem = $('#o_warehouses');
			var myHeaders = new Headers();
			myHeaders.append("Content-Type", "application/json");
			myHeaders.append("Cookie", "PHPSESSID=; YIICSRFTOKEN=");
			var raw = JSON.stringify({"modelName":"Address","calledMethod":"getWarehouses","methodProperties":{"SettlementRef":CityRef}});
			var requestOptions = {
			  method: 'POST',
			  headers: myHeaders,
			  body: raw,
			  redirect: 'follow'
			};
			fetch("https://api.novaposhta.ua/v2.0/json/Address/searchSettlements/", requestOptions)
            .then(response => response.json())
            .then(result => {
                if (result) {
                    var json = result.data;
                    var content = '<option value="" disabled="" selected="" hidden="">Выберите № отделения</option>';
                    $.each(json, function(ref, json) {
                            content += '<option value="'+json.Description+'">'+json.Description+'</option>';
                    });
                    $ulElem.html(content);
                    $ulElem.select2({});
                }
            })
            .catch(error => console.log('error', error));
        }
        /*---- Обработка события Click во внешнем поле------------------------*/
		document.addEventListener("click" , (e)=>{
			$('#o_cities').find('li').remove();
			$('#searchSettlementsDiv .scroll-element').hide();
			if($warSelect.val() != ''){
				$('#o_warehouse').find('li').remove();
				$('#searchSettlementsDiv2 .scroll-element').hide();
			}
			if($citySelect.val() == ''){
				$("#o_warehouses").val('');
				$("#o_warehouses").prop('disabled', true);
			}
        });
        /*----Функция-обработчик клика по пункту из списка городов ------------*/
		function applyCity(ref, description) {
			$('#oCityFilter').val(description);
			$('#li'+ref).addClass('active');
			if(description != ''){
				$("#o_warehouses").prop('disabled', false);
				warehousesList(ref);
			}
        }
/*--------------------------Блок Nova Poshta------------------------КОНЕЦ------*/
    </script>
@endsection