<div id="shop-cart-modal" class="modal-wrapper">
    <div class="modal-dialog shop-cart-modal-dialog">
        <div class="modal-body">
            <div class="modal-header">
                <div>Вы добавили товар в корзину</div>
                <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-content">
                <div class="cart-modal-content">
                </div>
            </div>
        </div>
    </div>
</div>