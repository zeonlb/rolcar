@extends('rolcar::layouts.master_custom')

@section('style')
    <link rel="stylesheet" href="{{Theme::asset('css/star-rating-svg.css')}}"/>
    <link rel="stylesheet" href="{{Theme::asset('js/vendor/magnific-popup-min.css')}}"/>

    <link href="{{Module::asset('admin:css/dropzone.css')}}" rel="stylesheet">
    @if ($product->lending_styles && strlen($product->lending_styles) > 0)
        {!! $product->lending_styles !!}
    @endif

@endsection
@section('javascript')
    <script src='{{Theme::asset('js/vendor/jquery.magnific-popup.min.js')}}'></script>
    <script src='{{Theme::asset('js/product-details.js')}}'></script>

    <!-- dropzone -->
    <script src="{{Module::asset('admin:js/dropzone/dropzone.js')}}"></script>
    {{--COMMENTS--}}
    <script>
        var uploadUrl = "{{ route('cms.image.store') }}";
        var token = "{{ csrf_token() }}";
        Dropzone.autoDiscover = false;
        var myDropzone = new Dropzone("div#myDrop", {
            url: uploadUrl,
            addRemoveLinks: false,
            maxFilesize: 5,
            maxFiles: 5,
            acceptedFiles: 'image/*',
            params: {
                _token: token
            }
        });
        myDropzone.on("success", function (file, resp) {
            if (resp.fileId) {
                var input = "<input name='files[]' type='hidden' value='" + resp.fileId + "' />";
                input += ' <i class="fa fa-times remove-com-img-item" aria-hidden="true"></i>';
                file.uniqueId = resp.fileId;
                $('.comment-img').append('<div class="col-md-3">' + file.previewTemplate.innerHTML + input + '</div>');
            }
        });
        $('.comment-img').on('click', '.remove-com-img-item', function (e) {
            e.preventDefault();
            $(this).parent().remove();
        });

        $(window).on('scroll', function () {
            if ($(document).scrollTop() >= $(document).height() / 100) {
                loadYoutube();
                loadMoreData();
                $(window).off('scroll');
            }
        });

        var video = '';
        <?php $videos = explode(" ", $product->video->youtube_code); ?>
                @foreach($videos as $video)
            video = video + '<div class="col-md-12"><iframe width="100%" height="400px" src="https://www.youtube.com/embed/{{$video}}" frameborder="0" allowfullscreen></iframe></div>';
        @endforeach


        $('#video-button').on('click', function () {
            $('.product-dt-video-desc-container').html(video + '<br>');
        });

        function loadYoutube() {
            $('.product-dt-video-container').html(video);
        }
        function loadMoreData() {
            $.ajax({
                url: '{{url("/product/$product->code")}}',
                method: "POST",
                data: {
                    id: "{{$product->id}}",
                    _token: "{{csrf_token()}}"
                },
                dataType: "text",
                success: function (data) {
                    if (data != '') {
                        $('.lending-block').html(data);
                    }
                },
                complete: function () {
                    $(document).ajaxStop(function (d, s) {
                        var js = d.createElement(s);
                        window.WepsterButtonsInit = {
                            hash: ''
                        };
                        js.src = 'https://my.wepster.com/share-tools/buttons/init.js';
                        document.getElementsByTagName('head')[0].appendChild(js);
                    }(document, 'script'));
                }
            });
        }
    </script>
    <script>
        $(function () {
            $('#comment-upload-video').on('click', function (e) {
                e.preventDefault();
                $('.shop-comment-video-modal').show();
                $('.shop-comment-video-modal.modal-dialog').show();
            });

            $('#comment-save-video').on('click', function (e) {
                e.preventDefault();
                var inp = $('#comment-video-input');
                var yUrl = inp.val();
                var uMatch = yUrl.match(/(youtu\.be\/|youtube\.com\/(watch\?(.*&)?v=|(embed|v)\/))([^\?&"'>]+)/);
                if (!uMatch) {
                    alert('Не правильная ссылка youtube');
                    return false;
                }

                var code = uMatch[5];
                if (code) {
                    var pUrl = '//img.youtube.com/vi/' + code + '/0.jpg';
                    $('.shop-comment-video-modal.modal-dialog').hide();
                    var html = '<div class="col-md-3 comment-video-thumb">  ' +
                        '<div class="dz-image">' +
                        '<a href="' + pUrl + '" target="_blank" ><img data-dz-thumbnail="" alt="dl_5_lamp.png" width="150" src="' + pUrl + '" id="Oval-2" stroke-opacity="0.198794158" stroke="#747474" fill-opacity="0.816519475" fill="#FFFFFF" sketch:type="MSShapeGroup"></a></path>      </g>    </svg>  </div>  <div class="dz-error-mark">    <svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">      <title>Error</title>      <defs></defs>      <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">        <g id="Check-+-Oval-2" sketch:type="MSLayerGroup" stroke="#747474" stroke-opacity="0.198794158" fill="#FFFFFF" fill-opacity="0.816519475">          <path d="M32.6568542,29 L38.3106978,23.3461564 C39.8771021,21.7797521 39.8758057,19.2483887 38.3137085,17.6862915 C36.7547899,16.1273729 34.2176035,16.1255422 32.6538436,17.6893022 L27,23.3431458 L21.3461564,17.6893022 C19.7823965,16.1255422 17.2452101,16.1273729 15.6862915,17.6862915 C14.1241943,19.2483887 14.1228979,21.7797521 15.6893022,23.3461564 L21.3431458,29 L15.6893022,34.6538436 C14.1228979,36.2202479 14.1241943,38.7516113 15.6862915,40.3137085 C17.2452101,41.8726271 19.7823965,41.8744578 21.3461564,40.3106978 L27,34.6568542 L32.6538436,40.3106978 C34.2176035,41.8744578 36.7547899,41.8726271 38.3137085,40.3137085 C39.8758057,38.7516113 39.8771021,36.2202479 38.3106978,34.6538436 L32.6568542,29 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2" sketch:type="MSShapeGroup"></path>    ' +
                        '</g>      </g>    </svg>  ' +
                        '</div><i class="fa fa-youtube-play " aria-hidden="true"></i><input name="comment[video_link]" type="hidden" value="' + yUrl + '">' +
                        '<i class="fa fa-times remove-com-img-item" aria-hidden="true"></i></div>';
                    $('.comment-img').append(html);
                } else {
                    alert('Не правильная ссылка youtube');
                    return false;
                }
            })
        });
    </script>
    {{--END COMMENT--}}

    <script type="text/javascript" src="{{Module::asset('shop:js/front/discount.js')}}"></script>
    {{--// GA--}}

    <script>
        $(function () {
            window.dataLayer = window.dataLayer || [];
            dataLayer.push({
                'ecommerce': {
                    'currencyCode': 'UAH',
                    'detail': {
                        'actionField': {
                            'list': '{{$product->name}}'
                        },
                        'products': [{
                            'name': '{{$product->name}}',
                            'id': '{{$product->sku}}',
                            'price': '{{$product->price}}',
                            'brand': '{{$product->brand->name}}'
                        }]
                    }
                },
                'event': 'gtm-ee-event',
                'gtm-ee-event-category': 'Enhanced Ecommerce',
                'gtm-ee-event-action': 'Product Details',
                'gtm-ee-event-non-interaction': 'True',
            });
        });
    </script>



    <script>
        $(function () {
            $('.upload-image-block.cimage').on('click', function (e) {
                e.preventDefault();
                $('#shop-comment-modal').show();
                $('.shop-comment-modal-dialog').show();
            })
        });
    </script>

    <script src='{{Theme::asset('js/jquery.star-rating-svg.js')}}'></script>
    <script type="text/javascript">
        $(".my-rating").starRating({
            initialRating: 2.5,
            strokeColor: '#894A00',
            strokeWidth: 10,
            starSize: 25,
            callback: function (currentRating, $el) {
                $(".my-rating").after('<input type="hidden" value="' + currentRating + '" name="comment[rating]">');
            }
        });
    </script>
    <script type="text/javascript">
        $('.gallery-item').magnificPopup({
            type: 'image',
            gallery: {
                enabled: true
            }
        });
    </script>
    <script type="text/javascript">
        $('.mfp-iframe').magnificPopup({
            type: 'video',
            gallery: {
                enabled: false
            }
        });
    </script>
@endsection
@section('content')
    @include('partials.bread_crumbs')
    <div id="shop-details-content" data-productKey="{{$product->id}}">
        <div class="shop-container container">
            <div class="row">
                <div class="col-md-12" id="product-main-header">
                    <h1>{{ucfirst($product->name)}}</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <ul class="nav nav-tabs">
                        <li data-tab="tab1" role="presentation" class="active"><a href="#">Все о товаре</a></li>
                        @if ($product->lending_html)
                            <li class="presentation"><a href="#presentation-text">Презентация</a></li>
                        @endif
                        <li data-tab="tab2" role="presentation"><a href="#">Характеристики</a></li>
                        @if ($product->video && $product->video->youtube_code)
                            <li data-tab="tab3" id="video-button" role="presentation"><a href="#">Видео обзор</a></li>
                        @endif
                        <li data-tab="tab4" role="presentation"><a href="#">Отзывы ({{$product->comments->count()}})</a>
                        </li>


                    </ul>
                </div>
            </div>

            <div class="tabs" id="tab1">
                <div class="col-md-12">
                    <div class="row">
                        <div class="product-details-block">

                            <!--PRODUCT PHOTOS DETAILS BLOCK-->
                            <div class="product-photo-block">
                                @if($product->photos->count())
                                    <div class="main-photo">
                                        <a data-mfp-src="/{!! $product->photos[0]->path !!}" href="#">
                                            <img src="{{ ThumbBuilder::resize($product->photos[0]->path, 'auto', 500) }}"
                                                 title="{{$product->name}}" alt="{{$product->name}}"/>
                                        </a>
                                    </div>
                                    <div class="photo-preview-block">
                                        @if ($product->photos->count() > 1)

                                            <div class="thumb-navigation" id="thumb-navigation-left">
                                                <img src="{{Theme::asset('images/thumb-navigation-left.jpg')}}"
                                                     alt="влево"/>
                                            </div>
                                            <div class="thumbs-block">
                                                <div class="thumbs-container">
                                                    @foreach($product->photos as $key => $photo)
                                                        @if ($key != 0)
                                                            <div class="photo-img-thumb">
                                                                <a data-mfp-src="/{!! $photo->path !!}" href="#">
                                                                    <img title="{{$product->name}}"
                                                                         alt="{{$product->name}}"
                                                                         src="{{ThumbBuilder::resize($photo->path, 47, 47)}}"/>
                                                                </a>
                                                            </div>
                                                        @endif
                                                    @endforeach
                                                </div>
                                            </div>
                                            <div class="thumb-navigation" id="thumb-navigation-right">
                                                <img src="{{Theme::asset('images/thumb-navigation-right.jpg')}}"
                                                     alt="вправо"/>
                                            </div>
                                        @endif
                                    </div>

                                    <div class="clearfix"></div>
                                @endif
                            </div>

                            <!--PRODUCT PRICE BLOCK-->
                            <div class="product-price-details">
                                <?php
                                $rating = ceil($product->comments->filter(function ($value) {
                                    return $value->rating > 0;
                                })->avg('rating'));
                                ?>
                                @if ($product->is_present)
                                    <div class="product_present">
                                        <img class="product_present_icon" src="{{Theme::asset('images/present.svg')}}"
                                             alt="present"/>
                                        <a href="/video-review" target="_blank">Дарим за видео отзыв!</a>
                                    </div>
                                @endif
                                <div class="show-box-rating">
                                    <p class="p-1">Средняя оценка пользователя:</p>
                                    <div class="box-stars">
                                        @for ($i = 1; $i <= 5; $i++)
                                            <img class="img-1"
                                                 @if ($rating >= $i)
                                                 src="{{Theme::asset('images/product_details/sct-1_star-2.png')}}"
                                                 @else
                                                 src="{{Theme::asset('images/product_details/sct-1_star-1.png')}}"
                                                    @endif
                                            />
                                        @endfor
                                    </div>

                                    <p class="p-2">
                                        ({{$product->comments->where('processed', 1)->where('parent_id', null)->count()}}
                                        отзывов)</p>
                                </div>
                                @if (!$product->discount || !$product->discount->isActive())
                                    <div class="product-price-block @if (!$product->pre_order_date && $product->count == 0) disabled @endif">{{(new Modules\Shop\Utils\Price\Processor)->calculate($product)->actualPrice()}}
                                        <small>грн@if($product->price_for)
                                                /{{trans('shop::product.'.$product->price_for)}}@endif</small>
                                    </div>
                                @else
                                    <div class="product-sale-price-block @if (!$product->pre_order_date && $product->count == 0) disabled @endif">{{(new Modules\Shop\Utils\Price\Processor)->calculate($product)->originPrice()}}
                                        грн@if($product->price_for)
                                            /{{trans('shop::product.'.$product->price_for)}}@endif</div>
                                    <div class="product-price-block  @if (!$product->pre_order_date && $product->count == 0) disabled @endif">{{round((new Modules\Shop\Utils\Price\Processor)->calculate($product)->actualPrice())}}
                                        <small>грн@if($product->price_for)
                                                /{{trans('shop::product.'.$product->price_for)}}@endif</small>
                                    </div>
                                @endif
                                 @if ($product->discount && $product->discount->isActive())
                                    <div class="product-details-action-timer">
                                        <div>До конца акции</div>
                                        <div date-value="{{$product->discount->date_end}}" class="action-timer timer">-
                                            дн
                                            --:--:--
                                        </div>
                                    </div>
                                @endif
                                <div class="buy-block">


                                    @if (!$product->pre_order_date && $product->count == 0)
                                        <button class="hbtn btn-buy-disabled" disabled type="submit">Нет в наличии
                                        </button>
                                    @else
                                        {!! form($form) !!}
                                        <a class="link-one-click-modal" data-toggle="modal"
                                           data-target="#one-click-modal">Заказать в 1 клик</a>
                                    @endif
                                    {{--<a id="buy-credit-link" href="#">В кредит</a>--}}
                                </div>

                                @if ($product->pre_order_date && $product->getPreOrderDiffHours() > 0)
                                    <?php $count = $product->getPreOrderDiffHours() ?>
                                    <br>
                                    <p class="attributes-short-price">Товар прибудет
                                        через {{$count}} {{Lang::choice('день|дня|дней', $count, [], 'ru')}}</p>
                                @endif

                                <p class="attributes-short-desc">Артикул: {{$product->sku}}</p>
                                {{--SOCIAL BTNS--}}
                                @include('rolcar::shop.partials.socials')
                            </div>
                            <div class="clear"></div>
                            <!--PRODUCT DESCRIPTION-->
                            <div class="product-details-description">
                                <h2 class="product-details-description-header">Описание {{ucfirst($product->name)}}</h2>
                                {!! $product->description !!}
                            </div>
                            <!--PRODUCT ATTRIBUTES-->
                            <div>
                                <h2 class="product-details-description-header">Подробные
                                    характеристики {{ucfirst($product->name)}}</h2>
                                <table class="product-characteristics">
                                    <tbody>

                                    @foreach($product->options as $key => $option)
                                        @if ($key <= 15 && $option->attribute->visible && $option->attribute->visible && !$option->attribute->option)
                                            <tr>
                                                <td>{{$option->attribute->name}}</td>
                                                <td>
                                                    @if ($attrName = $option->productOptions->getAttributeValue('name'))
                                                        {{$attrName}}
                                                    @else
                                                        @if (is_array(json_decode($option->value, true)))
                                                            {{implode(',', json_decode($option->value, true))}}
                                                        @else
                                                            {{json_decode($option->value)}}
                                                        @endif
                                                    @endif
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="short-product-price-details">

                            <div class="product-desc-delivery">
                                {!! \Modules\Shop\Entities\Configuration::where('code', 'delivery_text')->first()->value !!}
                            </div>
                            {{-- COMMENTS BLOCK --}}
                            @if ($product->comments->count())
                                <div class="product-desc-comments short-comments">
                                    <h2 class="product-details-description-header">ОТЗЫВЫ ПОКУПАТЕЛЕЙ</h2>
                                    @foreach($product->comments->take(3) as $comment)
                                        @include('rolcar::shop.partials.comment')
                                    @endforeach
                                    <button id="learn-more-comments" class="btn btn-default"><i
                                                class="glyphicon glyphicon-comment"></i>ВСЕ ОТЗЫВЫ
                                    </button>
                                </div>
                            @endif
                            {{--END COMMENTS BLOCK--}}
                        </div>
                    </div>
                </div>
                @if ($product->video && strlen($product->video->youtube_code))
                    <div class="col-md-12 product-details-video-block">
                        @if($product->video->title || $product->video->description )
                            <div class="col-md-12">
                                @if($product->video->title)
                                    <h2 class="product-details-description-header">{{$product->video->title}}</h2>
                                @endif
                                @if($product->video->description)
                                    <p>{{$product->video->description}}</p>
                                @endif
                            </div>
                        @endif
                        <div class="col-md-12">
                            <div class="product-dt-video-container row">
                            </div>
                        </div>
                    </div>
                @endif
            </div>
            {{-- CHARACTERISTIC TABS  --}}
            <div class="tabs" id="tab2">
                <div class="col-md-12 product-details-additional-block">
                    <div class="row">
                        <div class="product-details-block">
                            <!--PRODUCT ATTRIBUTES-->
                            <div>
                                <h2 class="product-details-description-header">Подробные
                                    характеристики {{ucfirst($product->name)}}</h2>
                                <table class="product-characteristics">
                                    <tbody>
                                    @foreach($product->options as $key => $option)
                                        @if ($option->attribute->visible && !$option->attribute->option)
                                            <tr>
                                                <td>{{$option->attribute->name}}</td>
                                                <td>
                                                    @if ($attrName = $option->productOptions->getAttributeValue('name'))
                                                        {{$attrName}}
                                                    @else
                                                        @if (is_array(json_decode($option->value, true)))
                                                            {{implode(',', json_decode($option->value, true))}}
                                                        @else
                                                            {{json_decode($option->value)}}
                                                        @endif
                                                    @endif
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                    </tbody>

                                </table>
                            </div>
                        </div>
                        <div class="short-product-price-details">
                            @include('rolcar::shop.partials.product_short_details')
                        </div>
                    </div>
                </div>
            </div>
            {{-- END CHARACTERISTIC TABS  --}}

            @if ($product->video && $product->video->youtube_code)

                {{-- VIDEO TABS  --}}
                <div class="tabs" id="tab3">
                    <div class="row product-details-additional-block">
                        <div class="col-md-12">
                            <!--PRODUCT VIDEO-->
                            <div>
                                @if($product->video->title)
                                    <div class="col-md-12">
                                        <h2 class="product-details-description-header">{{$product->video->title}}</h2>
                                    </div>
                                @endif
                                <div class="col-md-12 product-details-video-block">
                                    <div class="product-dt-video-desc-container">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- END VIDEO TABS  --}}
            @endif
            {{-- COMMENT TABS  --}}
            <div class="tabs" id="tab4">
                <div class="col-md-12 product-details-additional-block">
                    <div class="row">
                        <div class="product-details-block">
                            {{-- COMMENTS BLOCK --}}
                            @if ($product->comments->count())
                                <div class="product-desc-comments">
                                    <h2 class="product-details-description-header">ОТЗЫВЫ ПОКУПАТЕЛЕЙ</h2>
                                    @foreach($product->comments as $comment)
                                        @include('rolcar::shop.partials.comment')
                                    @endforeach
                                    {{--<a href="#">--}}
                                    {{--<button id="learn-more-comments" class="btn btn-default"><i class="glyphicon glyphicon-comment"></i>ПОКАЗАТЬ ЕЩЕ ОТЗЫВЫ</button>--}}
                                    {{--</a>--}}
                                </div>
                            @endif
                            <br>
                            <div class="product-write-comment">
                                <h2 class="product-details-description-header">ОСТАВИТЬ ОТЗЫВ</h2>
                                <form action="{{route('product.comment.add', ['id' => $product->id])}}" method="POST">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                    <div class="my-rating"></div>
                                    <div class="form-group">
                                        <input required="" type="text" name="comment[name]" class="form-control"
                                               placeholder="Ваше имя"/>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" name="comment[advantages]" class="form-control"
                                               placeholder="Достоинства"/>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" name="comment[disadvantages]" class="form-control"
                                               placeholder="Недостатки"/>
                                    </div>
                                    <div class="form-group comment-upl-block">
                                        <div class="upload-image-block cimage">
                                            <i class="fa fa-file-image-o" aria-hidden="true"></i>
                                            <p>Загрузить фото</p>
                                        </div>
                                        <div id="comment-upload-video" class="upload-image-block">
                                            <i class="fa fa-youtube" aria-hidden="true"></i>
                                            <p>Загрузить видео</p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="comment-img col-md-12"></div>
                                    <div class="form-group">
                                        <textarea required="" name="comment[comment]" id="comment_message"
                                                  class="form-control" rows="5"
                                                  placeholder="Ваш отзыв по товару {{$product->name}}"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <button class="hbtn btn-default" type="submit">Отправить</button>
                                    </div>
                                </form>
                            </div>
                            {{--END COMMENTS BLOCK--}}
                        </div>
                        <div class="short-product-price-details">
                            @include('rolcar::shop.partials.product_short_details')
                        </div>
                    </div>
                </div>
            </div>
            {{-- END COMMNETS TAB  --}}
            @if ($product->crossSales->count())
                <div class="col-md-12 product-details-similar-products">
                    <div class="pdsp-slider-navigation">
                        <i class="fa fa-angle-left" id="cross-sales-left"></i><i id="cross-sales-right"
                                                                                 class="fa fa-angle-right"></i>
                    </div>
                    <h4 class="pdsp-header">С этим товаром покупают</h4>
                    <div style="position: inherit;" class="products-list cross-sales-list">
                        @foreach($product->crossSales->take(20) as $cp)
                            @include('rolcar::shop.partials.product', ['product' => $cp])
                        @endforeach
                    </div>
                </div>
            @endif

            @if($seenProducts)
                <div class="col-md-12 product-details-similar-products">
                    @if ($seenProducts->count() > 4)
                        <div class="pdsp-slider-navigation">
                            <i class="fa fa-angle-left" id="seen-list-left"></i><i id="seen-list-right" ЫВ
                                                                                   class="fa fa-angle-right"></i>
                        </div>
                    @endif
                    <h4 class="pdsp-header">Вы недавно смотрели</h4>
                    <div class="products-list seen-list">
                        @foreach($seenProducts as $sp)
                            @include('rolcar::shop.partials.product', ['product' => $sp])
                        @endforeach
                    </div>
                </div>
            @endif
        </div>
        <div class="lending-block">
        </div>
    </div>
    @include('rolcar::partials.one-click-modal')
    @include('rolcar::shop.partials.shop_comment_modal')
    @include('rolcar::shop.partials.shop_comment_video_modal')
@endsection