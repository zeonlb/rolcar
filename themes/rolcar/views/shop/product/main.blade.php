@extends('rolcar::layouts.master_custom')
@section('content')
    <div class="shop-list-container">
        <div class="breadcrumb-container">
            <div class="container">
                @if ($categoryManager)
                    <ol class="breadcrumb">
                        @foreach($categoryManager->getBreadCrumbs() as $name => $url)
                            @if($url)
                                <li><a href="{{$url}}">{{$name}}</a></li>
                            @else
                                <li class="active">{{$name}}</li>
                            @endif
                        @endforeach
                    </ol>
                @endif
            </div>
        </div>
        <div id="shop-content">
            <div class="container shop-container shop-spec-container">
                @include('rolcar::partials.mobile_catalog_modal')
                {{--END PRODUCTS LIST CONTAINER--}}
                <div class="col-md-12">
                    <div class="product-filter-content col-md-2">
                        <div class="product-filter">
                            @if ($groupedCategories)
                                <div>
                                    <p id="products-main-category-header">Категории</p>
                                </div>
                                @foreach($groupedCategories as $category)
                                    <div class="products-main-child">
                                        <a href="{{route('product.index', [$category['code']])}}"
                                           class="products-main-child-header">{{$category['name']}}:</a>
                                        <div class="category-list">
                                            <ul>
                                                <?php
                                                $children = array_values($category['children']??[]);
                                                $show = count($children) > 5;
                                                ?>
                                                @foreach($children as $key => $child)
                                                    <li @if ($key > 4) class="hidden" @endif>
                                                        <a href="{{route('product.index', [$child['code']])}}">{{$child['name']}}</a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                            @if ($show)
                                                <img class="filter-show-all fshow" alt="показать"
                                                     src="{{Theme::asset('images/filter-show.png')}}"/>
                                                <img class="filter-show-all hidden fhide" alt="скрыть"
                                                     src="{{Theme::asset('images/filter-hide.png')}}"/>
                                            @endif

                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>

                    <!--List product block-->
                    <div class="product-list-block col-md-8">
                        <div class="product-category-content">
                            @if ($pageInfo->meta && $pageInfo->meta->seo_h1)
                                <h1>{{$pageInfo->meta->seo_h1}}</h1>
                            @elseif ($pageInfo->name)
                                <h1>{{$pageInfo->name}}</h1>
                            @else
                                <h1>Каталог товаров:</h1>
                            @endif
                        </div>
                        <div class="products-list col-md-12">
                            @foreach($topCategories as $category)                            
                                @include('rolcar::shop.partials.top_category')
                            @endforeach
                        </div>

                        @if($topProducts)
                            <div class="col-md-12 product-details-similar-products">
                                @if ($topProducts->count() > 4)
                                    <div class="pdsp-slider-navigation">
                                        <i class="fa fa-angle-left" id="cross-sales-left"></i><i id="cross-sales-right"
                                                                                                 class="fa fa-angle-right"></i>
                                    </div>
                                @endif
                                <h4 class="pdsp-header">Последние поступления</h4>
                                <div class="cross-sales-list">
                                    @foreach($topProducts as $product)
                                        @include('rolcar::shop.partials.product')
                                        {{--// GA--}}
                                        <script>
                                            ga('ec:addImpression', {
                                                'id': '{{$product->sku}}',
                                                'name': '{{$product->name}}',
                                                'brand': '{{$product->brand->name}}',
                                                'list': 'Последние поступления',
                                            });
                                        </script>
                                    @endforeach
                                </div>
                            </div>
                            <script type="text/javascript">
				ga('ec:send');
                            </script>
                        @endif

                        @if($seenProducts)
                            <div class="col-md-12 product-details-similar-products">
                                @if ($seenProducts->count() > 4)
                                    <div class="pdsp-slider-navigation">
                                        <i class="fa fa-angle-left" id="seen-list-left"></i><i id="seen-list-right"
                                                                                               class="fa fa-angle-right"></i>
                                    </div>
                                @endif
                                <h4 class="pdsp-header">Вы недавно смотрели</h4>
                                <div class="products-list seen-list">
                                    @foreach($seenProducts as $product)
                                        @include('rolcar::shop.partials.product')
                                    @endforeach
                                </div>
                            </div>
                        @endif
                        @if($comments->count() > 0)
                            <div>
                                <div class="comment-list">
                                    <h4 class="pdsp-header">Отзывы пользователей</h4>
                                    @foreach($comments as $key => $comment)
                                        @include('rolcar::shop.partials.one_comment')
                                        {{--// GA--}}
                                        <script>
                                            ga('ec:addImpression', {
                                                'id': '{{$comment->product->sku}}',
                                                'name': '{{$comment->product->name}}',
                                                'brand': '{{$comment->product->brand->name}}',
                                                'list': 'Comments block',
                                            });
                                        </script>
                                    @endforeach
                                </div>
                                <script type="text/javascript">
				    ga('ec:send');
                                </script>
                                @if ($totalComments > 3)
                                    <div class="col-md-12 comment-more-block">
                                        <a href="#">
                                            <button class="hbtn btn-comment-more">Показать еще отзывы <i
                                                        class="fa fa-angle-down"></i></button>
                                        </a>
                                    </div>
                                @endif
                            </div>
                        @endif
                        <div class="mobile-filter-btn">
                            <button type="button" class="btn btn-default" aria-label="Left Align">
                                <span class="glyphicon glyphicon-filter" aria-hidden="true"></span> Категории
                            </button>
                        </div>
                    </div>
                    <div class="advance-block col-md-2">
                        {{--<div class="advance-header">--}}
                        {{--<h6>Советы от rolcar</h6>--}}
                        {{--<img src="{{Theme::asset('images/advice-header-img.png')}}">--}}
                        {{--</div>--}}
                        {{--<div class="advance-content">--}}
                        {{--<div class="advance-text">--}}
                        {{--<p>Lorem Ipsum - это текст-"рыба", <a href="#">часто используемый</a> в печати и--}}
                        {{--вэб-дизайне. Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала--}}
                        {{--XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм--}}
                        {{--шрифтов.</p>--}}
                        {{--</div>--}}
                        {{--<div class="advance-more-details">--}}
                        {{--<a href="#">Все советы <span class="glyphicon glyphicon-chevron-right"></span></a>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="clear"></div>--}}
                        {{--</div>--}}
                        @if ($banners)
                            @foreach($banners as $banner)
                                <div class="adv-b">
                                    <a href="{{$banner->url}}"><img src="/{{$banner->img}}" alt="{{$banner->title}}"
                                                                    title="{{$banner->title}}"></a>

                                </div>
                            @endforeach
                        @endif
                    </div>

                    {{--END PRODUCTS LIST CONTAINER--}}
                </div>
                <div class="clear"></div>
                @if ($articles->count() > 0)
                    <div class="top-blog-posts col-md-12">
                        <h2>Последние публикации</h2>
                        @foreach($articles as $article)
                            <div class="col-xs-12 col-sm-6 col-md-3">
                                <div class="blog-post">
                                    <a href="{{route('blog.view', $article->code)}}">
                                        <div class="blog-img-block">
                                            @if ($article->img)
                                                <img width="210" class="product-thumb"
                                                     src="{{ \Modules\Cms\Core\Images\ThumbBuilder::resize($article->img , 210, 'auto') }}"
                                                     alt="{{$article->title}}"/>
                                            @endif
                                        </div>
                                    </a>
                                    <div class="post-date">
                                        <p>{{ Carbon\Carbon::parse($article->created_at)->format('d-m-Y') }}</p>
                                    </div>
                                    <div><h3><a href="{{route('blog.view', $article->code)}}">{{$article->title}}</a></h3>
                                    </div>
                                    <div class="text-left">
                                        <a class="btn btn-gold" href="{{route('blog.view', $article->code)}}">
                                            Подробнее
                                        </a>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection


@section('javascript')
    <script type="text/javascript" src="{{Theme::asset('js/jquery.jtruncate.min.js')}}"></script>
    <script type="text/javascript" src="{{Module::asset('shop:js/front/product.js')}}"></script>
    <script type="text/javascript" src="{{Module::asset('shop:js/front/product_filter.js')}}"></script>
    <script type="text/javascript" src="{{Module::asset('shop:js/front/discount.js')}}"></script>
    <script src='{{Theme::asset('js/product-details.js')}}'></script>
    <script type="text/javascript">
        $.filterUrlGenerator.init('/', JSON.parse('{}'));
    </script>
@endsection
@section('style')
    <link rel="stylesheet" href="{{Theme::asset('css/blog.min.css')}}">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.min.css">
    @if ($icons->count() > 0)
        <style>
            @foreach($icons as $icon)
                .product-icon-{{$icon->id}} {
                top: {{$icon->top}}px!important;
                bottom: {{$icon->bottom}}px!important;
                @if ($icon->left != 0)
left: {{$icon->left}}px!important;
                @endif
                @if ($icon->right != 0)
right: {{$icon->right}}px!important;
                @endif
                @if ($icon->right == 0 && $icon->left == 0)
right: {{$icon->right}}px!important;
            @endif
        }
            @endforeach
        </style>
    @endif
@endsection
