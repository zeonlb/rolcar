@extends('rolcar::layouts.master_custom')

@section('content')
    <?php $path = explode('/', Request::path()) ?>
    <div class="shop-list-container">
        <div class="breadcrumb-container">
            <div class="container">
                @if ($categoryManager)
                    <ol class="breadcrumb">
                        @foreach($categoryManager->getBreadCrumbs() as $name => $url)
                            @if($url)
                                <li><a href="{{$url}}">{{$name}}</a></li>
                            @else
                                <li class="active">{{$name}}</li>
                            @endif
                        @endforeach
                    </ol>
                @endif
            </div>
        </div>
        <div id="shop-content">
            <div class="container shop-container shop-spec-container container_full_width">
                @include('rolcar::partials.mobile_filter_modal')
                {{--END PRODUCTS LIST CONTAINER--}}

                @inject('topComments', 'Modules\Shop\Http\Controllers\Front\CommentController')
                {!! $topComments::topComments() !!}

                <div class="col-md-12 box_main">
                    <div class="product-filter-content  col-md-2">

                        <div class="product-filter">
                            <div class="product-filter-header-block">
                                <h4>Фильтр товара</h4>
                            </div>
                            @if($filterObjects)
                                <div class="product-filter-block">
                                    <div class="header-h5">Выбранные фильтры:</div>
                                    <div class="chosen-filters">
                                        @foreach($filterObjects as  $filter)
                                            <div class="chosen-filter-block">
                                                <p>{{$filter['attribute']->name}}:</p>
                                                @foreach($filter['options'] as $option)
                                                    <button class="btn btn-primary" oCode="{{$option->code}}"
                                                            type="button">
                                                        {{$option->name}} <span class="close-filter-btn"
                                                                                oCode="{{$option->code}}"
                                                                                aria-hidden="true">×</span>
                                                    </button>
                                                @endforeach
                                            </div>
                                        @endforeach
                                        <br>
                                        <a class="chosen-filters-reset-all">Сбросить все фильтры</a>
                                    </div>
                                    <br>
                                </div>
                            @endif
                            @if (count($brands) > 0)
                                <div class="product-filter-block">
                                    <div class="header-h5">Бренд:</div>
                                    <ul>
                                        @foreach($brands as $brand)
                                            <li>
                                                <div class="checkbox">
                                                    <input id="d-brand-{{$brand->code}}" data-filter="brand"
                                                           type="checkbox"
                                                           name="brands[{{$brand->code}}]" value="{{$brand->code}}">
                                                    <label for="d-brand-{{$brand->code}}" class="checkbox-label"><a
                                                                href="#">{{$brand->name}}</a></label>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            @if($actualMin !== null && $actualMax !== null && $actualMin !== $actualMax)
                                <div class="product-filter-block filter-price-range">
                                    <div class="header-h5">Цена:</div>
                                    <div class="product-filter-inputs">
                                        <input type="text" name="pf" class="sliderValue p-from" data-index="0"
                                               value="{{$actualMin}}"/> -
                                        <input type="text" name="pt" class="sliderValue p-to" data-index="1"
                                               value="{{$actualMax}}"/>
                                        <span>грн.</span>
                                    </div>

                                    <div id="slider-range"></div>
                                </div>
                            @endif
                            @foreach($filters as $filter)
                                <div class="product-filter-block">
                                    <div class="header-h5">{{$filter->name}}:</div>
                                    <?php $show = true; $count = 0; ?>
                                    <div class="product-filter-list">
                                        <ul>
                                            @foreach($filter->options->sortBy('name') as $key => $option)
                                                @if (isset($aOptions[$filter->id]) &&
                                                ((is_array($aOptions[$filter->id]) && in_array($option->id, $aOptions[$filter->id]))
                                                 || (!is_array($aOptions[$filter->id]) &&$aOptions[$filter->id] == $option->id)))
                                                    <?php
                                                    $count++;
                                                    if ($count > 8) {
                                                        $show = false;
                                                    }
                                                    ?>
                                                    <li @if (!$show) class="hidden" @endif>
                                                        <div class="checkbox">
                                                            <input id="d-{{$filter->code}}-{{$option->code}}"
                                                                   data-filter="{{$filter->code}}" type="checkbox"
                                                                   name="attribute[{{$filter->code}}][]"
                                                                   value="{{$option->code}}">
                                                            <label for="d-{{$filter->code}}-{{$option->code}}"
                                                                   class="checkbox-label"><a
                                                                        href="#">{{$option->name}}</a></label>
                                                        </div>
                                                    </li>
                                                @endif
                                            @endforeach
                                        </ul>
                                        @if (!$show)
                                            <img class="filter-show-all fshow" alt="показать фильтр"
                                                 src="{{Theme::asset('images/filter-show.png')}}"/>
                                            <img class="filter-show-all hidden fhide" alt="скрыть фильтр"
                                                 src="{{Theme::asset('images/filter-hide.png')}}"/>
                                        @endif
                                    </div>

                                </div>
                            @endforeach

                        </div>
                    </div>
                    <!--List product block-->
                    <div class="product-main-block  col-md-10">
                        <div class="mobile-filter-btn">
                            <button type="button" class="btn btn-default" aria-label="Left Align">
                                <span class="glyphicon glyphicon-filter" aria-hidden="true"></span> Фильтр
                            </button>
                        </div>
                        <div class="product-category-content">
                            @if ($pageInfo->meta && $pageInfo->meta->seo_h1)
                                <h1>{{$pageInfo->meta->seo_h1}}</h1>
                            @elseif ($pageInfo->name)
                                <h1>{{$pageInfo->name}}</h1>
                            @else
                                <h1>Список товаров:</h1>
                            @endif
                            {{--@if ($categoryManager && $categoryManager->getFirstSeoText())--}}
                            {{--<div class="product-category-content-desc">--}}
                            {{--<p>{!! $categoryManager->getFirstSeoText() !!}</p>--}}
                            {{--</div>--}}
                            {{--@endif--}}
                        </div>
                        @if ( $path[1] == 'avtolampu' || $path[1] == 'dop-led-fary' || $path[1] == 'oborydovanie')
                            <div class="box_link_lamps">
                                <a target="_blank" href="https://rolcar.com.ua/podobrat-lampu-po-avto"
                                   title="Купить Ксеноновые лампы BREVIA H4+50% 5500K 12450MP" class="hbtn btn-buy">Подобрать
                                    лампы по авто</a>
                            </div>
                        @endif
                        @if ($products->count() > 0)
                            <div class="clear"></div>
                            <div class="products-list main-list col-md-12">
                                <?php $widthImg = 230; ?>
                                @foreach($products as $product)
                                    @include('rolcar::shop.partials.product')
                                @endforeach
                            </div>
                            <div class="clear"></div>
                            <div class="shop-pagination-block col-md-12">
                                {!! $products->appends(['filter' => \Illuminate\Support\Facades\Input::get('filter')])->render() !!}
                            </div>

                        @else
                            <div class="product-list-block">
                                <div class="product-category-content">
                                    <br/>
                                    <br/>
                                    <br/>
                                    <br/>
                                    <br/>
                                    <h2>Товара по вашему запросу не найдено</h2>
                                    <br/><br/><br/><br/>
                                </div>
                            </div>
                        @endif

                        @if($seenProducts)
                            <div class="col-md-12 product-details-similar-products">
                                @if ($seenProducts->count() > 4)
                                    <div class="pdsp-slider-navigation">
                                        <i class="fa fa-angle-left" id="seen-list-left"></i><i id="seen-list-right"
                                                                                               class="fa fa-angle-right"></i>
                                    </div>
                                @endif
                                <h4 class="pdsp-header">Вы недавно смотрели</h4>
                                <div class="products-list seen-list">
                                    <?php $widthImg = 183; ?>
                                    @foreach($seenProducts as $product)
                                        @include('rolcar::shop.partials.product')
                                    @endforeach
                                </div>
                            </div>
                        @endif

                        {{--<div class="product-category-content">--}}
                        {{--@if ($categoryManager && $categoryManager->getSecondSeoText())--}}
                        {{--<div class="product-category-content-desc">--}}
                        {{--<p>{!! $categoryManager->getSecondSeoText() !!}</p>--}}
                        {{--</div>--}}
                        {{--@endif--}}
                        {{--</div>--}}
                    </div>
                </div>

                {{--END PRODUCTS LIST CONTAINER--}}

            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script type="text/javascript" src="{{Theme::asset('js/jquery.jtruncate.js')}}"></script>
    <script type="text/javascript" src="{{Module::asset('shop:js/front/product.js')}}"></script>
    <script type="text/javascript" src="{{Module::asset('shop:js/front/product_filter.js')}}"></script>
    <script src='{{Theme::asset('js/vendor/jquery.magnific-popup.min.js')}}'></script>
    <script src='{{Theme::asset('js/product-details.js')}}'></script>
    <script type="text/javascript" src="{{Module::asset('shop:js/front/discount.js')}}"></script>
    <script type="text/javascript">
        $.filterUrlGenerator.init('{{$routeUrl}}', JSON.parse('{!! json_encode($filterData) !!}'));
    </script>
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>

        $(function () {
            $("#slider-range, #mob-slider-range").slider({
                range: true,
                min: {{$minPrice}},
                max: {{$maxPrice}},
                values: [ {{$actualMin}}, {{$actualMax}} ],
                slide: function (event, ui) {
                    for (var i = 0; i < ui.values.length; ++i) {
                        $("input.sliderValue[data-index=" + i + "]").val(ui.values[i]);
                    }
                },
                change: function (event, ui) {
                    var p_from = $(this).parent().parent().find('.p-from');
                    var p_to = $(this).parent().parent().find('.p-to');
                    $.filterUrlGenerator.pushKeyValueToArray($.filterUrlGenerator.filterValue, p_from.attr('name'), p_from.val(), true);
                    $.filterUrlGenerator.pushKeyValueToArray($.filterUrlGenerator.filterValue, p_to.attr('name'), p_to.val(), true);
                    $.filterUrlGenerator.submitForm();
                }
            });
            $("input.sliderValue").change(function () {
                var $this = $(this);
                $("#slider").slider("values", $this.data("index"), $this.val());
            });
        });
    </script>

    {{--// GA--}}
    <script>
        $(function () {
            @if (@$products)
                window.dataLayer = window.dataLayer || [];
            dataLayer.push({
                'ecommerce': {
                    'currencyCode': 'UAH',
                    'impressions': [
                            @foreach($products as $key => $product)
                        {
                            'name': '{{$product->name}}',
                            'id': '{{$product->sku}}',
                            'price': '{{$product->price}}',
                            'brand': '{{$product->brand->name}}',
                            'category': '{{$categoryManager->category->name}}',
                            'list': 'Main catalog',
                            'position': '{{$key + 1}}'
                        },
                        @endforeach
                    ]
                },
                'event': 'gtm-ee-event',
                'gtm-ee-event-category': 'Enhanced Ecommerce',
                'gtm-ee-event-action': 'Product Impressions',
                'gtm-ee-event-non-interaction': 'True',
            });

            window.dataLayer = window.dataLayer || [];
            dataLayer.push({
                'ecommerce': {
                    'currencyCode': 'UAH',
                    'click': {
                        'actionField': {'list': 'Main catalog'},
                        'products': [
                                @foreach($products as $key => $product)
                            {
                                'name': '{{$product->name}}',
                                'id': '{{$product->sku}}',
                                'price': '{{$product->price}}',
                                'brand': '{{$product->brand->name}}',
                                'category': '{{$categoryManager->category->name}}',
                                'position': '{{$key + 1}}'
                            },
                            @endforeach
                        ]
                    }
                },
                'event': 'gtm-ee-event',
                'gtm-ee-event-category': 'Enhanced Ecommerce',
                'gtm-ee-event-action': 'Product Clicks',
                'gtm-ee-event-non-interaction': 'False',
            });

            @endif
        });
    </script>

@endsection
@section('style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.min.css">
    @if ($icons->count() > 0)
        <style>
            @foreach($icons as $icon)
                .product-icon-{{$icon->id}}  {
                top: {{$icon->top}}px !important;
                bottom: {{$icon->bottom}}px !important;
                @if ($icon->left != 0)
 left: {{$icon->left}}px !important;
                @endif
                @if ($icon->right != 0)
 right: {{$icon->right}}px !important;
                @endif
                @if ($icon->right == 0 && $icon->left == 0)
 right: {{$icon->right}}px !important;
            @endif

            }
            @endforeach
        </style>
    @endif
@endsection

@section('headers')
    @if($products->previousPageUrl())
        <link rel="prev" href="{{strtr($products->previousPageUrl(), ['?page=1' => ''])}}">
    @endif
    @if($products->nextPageUrl())
        <link rel="next" href="{{$products->nextPageUrl()}}">
    @endif
@endsection
