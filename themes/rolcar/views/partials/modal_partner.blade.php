<div id="myModal-rolcar" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h4 class="modal-title">Чтобы стать партнером, заполните заявку ниже</h4>
		  </div>
		  <div class="modal-body">
			<form action="/api/v1/form-request">
				<input type="hidden" name="source" tabindex="-1" value="rolcar">
				<input type="hidden" name="form_name" tabindex="-1" value="Стать партнером">
				<input type="hidden" name="pages_type" tabindex="-1" value="Главная">
				<div class="form-group">
				<label>Ваше Имя
					<input type="text" name="Name" class="form-control"></label>
				</div>
				<div class="form-group">
				<label>Телефон, в формате: 098 000 00 00
					<input type="tel" required name="Phone" class="form-control phone" id="idPhone"></label>
				</div>
				<div class="form-group">
				  <label>Ваш Email 
					<input type="tel" name="Email" class="form-control" id="idEmail" required>
				  </label>
				</div>
				<button type="submit" class="btn btn-default">СТАТЬ ПАРНЁРОМ</button>
			</form>
		  </div>
		</div>
	</div>
</div>
