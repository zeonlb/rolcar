<div id="mobile-catalog-modal" class="modal-wrapper">
    <div class="modal-dialog mobile-filter-modal-dialog">
        <div class="modal-body">
            <div class="modal-header">
                <div>Категории</div>
                <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-content">
                <div class="filter-modal-content">
                    <div class="product-mobile-filter">
                        @if ($groupedCategories)
                            @foreach($groupedCategories as $category)
                                <div class="products-main-child">
                                    <a href="{{route('product.index', [$category['code']])}}"
                                       class="products-main-child-header">{{$category['name']}}:</a>
                                    <div class="category-list">
                                        <ul>
                                            <?php
                                            $children = array_values($category['children']??[]);
                                            $show = count($children) > 5;
                                            ?>
                                            @foreach($children as $key => $child)
                                                <li @if ($key > 4) class="hidden" @endif>
                                                    <a href="{{route('product.index', [$child['code']])}}">{{$child['name']}}</a>
                                                </li>
                                            @endforeach
                                        </ul>
                                        @if ($show)
                                            <img class="filter-show-all" id="fshow"
                                                 src="{{Theme::asset('images/filter-show.png')}}"/>
                                            <img class="filter-show-all hidden" id="fhide"
                                                 src="{{Theme::asset('images/filter-hide.png')}}"/>
                                        @endif

                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>