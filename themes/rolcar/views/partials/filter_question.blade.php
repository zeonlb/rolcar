<div  tabindex="-1" role="dialog" id="filter-question-modal" class="modal fade modal-wrapper modal-wrapper-yellow">
    <div class="modal-dialog filter-question-modal-dialog">
        <div class="modal-body filter-modal-body">
            <div class="modal-header">
                <div>Выберите специальную технологию</div>
                <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="filter-question-text"><p>Данный тип лампы в Вашем автомобиле имеет несколько модификаций. Чтобы подобрать правильную лампу на Ваш автомобиль, выберите свою модификацию.</p></div>
            <div class="modal-content modal-rc-content">
                <div>
                    <div class="filter-question-body">
                        <div id="questions" class="select-wrapper wrapper-dropdown-medium dropdown-yellow">
                            <div class="chosen-value">
                                <span>Выберите тип лампы</span>
                                <img alt="left" class="select-arrow" src="{{Theme::asset('images/select-arrow-yellow-btn.png')}}"/>
                            </div>

                            <ul id="filter-question-modal-select" class="dropdown" data-source="questions" >
                            </ul>
                        </div>
                        <div class="filter-question-block">
                            <h3>Не знаете свою технологию?</h3>
                            <img class="filter-question-show-contact-form" src="{{Theme::asset('images/filter-question-addcontact-arrow.png')}}" alt="add contact">
                        </div>
                    </div>
                </div>
            </div>
            <div class="filter-form-add-phone">
                <p>Введите свой контактный номер и наши менеджеры помогут вам с выбором</p>
                <form class="form-inline">
                    <div class="form-group">
                        <input class="form-control" id="filter-contact" placeholder="Введите контактный номер">
                    </div>
                    <button type="button" class="btn filter-contact-btn btn-gold">OK</button>
                </form>
            </div>
        </div>
    </div>
</div>