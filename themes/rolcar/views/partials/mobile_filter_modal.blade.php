<div id="mobile-filter-modal" class="modal-wrapper">
    <div class="modal-dialog mobile-filter-modal-dialog">
        <div class="modal-body">
            <div class="modal-header">
                <div>Фильтр</div>
                <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">×</span></button>
            </div>
            <div class="modal-content">
                <div class="filter-modal-content">
                    <div class="product-mobile-filter">
                        @if($filterObjects)
                            <div class="product-filter-block">
                                <h5>Выбранные фильтры:</h5>
                                <div class="chosen-filters">
                                    @foreach($filterObjects as  $filter)
                                        <div class="chosen-filter-block">
                                            <p>{{$filter['attribute']->name}}:</p>
                                            @foreach($filter['options'] as $option)
                                                <button class="btn btn-primary" oCode="{{$option->code}}" type="button">
                                                    {{$option->name}} <span class="close-filter-btn"
                                                                            oCode="{{$option->code}}"
                                                                            aria-hidden="true">×</span>
                                                </button>
                                            @endforeach
                                        </div>
                                    @endforeach
                                    <br>
                                    <a class="chosen-filters-reset-all">Сбросить все фильтры</a>
                                </div>
                                <br>
                            </div>
                        @endif
                        @if (count($brands) > 0)
                            <div class="product-filter-block">
                                <div class="header-h5">Бренд:</div>
                                <ul>
                                    @foreach($brands as $brand)
                                        <li>
                                            <div class="checkbox">
                                                <input id="m-brand-{{$brand->code}}" data-filter="brand" type="checkbox"
                                                       name="brands[{{$brand->code}}]" value="{{$brand->code}}">
                                                <label for="m-brand-{{$brand->code}}"  class="checkbox-label"><a
                                                            href="">{{$brand->name}}</a></label>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if($actualMin !== null && $actualMax !== null && $actualMin !== $actualMax)
                            <div class="product-filter-block filter-price-range">
                                <div class="header-h5">Цена:</div>
                                <div class="product-filter-inputs">
                                    <input type="text" name="pf" class="sliderValue p-from" data-index="0"
                                           value="{{$actualMin}}"/> -
                                    <input type="text" name="pt" class="sliderValue p-to" data-index="1"
                                           value="{{$actualMax}}"/>
                                    <span>грн.</span>
                                </div>

                                <div id="mob-slider-range"></div>
                            </div>
                        @endif
                        @foreach($filters as $filter)
                            <div class="product-filter-block">
                                <div class="header-h5">{{$filter->name}}:</div>
                                <?php $show = true; $count = 0; ?>
                                <div class="product-filter-list">
                                    <ul>
                                        @foreach($filter->options->sortBy('name') as $key => $option)
                                            @if (isset($aOptions[$filter->id]) &&
                                            ((is_array($aOptions[$filter->id]) && in_array($option->id, $aOptions[$filter->id]))
                                             || (!is_array($aOptions[$filter->id]) &&$aOptions[$filter->id] == $option->id)))
                                                <?php
                                                $count++;
                                                if ($count > 5) {
                                                    $show = false;
                                                }
                                                ?>
                                                <li @if (!$show) class="hidden" @endif>
                                                    <div class="checkbox">
                                                        <input id="m-{{$filter->code}}-{{$option->code}}" data-filter="{{$filter->code}}" type="checkbox"
                                                               name="attribute[{{$filter->code}}][]"
                                                               value="{{$option->code}}">
                                                        <label for="m-{{$filter->code}}-{{$option->code}}" class="checkbox-label"><a
                                                                    href="">{{$option->name}}</a></label>
                                                    </div>
                                                </li>
                                            @endif
                                        @endforeach
                                    </ul>
                                    @if (!$show)
                                        <img class="filter-show-all fshow" alt="показать фильтр"
                                             src="{{Theme::asset('images/filter-show.png')}}"/>
                                        <img class="filter-show-all hidden fhide" alt="скрыть фильтр"
                                             src="{{Theme::asset('images/filter-hide.png')}}"/> @endif
                                </div>

                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>