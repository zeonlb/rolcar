@if ($category->parent_id == 0)
    <?php $key= 0 ?>
@endif
@if ((count($category->children) > 0) AND ($category->parent_id > 0))
    
    <li> 
        <span class="menu_open-{{$key}}"></span>
        <a href="{{$category->url}}" class="link-1"> 
            {{$category->alias}}
        </a>

@else

    <li>
        <a href="{{$category->url}}" class="link-1"> 
            {{$category->alias}}
        </a>
@endif

@if (count($category->children) > 0)   
    <?php $key++; ?>
    <div class="submenu-{{$key}}">
        <ul  class="ul_submenu-{{$key}}">
            
            @foreach($category->children as $category)
                @include('partials.navigation.categories', ['category'=>$category, 'key' => $key ] )
            @endforeach
         </ul>
     </div>
@endif

</li>