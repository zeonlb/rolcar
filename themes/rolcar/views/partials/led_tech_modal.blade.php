<div id="led-tech-info-modal" class="modal-wrapper">
    <div class="modal-dialog led-tech-info-modal-dialog">
        <div class="modal-body">
            <div class="modal-header">
                <div>Специальная технология</div>
                <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-content modal-rc-content">
                <div class="led-tech-modal-text">
                    <p>
                        Этот тип лампы на вашем автомобиле работает по специальной светодиодной LED технологии. К сожалению, у нас сейчас нет лампочки на данный тип света.
                        Но мы делаем все возможное, чтобы это исправить.
                    </p>
                </div>
                <button class="btn btn-gold led-tech-ask" type="submit">СООБЩИТЬ, КОГДА ПОЯВИТСЯ</button>
            </div>
        </div>
    </div>
</div>