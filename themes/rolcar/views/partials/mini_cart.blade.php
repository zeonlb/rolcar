<li id="mini-cart-block"><a href="#"><button class="hbtn btn-gold ">Корзина <span class="fa fa-shopping-cart"></span></button></a>
    <div class="mini-cart">
        <?php
        use Modules\Shop\Utils\Checkout\OrderCalculatorFacade;$count = 0;
            if (Session::get('shop.cart'))
            foreach(Session::get('shop.cart')  as $cart) {
                $count += $cart['count'];
            }
            $orderCalcFacade = new OrderCalculatorFacade();
            $orderCalculator = $orderCalcFacade->getOrderCalculation();
        ?>
        <p>В корзине <b>{{$count}}</b> {{ Lang::choice('товар|товара|товаров', $count, [], 'ru')}}</p>
        <p>на сумму <b>{{$orderCalculator->getTotalSum()}}</b> грн.</p>
        <a href="{{route('shop.checkout.list')}}">
            <button id="mini-cart-btn" class="hbtn btn-gold" onclick="ga('send', 'pageview', '/oformit_zakaz'); return true;">Оформить заказ</button>
        </a>
    </div>
</li>