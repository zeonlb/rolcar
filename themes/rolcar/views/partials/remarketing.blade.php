<!-- Global site tag (gtag.js) - Google Ads: xxxxxxxxx -->
<script async src="https://www.googletagmanager.com/gtag/js"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'AW-xxxxxxxxx');
</script>

<script type="text/javascript" data-no-defer>
	@if (  end($path) == "" )
    gtag('event', 'page_view', {
        'send_to': 'AW-xxxxxxxxx',
        'ecomm_pagetype': 'home',
        'ecomm_totalvalue': 0,
    });
    @elseif ( isset($products) &&  end($path) == 'search' )
            gtag('event', 'page_view', {
                'send_to': 'AW-xxxxxxxxx',
                'ecomm_prodid': ['{!! $products->implode('sku', "','")  !!}'],
                'ecomm_pagetype': 'searchresults',
                'ecomm_totalvalue': 0
            });
	@elseif ( isset($cartsCollection) && end($path) == 'cart' )
    gtag('event', 'page_view', {
        'ecomm_prodid': ['{!! implode("','", array_map(function($item) { return $item['product']->sku; }, $cartsCollection))  !!}'],
        'ecomm_pagetype': 'cart',
        'ecomm_totalvalue': {{$orderCalculator->getTotalSum()}}
    });
    @elseif (  isset($product)  && $path[0] == 'product')

    gtag('event', 'page_view', {
        'send_to': 'AW-xxxxxxxxx',
        'ecomm_prodid': '{{$product->sku}}',
        'ecomm_pagetype': 'product',
        'ecomm_totalvalue': {{(new Modules\Shop\Utils\Price\Processor)->calculate($product)->actualPrice()}}
    });
	@elseif ( isset($skus) &&  end($path) == 'create' )
        gtag('event', 'page_view', {
            'send_to': 'AW-xxxxxxxxx',
            'ecomm_pagetype': 'purchase',
            'ecomm_prodid': ['{!! implode("','", $skus)  !!}'],
            'ecomm_totalvalue': {{$order->total_price}}
        });


	@else
		@if ( isset($products) )
			<?php
				$categoryName = $categoryManager->category->name ?? 'other';
			?>
			@if ( isset($product->sku) )
                gtag('event', 'page_view', {
                    'send_to': 'AW-xxxxxxxxx',
                    'ecomm_pagetype': 'catalog',
                    'ecomm_prodid': ['{!! $products->implode('sku', "','")  !!}'],
                    'ecomm_totalvalue':  0
                });
			@endif
		@else
			var google_tag_params = {
			'ecomm_pagetype': 'other'
			};
		@endif
	@endif

</script>

<div class="text-center upper-header-banner"><a  target="_blank" href="/covid">Мы работаем в штатном режиме! Подробнее...</a></div>