@if ($errors->any())
    <div class="alert alert-danger" role="alert">
        <span class="sr-only"> Error:</span>
        @foreach($errors->all() as $field => $error)
            <div>{{$error}}</div>
        @endforeach
    </div>
@endif
@if (session('success'))
    <script type="text/javascript">
        // $("#one-click-modal-success").addClass("active");
        $('document').ready(function () {
            var stack_bar_top = {"dir1": "down", "dir2": "right", "push": "top", "spacing1": 0, "spacing2": 0};
            var type = "success";
            new PNotify({
                title: "",
                text: "{{session('success')}}",
                addclass: "stack-bar-top",
                cornerclass: "",
                width: "100%",
                delay: 6000,
                type: type,
                stack: stack_bar_top
            })
        });
    </script>
@endif
@if (session('show_cart'))
    <script>
	$('document').ready(function () {
	    ShopCart.init()
	});
    </script>
@endif

@if (session('ga_success_ordered_script'))
    <script type="text/javascript">
        $('document').ready(function () {
            {!! Session::pull('ga_success_ordered_script', '') !!}
        });
    </script>
@endif


@if (Session::has('message'))
   <div class="alert alert-success" role="alert">
       {{ Session::get('message') }}
   </div>
@endif

@if (Session::has('one_click_ga'))
    <script type="text/javascript">
	$('document').ready(function () {
	    ga('send', 'event', '1Click', 'Send');
	});
    </script>
@endif
@if (Session::has('landing_one_click'))
    <script type="text/javascript">
	$('document').ready(function () {
	    ga('send', 'event', '1Click_Lend', 'Send_lend');
        fbq('track', 'Lead');
	});
    </script>
@endif

@if (Session::has('partner_ga'))
    <script type="text/javascript">
	$('document').ready(function () {
	    ga('send', 'event', 'Partner', 'Send');
        fbq('track', 'Lead');
	});
    </script>
@endif
@if (Session::has('recall_ga'))
    <script type="text/javascript">
    $('document').ready(function () {
        ga('send', 'event', 'Zvonok', 'Send'); 
        fbq('track', 'Lead');
    });
    </script>
@endif
