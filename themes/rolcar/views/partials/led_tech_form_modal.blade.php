<div id="led-tech-form-modal" class="modal-wrapper">
    <div class="modal-dialog led-tech-form-modal-dialog">
        <div class="modal-body">
            <div class="modal-header">
                <div>Введите ваш E-mail</div>
                <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-content modal-rc-content">
                <div class="led-tech-modal-text">
                    <p>
                        Введите свой адрес электронной почты и мы вас оповестим, когда данный тип лампы появится в наличии.
                    </p>
                </div>
                <form action="{{route('cms.contact_request.create')}}" method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="text" required class="form-control" name="email" placeholder="Введите ваш email:"/>
                    <input  id="led-tech-form-name" type="hidden"  class="form-control" name="name" value="LED"/>
                    <input  id="led-tech-form-message"  type="hidden"  class="form-control" name="message" value="LED технология"/>
                    <br />
                    <br />
                    <div class="recall-btn-block">
                        <button type="submit" class="btn btn-gold">СОХРАНИТЬ</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>