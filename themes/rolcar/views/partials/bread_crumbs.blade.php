@if (isset($breadCrumbs) && !empty($breadCrumbs))

    <div id="catalog-breadcrumb" class="breadcrumb-container">
        <div class="container">
            <div class="row">
                <ol class="breadcrumb">
                    <?php  $i = 1; ?>
                    @foreach($breadCrumbs as $name => $url)
                        @if ($i != count($breadCrumbs))
                            <li><a href="{{$url}}">{{$name}}</a></li>
                        @else
                            <li class="active">{{$name}}</li>
                        @endif
                        <?php $i++; ?>
                    @endforeach
                </ol>
            </div>
        </div>
    </div>

@endif