<div id="recall-modal" class="modal-wrapper modal fade" role="dialog">
    <div class="modal-dialog recall-modal-dialog">
        <div class="modal-body">
            <div class="modal-header">
                <button type="button" class="close-modal btn-default" data-dismiss="modal">Закрыть</button>
                <img class="modal_phone" src="/themes/rolcar/images/phone_modal.png" alt="phone_modal">
                <div class="header-h5">Введите Ваш номер телефона </div>
            </div>
            <div class="modal-content modal-rc-content">
                <form class="recall-request-form" action="/api/v1/form-request" method="post">
                    <input type="hidden" name="source" tabindex="-1" value="rolcar">
                    <input type="hidden" name="form_name" tabindex="-1" value="Перезвони мне">
                    <input type="hidden" name="pages_type" tabindex="-1" value="Главная">
                    <input type="text" required="" class="phone form-control" name="Phone" placeholder="+38 (000) 000-00-00">
                    <div class="recall-btn-block">
                        <button type="submit" class="btn btn-gold button_submit">Перезвоните мне</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>