@if ( isset($product) )
<div id="one-click-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">Введите Ваш номер телефона и мы перезвоним Вам</h4>
            </div>
            <div class="modal-body">
                <form id="one-click-order" action="{{route('shop.order.oneclick')}}" method="POST">
                    <div class="form-group">
                        <label>Введите Ваш телефон</label>
                        <input type="hidden" name="product_id" value="{{$product->id}}">
                        <input type="hidden" name="romax_id" value="{{$product->romax_id}}">
                        <input type="hidden" name="page" value="rolcar lending ultra vision">
                        <input type="text" name="phone" class="form-control phone" placeholder="066 999 99 99" maxlength="14" required>
                    </div>
                    <button type="submit" onclick="fbq('track', 'Lead');" class="btn btn-default" >Перезвонить мне</button>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="one-click-modal-success" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Спасибо за вашу заявку на звонок!</h4>
                <p>В ближайшее время с вами свяжется менеджер!</p>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
@endif
