<?php
$navigationGenerator = new \Modules\Cms\System\Navigation\NavigationGenerator();
$navigationGenerator->generate();
?>
@extends('rolcar::layouts.master_custom')

@section('content')
    <div class="not-found-content">
        <div class="container">
            <div class="nf-container">
                <img src="{{Theme::asset('images/404.png')}}" alt="Страница не найдена">
                <h1>Страница которую вы искали не найдена</h1>
                <p id="nf-header-add-text">для навигации воспользуйтесь меню сверху</p>
            </div>
        </div>
    </div>
@stop

@section('style')
    <link rel="stylesheet" href="{{Theme::asset('css/404.min.css')}}"/>
    <link rel="stylesheet" href="{{Theme::asset('css/catalog.min.css')}}"/>
@stop

@section('javascript')
    <script>
        $('document').ready(function () {
            $('ul li').hover(function () {
                var src = $(this).find('img').attr('src');
                var rd = src.split('.');
                rd[rd.length - 2] = rd[rd.length - 2] + '-hover';
                $(this).find('img').attr('src', rd.join('.'))
                $(this).find('a').css("color", "#ffc427")
            }, function () {
                var src = $(this).find('img').attr('src');
                var rd = src.split('.');
                rd[rd.length - 2] = rd[rd.length - 2].replace("-hover", "");
                $(this).find('img').attr('src', rd.join('.'))
                $(this).find('a').css("color", "#ffffff")
            });
        })
    </script>
@stop
