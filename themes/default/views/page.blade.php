<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
        @if ($pageInfo->meta->meta_title)
            <title>{{$pageInfo->meta->meta_title}}</title>
            @else
                <title>Default</title>
        @endif
        @if ($pageInfo->meta->meta_description)
                <meta name="description" content="{{$pageInfo->meta->meta_description}}">
        @endif
        @if ($pageInfo->meta->meta_keywords)
                <meta name="keywords" content="{{$pageInfo->meta->meta_keywords}}">
        @endif
        @if ($pageInfo->meta->meta_robot_index)
                <meta name="robots" content="index,follow">
        @endif

</head>
<body>
<h1 style="color: yellow;">{!! html_entity_decode($pageInfo->name) !!}</h1>
<navigation>
    {!!Menu::get('cms.main.navigation')!!}
</navigation>
<section class="body">
    {!! html_entity_decode($pageInfo->html) !!}
</section>
<footer>
    {!!Menu::get('cms.footer.navigation')!!}
</footer>
</body>
</html>