# ClonCar

### Installing

A step by step series of examples that tell you how to get a development env running
Write several commands in project path.
Create env file and configure database

```
cp ./.env.example ./.env
```

Set up permissions

``
sudo chgrp -R www-data storage bootstrap/cache
sudo chmod -R ug+rwx storage bootstrap/cache
``

Install

```
docker-compose up
```

```
docker-compose exec php php artisan cms:init
```

Run gulp
```
docker-compose exec php gulp 
```

Go to htt://127.0.0.1:81


