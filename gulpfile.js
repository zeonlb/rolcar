var gulp = require('gulp');

var cssmin = require('gulp-cssmin');
var jsmin = require('gulp-jsmin');
var rename = require('gulp-rename');
const image = require('gulp-image');

gulp.task('compressJs', function () {
    return gulp.src('public/js/bootstrap-popover.js')
        .pipe(uglify())
        ;
});

gulp.task('css', function () {
    return gulp.src(['themes/*/assets/css/*.css', '!themes/*/assets/css/*.min.css'])
        .pipe(cssmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(function(file) {
            return file.base;
        }));
});

gulp.task('image', () => {
    return gulp.src(['themes/*/assets/images/**/*'])
        .pipe(image({
            pngquant: true,
            optipng: false,
            zopflipng: true,
            jpegRecompress: false,
            mozjpeg: true,
            guetzli: false,
            gifsicle: true,
            svgo: true,
            concurrent: 10,
            quiet: true // defaults to false
        }))
        ;
});

gulp.task('js', function () {
    return gulp.src(['themes/*/assets/js/*.js'])
        .pipe(jsmin())
        .pipe(rename({suffix: '.min'}))
       ;
});

gulp.task('default', function () {
    gulp.run('css');
    gulp.run('js');
    gulp.run('image');
});

gulp.task('listen', function () {
    return gulp.watch('themes/*/assets/css/*.css', function () {
        gulp.watch('css');
    })
});

var elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');
require('laravel-elixir-clear');
/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function (mix) {
    mix.webpack('../../../modules/Tracking/Assets/js/track.js', 'modules/Tracking/Assets/dist/app.js')
});


