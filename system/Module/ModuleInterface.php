<?php

namespace System\Module;

use Modules\Cms\Seo\SiteMapGenerator;

/**
 * Basic interface for registering module
 *
 * Interface ModuleInterface
 * @package System\Module
 */
interface ModuleInterface
{

    public function getAdminNavigation();

    public function getFrontNavigation();

    public function getName();

    public function getDescription();

    public function setSettingFields();

}