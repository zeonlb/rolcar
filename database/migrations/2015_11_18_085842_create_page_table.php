<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePageTable extends Migration
{
    /**
     * Create CMS pages table
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_page', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('url')->unique();
            $table->longText('html')->nullable();
            $table->boolean('visible')->default(false);
            $table->integer('sort')->defaut(0);

            $table->integer('meta_id')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('meta_id')->references('id')->on('cms_meta');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cms_page', function(Blueprint $table) {
            $table->dropForeign('cms_page_meta_id_foreign');
        });
        Schema::drop('cms_page');
    }
}
