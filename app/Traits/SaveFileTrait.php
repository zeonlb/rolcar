<?php
/**
 * Created by PhpStorm.
 * User: rovel
 * Date: 26.03.17
 * Time: 19:54
 */

namespace App\Traits;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

trait SaveFileTrait
{
    protected function saveFile(Request $request, $varName = 'file') {
        $rules = array(
            $varName => 'image|max:5000',
        );

        $validation = Validator::make($request->all(), $rules);

        if (!$validation->valid()) {
            throw new \Exception($validation->errors()->first());
        }

        $destinationPath = strtolower(sprintf("uploads/%s/", str_random(8)));

        if (!File::exists($destinationPath)) {
            File::makeDirectory($destinationPath, 0775, true);
        }
        $extension = $request->file($varName)->getClientOriginalExtension(); // getting file extension
        $fileName = md5(uniqid()) . '.' . $extension; // renameing image
        $upload_success = $request->file($varName)->move($destinationPath, $fileName); // uploading file to given path
        if (!$upload_success) {
            throw new \Exception('Error to save file');

        }
        return  $destinationPath . $fileName;
    }
}