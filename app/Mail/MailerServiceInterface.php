<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 08.08.2018
 * Time: 17:40
 */

namespace App\Mail;


interface MailerServiceInterface
{
    public function startEventAutomation360(string $event, array $params = []) ;
    public function createClient(array $params = []) ;
    public function updateClient($id, array $params = []) ;
    public function bulkUpdateClient(array $params = []);
    public function deleteClient($id) ;
}