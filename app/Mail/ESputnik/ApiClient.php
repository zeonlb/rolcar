<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 08.08.2018
 * Time: 16:45
 */

namespace App\Mail\ESputnik;


use App\Mail\MailerServiceInterface;
use GuzzleHttp\Client;
use Modules\Shop\Entities\ShopClient;

class ApiClient implements MailerServiceInterface
{
    const BASE_URL = 'https://esputnik.com/api/v1/';


    protected $user;

    protected $password;

    /**
     * ApiClient constructor.
     * @param string $user
     * @param string $password
     */
    public function __construct(string $user, string $password)
    {
        $this->password = $password;
        $this->user = $user;

    }

    public function startEventAutomation360(string $event, array $params = [])
    {
        $pRes = [];
        foreach ($params as $name => $value) {
            $pRes[] = compact('name', 'value');
        }
        if ($params['email'] ?? false) {
            $pRes[] = ['name' => 'EmailAddress', 'value' => $params['email']];
        }
        $keyValue = $params['email'] ?? $params['phone'];
        $data = [
            'eventTypeKey' => $event,
            'keyValue' => $keyValue,
            'params' => $pRes
        ];
        return $this->sendRequest('event', 'post', $data);
    }


    private function sendRequest(string $url, string $method, array $data)
    {
        if (!env('ESPUTNIK_ENABLED', false)) {
            return false;
        }
        $client = new Client([
            'base_uri' => self::BASE_URL,
            'timeout' => 0,
            'allow_redirects' => false,
        ]);

        $result = $client->request(
            $method,
            $url,
            [
                'auth' => [$this->user, $this->password],
                'json' => $data

            ]);
        return $result;
    }

    public function createClient(array $params = [])
    {
        $resp = $this->sendRequest('contact', 'post', $params);
        if (!$resp) {
            return false;
        }
        return json_decode($resp->getBody()->getContents())->id;
    }

    public function sendSms(array $params = [])
    {
        return $this->sendRequest('message/sms', 'post', $params);
    }


    public function updateClient($id, array $params = [])
    {
        return $this->sendRequest('contact/' . $id, 'put', $params);
    }

    public function subscribe($id, array $params = [])
    {
        return $this->sendRequest('contact/subscribe', 'post', $params);
    }

    public function bulkUpdateClient(array $params = [])
    {
        return $this->sendRequest('contacts', 'post', $params);
    }

    public function deleteClient($id)
    {
        return $this->sendRequest('contact/' . $id, 'delete', []);
    }
}