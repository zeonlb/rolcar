<?php

namespace App\Providers;

use App\Mail\ESputnik\ApiClient;
use Illuminate\Support\ServiceProvider;

/**
 * Class EsputnilServiceProvider
 * @package App\Providers
 */
class EsputnikServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton(ApiClient::class, function ($app) {
            $config = $app['config']->get('esputnik');
            return new ApiClient(
                $config['api_user'],
                $config['api_password']
            );
        });

        $this->app->alias(ApiClient::class, 'esputnik');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
      //
    }
}
