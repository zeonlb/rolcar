<?php

namespace App\Jobs;

use GuzzleHttp\Client;
use Illuminate\Contracts\Bus\SelfHandling;
use Log;

class SendOrdersRomax extends Job implements SelfHandling
{

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($order, $product)
    {
        $this->order = $order;
        $this->product = $product;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $client = new Client();
        $order = $this->order;

        if (isset($order->first_name) && isset($order->email)) {
            $romaxClient = $client->request('POST', env('ROMAX_SERVER') . '/api/v2/shop/clients', [
                'exceptions' => false,
                'headers' => [
                    'api-token' => env('ROMAX_TOKEN')
                ],
                'form_params' => [
                    'name' => $order->first_name,
                    'email' => $order->email,
                    'phone' => convertPhone($order->phone)
                ]
            ]);
            $romaxClient = $romaxClient->getBody()->getContents();
            $romaxClient = json_decode($romaxClient);
        }

        if (isset($romaxClient)) {
            $delivery = json_decode($order->delivery);
            $formParams = [
                'client_id' => $romaxClient->id,
                'source' => 14,
                'first_name' => $order->first_name,
                'email' => $order->email,
                'phone' => convertPhone($order->phone),
                'city' => $delivery->city,
                'delivery_type' => $delivery->type,
                'delivery_street' => ($delivery->adress) ?? '',
                'delivery_warehouse' => ($delivery->warehouse) ?? '',
                'comment' => $order->description,
                'promocode' => $order->promocode,
                'payment_type' => $order->payment,
                'products' => $this->product,
                'utm' => session('utm_data', [])
            ];
            if ($order->paymentRel) {
                $formParams['payment_status'] = $order->paymentRel->status;
            }
            $response = $client->post(env('ROMAX_SERVER') . '/api/v2/shop/orders', [
//                'exceptions' => false,
                'headers' => [
                    'api-token' => env('ROMAX_TOKEN')
                ],
                'form_params' => $formParams
            ]);
            $result = json_decode($response->getBody()->getContents(), JSON_OBJECT_AS_ARRAY);
            $order->resource_id = $result['id'];
            $order->save();
            Log::info("sent order to Romax", [
                'module' => 'romax',
                'action' => 'order.create',
                'params' => [
                    'client_id' => $romaxClient->id ?? null,
                    'source' => 14,
                    'first_name' => $order->first_name,
                    'email' => $order->email,
                    'phone' => convertPhone($order->phone),
                    'city' => $delivery->city,
                    'payment_type' => $order->payment,
                    'delivery_type' => $delivery->type,
                    'delivery_street' => ($delivery->adress) ?? '',
                    'delivery_warehouse' => ($delivery->warehouse) ?? '',
                    'comment' => $order->description,
                    'promocode' => $order->promocode,
                    'products' => $this->product,
                    'utm' => session('utm_data', [])
                ]
            ]);
        } else {
            $response =  $client->request('POST', env('ROMAX_SERVER') . '/api/v2/shop/orders', [
                'exceptions' => false,
                'headers' => [
                    'api-token' => env('ROMAX_TOKEN')
                ],
                'form_params' => [
                    'source' => 14,
                    'first_name' => 'Нету (1 клик)',
                    'phone' => convertPhone($order->phone),
                    'delivery_type' => 'samovivoz',
                    'products' => $this->product,
                    'utm' => session('utm_data', [])
                ]
            ]);
            $result = json_decode($response->getBody()->getContents(), JSON_OBJECT_AS_ARRAY);
            $order->resource_id = $result['id'];
            $order->save();
            Log::info("sent order to Romax", [
                'module' => 'romax',
                'action' => 'order.create',
                'params' => [
                    'source' => 14,
                    'first_name' => 'Нету (1 клик)',
                    'phone' => convertPhone($order->phone),
                    'delivery_type' => 'samovivoz',
                    'products' => $this->product,
                    'utm' => session('utm_data', [])
                ]
            ]);
        }


    }
}
