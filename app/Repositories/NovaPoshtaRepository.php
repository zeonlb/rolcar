<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 09.08.2018
 * Time: 13:38
 */

namespace App\Repositories;


use DateTime;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Modules\Shop\Entities\NovaposhtaAccount;
use NovaPoshta\ApiModels\InternetDocument;
use NovaPoshta\Config;
use NovaPoshta\Models\BackwardDeliveryData;
use NovaPoshta\Models\CounterpartyContact;
use NovaPoshta\Models\OptionsSeat;


class NovaPoshtaRepository
{
    const HTTPS_API_NOVAPOSHTA_UA_V_2_0_JSON = 'https://api.novaposhta.ua/v2.0/json/';

    private $client;
    private $token;

    public function __construct()
    {
        $this->client = new Client();
        $this->token = \request()->header('NP-API-KEY', env('NOVAPOSHTA_API_KEY'));

        Config::setApiKey($this->token);
        Config::setFormat(Config::FORMAT_JSONRPC2);
        Config::setLanguage(Config::LANGUAGE_RU);
    }

    /**
     * @param string $city
     * @return mixed
     */
    public function calculateDelivery(Request $request)
    {
        $account = NovaposhtaAccount::where('token', $this->token)->firstOrFail();
        $params = $request->get('params');
        $data = [
            "modelName" => "InternetDocument",
            "calledMethod" => "getDocumentPrice",
            "methodProperties" => [
                "CitySender" => $account->params['city_ref'],
                "CityRecipient" => $request->get('recipient')['CityRef'],
                "ServiceType" => $request->get('ServiceType'),
                "Cost" => $params['Cost'],
                "CargoType" => $params['CargoType'],
            ],
            'apiKey' => $this->token
        ];
        if (!$params['IsShortDelivery']) {
            $data['methodProperties']['Seats'] = [];
            foreach ($params['Seats'] as $seat) {
                $data['methodProperties']['Seats'][] = $seat;
            }
        } else {
            $shortSeats = $params['ShortSeats'];
            $data['methodProperties']["Weight"] = $shortSeats['weight'];
            if ($shortSeats['volume']??null) {
                $data['methodProperties']["VolumeGeneral"] = $shortSeats['volume'];
            }
            $data['methodProperties']["SeatsAmount"] = $shortSeats['seats'];
        }

        if ($params['BackwardDeliveryData'] ?? null) {
            $data["methodProperties"]["RedeliveryCalculate"] = [
                "CargoType" => $params['BackwardDeliveryData']['CargoType'] ?? 'Money',
                "Amount" => $params['BackwardDeliveryData']['RedeliveryString']
            ];
        }
        $resp = $this->client->post(self::HTTPS_API_NOVAPOSHTA_UA_V_2_0_JSON,
            [
                'json' => $data
            ]
        );
        return response(json_decode($resp->getBody()->getContents(), JSON_OBJECT_AS_ARRAY), $resp->getStatusCode());
    }

    public function createDocument(Request $request)
    {
        $params = $request->get('params');
        $senderData = $request->get('sender');
        $params['DateTime'] = (new DateTime())->format('d.m.Y');
        $recipientData = $request->get('recipient');

        $account = NovaposhtaAccount::where('token', $this->token)->firstOrFail();
        $recipientData['Phone'] = convertPhone($recipientData['Phone']);
        $recipientContact = $this->saveRecipient(array_merge($recipientData, [
            "CounterpartyType" => "PrivatePerson",
            "CounterpartyProperty" => "Recipient"
        ]));
        if (!$recipientContact['success']) {
            return $recipientContact;
        }
        $createdSender = array_first($this->getSenderData($params['Token']), function ($i, $v) use ($account) {
            return ($v['Description'] === $account->fio);
        });
        if (!$createdSender) {
            abort(403, 'Ошибка при поиске отправителя. ФИО разнятся.');
        }
        $senderContact = $this->getSenderContact($createdSender['Ref']);
        if (!$senderContact['success']) {
            return $senderContact;
        }
        $sender = new CounterpartyContact();
        $sender->setCity($account->params['city_ref'])
            ->setRef($createdSender['Ref'])
            ->setAddress($account->params['warehouse_ref'])
            ->setContact($senderContact['data'][0]['Ref'])
            ->setPhone(convertPhone($account->params['phone']));
        $recipient = new CounterpartyContact();
        $recipient
            ->setCity($recipientData['CityRef'])
            ->setRef($recipientContact['data'][0]['Ref'])
            ->setAddress($recipientData['RecipientAddress'])
            ->setContact($recipientContact['data'][0]['ContactPerson']['data'][0]['Ref'])
            ->setPhone(convertPhone($recipientData['Phone']));


        $internetDocument = new InternetDocument();
        $internetDocument->setSender($sender);
        $internetDocument->setRecipient($recipient);

        if ($params['BackwardDeliveryData']) {
            $backwardDelivery = new BackwardDeliveryData();
            $backwardDelivery->setPayerType($params['BackwardDeliveryData']['PayerType']);
            $backwardDelivery->setCargoType($params['BackwardDeliveryData']['CargoType']);
            $backwardDelivery->setRedeliveryString($params['BackwardDeliveryData']['RedeliveryString']);
            $internetDocument->addBackwardDeliveryData($backwardDelivery);
        }
        if (!$params['IsShortDelivery']) {
            foreach ($params['Seats'] as $seat) {
                $optionsSeat = new OptionsSeat();
                $optionsSeat->setVolumetricHeight($seat['height'])
                    ->setVolumetricLength($seat['length'])
                    ->setVolumetricWidth($seat['width'])
                    ->setWeight($seat['weight']);
                $internetDocument->addOptionsSeat($optionsSeat);
            }
        } else {
            $shortSeats = $params['ShortSeats'];
            $internetDocument->setWeight($shortSeats['weight']);
            if ($shortSeats['volume']??null) {
                $internetDocument->setVolumeGeneral($shortSeats['volume']);
            }
            $internetDocument->setSeatsAmount($shortSeats['seats']);
        }
        $internetDocument->setServiceType($params['ServiceType']);
        $internetDocument->setPayerType($params['PayerType']);
        $internetDocument->setPaymentMethod($params['PaymentMethod']);
        $internetDocument->setCargoType($params['CargoType']);
        $internetDocument->setCost($params['Cost']);
        $internetDocument->setDescription($params['Description']);
        $internetDocument->setDateTime((new DateTime())->format('d.m.Y'));
        if($ref = $params['Ref']??null) {
            $internetDocument->setRef($ref);
            $respData = $internetDocument->update();
            $action = 'update.document';
        } else {
            $action = 'create.document';
            $respData = $internetDocument->save();
        }
        $data = ['module' => 'np', 'action' => $action, 'params' => ['request' => $params, 'response' => $respData]];
        Log::info("creating document", $data);
        return $respData;
    }

    public function createDocumentOld($params)
    {
        $resp = $this->client->post(self::HTTPS_API_NOVAPOSHTA_UA_V_2_0_JSON,
            [
                'json' => [
                    "modelName" => "InternetDocument",
                    "calledMethod" => "save",
                    "methodProperties" => $params,
                    'apiKey' => $this->token,
                ]
            ]
        );
        $respData = json_decode($resp->getBody()->getContents(), JSON_OBJECT_AS_ARRAY);
        $data = ['module' => 'np', 'action' => 'create.document', 'params' => ['request' => $params, 'response' => $respData]];
        Log::info("creating document", $data);
        return response($respData, $resp->getStatusCode());
    }

    public function saveRecipient($params)
    {
        $data = [
            'apiKey' => $this->token,
            'modelName' => 'Counterparty',
            'calledMethod' => 'save',
            'methodProperties' => $params
        ];
        $resp = $this->client->post(self::HTTPS_API_NOVAPOSHTA_UA_V_2_0_JSON,
            [
                'json' => $data
            ]
        );
        return json_decode($resp->getBody()->getContents(), JSON_OBJECT_AS_ARRAY);
    }

    public function getSenderData($token)
    {
        return Cache::remember(md5('np.sender.getCounterparties.'. $this->token), 10000, function () use ($token) {
            $data = [
                'apiKey' => $token,
                "modelName" => "Counterparty",
                "calledMethod" => "getCounterparties",
                "methodProperties" => [
                    "CounterpartyProperty" => "Sender"
                ]];
            $resp = $this->client->post(self::HTTPS_API_NOVAPOSHTA_UA_V_2_0_JSON,
                [
                    'json' => $data
                ]
            );
            return json_decode($resp->getBody()->getContents(), JSON_OBJECT_AS_ARRAY)['data'];
        });
    }

    public function getSenderContact(string $ref)
    {
        return Cache::remember(md5('np.sender.getCounterpartyContactPerson.' . $this->token), 10000, function () use ($ref) {
            $data = [
                'apiKey' => $this->token,
                "modelName" => "Counterparty",
                "calledMethod" => "getCounterpartyContactPersons",
                "methodProperties" => [
                    "Ref" => $ref
                ]];
            $resp = $this->client->post(self::HTTPS_API_NOVAPOSHTA_UA_V_2_0_JSON,
                [
                    'json' => $data
                ]
            );
            return json_decode($resp->getBody()->getContents(), JSON_OBJECT_AS_ARRAY);
        });
    }

    /**
     * @param string $city
     * @return mixed
     */
    public function getCityData(string $city = 'all')
    {
        if ('all' === $city) {
            $cacheKey = md5('np_city_data_new' . $city);
            return Cache::remember(md5($cacheKey), 1000, function () {
                $resp = $this->client->post('https://api.novaposhta.ua/v2.0/json/',
                    [
                        'json' => [
                            'modelName' => 'Address',
                            'calledMethod' => 'getCities',
                            'methodProperties' => [
                                'Language' => 'ru',
                                'Warehouse' => 1,
                            ],
                            'apiKey' => $this->token
                        ]
                    ]
                );
                $response = json_decode($resp->getBody()->getContents(), JSON_OBJECT_AS_ARRAY);
                return $response;
            });
        } else {
            $resp = $this->client->post(self::HTTPS_API_NOVAPOSHTA_UA_V_2_0_JSON,
                [
                    'json' => [
                        'modelName' => 'Address',
                        'calledMethod' => 'getCities',
                        'methodProperties' => [
                            'FindByString' => $city,
                            'Language' => 'ru',
                            'Warehouse' => 1,
                        ],
                        'apiKey' => $this->token
                    ]
                ]
            );
            return json_decode($resp->getBody()->getContents(), JSON_OBJECT_AS_ARRAY);
        }
    }

    public function getWarhouse(string $city)
    {
        return Cache::remember(md5('np_warehouse_' . $city), 60, function () use ($city) {

            $resp = $this->client->post('http://api.novaposhta.ua/v2.0/json/Address/searchSettlements/',
                [
                    'json' => [
                        'modelName' => 'AddressGeneral',
                        'calledMethod' => 'getWarehouses',
                        'methodProperties' => [
                            'CityName' => $city,
                            'Limit' => 200,
                            'Language' => 'ru'
                        ],
                        'apiKey' => $this->token
                    ]
                ]
            );

            return json_decode($resp->getBody()->getContents(), JSON_OBJECT_AS_ARRAY);
        });
    }

    public function cargoTypes()
    {
        return Cache::remember(md5('np_getCargoTypes_' . $this->token), 600, function () {

            $resp = $this->client->get('https://api.novaposhta.ua/v2.0/json/ru',
                [
                    'json' => [
                        "modelName" => "Common",
                        "calledMethod" => "getCargoTypes",
                        'apiKey' => $this->token
                    ]
                ]
            );

            return json_decode($resp->getBody()->getContents(), JSON_OBJECT_AS_ARRAY);
        });
    }

}