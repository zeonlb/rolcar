<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Pingpong\Themes\ThemeFacade;

class ThemeAssets extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'theme:assets {type?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create symbol links for assets';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $type = $this->argument('type') ?? 'symbol';
        foreach (ThemeFacade::all() as $name => $theme) {
            $publicPath = base_path('public');
            $themePath = $publicPath . '/themes';
            if (!file_exists($themePath)) {
                mkdir($themePath);
            }
            if ('symbol' == $type) {
                shell_exec("ln -s " . $theme->getPath() . "/assets/ $publicPath/themes/" . $theme->getName());
            } else {
                shell_exec("copy " . $theme->getPath() . "/assets/ $publicPath/themes/" . $theme->getName());
            }
        }
    }
}
