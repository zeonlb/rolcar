<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Pingpong\Modules\Facades\Module;

class CMSAssets extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'module:assets {type?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create symbol links for assets';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $type = $this->argument('type') ?? 'symbol';
        foreach (Module::all() as $name => $module) {
            $publicPath = base_path('public');
            $modulePath = $publicPath . '/modules';
            if (!file_exists($modulePath)) {
                mkdir($modulePath);
            }
            if ('symbol' == $type) {
                shell_exec("ln -s " . $module->getPath() . "/Assets/ $publicPath/modules/" . $module->getName());
            } else {
                shell_exec("copy " . $module->getPath() . "/Assets/ $publicPath/modules/" . $module->getName());
            }
        }

    }
}
