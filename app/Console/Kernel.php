<?php

namespace App\Console;

use App\Console\Commands\CMSAssets;
use App\Console\Commands\Inspire;
use App\Console\Commands\ThemeAssets;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Inspire::class,
        CMSAssets::class,
        ThemeAssets::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('inspire')
                 ->hourly();
        $schedule->command('dropship:import-products', [ 'schedule' => 'day'])->daily();
        $schedule->command('dropship:import-products', [ 'schedule' => 'hour'])->hourly();
        $schedule->command('dropship:import-products', [ 'schedule' => 'week'])->weekly();
    }
}
