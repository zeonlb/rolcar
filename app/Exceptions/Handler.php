<?php

namespace App\Exceptions;

use App\CMSPage;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Modules\Cms\Entities\Meta;
use Modules\Cms\Entities\Page;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Pingpong\Themes\ThemeFacade as Theme;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        HttpException::class,
        ModelNotFoundException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        return parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if ($e instanceof ModelNotFoundException) {

            $e = new NotFoundHttpException($e->getMessage(), $e);
        }
        if ($e instanceof NotFoundHttpException) {
            $pageInfo = new Page();
            $pageInfo->name = 'Страница не найдена';
            $pageInfo->meta = new Meta();
            $pageInfo->meta->meta_title = 'Страница не найдена';
            $pageInfo->meta->meta_robot_index = false;
            return response()->view(Theme::getCurrent().'::errors.404', compact('pageInfo'), 404);
        }


        return parent::render($request, $e);
    }
}
