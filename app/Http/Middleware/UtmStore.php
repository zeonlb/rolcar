<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class UtmStore
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $utmData = $request->only([
            'gclid',
            'yclid',
            'ymclid',
            'fbclid',
            'utm_source',
            'utm_medium',
            'utm_campaign',
            'utm_term',
            'utm_content',
            'utm_id',
        ]);
        $utmData = array_filter($utmData, function ($i) {
            return strlen($i);
        });
        if ($utmData) {
            $utmCode = md5(json_encode($utmData));
            if (session('utm_code', '') !== $utmCode) {
                session(['utm_data' => $utmData, 'utm_code' => $utmCode]);
            }
        }
        return $next($request);
    }
}
