<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Modules\Cms\Repositories\PageRepository;

class BasicController extends Controller
{
    protected  $pageRepo;

    function __construct(PageRepository $pageRepo)
    {
        $this->pageRepo = $pageRepo;
    }

    /**
     * @param Request $request
     * @return string
     */
    public function index(Request $request)
    {
        $routeName = $request->getPathInfo();
        $pageInfo = $this->pageRepo->getPageInfo($routeName);

        return view('basic.page', compact('pageInfo'));
    }

}
