<?php

namespace App\Listeners;

use App\Events\SeoRedirectEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class EventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SeoRedirectEvent  $event
     * @return void
     */
    public function handle(SeoRedirectEvent $event)
    {
        //
    }
}
