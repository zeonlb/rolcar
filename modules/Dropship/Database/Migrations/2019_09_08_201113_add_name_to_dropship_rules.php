<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNameToDropshipRules extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dropship_rules', function(Blueprint $table)
        {
		$table->string('name');
		$table->boolean('active');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dropship_rules', function(Blueprint $table)
        {
		$table->dropColumn('name');
		$table->dropColumn('active');
        });
    }

}
