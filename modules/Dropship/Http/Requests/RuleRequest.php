<?php
namespace Modules\Dropship\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Contracts\Validation\Validator;

class RuleRequest extends Request{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
        	'name' => 'required',
            'dropship_store_id' => 'required',
            'columns' => 'required',
            'schedule' => 'required'
        ];
    }

}