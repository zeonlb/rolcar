<?php 

namespace Modules\Dropship\Http\Controllers\Admin;

use Pingpong\Modules\Routing\Controller;

use Illuminate\Http\Request;
use Modules\Dropship\Entities\Rule;
use Modules\Dropship\Entities\Store;
use Illuminate\Support\Facades\View;
use Modules\Dropship\Http\Requests\RuleRequest;


class RuleController extends Controller {
	
	public function index()
	{
		$rules = Rule::all();

		return View::make('dropship::admin.rule.list', compact('rules'));
	}
	
	public function create()
    {
        $stores = Store::where('active', 1)->get();

        return View::make('dropship::admin.rule.create', compact('stores'));
    }

    public function store(RuleRequest $request)
    {
        $postData = $request->all();

        $rule = new Rule;

        $columns = implode(',', $request->columns);
        $postData['columns'] = $columns;


        $rule->fill($postData);

        $rule->save();

        return redirect(Route('admin.dropship.rule.index'))->with('success', 'Правило успешно добавлено');
    }

	public function edit($id)
    {
        $rule = Rule::findOrFail($id);
        $stores = Store::where('active', 1)->get();
        $columns = explode(',', $rule->columns);

        return View::make('dropship::admin.rule.edit', compact('rule', 'stores', 'columns'));
    }

    public function update(RuleRequest $request, $id)
    {
        $rule = Rule::findOrFail($id);

        $postData = $request->all();

        $columns = implode(',', $request->columns);
        $postData['columns'] = $columns;
        
        $rule->fill($postData);

        $rule->save();

        return redirect(Route('admin.dropship.rule.index'))->with('success', 'Правило успешно обновлено.');
    }
   
    public function runNow(Request $request)
    {
        $exitCode = \Artisan::call('dropship:import-products', [
                'schedule' => 'now',
                'rule_id' => $request->id
            ]);

        return redirect()->back()->with('success', 'Данные загружены');
    }

	public function destroy($id)
	{
		$rule = Rule::findOrFail($id);
		$rule->delete();

		return redirect()->back()->with('success', ' Правило успешно удалено');
	}

}