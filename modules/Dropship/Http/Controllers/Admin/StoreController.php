<?php 

namespace Modules\Dropship\Http\Controllers\Admin;

use Pingpong\Modules\Routing\Controller;

use Illuminate\Http\Request;
use Modules\Dropship\Entities\Store;
use Illuminate\Support\Facades\View;
use Modules\Dropship\Http\Requests\StoreRequest;


class StoreController extends Controller {
	
	public function index()
	{
		$stores = Store::all();

		return View::make('dropship::admin.store.list', compact('stores'));
	}
	
	public function create()
    {
        return View::make('dropship::admin.store.create');
    }

    public function store(StoreRequest $request)
    {
        $postData = $request->all();

        $store = new Store;
        $store->fill($postData);

        $store->save();

        return redirect(Route('admin.dropship.store.index'))->with('success', 'Магазин успешно добавлен');
    }

	public function edit($id)
    {
        $store = Store::findOrFail($id);
        return View::make('dropship::admin.store.edit', compact('store'));
    }

    public function update(StoreRequest $request, $id)
    {
        $store = Store::findOrFail($id);
        $postData = $request->all();
        
        $store->fill($postData);

        $store->save();

        return redirect(Route('admin.dropship.store.index'))->with('success', 'Магазин успешно обновлен.');
    }
   

	public function destroy($id)
	{
		$store= Store::findOrFail($id);
		$store->delete();

		return redirect()->back()->with('success', ' магазин успешно удален');
	}

}