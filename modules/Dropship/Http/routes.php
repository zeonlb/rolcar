<?php
#Admin
Route::group([
    'prefix' => 'admin',
    'middleware' => ['admin.auth', 'admin.menu.storing'],
    'namespace' => 'Modules\Dropship\Http\Controllers'], function () {
    Route::group(['prefix' => 'dropship', 'namespace' => 'Admin'], function () {
        Route::resource('store', 'StoreController');
        Route::resource('rule', 'RuleController');
        Route::get('run_shedule', ['as' => 'admin.dropship.rule.run', 'uses' => 'RuleController@runNow']);

    });
});