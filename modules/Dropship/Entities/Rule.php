<?php namespace Modules\Dropship\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Dropship\Entities\Store;

class Rule extends Model
{

    protected $table = 'dropship_rules';
    protected $fillable = [
        'name',
        'dropship_store_id',
        'columns',
        'schedule',
        'active'
    ];

    public function store()
    {
        return $this->belongsTo(Store::class, 'dropship_store_id', 'id');
    }
}