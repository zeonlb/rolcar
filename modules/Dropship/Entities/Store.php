<?php namespace Modules\Dropship\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Dropship\Entities\DropshopRule;

class Store extends Model
{

    protected $table = 'dropship_stores';
    protected $fillable = [
        'name',
        'url',
        'active'
    ];

    public function rules()
    {
        return $this->hasMany(DropshipRule::class, 'id', 'dropship_store_id');
    }

}