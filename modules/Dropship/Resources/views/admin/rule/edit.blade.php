@extends('admin::layouts.master')

@section('content')
    <h1>Дропшипинг импорт</h1>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-bars"></i> Создать новое правило</h2>

                <div class="clearfix"></div>
            </div>
            <div class="x_content">

                <form name="add-new-page" enctype="multipart/form-data"
                      action="{{route('admin.dropship.rule.update', ['id' => $rule->id])}}"
                      method="POST"
                      class="form-horizontal form-label-left" novalidate="">
                    <input type="hidden" name="_method" value="put"/>

                    <div class="" role="tabpanel" data-example-id="togglable-tabs">

                        <div id="myTabContent" class="tab-content">
                            <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">

                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Активный</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div id="active" class="btn-group" data-toggle="buttons">
                                            <label class="btn btn-default @if ( $rule->active ) active @endif" data-toggle-class="btn-default" data-toggle-passive-class="btn-default">
                                                <input @if ( $rule->active ) checked="" @endif type="radio" name="active" value="1"> &nbsp; On &nbsp;
                                            </label>
                                            <label class="btn btn-danger " data-toggle-class="btn-default" data-toggle-passive-class="btn-default">
                                                <input type="radio" name="active" value="0" > Off
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Поставщик <span
                                                class="required">*</span>
                                    </label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="name" value="{{$rule->name}}" class="form-control" required>
                                    </div>
                                </div>

                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="dropship_store_id">Магазин <span
                                                class="required">*</span>
                                    </label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select class="form-control" id="dropship_store_id" name="dropship_store_id">
                                            @foreach ($stores as $store )
                                                <option value="{{$store->id}}" @if ($rule->dropship_store_id == $store->id) selected @endif > 
                                                    {{$store->name}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="columns">Поля <span
                                                class="required">*</span>
                                    </label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select multiple class="form-control" id="columns" name="columns[]">

                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="columns">Поля <span
                                                class="required">*</span>
                                    </label>
                                              <option value="prices" @if (in_array('prices', $columns)) selected @endif >
                                                 Цена/Валюта/Количество
                                              </option>
                                              <option value="brand" @if (in_array('brand', $columns)) selected @endif>
                                                Бренд/Артикул
                                              </option>
                                              <option value="photos" @if (in_array('photos', $columns)) selected @endif>
                                                Фотографии товара
                                              </option>
                                              <option value="params" @if (in_array('params', $columns)) selected @endif>
                                                Атрибуты товара
                                              </option>
                                              <option value="name" @if (in_array('name', $columns)) selected @endif>
                                                Название товара
                                              </option>
                                              <option value="description" @if (in_array('description', $columns)) selected @endif>
                                                Описание товара
                                              </option>
                                        </select>
                                    </div>
                                </div>

                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="schedule">Расписание <span
                                                class="required">*</span>
                                    </label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">

                                        <select class="form-control" id="schedule" name="schedule">
                                              <option value="now" @if ( $rule->schedule == 'now') selected @endif>
                                                Сейчас (один раз) 
                                              </option>
                                              <option value="hour" @if ( $rule->schedule == 'hour') selected @endif>Раз в час</option>
                                              <option value="day" @if ( $rule->schedule == 'day') selected @endif>Раз в день</option>
                                              <option value="week" @if ( $rule->schedule == 'week') selected @endif>Раз в неделю</option>
                                        </select>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                            <button id="send" type="submit" class="btn btn-success">Сохранить магазин</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    <script type="text/javascript">
        $(document).ready(function() {
            $('.mdb-select').materialSelect();
        });         
    </script>

@stop

@section('style')
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.0.2/css/font-awesome.css" rel="stylesheet">
    <link href="{{Module::asset('admin:css/editor/external/google-code-prettify/prettify.css')}}" rel="stylesheet">
    <link href="{{Module::asset('admin:css/editor/index.css')}}" rel="stylesheet">
@stop

