@extends('admin::layouts.master')

@section('content')
    <h1>Поставщики</h1>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-bars"></i> Создание поставщика</h2>

                <div class="clearfix"></div>
            </div>
            <div class="x_content">

                <form  enctype="multipart/form-data" name="add-new-page" action="{{route('admin.dropship.store.store')}}" method="POST"
                      class="form-horizontal form-label-left" novalidate="">

                    <div class="" role="tabpanel" data-example-id="togglable-tabs">

                        <div id="myTabContent" class="tab-content">
                            <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">

                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Название <span
                                                class="required">*</span>
                                    </label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input id="name"  name="name" class="form-control col-md-7 col-xs-12"
                                               required="required" type="text">
                                    </div>
                                </div>

                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="url">URL <span
                                                class="required">*</span>
                                    </label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input id="url"  name="url" class="form-control col-md-7 col-xs-12"
                                               required="required" type="text">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Активный</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div id="active" class="btn-group" data-toggle="buttons">
                                            <label class="btn btn-default active" data-toggle-class="btn-default" data-toggle-passive-class="btn-default">
                                                <input checked="" type="radio" name="active" value="1"> &nbsp; On &nbsp;
                                            </label>
                                            <label class="btn btn-danger " data-toggle-class="btn-default" data-toggle-passive-class="btn-default">
                                                <input type="radio" name="active" value="0" > Off
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                            <button id="send" type="submit" class="btn btn-success">Сохранить магазин</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@section('style')
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.0.2/css/font-awesome.css" rel="stylesheet">
    <link href="{{Module::asset('admin:css/editor/external/google-code-prettify/prettify.css')}}" rel="stylesheet">
    <link href="{{Module::asset('admin:css/editor/index.css')}}" rel="stylesheet">
@stop

