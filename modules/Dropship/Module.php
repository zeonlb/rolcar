<?php

namespace Modules\Dropship;

use Modules\Cms\Seo\SiteMapGenerator;
use System\Module\ModuleInterface;
use Pingpong\Menus\MenuFacade as Menu;

class Module implements ModuleInterface
{

    public function getAdminNavigation()
    {
        $menu = Menu::instance('admin.navigation');
        $menu->dropdown('Dropshiping import', function ($sub) {
            $sub->url('admin/dropship/store', 'Поставщики/Providers');
            $sub->url('admin/dropship/rule', 'Правила');
        },
            6,
            ['icon' => 'fa fa fa-list']
        );
    }

    public function getFrontNavigation()
    {
        // TODO: Implement getFrontNavigation() method.
    }

    public function getDescription()
    {
        // TODO: Implement getDescription() method.
    }

    public function getName()
    {
        // TODO: Implement getName() method.
    }

    public function setSettingFields()
    {
        // TODO: Implement setSettings() method.
    }

    public function getSiteMapData(): SiteMapGenerator
    {
        return new SiteMapGenerator();
    }
}