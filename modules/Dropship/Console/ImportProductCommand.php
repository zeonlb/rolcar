<?php namespace Modules\Dropship\Console;

use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Modules\Cms\Core\Images\ThumbBuilder;
use Modules\Cms\Entities\File;
use Modules\Cms\Entities\Meta;
use Modules\Dropship\Entities\Rule;
use Modules\Dropship\Entities\Setting;
use Modules\Shop\Entities\Brand;
use Modules\Shop\Entities\Currency;
use Modules\Shop\Entities\Product;
use Modules\Shop\Entities\ProductType;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use function simplexml_load_string;


class ImportProductCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'dropship:import-products';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import products - now, hour, day, week.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $products = Product::where('max_id', NULL)->get();
        foreach ($products as $product) {
            $this->destroyProduct($product);
        }

        if ($rule_id = $this->argument('rule_id')) {
            $rule = Rule::find($rule_id);
            $this->importProducts($rule);
        } else {
            $schedule = $this->argument('schedule');

            $rules = Rule::where('schedule', $schedule)->get();
            Log::info("schedule run", [
                'module' => 'dropship',
                'action' => 'console',
                'params' => ['rules' => $rules]
            ]);
            foreach ($rules as $rule) {
                $this->importProducts($rule);
            }
        }

    }


    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['schedule', InputArgument::REQUIRED, 'Schedule argument.'],
            ['rule_id', InputArgument::OPTIONAL, 'Role argument']
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
        ];
    }

    private function importProducts($rule)
    {

        if ($xml = simplexml_load_string(file_get_contents($rule->store->url))) {
            $columns = explode(',', $rule->columns);

            foreach ($xml->shop->offers->offer as $offer) {

                try {
                    $id = $offer->attributes()->id;
                    $available = $offer->attributes()->available;

                    $brand = $this->getBrand($offer->vendor);
                    $currency = $this->getCurrency($offer->currencyId);

                    if ($product = Product::where('max_id', $id)->first()) {

                        if (in_array('prices', $columns)) {
                            $product->price = $offer->recommendedPrice;
                            $product->count = $offer->stock_quantity;
                            $currency->shop_currency_id = $currency->id;
                        }
                        if (in_array('brand', $columns)) {
                            $product->shop_brand_id = $brand->id;
                            $product->sku = $offer->vendorCode;
                        }

                        if (in_array('name', $columns)) {
                            $product->name = $offer->name;
                        }

                        if (in_array('photos', $columns)) {
                            $this->removePhotos($product);
                            $this->addFiles($offer->picture, $product);
                        }

                        if (in_array('params', $columns)) {
                            echo "";
                        }

                        $product->save();
                    } else {
                        $product = new Product;

                        if (!$productType = ProductType::where('code', 'novinka')->first()) {
                            $productType = ProductType::create([
                                'name' => 'Новинка',
                                'code' => 'novinka'
                            ]);
                        }

                        $path = parse_url($offer->url, PHP_URL_PATH);
                        $parts = explode('/', $path);
                        $part = $parts[count($parts) - 1];


                        $product->shop_product_type_id = $productType->id;

                        $product->code = $part;
                        $product->price = $offer->recommendedPrice;
                        $product->count = $offer->stock_quantity;
                        $product->visible = 0;
                        $product->shop_currency_id = $currency->id;
                        $product->name = $offer->name;
                        $product->romax_id = $id;

                        $product->shop_brand_id = $brand->id;
                        $product->sku = $offer->vendorCode;

                        if ($offer->description) {
                            $product->description = $offer->description[0];
                        }
                        $product->save();

                        $files = $this->addFiles($offer->picture, $product);

                    }
                } catch (Exception $e) {
                    Log::error('Ошибка при выгрузке товара ', [
                            'module' => 'rolcar',
                            'action' => 'import.dropship',
                            'params' => [$offer],
                        ]
                    );
                }

            }
            Log::info('Выгрузка ' . $rule->name . ' прошла успешно.');
        } else {
            return false;
        }
    }

    private function getBrand($code)
    {
        if ($brand = Brand::where('code', $code)->first()) {
            return $brand;
        }

        $brand = Brand::create([
            'name' => $code,
            'code' => $code
        ]);

        $meta = Meta::create();
        $brand->meta()->associate($meta);
        $brand->save();

        return $brand;
    }

    private function getCurrency($code)
    {
        if ($currency = Currency::where('code', $code)->first()) {
            return $currency;
        }

        $currency = Currency::create([
            'name' => $code,
            'code' => $code
        ]);

        return $currency;
    }

    private function addFiles($files, $product)
    {
        $pictures = array();
        $count = 0;

        $dir = 'uploads/product-' . $product->id;
        $dirFull = public_path() . '/' . $dir;
        \File::makeDirectory($dirFull, $mode = 0777, true, true);

        foreach ($files as $key => $file) {
            $count++;
            $fileName = 'img-' . $product->id . '-' . $count . '.jpg';
            $filePath = $dirFull . '/' . $fileName;

            copy($file, $filePath);
            $thumb = ThumbBuilder::make($dir . '/' . $fileName, 'auto', 230);
            $picture = new File;

            $picture->name = $fileName;
            $picture->title = $fileName;
            $picture->path = $dir . '/' . $fileName;
            $picture->sort = 0;
            $picture->from_storage = 0;

            $picture->save();

            array_push($pictures, $picture);
        }

        $product->photos()->saveMany($pictures);

    }

    private function removePhotos($product)
    {
        foreach ($product->photos as $photo) {
            if (file_exists(public_path() . '/' . $photo->path)) {
                unlink(public_path() . '/' . $photo->path);
            }

            $photo->delete();
        }
        DB::table('shop_product_photos')->where('shop_product_id', $product->id)->delete();
    }

    private function destroyProduct($product)
    {
        $id = $product->id;
        $this->removePhotos($product);
        \DB::table('shop_product_photos')->where('shop_product_id', $id)->delete();
        \DB::table('shop_product_video')->where('shop_product_id', $id)->delete();
        \DB::table('shop_discounts')->where('shop_product_id', $id)->delete();
        if ($product->crossSales->count() > 0) {
            \DB::table('shop_cross_sale')->where('shop_product_id', $id)->delete();
        }
        if ($product->options->count() > 0) {
            \DB::table('shop_product_options')->where('shop_product_id', $id)->delete();
        }
        if ($product->comments->count() > 0) {
            \DB::table('shop_product_comments')->where('shop_product_id', $id)->delete();
        }
        $product->delete();
    }
}
