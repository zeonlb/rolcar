<?php namespace Modules\Log\Http\Controllers\Admin;

use App\Traits\SaveFileTrait;
use DB;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Modules\Log\Entities\File;
use Modules\Log\Entities\Log;
use Pingpong\Modules\Routing\Controller;
use Illuminate\Support\Facades\View;

class LogController extends Controller
{

    use SaveFileTrait;

    /**
     * @return mixed
     */
    public function index(Request $request)
    {
        $query = Log::orderBy('id', 'desc');
        if ('prom_problem_orders' === $request->get('module')) {
            $query->where('module', 'prom')->where('action', 'import.orders')->groupBy('message');
        } elseif ($model = $request->get('module')) {
            $query->where('module', $model);
        }
        if ($s = $request->get('search')) {
            $query->where('id', $s)->orWhere('action', 'like', '%'.$s.'%')
                ->orWhere('message', 'like', '%'.$s.'%');
        }
        $logs = $query->paginate($request->get('limit', 50));
        $modules  = array_column(DB::table('system_logs')->select('module')->distinct()->get(), 'module');
        return View::make('log::index', compact('logs', 'modules'));
    }

    /**
     * @return mixed
     */
    public function create()
    {
        return View::make('shop::admin.banner.create');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $banner = Banner::findOrFail($id);
        return View::make('shop::admin.banner.edit', compact('banner'));
    }

    /**
     * @param $id
     * @return mixed
     */
    public function mark(Request $request)
    {
        if (!$id = $request->get('id')) {
            return ['status' => 'fail'];
        }
        $file = File::findOrFail($id);
        $file->from_storage = true;
        $file->save();
        return ['status' => 'success'];
    }

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        $banner = Banner::findOrFail($id);
        $banner->delete();

        return redirect()->route('admin.shop.banner.index')->with('success', 'Banner successfully deleted!');
    }

    /**
     * @param Request $request
     * @param $id
     * @return RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $banner = Banner::findOrFail($id);
        $banner->fill($request->all());

        if ($request->file('file')) {
            $banner->img = $this->saveFile($request);
        }
        $banner->save();

        return redirect()->route('admin.shop.banner.index')->with('success', 'Banner successfully updated');
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {

        $banner = Banner::create($request->all());

        if ($request->file('file')) {
            $banner->img = $this->saveFile($request);
        }
        $banner->save();

        return redirect()->route('admin.shop.banner.index')->with('success', 'Banner successfully added');
    }

}