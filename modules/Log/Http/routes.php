<?php
#Admin
Route::group([
    'prefix' => 'admin',
    'middleware' => ['admin.auth', 'admin.menu.storing'],
    'namespace' => 'Modules\Log\Http\Controllers'], function () {
    Route::group(['prefix' => 'log', 'namespace' => 'Admin'], function () {
        Route::resource('log', 'LogController');
    });
});