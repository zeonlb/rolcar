<?php namespace Modules\Log\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Created by PhpStorm.
 * User: dat
 * Date: 21.01.18
 * Time: 20:22
 */
class Log extends Model
{
    protected $table = 'system_logs';

    protected $fillable = [
        'level',
        'message',
        'data',
        'action',
        'module'
    ];

    /**
     * @var array
     */
    protected $casts = [
        'data' => 'json'
    ];

    protected $dates = ['created_at', 'updated_at'];


}