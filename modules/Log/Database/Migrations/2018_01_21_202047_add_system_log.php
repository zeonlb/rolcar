<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSystemLog extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system_logs', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('level')->default('info');
            $table->string('action')->default(null);
            $table->mediumText('message')->default(null);
            $table->json('data')->default(null);
            $table->string('module')->default(null);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropColumn('cms_files', 'from_storage');
    }

}
