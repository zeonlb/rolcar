@extends('admin::layouts.master')

@section('content')
    <h1>Logs</h1>
    <div>
        <div class="col-md-12">
            <div class="col-md-10">
                <div class="box-pagination col-md-12 col-sm-6 text-left">
                    {!! $logs->appends(['search' => \Illuminate\Support\Facades\Input::get('search'),'module' => \Illuminate\Support\Facades\Input::get('module')])->render() !!}
                </div>
            </div>

        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <form>
                            <div style="float: left; margin-right: 25px">
                                <label>Module<br></label>
                                <?php
                                $sort = request()->all();
                                $sort['module'] = 'all';
                                $s = [];
                                foreach ($sort as $key => $val) {
                                    $s[] = $key . '=' . $val;
                                }
                                ?>
                                <a  href="?{{implode('&', $s)}}">
                                    <button class="btn @if (request('module') == 'all' || !request('module')) btn-primary @else btn-default  @endif">All</button>
                                </a>
                                @foreach($modules as $module)
                                    <?php
                                    if (!$module) continue;
                                    $s = [];
                                    $sort['module'] = $module;
                                    foreach ($sort as $key => $val) {
                                        $s[] = $key . '=' . $val;
                                    }
                                    ?>
                                    <a class="btn  @if (request('module') == $module) btn-primary @else btn-default  @endif" href="?{{implode('&', $s)}}">
                                        {{ucfirst($module)}}
                                    </a>
                                @endforeach
                                <?php
                                $sort = request()->all();
                                $sort['module'] = 'prom_problem_orders';
                                $s = [];
                                foreach ($sort as $key => $val) {
                                    $s[] = $key . '=' . $val;
                                }
                                ?>
                                <a class="btn  @if (request('module') == 'prom_problem_orders') btn-primary @else btn-default  @endif" href="?{{implode('&', $s)}}">
                                    Prom error orders
                                </a>
                            </div>
                            <div style="float: left">
                                <label>Limit<br>
                                    <select class="form-control" name="limit" style="padding: 6px;" name="product_type"
                                            aria-controls="example">
                                        <option @if(request()->get('limit') == 50) selected @endif>50</option>
                                        <option @if(request()->get('limit') == 100) selected @endif>100</option>
                                        <option @if(request()->get('limit') == 200) selected @endif>200</option>
                                        <option @if(request()->get('limit') == 500) selected @endif>500</option>
                                    </select>
                                </label>
                            </div>
                            <div style="float: left; margin-left: 15px">
                                <label><br>
                                    <input type="text" class="form-control"
                                           @if ($s = request()->get('search')) value="{{$s}}" @endif name="search"
                                           placeholder="Search">
                                </label>
                            </div>

                            <div style="float: left; margin-left: 15px">
                                <br>
                                <button class="btn btn-primary">Filter</button>
                            </div>
                        </form>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <table id="example" class="table table-striped responsive-utilities jambo_table">
                            <thead>
                            <tr class="headings">
                                @foreach(['id', 'level', 'module', 'action', 'message', 'created_at'] as $item)
                                    <th class="sorting">
                                        <?php
                                        $sort = request()->all();
                                        $sort['sort'] = $item;
                                        if (request()->get('dest') == 'asc') {
                                            $sort['dest'] = 'desc';
                                        } else {
                                            $sort['dest'] = 'asc';
                                        }
                                        $s = [];
                                        foreach ($sort as $key => $val) {
                                            $s[] = $key . '=' . $val;
                                        }
                                        ?>
                                        <a style="color: #fff;" href="?{{implode('&', $s)}}">{{ucfirst($item)}}</a>
                                    </th>
                                @endforeach
                                <th></th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($logs as $log)
                                <tr class="even pointer">
                                    <td>{{$log->id}}</td>
                                    <td>{{$log->level}}</td>
                                    <td>{{$log->module}}</td>
                                    <td>{{$log->action}}</td>
                                    <td>{{$log->message}}</td>
                                    <td>{{$log->created_at}}</td>
                                    <td> <button type="button" class="btn btn-info" data-dismiss="modal" data-toggle="modal"
                                                 data-target="#modal-{{$log->id}}"> view
                                        </button>
                                    <td>
                                        @if ($log->params)
                                            <button type="button" data-params="{{json_encode($log->params)}}" class="btn btn-xs btn-info modal-show">
                                                params
                                            </button>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="box-pagination col-md-12 col-sm-6 text-left">
                            {!! $logs->appends(['search' => \Illuminate\Support\Facades\Input::get('search'),'module' => \Illuminate\Support\Facades\Input::get('module')])->render() !!}
                        </div>

                        @foreach($logs as $log)
                            <div id="modal-{{$log->id}}" class="modal fade" role="dialog">
                                <div class="modal-dialog">

                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">×</button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="panel">
                                                {{dump($log->data)}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>

            <br/>
            <br/>
            <br/>

        </div>
    </div>
@stop
@include('log::modals.modal')
@section('javascript')
    <script type="text/javascript">
        $('.modal-show').on('click', function(e) {
            e.preventDefault();
            var modal = $('#log-modal');
            modal.find('.modal-body').html($(this).attr('data-params'));
            modal.modal('show')
        })
    </script>
@stop

