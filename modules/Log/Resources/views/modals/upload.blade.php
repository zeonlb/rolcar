<!-- Create navigation modal -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Upload files</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">

                        <p>Drag multiple files to the box below for multi upload or click to select files.
                            This is for demonstration purposes only, the files are not uploaded to any
                            server.</p>

                        <div class="dropzone dz-clickable" id="myDrop">
                            <div class="dz-default dz-message" data-dz-message="">
                                <span>Drop files here to upload</span>
                            </div>
                        </div>

                        <br/>
                        <br/>
                        <br/>
                        <br/>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
    </div>
</div>