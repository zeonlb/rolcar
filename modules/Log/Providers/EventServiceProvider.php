<?php

namespace Modules\Log\Providers;

use Exception;
use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;
use Modules\Log\Entities\Log as LogModel;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [

    ];

    /**
     * Register any other events for your application.
     *
     * @param DispatcherContract $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);
        Log::listen(function ($level, $message, $context) {
            if ($message instanceof Exception) {
                $context['action'] = $context['action'] ?? 'fatal';
                $context['module'] = $context['module'] ?? 'system';
                $params = $context['params'] ?? [];
                $context['params'] = array_merge($params, ['url' => request()->fullUrl(), 'file' => $message->getFile(), 'line' => $message->getLine(), 'trace' => $message->getTraceAsString()]);
                $message = $message->getMessage();
            }
            if (Schema::hasTable('system_logs')) {
                LogModel::create([
                    'level' => $level,
                    'message' => $message,
                    'action' => $context['action'] ?? '',
                    'data' => $context['params'] ?? [],
                    'module' => $context['module'] ?? '',
                ]);
            }
            return true;
        });
    }
}
