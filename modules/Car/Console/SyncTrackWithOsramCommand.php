<?php namespace Modules\Car\Console;


use Illuminate\Console\Command;
use Modules\Car\Entities\CaseQuestion;
use Modules\Car\Entities\CModel as Model;
use Illuminate\Support\Facades\DB;
use Modules\Car\Entities\LampJoinQuestion;
use Modules\Car\Entities\Lamps;
use Modules\Car\Entities\Mark;
use Modules\Car\Entities\Modification;
use Modules\Car\Entities\Motor;
use Modules\Shop\Entities\Attribute;
use Modules\Shop\Entities\AttributeOption;

class SyncTrackWithOsramCommand extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'car:osram-truck-sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Synchronize Products with osram.';
    private $countVolNotFound;

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        ini_set('memory_limit', '-1');

        try {
            $attr = Attribute::where('code', 'tsokol')->first();

            $marks = DB::table('osram_truck.marks')->select('*')->get();
            foreach ($marks as $mark) {
                $existingMark = Mark::where('name', 'like', $mark->m_name)->first();
                if (!$existingMark) {
                    $existingMark  = Mark::create(['name' => $mark->m_name, 'code' => generate_code($mark->m_name)]);
                }

                $models = DB::table('osram_truck.models')->where('mark_id', $mark->m_id)->get();


                foreach ($models as $model) {
                    echo PHP_EOL.'START processing model '.$model->name;
                    $existingModel = Model::firstOrCreate([
                        'name' => $model->name,
                        'code' => generate_code($model->code),
                        'lib_car_transport_id' => 3,
                        'lib_car_mark_id' => $existingMark->id
                    ]);

                    $existingModify = Modification::firstOrCreate([
                        'name' => $model->name,
                        'code' => generate_code($model->code),
                        'lib_car_type_id' => 12,
                        'lib_car_model_id' => $existingModel->id
                    ]);

                    $this->processFound($existingModify, $model, $attr);
                }
            }
        } catch (\Exception $e) {
            echo "<pre>"; print_r([$e->getFile(), $e->getLine(), $e->getMessage()]); echo "</pre>";die;
        }
    }




    private function processFound($modification, $model, $attr)
    {
        $motors = DB::table('osram_truck.motors')
            ->where('model_id', $model->id)
            ->get();
        $map = [
            'Low beam' => 'ft2',
            'High beam' => 'ft4',
            'Parking light' => 'ft13',
            'Fog lamps' => 'ft8',
            'Front direction indicator' => 'ft7',
            'Side direction indicator' => 'ft1',
            'Rear direction indicator' => 'bt7',
            'Tail light' => 'bt8',
            'Stop lamp' => 'bt2',
            'Back-up lamp' => 'bt5',
            'Rear fog lamp' => 'bt3',
            'License plate light' => 'bt4',
            'Auxiliary stop light' => 'bt1',
            'Interior light' => 'it4',
            'Boot light' => 'it7',
            'Reading light' => 'it3',
            'Glove compartment light' => 'it2',
            'Surround light' => 'ft9',
            'Daytime running light' => 'ft6',
            'Cornering light' => 'ft5',
            'Entry light' => 'it8',
            'Footwell light' => 'it1',
            'Door light' => 'it5',
            'Door safety light' => 'it6',
            'Additional high beam headlamp' => 'ft11',
            'Side marker lamp' => 'ft12',
            'Side light' => 'ft3',
            'Engine compartment light' => 'it9',
//            'Fog light unit'
        ];
        $tsokols = array_column($attr->options->toArray(), 'code', 'id');
        foreach ($motors as $motor) {
            $mData = [
                'lib_car_modification_id' => $modification->id,
                'motor' => $motor->type_name,
                'power' => $motor->type_kw,
                'year_start' => substr($motor->type_from, 0, 4),
                'year_end' => substr($motor->type_to, 0, 4),
            ];

            $motorObj = Motor::firstOrCreate($mData);
            $lamps = DB::table('osram_truck.lamps')
                ->where('motor_type_id', $motor->type_id)
                ->get();
            $options = [];

            foreach ($lamps as $k => $lamp) {
                $lampType = $lamp->lamp_name;
                if(!isset($map[$lampType])) {
                    continue;
                }
                $options[$map[$lampType]] = [];
                $tech = DB::table('osram_truck.tech')
                    ->where('osram_truck.tech.lamp_id', $lamp->id)
                    ->get();
                if (count($tech) > 1) {
                    foreach ($tech as $t) {
                        $tecReqdata = json_decode($t->technology, 1);
                        $tecResdata = json_decode($t->response, 1)['result'];
                        $question = $tecReqdata['technology_name'];
                        $mesCode = md5(strtolower(strtr(trim($question), [' ' => '_'])));
                        $question = CaseQuestion::firstOrCreate(
                            ['code' => $mesCode, 'message' => $question]
                        );
                        $currentTech = current($tecResdata);
                        $voltageReg = "#([0-9]{1,3}V)#";
                        $voltage = [];
                        if (preg_match($voltageReg, $currentTech['lamp_info'], $out)) {
                            $out = array_filter($out, 'strtoupper');
                            $voltage = array_unique($out);
                        } else {
                            if (!is_int(strpos($currentTech['osram_bestnr'], 'LEDDRL')) &&
                                $currentTech['osram_bestnr'] != 'LED' &&
                                $currentTech['lamp_info'] != 'Entry1'
                            ) {
                                echo "<pre>";
                                print_r([$motor, $modification, $currentTech]);
                                echo "</pre>";
                                $this->countVolNotFound++;
                                echo ' =========---     Not Found VOLTAGE' . PHP_EOL;
                            }
                        }
                        $ts = strtr(strtolower($currentTech['osram_ece']), [' ' => '_',',' => '']);
                        if (!$ts) {
                            if ('led' == strtolower($currentTech['osram_bestnr'])) {
                                $currentTech['osram_ece'] = 'LED';
                                $ts = 'led';
                            } elseif (is_int(strpos($currentTech['osram_bestnr'], 'LEDDRL'))) {
                                $currentTech['osram_ece'] = 'LEDDRL';
                                $ts = 'leddrl';
                            }  elseif (is_int(strpos($currentTech['osram_bestnr'], 'LEDFOG'))) {
                                $currentTech['osram_ece'] = 'LEDFOG';
                                $ts = 'ledfog';
                            } elseif ($currentTech['osram_bestnr'] == 'Entry1') {
                                $currentTech['osram_ece'] = 'LED';
                                $ts = 'led';
                            } else {
                                continue;
                            }
                        }

                        if (!$currentTech['osram_ece'] || !$ts) {
                            echo 'NOT FOUND LAMP';
                            echo "<pre>"; print_r($currentTech); echo "</pre>";
                            continue;
                        }


                        LampJoinQuestion::firstOrCreate([
                            'lamp_type' => $map[$lampType],
                            'lamp_type_question_id' => $question->id,
                            'motor_id' => $motorObj->id,
                            'tsokol' => $ts
                        ]);
                        AttributeOption::firstOrCreate([
                            'name' => $currentTech['osram_ece'],
                            'code' => $ts,
                            'shop_attribute_id' => $attr->id,
                        ]);
                        $options[$map[$lampType]][$ts] = $voltage;
                    }

                } else {
                    if (!isset(current($tech)->response)) {
                        echo 'ERROR: <pre>'; print_r($tech, 1); echo "</pre>";
                        continue;
                    }
                    $tecResdata = json_decode(current($tech)->response, 1)['result'];
                    foreach ($tecResdata as $t) {
                        $voltageReg = "#([0-9]{1,3}V)#";
                        $voltage = [];
                        if (preg_match($voltageReg, $t['lamp_info'], $out)) {
                            $out = array_filter($out, 'strtoupper');
                            $voltage = array_unique($out);
                        } else {
                            if (
                                !is_int(strpos($t['osram_bestnr'], 'LEDDRL')) &&
                                !is_int(strpos($t['osram_bestnr'], 'LEDFOG')) &&
                                $t['osram_bestnr'] != 'LED' &&
                                $t['lamp_info'] != 'Entry1'
                            ) {
                                echo "<pre>";
                                print_r([$motor, $modification, $t]);
                                echo "</pre>";
                                $this->countVolNotFound++;
                                echo ' =========+++     Not Found VOLTAGE' . PHP_EOL;
                            }
                        }
                        $ts = strtr(strtolower($t['osram_ece']), [' ' => '_', ',' => '']);
                        if (!$ts) {
                            if ('led' == strtolower($t['osram_bestnr'])) {
                                $t['osram_ece'] = 'LED';
                                $ts = 'led';
                            } elseif (is_int(strpos($t['osram_bestnr'], 'LEDDRL'))) {
                                $t['osram_ece'] = 'LEDDRL';
                                $ts = 'leddrl';
                            }  elseif (is_int(strpos($t['osram_bestnr'], 'LEDFOG'))) {
                                $t['osram_ece'] = 'LEDFOG';
                                $ts = 'ledfog';
                            } elseif ($t['osram_bestnr'] == 'Entry1') {
                                $t['osram_ece'] = 'LED';
                                $ts = 'led';
                            }
                        }
                        if (!$t['osram_ece'] || !$ts) {
                            echo 'NOT FOUND LAMP';
                            echo "<pre>"; print_r($t); echo "</pre>";
                            continue;
                        }
                        AttributeOption::firstOrCreate([
                            'name' => $t['osram_ece'],
                            'code' => $ts,
                            'shop_attribute_id' => $attr->id,
                        ]);
                        $options[$map[$lampType]][$ts] = $voltage;
                    }
                }

            }
            Lamps::create(
                ['lib_car_motor_id' => $motorObj->id,
                    'options' => json_encode($options)]
            );

        }

        echo PHP_EOL.'Modification has been finished: '.$modification->name.PHP_EOL;
    }


    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
        ];
    }

}
