<?php namespace Modules\Car\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Modules\Car\Entities\CaseQuestion;
use Modules\Car\Entities\LampJoinQuestion;
use Modules\Car\Entities\Lamps;
use Modules\Car\Entities\Mark;
use Modules\Car\Entities\Modification;
use Modules\Car\Entities\Motor;
use Modules\Car\Mangers\TypeLampManager;
use Modules\Shop\Entities\Attribute;
use Modules\Shop\Entities\AttributeOption;
use Modules\Shop\Entities\ProductOptions;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class SyncWithOsramCommand extends Command
{

    public $countVolNotFound = 0;
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'car:osram-sedan-sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Synchronize Products with osram.';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        ini_set('memory_limit', '-1');

        try {
            DB::delete('delete from lib_car_motor_j_questions');
            DB::delete('delete from lib_car_lamp_type_questions');
            DB::delete('delete from lib_car_motor');
            DB::delete('delete from lib_car_motor_lamps');
            $attr = Attribute::where('code', 'tsokol')->first();
            ProductOptions::where('shop_attribute_id', $attr->id)->delete();
            AttributeOption::where('shop_attribute_id', $attr->id)->delete();

            $replace_pairs = ['_' => '', '[' => '', ']' => '', '(' => '', ')' => '', ' ' => '','mercedes'=>''];
            foreach ($replace_pairs as $k => $v) {
                DB::update("UPDATE osram.models as t1 SET t1.`name`= REPLACE(t1.`name`, '$k', '$v')");
            }
            $modifications = Modification::with('model.mark')
                ->where('osram_code', '<>', 'NULL')
                ->get();
            echo "TOTAL COUNT : " . count($modifications) . PHP_EOL;
            $foundCount = $notFound = 0;
            foreach ($modifications as $modification) {
                $code = strtolower($modification->model->mark->code . trim($modification->osram_code));
                $code = strtr($code,
                    $replace_pairs
                );
                $model = DB::table('osram.models')
                    ->select('osram.models.*')
                    ->join('osram.marks', 'osram.marks.m_id', '=', 'osram.models.mark_id')
                    ->where('osram.models.code', 'like', "%$code%")
                    ->first();

                if ($model) {

                    $this->processFound($modification, $model, $attr);
                    $foundCount++;
                } else {
                    echo $modification->model->mark->name . ' - ' . $modification->model->name . PHP_EOL;
                    $notFound++;
                    $ndCode[] = $code;
                }
            }
            echo "FOUND COUNT : " . ($foundCount) . PHP_EOL;
            echo "NOT FOUND COUNT : " . ($notFound) . PHP_EOL;
//            echo "<pre>"; print_r($ndCode); echo "</pre>"; die;
        } catch (\Exception $e) {
            echo "<pre>";
            print_r([$e->getFile(), $e->getLine(), $e->getMessage()]);
            echo "</pre>";
            die;
        }

        die;
    }


    private function processFound($modification, $model, $attr)
    {
        $motors = DB::table('osram.motors')
            ->where('osram.motors.model_id', $model->id)
            ->get();
        $map = [
            'Low beam' => 'ft2',
            'High beam' => 'ft4',
            'Parking light' => 'ft3',
            'Fog lamps' => 'ft8',
            'Front direction indicator' => 'ft7',
            'Side direction indicator' => 'ft1',
            'Rear direction indicator' => 'bt7',
            'Tail light' => 'bt8',
            'Stop lamp' => 'bt2',
            'Back-up lamp' => 'bt5',
            'Rear fog lamp' => 'bt3',
            'License plate light' => 'bt4',
            'Auxiliary stop light' => 'bt1',
            'Interior light' => 'it4',
            'Boot light' => 'it7',
            'Reading light' => 'it3',
            'Glove compartment light' => 'it2',
            'Surround light' => 'ft9',
            'Daytime running light' => 'ft6',
            'Cornering light' => 'ft5',
            'Entry light' => 'it8',
            'Footwell light' => 'it1',
            'Door light' => 'it5',
            'Door safety light' => 'it6',
            'Additional high beam headlamp' => 'ft11',
//            'Side marker lamp',
//            'Side light',
            'Engine compartment light' => 'it9',
//            'Fog light unit'
        ];
        foreach ($motors as $motor) {
            $mData = [
                'lib_car_modification_id' => $modification->id,
                'motor' => $motor->type_name,
                'power' => $motor->type_kw,
                'year_start' => substr($motor->type_from, 0, 4),
                'year_end' => substr($motor->type_to, 0, 4),
            ];

            $tsokols = array_column($attr->options->toArray(), 'code', 'id');
            $motorObj = Motor::create($mData);
            $lamps = DB::table('osram.lamps')
                ->where('osram.lamps.motor_type_id', $motor->type_id)
                ->get();
            $options = [];
            foreach ($lamps as $k => $lamp) {
                $lampType = $lamp->lamp_name;
                if (!isset($map[$lampType])) {
                    continue;
                }
                $options[$map[$lampType]] = [];
                $tech = DB::table('osram.tech')
                    ->where('osram.tech.lamp_id', $lamp->id)
                    ->get();

                if (count($tech) > 1) {
                    $voltageReg = "#([0-9]{1,3}V)#";

                    foreach ($tech as $t) {
                        $tecReqdata = json_decode($t->technology, 1);
                        $tecResdata = array_values(json_decode($t->response, 1));
                        $question = $tecReqdata['technology_name'];
                        $mesCode = md5(strtolower(strtr(trim($question), [' ' => '_', ',' => ''])));
                        $question = CaseQuestion::firstOrCreate(
                            ['code' => $mesCode, 'message' => $question]
                        );
                        $voltage = [];

                        $currentTech = $tecResdata[0];
                        if (preg_match($voltageReg, $currentTech['lamp_info'], $out)) {
                            $out = array_filter($out, 'strtoupper');
                            $voltage = array_unique($out);
                        } else {
                            if (!is_int(strpos($currentTech['osram_bestnr'], 'LEDDRL')) &&
                                $currentTech['osram_bestnr'] != 'LED' &&
                                $currentTech['lamp_info'] != 'Entry1'
                            ) {
                                echo "<pre>";
                                print_r([$motor, $modification, $currentTech]);
                                echo "</pre>";
                                $this->countVolNotFound++;
                                echo ' =========---     Not Found VOLTAGE' . PHP_EOL;
                            }
                        }
                        $ts = strtr(strtolower($currentTech['osram_ece']), [' ' => '_', ',' => '']);
                        if (!$ts) {
                            if ('led' == strtolower($currentTech['osram_bestnr'])) {
                                $currentTech['osram_ece'] = $currentTech['osram_bestnr'];
                                $ts = 'led';
                            } elseif (is_int(strpos($currentTech['osram_bestnr'], 'LEDDRL'))) {
                                $currentTech['osram_ece'] = 'LEDDRL';
                                $ts = 'leddrl';
                            } elseif ($currentTech['osram_bestnr'] == 'Entry1') {
                                $currentTech['osram_ece'] = 'LED';
                                $ts = 'led';
                            } else {
                                continue;
                            }
                        }

                        LampJoinQuestion::firstOrCreate([
                            'lamp_type' => $map[$lampType],
                            'lamp_type_question_id' => $question->id,
                            'motor_id' => $motorObj->id,
                            'tsokol' => $ts
                        ]);
                        AttributeOption::firstOrCreate([
                            'name' => $currentTech['osram_ece'],
                            'code' => $ts,
                            'shop_attribute_id' => $attr->id,
                        ]);
                        $options[$map[$lampType]][$ts] = $voltage;
                    }

                } else {
                    if (!isset(current($tech)->response)) {
                        echo "ERROR: <pre>";
                        print_r($tech, 1);
                        echo "</pre>";
                        continue;
                    }
                    $tecResdata = json_decode(current($tech)->response, 1);
                    foreach ($tecResdata as $t) {
                        $voltageReg = "#([0-9]{1,3}V)#";
                        $voltage = [];
                        if (preg_match($voltageReg, $t['lamp_info'], $out)) {
                            $out = array_filter($out, 'strtoupper');
                            $voltage = array_unique($out);
                        } else {
                            if (!is_int(strpos($t['osram_bestnr'], 'LEDDRL')) &&
                                $t['osram_bestnr'] != 'LED' &&
                                $t['lamp_info'] != 'Entry1'
                            ) {
                                echo "<pre>";
                                print_r([$motor, $modification, $t]);
                                echo "</pre>";
                                $this->countVolNotFound++;
                                echo ' =========+++     Not Found VOLTAGE' . PHP_EOL;
                            }
                        }
                        $ts = strtr(strtolower($t['osram_ece']), [' ' => '_', ',' => '']);
                        if (!$ts) {
                            if ('led' == strtolower($t['osram_bestnr'])) {
                                $t['osram_ece'] = $t['osram_bestnr'];
                                $ts = 'led';
                            } elseif (is_int(strpos($t['osram_bestnr'], 'LEDDRL'))) {
                                $t['osram_ece'] = 'LEDDRL';
                                $ts = 'leddrl';
                            }elseif ($t['lamp_info'] == 'Entry1') {
                                $t['osram_ece'] = 'LED';
                                $ts = 'led';
                            }
                        }
                        if (!$t['osram_ece'] || !$ts) {
                            continue;
                        }
                        AttributeOption::firstOrCreate([
                            'name' => $t['osram_ece'],
                            'code' => $ts,
                            'shop_attribute_id' => $attr->id,
                        ]);
                        $options[$map[$lampType]][$ts] = $voltage;
                    }
                }
            }
            Lamps::create(
                ['lib_car_motor_id' => $motorObj->id,
                    'options' => json_encode($options)]
            );
//            echo "<pre>!!!!!!!!";
//            print_r($options);
//            echo "</pre>";
//            die;
        }
    }

    public function updateModelCode()
    {
        $models = DB::table('osram.models')
            ->select(['*', 'osram.models.id as model_id'])
            ->join('osram.marks', 'osram.marks.m_id', '=', 'osram.models.mark_id')
            ->get();
        foreach ($models as $model) {
            $code = strtr(strtolower($model->m_name . $model->name), [' ' => '_']);
            DB::table('osram.models')->where('id', $model->model_id)->update(['code' => $code]);
//            echo "<pre>"; print_r($model); echo "</pre>";die;
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
        ];
    }

}
