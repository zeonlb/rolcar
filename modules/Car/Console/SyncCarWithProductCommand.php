<?php namespace Modules\Car\Console;

use Illuminate\Console\Command;
use Modules\Car\Mangers\TypeLampManager;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class SyncCarWithProductCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'car:products-sync';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Synchronize Products with Cars by lamp types.';

	/**
	 * Create a new command instance.
	 *
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		(new TypeLampManager())->syncMotorsWithProducts();
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
		];
	}

}
