<?php namespace Modules\Car\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Modules\Car\Mangers\TypeLampManager;

class FixCodesCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'car:fix-code';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Synchronize Products with Cars by lamp types.';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        foreach ([
                     'lib_car_transport',
                     'lib_car_mark',
                     'lib_car_model',
                     'lib_car_modification',
                     'shop_product'
                 ] as $table) {
            DB::update("UPDATE $table SET code=REPLACE(code, '-', '_')");
            DB::update("UPDATE $table SET code=REPLACE(code, '/', '_')");
            DB::update("UPDATE $table SET code=REPLACE(code, '\"', '')");
        }


        $limit = 1000;
        $offset = 0;

        while (true) {
            $query = "SELECT id, options  FROM lib_car_motor_lamps   WHERE options <> '[]' LIMIT $offset,$limit";
            $lamps = DB::select($query);
            if ($lamps) {
                echo $offset . PHP_EOL;
            } else {
                break;
            }
            $offset += $limit;
            foreach ($lamps as $lamp) {
                $options = json_decode($lamp->options, JSON_OBJECT_AS_ARRAY);
                if (json_last_error()) {
                    continue;
                }
                try {
                    foreach ($options as $code => $opt) {
                        $newOpt = [];
                        if (is_array($opt))
                            foreach ($opt as $t => $d) {
                                $old = $t;
                                $t = strtr($old, ['-' => '_', '/' => '_', '\\' => '']);
                                $newOpt[$t] = $d;
                            }
                        $options[$code] = $newOpt;
                    }
                } catch (\Exception $e) {
                    echo "<pre>";
                    print_r($opt);
                    echo "</pre>";
                    die;
                    echo "<pre>";
                    print_r($e->getLine());
                    echo "</pre>";
                    die;
                }


                DB::update('UPDATE lib_car_motor_lamps SET `options` = \'' . json_encode($options) . '\' WHERE id = ' . $lamp->id);

            }


        }
        return "OK";
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
        ];
    }

}
