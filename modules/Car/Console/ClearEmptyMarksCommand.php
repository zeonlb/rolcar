<?php namespace Modules\Car\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Modules\Car\Mangers\TypeLampManager;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ClearEmptyMarksCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'car:clear-empty-marks';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Clear marks without filled lamps.';

	/**
	 * Create a new command instance.
	 *
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        DB::delete('DELETE FROM `lib_car_modification` WHERE (SELECT COUNT(*) FROM lib_car_motor WHERE lib_car_motor.lib_car_modification_id = `lib_car_modification`.id) = 0');
        DB::delete('DELETE FROM `lib_car_model` WHERE (SELECT COUNT(*) FROM lib_car_modification WHERE lib_car_model.id = lib_car_modification.lib_car_model_id) = 0');
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
		];
	}

}
