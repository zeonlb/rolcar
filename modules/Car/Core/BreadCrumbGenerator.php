<?php
namespace Modules\Car\Core;


use Modules\Car\Entities\CModel;
use Modules\Car\Entities\Mark;
use Modules\Car\Entities\Modification;
use Modules\Car\Entities\Motor;
use Modules\Car\Entities\Transport;

class BreadCrumbGenerator {
    public static function make(array $classes)
    {
        $result = [];
        $result['Интернет-магазин автоламп'] =  url('/');
        $result['Подбор по автомобилю'] =  url('/catalog');
        foreach ($classes as $class) {
            switch (true) {
                case $class instanceof Transport :
                    $result[$class->name] =  url('/catalog/marks', ['id' => $class->id]);
                    break;
                case $class instanceof Mark :
                    $result[$class->name] =  url('/catalog/models', ['id' => $class->id]);
                    break;
                case $class instanceof CModel :
                    $result[$class->transport->name] =  url('/catalog/marks', ['id' => $class->transport->id]);
                    $result[$class->mark->name] =  url('/catalog/models', ['id' => $class->mark->id, 'transportId' => $class->transport->id]);
                    $result[$class->name] =  url('/catalog/models', ['id' => $class->id]);
                    break;
                case $class instanceof Modification :
                    $result[$class->model->transport->name] =  url('/catalog/marks', ['id' => $class->model->transport->id]);
                    $result[$class->model->mark->name] =  url('/catalog/models', ['id' => $class->model->mark->id, 'transportId' => $class->model->transport->id]);
                    $result[$class->model->name] =  url('/catalog/modifications', ['id' => $class->model->id]);
                    $result[$class->name] =  url('/catalog/models', ['id' => $class->id]);
                    break;
                case $class instanceof Motor :
                    $result[$class->modification->model->transport->name] =  url('/catalog/marks', ['id' => $class->modification->model->transport->id]);
                    $result[$class->modification->model->mark->name] =  url('/catalog/models', ['id' => $class->modification->model->mark->id, 'transportId' => $class->modification->model->transport->id]);
                    $result[$class->modification->model->name] =  url('/catalog/modifications', ['id' => $class->modification->model->id]);
                    $result[$class->modification->name] =  url('/catalog/motors', ['id' => $class->modification->id]);
                    if ($class->year_end == 0) {
                        $class->year_end = date('Y');
                    }
                    $result[$class->motor." ({$class->year_start}-{$class->year_end} г.)"] =  url('/catalog/lamps', ['id' => $class->id]);
                    break;

            }
        }


        return $result;

    }
}