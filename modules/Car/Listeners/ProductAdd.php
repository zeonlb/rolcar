<?php  namespace Modules\Car\Listeners;


use Illuminate\Support\Facades\Cache;
use Modules\Car\Mangers\CarProductManager;
use Modules\Shop\Entities\Product;

class ProductAdd
{
    public function handle(Product $product, $request)
    {
        Cache::flush();
        $carProductManager =  new CarProductManager();
        if ($request->has('motors')) {
            $motors = array_values($request->get('motors'));
            $carProductManager->store($product->id, $motors);
        }

    }
}