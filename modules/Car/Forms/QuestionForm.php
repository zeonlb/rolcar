<?php namespace Modules\Car\Forms;

use Kris\LaravelFormBuilder\Form;
use Modules\Car\Entities\Mark;
use Modules\Car\Entities\Transport;


class QuestionForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('message', 'text', [
                'rules' => 'required|min:1',
                'label' => 'Question'
            ])
            ->add('code', 'text', [
                'rules' => 'required|min:1',
                'label' => 'Code'
            ])
            ->add('submit', 'submit',
                [
                    'label' => 'Save',
                    'template' => 'admin::vendor.laravel-form-builder.button_modal',
                    'attr' => ['class' => 'btn btn-success']]);
    }
}