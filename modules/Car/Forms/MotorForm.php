<?php namespace Modules\Car\Forms;

use Kris\LaravelFormBuilder\Form;
use Modules\Car\Entities\Mark;
use Modules\Car\Entities\Transport;


class MotorForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('motor', 'text', [
                'rules' => 'required|min:1|max:255',
                'label' => 'Motor'
            ])
            ->add('year_start', 'text', [
                'rules' => 'required|min:1|max:255',
                'label' => 'Start year'
            ])
            ->add('year_end', 'text', [
                'rules' => 'required|min:1|max:255',
                'label' => 'End year'
            ])
            ->add('power', 'text', [
                'rules' => 'min:2|max:255',
                'label' => 'Power'
            ])
            ->add('motor_capacity', 'text', [
                'rules' => 'min:2|max:255',
                'label' => 'Motor Capacity'
            ])
            ->add('speed_100', 'text', [
                'rules' => 'min:2|max:255',
                'label' => 'Speed to 100'
            ])
            ->add('max_speed', 'text', [
                'rules' => 'min:2|max:255',
                'label' => 'Max speed'
            ])
            ->add('consumption', 'text', [
                'rules' => 'min:2|max:255',
                'label' => 'Consumption'
            ])
            ->add('submit', 'submit',
                [
                    'label' => 'Save',
                    'template' => 'admin::vendor.laravel-form-builder.button_modal',
                    'attr' => ['class' => 'btn btn-success']]);
    }
}