<?php namespace Modules\Car\Forms;

use Kris\LaravelFormBuilder\Form;
use Modules\Car\Entities\CModel;
use Modules\Car\Entities\Type;


class ModificationForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('name', 'text', [
                'rules' => 'required|min:1',
                'label' => 'Name'
            ])
            ->add('code', 'text', ['rules' => 'required|min:1'])
            ->add('osram_code', 'text', ['rules' => 'required|min:1'])
            ->add('lib_car_type_id', 'entity', [
                'class' => 'Modules\Car\Entities\Type',
                'label' => 'Car type',
                'rules' => 'required',
                'property' => 'name',
                'query_builder' => function (Type $entity) {
                    return $entity->all();
                }
            ])
            ->add('submit', 'submit',
                [
                    'label' => 'Save',
                    'template' => 'admin::vendor.laravel-form-builder.button_modal',
                    'attr' => ['class' => 'btn btn-success']]);
    }
}