<?php namespace Modules\Car\Forms;

use Kris\LaravelFormBuilder\Form;
use Modules\Car\Entities\Mark;
use Modules\Car\Entities\Transport;


class ModelForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('name', 'text', [
                'rules' => 'required|min:1',
                'label' => 'Name'
            ])
            ->add('code', 'text', ['rules' => 'required|min:1'])
            ->add('lib_car_mark_id', 'entity', [
                'class' => 'Modules\Car\Entities\Mark',
                'label' => 'Mark',
                'rules' => 'required',
                'property' => 'name',
                'query_builder' => function (Mark $mark) {
                    return $mark->all();
                }
            ])
            ->add('lib_car_transport_id', 'entity', [
                'class' => 'Modules\Car\Entities\Transport',
                'rules' => 'required',
                'label' => 'Transport',
                'property' => 'name',
                'query_builder' => function (Transport $entity) {
                    return $entity->all();
                }
            ])
            ->add('img', 'file',
                ['rules' => 'image'])

            ->add('submit', 'submit',
                [
                    'label' => 'Save',
                    'template' => 'admin::vendor.laravel-form-builder.button_modal',
                    'attr' => ['class' => 'btn btn-success']]);
    }
}