<?php namespace Modules\Car\Forms;

use Kris\LaravelFormBuilder\Form;
use Modules\Car\Entities\Mark;
use Modules\Car\Entities\Transport;


class MarkForm extends Form
{
    public function buildForm()
    {
        $this
            ->setFormOption('enctype', 'multipart/form-data')
            ->add('name', 'text', [
                'rules' => 'required|min:1',
                'label' => 'Name'
            ])
            ->add('code', 'text',
                ['rules' => 'required|min:1'])

            ->add('img', 'file',
                ['rules' => 'image'])

            ->add('submit', 'submit',
                [
                    'label' => 'Save',
                    'template' => 'admin::vendor.laravel-form-builder.button_modal',
                    'attr' => ['class' => 'btn btn-success']]);
    }
}