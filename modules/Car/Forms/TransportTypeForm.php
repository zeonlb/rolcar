<?php namespace Modules\Car\Forms;

use Kris\LaravelFormBuilder\Form;
use Modules\Car\Entities\Mark;
use Modules\Car\Entities\Transport;


class TransportTypeForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('name', 'text', [
                'rules' => 'required|min:1',
                'label' => 'Name'
            ])
            ->add('code', 'text',
                ['rules' => 'required|min:1'])
            ->add('submit', 'submit',
                [
                    'label' => 'Save',
                    'template' => 'admin::vendor.laravel-form-builder.button_modal',
                    'attr' => ['class' => 'btn btn-success']]);
    }
}