 @extends('admin::layouts.master')

@section('content')
    <h1>Marks</h1>
    <div class="col-md-12 text-right">
        <div class="col-md-7"></div>
        <div class="col-md-5">
            @if (isset($backBtnUrl))
                <a href="{{$backBtnUrl}}">
                    <button type="button" class="btn btn-default" >
                        Back
                    </button>
                </a>
            @endif
            <button type="button" class="btn btn-warning" id="car-entity-edit">
                Edit
            </button>
            <a href="{{route('admin.car.mark.create', ['id' => $id])}}">
                <button type="button" class="btn btn-success">
                    Create mark
                </button>
            </a>
            <a href="#">
                <button type="button" class="btn btn-success" data-toggle="modal" data-target=".bs-example-modal-lg">
                    Create model
                </button>
                @include('admin::partials.modals.create_entity')
            </a>
        </div>
    </div>
    <div class="car-types col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>{{$transport->name}}</h2>
                <div class="nav navbar-right panel_toolbox">
                    <form><input name="search" placeholder="search" type="text" value="{{$search}}" class="form-control"></form>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                @foreach($marks as  $mark)
                    <div class="col-md-2">
                        <a href="{{url('admin/car/model/show', ['id' => $mark->id, 'transport_id' => $mark->lib_car_transport_id])}}">
                            <h5 sid="{{$mark->id}}">
                                {{$mark->name}}
                            </h5>
                        </a>
                    </div>
                @endforeach
                <div class="clearfix"></div>
            </div>
        </div>
     </div>
        <div class="clearfix"></div>
        @stop

        @section('javascript')
            <!-- bootstrap progress js -->
            <script src="{{Module::asset('admin:js/progressbar/bootstrap-progressbar.min.js')}}"></script>
            <script src="{{Module::asset('admin:js/nicescroll/jquery.nicescroll.min.js')}}"></script>
            <script>
                $('document').ready(function(){
                    $('#car-entity-edit').on('click', function(e){
                        e.preventDefault();
                        $('.x_content h5').each(function(index)
                        {
                            var id = $(this).attr('sid');
                            var html = '&nbsp&nbsp<span>';
                            html += "<a class='edit' href='/admin/car/mark/"+id+"/edit/?transport_id={{$id}}'><i class='glyphicon glyphicon-edit'></i></a>";
                            html += '&nbsp<form class="car-entity-delete-form" method="POST" action="/admin/car/mark/'+id+'">';
                            html += '<i class="entity-delete glyphicon glyphicon-trash"></i>';
                            html += '<input type="hidden" name="id" value="'+id+'" />';
                            html += '<input type="hidden" name="_method" value="DELETE" />';
                            html += '<input type="hidden" name="_token" value="{{csrf_token()}}" />';
                            html += '</form>';
                            html += '</span>';
                            $(this).parent().after(html);
                        });
                    });
                });
            </script>
        @stop

        @section('style')
            <style>
                .x_content span {
                    position: relative;
                    top: 8px;
                    left: 8px;
                }
                .x_content h5 {
                    float: left;
                }
                .car-types {
                    margin-top: 50px;
                }
            </style>
@stop
