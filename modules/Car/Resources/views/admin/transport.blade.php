@extends('admin::layouts.master')

@section('content')
    <h1>Transport</h1>
    <div class="col-md-12">
        <div class="col-md-6"></div>
        <div class="text-right col-md-6">
            <button type="button" class="btn btn-warning" id="car-entity-edit">
                Edit
            </button>
            <a href="{{route('admin.car.transport.create')}}">
                <button type="button" class="btn btn-success">
                    Create new transport
                </button>
            </a>
            <a href="{{route('admin.car.lamp.sync')}}">
                <button type="button" class="btn btn-primary">
                    Synchronize
                </button>
            </a>
            <a href="{{route('admin.car.catalog.export')}}">
                <button type="button" class="btn btn-info">
                    Export
                </button>
            </a>

        </div>
    </div>
    <div class="car-types col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Type auto</h2>

                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                @foreach($transports as  $transport)
                    <div class="col-md-4">
                        <a href="{{route('admin.car.mark.show', ['id' => $transport->id])}}">
                            <h3 sid="{{$transport->id}}">
                                {{$transport->name}}
                            </h3>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <br clear="both"/>
    <div class="clearfix"></div>
@stop

@section('javascript')
    <!-- bootstrap progress js -->
    <script src="{{Module::asset('admin:js/progressbar/bootstrap-progressbar.min.js')}}"></script>
    <script src="{{Module::asset('admin:js/nicescroll/jquery.nicescroll.min.js')}}"></script>
    @include('admin::partials.js.form')
    <!-- form validation -->
    <script type="text/javascript">
        $(document).ready(function () {
            // initialize the validator function
            validator.message['date'] = 'not a real date';

            // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
            $('form')
                    .on('blur', 'input[required], input.optional, select.required', validator.checkField)
                    .on('change', 'select.required', validator.checkField)
                    .on('keypress', 'input[required][pattern]', validator.keypress);

            $('.multi.required')
                    .on('keyup blur', 'input', function () {
                        validator.checkField.apply($(this).siblings().last()[0]);
                    });
            $('form').submit(function (e) {
                e.preventDefault();
                $('#html').val($('#editor').html());
                var submit = true;
                // evaluate the form using generic validaing
                if (!validator.checkAll($(this))) {
                    submit = false;
                }

                if (submit)
                    this.submit();
                return false;
            });
        });
    </script>
    <!-- /form validation -->
    <script>
        $('document').ready(function(){
            $('#car-entity-edit').on('click', function(e){
                e.preventDefault();
                if ($('.car-entity-delete-form').length == 0) {
                    $('.x_content h3').each(function (index) {

                        var id = $(this).attr('sid');
                        var html = '&nbsp&nbsp<span>';
                        html += "<a class='edit' href='/admin/car/transport/" + id + "/edit'><i class='glyphicon glyphicon-edit'></i></a>";
                        html += '&nbsp<form class="car-entity-delete-form" method="POST" action="/admin/car/transport/' + id + '">';
                        html += '<i class="entity-delete glyphicon glyphicon-trash"></i>';
                        html += '<input type="hidden" name="id" value="' + id + '" />';
                        html += '<input type="hidden" name="_method" value="DELETE" />';
                        html += '<input type="hidden" name="_token" value="{{csrf_token()}}" />';
                        html += '</form>';
                        html += '</span>';
                        $(this).parent().after(html);

                    });
                }
            });
        });
    </script>
@stop

@section('style')
    <style>
        .car-types {
            margin-top: 50px;
        }
        .x_content h3 {
            float: left;
        }
        .col-md-4 a {
            display: inline-flex;
        }
    </style>
@stop
