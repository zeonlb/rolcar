@extends('admin::layouts.master')

@section('content')
    <h1>Models</h1>
    <div class="col-md-12 text-right">
        <div class="col-md-8"></div>
        <div class="col-md-4">
            @if (isset($backBtnUrl))
                <a href="{{$backBtnUrl}}">
                    <button type="button" class="btn btn-default">
                        Back
                    </button>
                </a>
            @endif
            <button type="button" class="btn btn-warning" id="car-entity-edit">
                Edit
            </button>
            <a href="{{route('admin.car.model.create', ['id' => $id, 'transportId' => $transport_id])}}">

                <button type="button" class="btn btn-success">
                    Create model
                </button>
            </a>
        </div>
    </div>
    <div class="car-types col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>@if (isset($models[0])){{$models[0]->mark->name}}@endif</h2>
                <div class="nav navbar-right panel_toolbox">
                    <form><input name="search" placeholder="search" type="text" value="{{$search}}" class="form-control"></form>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">

                @foreach($models as  $model)
                    <div class="col-md-2">
                        <a href="{{route('admin.car.modification.show', ['id' => $model->id])}}">
                            <h5 sid="{{$model->id}}">
                                {{$model->name}}
                            </h5>
                        </a>
                        @if (isset($filledPercent[$model->id]) && ($filledPercent[$model->id]['percent']) != 0)
                            <?php
                            $percent =  number_format((($filledPercent[$model->id]['percent']) / ($filledPercent[$model->id]['count'])), 1); ?>
                            <span class="@if ($percent < 20) text-danger @elseif($percent > 20 && $percent < 60) text-warning @else text-success @endif">{{$percent}}%    </span>
                        @else
                            <span class="text-danger">0%</span>
                        @endif


                    </div>

                @endforeach
            </div>
        </div>
        @stop

        @section('javascript')
                <!-- bootstrap progress js -->
        <script src="{{Module::asset('admin:js/progressbar/bootstrap-progressbar.min.js')}}"></script>
        <script src="{{Module::asset('admin:js/nicescroll/jquery.nicescroll.min.js')}}"></script>
        @include('admin::partials.js.form')
                <!-- form validation -->
        <script type="text/javascript">
            $(document).ready(function () {
                // initialize the validator function
                validator.message['date'] = 'not a real date';

                // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
                $('form')
                        .on('blur', 'input[required], input.optional, select.required', validator.checkField)
                        .on('change', 'select.required', validator.checkField)
                        .on('keypress', 'input[required][pattern]', validator.keypress);

                $('.multi.required')
                        .on('keyup blur', 'input', function () {
                            validator.checkField.apply($(this).siblings().last()[0]);
                        });
                $('form').submit(function (e) {
                    e.preventDefault();
                    $('#html').val($('#editor').html());
                    var submit = true;
                    // evaluate the form using generic validaing
                    if (!validator.checkAll($(this))) {
                        submit = false;
                    }

                    if (submit)
                        this.submit();
                    return false;
                });
            });
        </script>
        <!-- /form validation -->
        <script>
            $('document').ready(function () {
                $('#car-entity-edit').on('click', function (e) {
                    e.preventDefault();
                    $('.x_content h5').each(function (index) {
                        var id = $(this).attr('sid');
                        var html = '&nbsp&nbsp<span>';
                        html += "<a class='edit' href='/admin/car/model/edit/" + id + "'><i class='glyphicon glyphicon-edit'></i></a>";
                        html += '&nbsp<form class="car-entity-delete-form" method="POST" action="/admin/car/model/destroy/' + id + '">';
                        html += '<i class="entity-delete glyphicon glyphicon-trash"></i>';
                        html += '<input type="hidden" name="id" value="' + id + '" />';
                        html += '<input type="hidden" name="_method" value="DELETE" />';
                        html += '<input type="hidden" name="_token" value="{{csrf_token()}}" />';
                        html += '</form>';
                        html += '</span>';
                        $(this).parent().after(html);
                    });
                });
            });
        </script>
        @stop

        @section('style')
            <style>
                .x_content span {
                    position: relative;
                    top: 8px;
                    left: 8px;
                }

                .x_content h5 {
                    float: left;
                }

                .car-types {
                    margin-top: 50px;
                }

                /*.edit { float: left}*/
            </style>
@stop
