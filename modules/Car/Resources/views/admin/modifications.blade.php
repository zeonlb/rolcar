@extends('admin::layouts.master')

@section('content')
    <h1>Modification</h1>
    <div class="col-md-12 text-right">
        <div class="col-md-6"></div>
        <div class="col-md-6">
            @if (isset($backBtnUrl))
                <a href="{{$backBtnUrl}}">
                    <button type="button" class="btn btn-default" >
                        Back
                    </button>
                </a>
            @endif

                <a href="{{route('admin.car.catalog.mark.export', ['id' => $model->id])}}">
                    <button type="button" class="btn btn-info" >
                        Export
                    </button>
                </a>
            <button type="button" class="btn btn-warning" id="car-entity-edit">
                    Edit
            </button>
            <a href="{{route('admin.car.modification.create', ['id' => $model->id])}}">
                <button type="button" class="btn btn-success" >
                    Create modification
                </button>
            </a>
                <a href="#">
                    <button type="button" class="btn btn-success" data-toggle="modal" data-target=".bs-example-modal-lg">
                        Create transport type
                    </button>
                    @include('admin::partials.modals.create_entity')
                </a>
        </div>
    </div>
    <div class="car-types col-md-12">
        <div class="x_panel">
            <div class="x_title">
            @if (isset($modifications[0]))
                    <h2>{{$modifications[0]->model->name}}</h2>
                @endif
                <div class="nav navbar-right panel_toolbox">
                    <form><input name="search" placeholder="search" type="text" value="{{$search}}" class="form-control"></form>
                </div>
                    <div class="clearfix"></div>
            </div>
            <div class="x_content">
        @if (count($modifications))
        @foreach($modifications as  $modification)
            <div class="col-md-2">
                <a href="{{route('admin.car.motor.show', ['id' => $modification->id])}}">
                    <h5 class="car-entity-header @if ($modification->osram_code) text-danger @endif" sid="{{$modification->id}}">
                        {{$modification->name}} [{{$modification->type->name}}]
                    </h5>
                    @if (isset($filledPercent[$modification->id]))
                        <p class="@if($filledPercent[$modification->id] < 20) text-danger  @elseif($filledPercent[$modification->id] > 20 && $filledPercent[$modification->id] < 60) text-warning @else text-success @endif">{{number_format($filledPercent[$modification->id], 1)}}%</p>
                    @else
                        <p class=" text-danger">0.0%</p>
                    @endif
                </a>
            </div>
        @endforeach
            @endif
                </div>
    </div>
@stop

@section('javascript')
            <!-- bootstrap progress js -->
            <script src="{{Module::asset('admin:js/progressbar/bootstrap-progressbar.min.js')}}"></script>
            <script src="{{Module::asset('admin:js/nicescroll/jquery.nicescroll.min.js')}}"></script>
            @include('admin::partials.js.form')
            <!-- form validation -->
            <script type="text/javascript">
                $(document).ready(function () {
                    // initialize the validator function
                    validator.message['date'] = 'not a real date';

                    // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
                    $('form')
                            .on('blur', 'input[required], input.optional, select.required', validator.checkField)
                            .on('change', 'select.required', validator.checkField)
                            .on('keypress', 'input[required][pattern]', validator.keypress);

                    $('.multi.required')
                            .on('keyup blur', 'input', function () {
                                validator.checkField.apply($(this).siblings().last()[0]);
                            });
                    $('form').submit(function (e) {
                        e.preventDefault();
                        $('#html').val($('#editor').html());
                        var submit = true;
                        // evaluate the form using generic validaing
                        if (!validator.checkAll($(this))) {
                            submit = false;
                        }

                        if (submit)
                            this.submit();
                        return false;
                    });
                });
            </script>
            <!-- /form validation -->

            <script>
                $('document').ready(function(){
                    $('#car-entity-edit').on('click', function(e){
                        e.preventDefault();
                        $('.x_content h5').each(function(index)
                        {
                            var id = $(this).attr('sid');
                            var html = '&nbsp&nbsp<span>';
                            html += "<a class='edit' href='/admin/car/modification/"+id+"/edit'><i class='glyphicon glyphicon-edit'></i></a>";
                            html += '&nbsp<form class="car-entity-delete-form" method="POST" action="/admin/car/modification/'+id+'">';
                            html += '<i class="entity-delete glyphicon glyphicon-trash"></i>';
                            html += '<input type="hidden" name="id" value="'+id+'" />';
                            html += '<input type="hidden" name="_method" value="DELETE" />';
                            html += '<input type="hidden" name="_token" value="{{csrf_token()}}" />';
                            html += '</form>';
                            html += '</span>';
                            $(this).parent().after(html);
                        });
                    });
                });
            </script>

@stop

@section('style')
    <style>
        .car-types {
            margin-top: 50px;
        }
    </style>
@stop
