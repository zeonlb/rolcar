<!-- Create navigation modal -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span
                            aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Create new motor configuration</h4>
            </div>
            <div class="modal-body">

                <form method="GET" action="{{route('admin.car.modifications.create')}}" accept-charset="UTF-8"
                      novalidate=""
                      class="form-horizontal form-label-left">
                    <input name="_token" type="hidden" value="<?= csrf_token() ?>">
                    <input type="hidden" name="lib_car_modification_id" value="{{$modification->id}}"/>

                    <div class="item form-group ">

                        <label for="name" class="control-label col-md-3 col-sm-3 col-xs-12">Name</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input class="form-control" required="required" minlength="2" name="name" type="text"
                                   id="name">
                        </div>

                    </div>
                    <div class="item form-group ">
                        <label for="code" class="control-label col-md-3 col-sm-3 col-xs-12">Code</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input class="form-control" required="required" minlength="2" name="code" type="text"
                                   id="code">
                        </div>
                    </div>
                    <div class="item form-group ">

                        <label for="code" class="control-label col-md-3 col-sm-3 col-xs-12">Year start</label>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input class="form-control" required="required" minlength="2" name="year_start" type="text"
                                   id="code">
                        </div>
                    </div>
                    <div class="item form-group ">
                        <label for="code" class="control-label col-md-3 col-sm-3 col-xs-12">Year end</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input class="form-control" required="required" minlength="2" name="year_end" type="text"
                                   id="code">
                        </div>
                    </div>
                    <br>

                    <div class="modal-footer">
                        <div class="col-md-9">
                            <button class="btn btn-success" type="submit">Save</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>