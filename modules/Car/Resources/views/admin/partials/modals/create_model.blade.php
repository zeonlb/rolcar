<!-- Create navigation modal -->
<div class="modal fade create-model" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Create new model</h4>
                </div>
                <div class="modal-body">
                    {!! form($model_form) !!}
                    </div>
            </div>
    </div>
</div>