@extends('admin::layouts.master')

@section('content')
    <h1>Lamps</h1>
    <div class="col-md-12 text-right">
        <div class="col-md-7"></div>
        <div class="col-md-5">
            @if (isset($backBtnUrl))
                <a href="{{$backBtnUrl}}">
                    <button type="button" class="btn btn-default">
                        Back
                    </button>
                </a>
            @endif
        </div>
    </div>
    <div class="car-types col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>{{$entity->modification->model->mark->name}} {{$entity->modification->model->name}} {{$entity->modification->name}} {{$entity->motor}}
                    ({{$entity->power}})</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?php $count = 0; ?>
                <form action="{{route('admin.car.lamps.update', ['id' => $entity->id])}}" method="POST">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="_method" value="PUT">
                    <div class="item form-group col-xs-12">
                            <div class="col-md-12">
                                @foreach($categories as $category)
                                <label class="col-md-2">{{trans('car::lamps.' . $category->code)}}</label>
                                <div class="col-md-10">
                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                            <select ctype="{{$category->code}}" class="form-control options-select"
                                                    name="options[{{$category->code}}][]"
                                                    multiple>
                                                <?php $count++; ?>
                                                @foreach($attribute->options as $option)
                                                    <option
                                                            @if (isset($selectedOptions[$category->code]) && isset($selectedOptions[$category->code][$option->code]))
                                                            selected
                                                            @endif
                                                            value="{{$option->code}}">{{$option->name}}</option>
                                                @endforeach
                                            </select>
                                            <br>
                                            <div class="cases">
                                                <div>
                                                    @if (isset($cases[$category->code]))
                                                        @foreach($cases[$category->code] as $case)
                                                            <?php $code = md5(microtime() . $case['lamp_type'] . $case['lamp_type_question_id'] . $case['tsokol']) ?>
                                                            <div class="panel">
                                                                <label>Вопрос</label>
                                                                <select required=""
                                                                        name="cases[{{$category->code}}][{{$code}}][qcode]"
                                                                        class="form-control">
                                                                    <option></option>
                                                                    @foreach($questions as $question)
                                                                        <option @if($question['id'] === $case['lamp_type_question_id']) selected
                                                                                @endif
                                                                                value="{{$question['id']}}">{{$question['message']}}</option>
                                                                    @endforeach
                                                                </select>
                                                                <label>Цоколь</label>
                                                                <select required=""
                                                                        name="cases[{{$category->code}}][{{$code}}][tsokol]"
                                                                        class="form-control">
                                                                    <option></option>
                                                                    @foreach($attribute->options as $option)
                                                                        <option
                                                                                @if ($option->code == $case['tsokol'])
                                                                                selected
                                                                                @endif
                                                                                value="{{$option->code}}">{{$option->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                                <br>
                                                                <a href="#"
                                                                   class="delete-case btn btn-danger">Delete</a>
                                                                <hr>
                                                            </div>
                                                        @endforeach
                                                    @endif


                                                    <br>
                                                    <a href="#" ccode="{{$category->code}}"
                                                       class="add-case btn btn-success">Add case</a>
                                                </div>
                                            </div>
                                            <input type="hidden" name="count_positions" value="{{$count}}">
                                        </div>
                                        <div id="voltage-block" class="col-md-6">
                                            @if (isset($selectedOptions[$category->code]))
                                                @foreach($selectedOptions[$category->code] as $type => $voltages)
                                                    @foreach($voltages as $voltage)
                                                        <label>Вольтаж {{$type}}</label>
                                                        <select id="{{$category->code.$type}}"
                                                                name="voltage[{{$category->code}}][{{$type}}][]"
                                                                class="form-control">
                                                            <option></option>
                                                            @foreach($vAttributes->options as $av)
                                                                <option @if(strtr(strtolower($av['code']), ['v' => '']) == strtr(strtolower($voltage), ['v' => ''])) selected
                                                                        @endif
                                                                        value="{{$av['code']}}">{{$av['code']}}</option>
                                                            @endforeach
                                                        </select>
                                                        <hr>
                                                    @endforeach
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                            <div class="col-md-12" style="margin-top: 20px">
                                <label class="col-md-4">Accept for
                                    all {{$entity->modification->model->mark->name}} {{$entity->modification->model->name}} {{$entity->modification->name}}
                                    motors</label>
                                <div class="col-md-6">
                                    <input type="checkbox" value="1" name="accept_for_all_motors">
                                </div>
                            </div>

                            <div class="col-md-12" style="margin-top: 20px">
                                <button class="btn btn-primary" type="submit">Save</button>
                            </div>
                </form>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
@stop

@section('javascript')
    <!-- bootstrap progress js -->
    <script src="{{Module::asset('admin:js/progressbar/bootstrap-progressbar.min.js')}}"></script>
    <script src="{{Module::asset('admin:js/nicescroll/jquery.nicescroll.min.js')}}"></script>
    <script>
        var questions = JSON.parse('{!! json_encode($questions, JSON_HEX_QUOT|JSON_HEX_APOS) !!}');
        var options = JSON.parse('{!! json_encode($attribute->options, JSON_HEX_QUOT|JSON_HEX_APOS) !!}');
        $(document).ready(function () {
            var count = 99999999999;
            $('.add-case').on('click', function (e) {
                e.preventDefault();
                var html = '<div class="panel">';
                html += '<label>Вопрос</label>';
                html += '<select required name="cases[' + $(this).attr('ccode') + '][' + count + '][qcode]" class="form-control">';
                html += '<option></option>';
                for (var i in questions) {
                    html += '<option value="' + questions[i]['id'] + '">' + questions[i]['message'] + '</option>';
                }
                html += '</select>';
                html += '<label>Цоколь</label>';
                html += '<select required name="cases[' + $(this).attr('ccode') + '][' + count + '][tsokol]" class="form-control">';
                html += '<option></option>';
                for (var i in options) {
                    html += '<option value="' + options[i]['code'] + '">' + options[i]['name'] + '</option>';
                }
                html += '</select><br> <a href="#" class="delete-case btn btn-danger">Delete</a> <hr />';
                html += '</div>';
                $(this).parent().before(html);
                count--;
            });

            $('.x_content').on('click', '.delete-case', function (e) {
                e.preventDefault();
                $(this).parent().remove();
            })
        });
    </script>

    <script>
        var voltages = JSON.parse('{!! json_encode($vAttributes, JSON_HEX_QUOT|JSON_HEX_APOS) !!}');
        console.log(voltages)
        $(document).ready(function () {
            $('.options-select').on('change', function () {
                var type = $(this).attr('ctype');
                var chosenLamps = $(this).val();
                var voltageBlock = $(this).parent().parent().find('#voltage-block');

                for (var i in chosenLamps) {
                    if (voltageBlock.find('#' + type + chosenLamps[i]).length) {
                        continue;
                    }
                    var html = ' <label>Вольтаж ' + chosenLamps[i] + '</label>' +
                        '<select id="' + type + chosenLamps[i] + '" name="voltage[' + type + '][' + chosenLamps[i] + '][]"  ' +
                        'class="form-control"><option></option>';
                    for (var j in voltages.options) {
                        html += '<option value="' + voltages.options[j].code + '">' + voltages.options[j].code + '</option>';

                    }
                    html += '</select> <hr>';
                    voltageBlock.append(html)
                }
            })
        });
    </script>
@stop

@section('style')
    <style>
        .x_content span {
            position: relative;
            top: 8px;
            left: 8px;
        }

        .x_content h5 {
            float: left;
        }

        .car-types {
            margin-top: 50px;
        }
    </style>
@stop
