@extends('admin::layouts.master')

@section('content')
    <h1>Motors</h1>
    <div class="col-md-12 text-right">
        <div class="col-md-6"></div>
        <div class="text-right col-md-6">
            @if ($backBtnUrl)
                <a href="{{$backBtnUrl}}">
                    <button type="button" class="btn btn-default">
                        Back
                    </button>
                </a>
            @endif
            <a href="{{route('admin.car.motor.create', ['id' => $modification->id])}}">
                <button type="button" class="btn btn-success">
                    Add motor
                </button>
            </a>
            <a href="{{route('admin.car.lamp.sync')}}">
                <button type="button" class="btn btn-primary">
                    Synchronize
                </button>
            </a>
        </div>
    </div>
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>{{$modification->name}}</h2>

                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table">
                    <thead>
                    <tr>
                        <th>Motor</th>
                        <th>Power</th>
                        <th>Year start</th>
                        <th>Year end</th>
                        <th>Filled</th>
                        <th>Parent</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($motors as  $motor)
                        <tr>
                            <th>{{$motor->motor}}</th>
                            <td>{{$motor->power}} ({{ceil (((float)$motor->power) / 1.3596) }}kw)</td>
                            <td>{{$motor->year_start}}</td>
                            <td>{{$motor->year_end}}</td>
                            <td>{{(int)$motor->filled_percent}}%</td>
                            <td>
                                <select class="form-control parent-choose" data-id="{{$motor->id}}"
                                        name="parent_id[{{$motor->id}}]">
                                    <option value="delete"></option>
                                    @foreach($motors as $pm)
                                        @if ($pm->id != $motor->id)
                                            <option @if ($pm->id == $motor->parent_id) selected
                                                    @endif value="{{$pm->id}}">
                                                {{$pm->motor}} {{$pm->power}} ({{ceil (((float)$pm->power) / 1.3596) }}kw)
                                            </option>
                                        @endif
                                    @endforeach
                                </select>
                            </td>
                            <td>
                                <a class="car-entity-struct"
                                   href="{{route('admin.car.lamps.edit', ['id' => $motor->id])}}"><i
                                            class='glyphicon glyphicon-cog'></i></a>
                                <a class="car-entity-edit"
                                   href="{{route('admin.car.motor.edit', ['id' => $motor->id])}}"><i
                                            class='glyphicon glyphicon-edit'></i></a>
                                <form class="car-entity-delete-form" method="POST"
                                      action="{{route('admin.car.motor.destroy', ['id' => $motor->id])}}">
                                    <input type="hidden" name="_method" value="delete"/>
                                    <input name="_token" type="hidden" value="{{csrf_token()}}">
                                    <i class='entity-delete glyphicon glyphicon-trash'></i>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
@stop

@section('javascript')
    <!-- bootstrap progress js -->
    <script src="{{Module::asset('admin:js/progressbar/bootstrap-progressbar.min.js')}}"></script>
    <script src="{{Module::asset('admin:js/nicescroll/jquery.nicescroll.min.js')}}"></script>
    @include('admin::partials.js.form')
    <!-- form validation -->

    <script>
        $('document').ready(function () {
            $('#car-entity-edit').on('click', function (e) {
                e.preventDefault();
                $('.x_content h3').each(function (index) {
                    var id = $(this).attr('sid');
                    var html = '&nbsp&nbsp<span>';
                    html += "<a class='edit' href='/admin/car/modification/" + id + "/edit'><i class='glyphicon glyphicon-edit'></i></a>";
                    html += '&nbsp<form class="car-entity-delete-form" method="POST" action="/admin/car/modification/' + id + '">';
                    html += '<i class="entity-delete glyphicon glyphicon-trash"></i>';
                    html += '<input type="hidden" name="id" value="' + id + '" />';
                    html += '<input type="hidden" name="_method" value="DELETE" />';
                    html += '<input type="hidden" name="_token" value="{{csrf_token()}}" />';
                    html += '</form>';
                    html += '</span>';
                    $(this).parent().after(html);
                });
            });
            $('.parent-choose').change(function () {
                var mid = $(this).attr('data-id');
                var parent_id = $(this).val();
                if (!parent_id) {
                    parent_id = "";
                }
                console.log(parent_id);
                console.log($(this).attr('data-id'));
                $.ajax({
                    "type": "put",
                    "url": "/admin/car/motor/" + mid,
                    "data": {"parent_id": parent_id},
                    "success": function (response) {
                        if ("success" == response.status) {
                            new PNotify({
                                title: 'Success!',
                                text: 'Successfully updated',
                                type: 'success'
                            });
                        }
                    }
                })
            })
        });
    </script>
@stop

@section('style')
    <style>
        .car-types {
            margin-top: 50px;
        }
    </style>
@stop
