@extends('admin::layouts.master')

@section('content')
    <h1>{{$header}}</h1>
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>{{$header}}</h2>
                @if (isset($backBtnUrl))
                    <div class="text-right">
                        <a href="{{$backBtnUrl}}">
                            <button type="button" class="btn btn-default">
                                Back
                            </button>
                        </a>
                    </div>

                @endif
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <br/><br/>
                {!! form_start($form) !!}
                <div class="" role="tabpanel" data-example-id="togglable-tabs">
                    <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab"
                                                                  data-toggle="tab" aria-expanded="true">General</a>
                        </li>
                        @if (isset($entity->meta))
                            <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab"
                                                                data-toggle="tab" aria-expanded="false">SEO</a>
                        @endif
                    </ul>
                    <div id="myTabContent" class="tab-content">
                        <div role="tabpanel" class="tab-pane fade active in" id="tab_content1"
                             aria-labelledby="home-tab">
                            {!! form($form) !!}
                        </div>
                        @if (isset($entity->meta))
                            <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
                                <span class="section">Meta data</span>

                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="meta_title">Meta
                                        title</label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input id="meta_title" class="form-control col-md-7 col-xs-12"
                                               name="meta_title"
                                               value="{{$entity->meta->meta_title}}"
                                               placeholder="Title" type="text">
                                    </div>
                                </div>

                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="meta_keywords">Meta
                                        keys</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input value="{{$entity->meta->meta_keywords}}" id="tags_1" name="meta_keywords"
                                               type="text" class="tags form-control" value=""/>
                                        <div id="suggestions-container"
                                             style="position: relative; float: left; width: 250px; margin: 10px;"></div>
                                    </div>
                                </div>
                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="meta_redirect_url">Meta
                                        redirect url</label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input placeholder="/new/url" value="{{$entity->meta->meta_redirect_url}}"
                                               type="text" id="meta_redirect_url" name="meta_redirect_url"
                                               class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>

                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="meta_description">Meta
                                        description</label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea placeholder="Your page description" id="meta_description"
                                                  name="meta_description"
                                                  class="form-control col-md-7 col-xs-12">{{$entity->meta->meta_description}}</textarea>
                                    </div>
                                </div>
                                <span class="section">Seo data</span>

                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="meta_description">Seo
                                        text</label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea placeholder="Your seo text" id="seo_text" name="seo_text"
                                                  class="form-control col-md-7 col-xs-12">{{$entity->meta->seo_text}}</textarea>
                                    </div>
                                </div>
                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="meta_description">Seo
                                        text 2</label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea placeholder="Your seo text" id="seo_text_2" name="seo_text_2"
                                                  class="form-control col-md-7 col-xs-12">{{$entity->meta->seo_text_2}}</textarea>
                                    </div>
                                </div>
                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="meta_description">Seo
                                        footer text</label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea placeholder="Your seo footer text" id="seo_footer_text"
                                                  name="seo_footer_text"
                                                  class="form-control col-md-7 col-xs-12">{{$entity->meta->seo_footer_text}}</textarea>
                                    </div>
                                </div>
                                <div class="ln_solid"></div>
                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-3">
                                        <button id="send" type="submit" class="btn btn-success">Save</button>
                                    </div>
                                </div>
                            </div>
                    </div>
                    @endif
                </div>
                {!! form_start($form) !!}
            </div>
        </div>
    </div>
@stop

@section('javascript')
    <!-- bootstrap progress js -->
    <script src="{{Module::asset('admin:js/progressbar/bootstrap-progressbar.min.js')}}"></script>
    <script src="{{Module::asset('admin:js/nicescroll/jquery.nicescroll.min.js')}}"></script>
    @include('admin::partials.js.form')
    <!-- input tags -->
    <script>
        function onAddTag(tag) {
            alert("Added a tag: " + tag);
        }

        function onRemoveTag(tag) {
            alert("Removed a tag: " + tag);
        }

        function onChangeTag(input, tag) {
            alert("Changed a tag: " + tag);
        }

        $(function () {
            $('#tags_1').tagsInput({
                width: 'auto'
            });
        });
    </script>
    <!-- form validation -->
    <script type="text/javascript">
        $(document).ready(function () {
            // initialize the validator function
            validator.message['date'] = 'not a real date';

            // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
            $('form')
                .on('blur', 'input[required], input.optional, select.required', validator.checkField)
                .on('change', 'select.required', validator.checkField)
                .on('keypress', 'input[required][pattern]', validator.keypress);

            $('.multi.required')
                .on('keyup blur', 'input', function () {
                    validator.checkField.apply($(this).siblings().last()[0]);
                });
            $('form').submit(function (e) {
                e.preventDefault();
                $('#html').val($('#editor').html());
                var submit = true;
                // evaluate the form using generic validaing
                if (!validator.checkAll($(this))) {
                    submit = false;
                }

                if (submit)
                    this.submit();
                return false;
            });
        });
    </script>
    <!-- /form validation -->
    <!-- Text editor -->
    <script>
        CKEDITOR.replace('seo_footer_text');
        CKEDITOR.config.htmlEncodeOutput = false;
        CKEDITOR.config.entities = false;
        CKEDITOR.config.basicEntities = false;
        CKEDITOR.config.configentities = false;
        CKEDITOR.config.forceSimpleAmpersand = true;
        CKEDITOR.config.allowedContent = true;
        CKEDITOR.config.extraPlugins = 'font';
        CKEDITOR.config.skin = 'bootstrapck';
    </script>
    <!-- /Text editor -->
    <!-- Text editor -->
    <script>
        CKEDITOR.replace('seo_text');
        CKEDITOR.config.htmlEncodeOutput = false;
        CKEDITOR.config.entities = false;
        CKEDITOR.config.basicEntities = false;
        CKEDITOR.config.configentities = false;
        CKEDITOR.config.forceSimpleAmpersand = true;
        CKEDITOR.config.allowedContent = true;
        CKEDITOR.config.extraPlugins = 'font';
        CKEDITOR.config.skin = 'bootstrapck';
    </script>
    <!-- /Text editor -->
    <!-- Text editor -->
    <script>
        CKEDITOR.replace('seo_text_2');
        CKEDITOR.config.htmlEncodeOutput = false;
        CKEDITOR.config.entities = false;
        CKEDITOR.config.basicEntities = false;
        CKEDITOR.config.configentities = false;
        CKEDITOR.config.forceSimpleAmpersand = true;
        CKEDITOR.config.allowedContent = true;
        CKEDITOR.config.extraPlugins = 'font';
        CKEDITOR.config.skin = 'bootstrapck';
    </script>
    <!-- /Text editor -->
@stop

@section('style')
    <style>
        .car-types {
            margin-top: 50px;
        }
    </style>
@stop
