<div class="schema-lamp">
    <div class="image-schema-block" id="sedan-schema-inside">
        <img src="{{Theme::asset('images/sedan-lamp-schema-inside.png')}}" alt="Легковой автовобиль"/>
    </div>
    <div class="description-block">
        @if  (isset($lamps['it']['it1']))
        <div nid="it1" id="s-s-21"><p>Подсветка для ног</p><img src="{{Theme::asset('images/arrow-sedan-ss21.png')}}"/></div>
        @endif
        @if  (isset($lamps['it']['it2']))
        <div nid="it2" id="s-s-22"><p>Свет бардочка</p><img src="{{Theme::asset('images/arrow-sedan-ss22.png')}}"/></div>
        @endif
        @if  (isset($lamps['it']['it3']))
        <div  nid="it3"  id="s-s-23"><p>Лампа для чтения</p><img src="{{Theme::asset('images/arrow-sedan-ss23.png')}}"/></div>
        @endif
        @if  (isset($lamps['it']['it4']))
        <div  nid="it4"  id="s-s-24"><p>Внутрисалонный свет</p><img src="{{Theme::asset('images/arrow-sedan-ss24.png')}}"/></div>
        @endif
        @if  (isset($lamps['it']['it5']))
        <div  nid="it5" id="s-s-25"><p class="right-description">Подсветка двери</p><img src="{{Theme::asset('images/arrow-sedan-ss25.png')}}"/></div>
        @endif
        @if  (isset($lamps['it']['it6']))
        <div nid="it6" id="s-s-26"><p class="right-description">Габаритный огонь двери</p><img src="{{Theme::asset('images/arrow-sedan-ss26.png')}}"/></div>
        @endif
        @if  (isset($lamps['it']['it7']))
        <div  nid="it7" id="s-s-27"><p class="right-description">Подсветка багажника</p><img src="{{Theme::asset('images/arrow-sedan-ss27.png')}}"/></div>
        @endif
        @if  (isset($lamps['it']['it8']))
        <div  nid="it8"  id="s-s-28"><p class="right-description">Подсветка порогов</p><img src="{{Theme::asset('images/arrow-sedan-ss28.png')}}"/></div>
        @endif
        @if  (isset($lamps['it']['it9']))
        <div  nid="it9"  id="s-s-32"><p >Подсветка мотора</p><img src="{{Theme::asset('images/arrow-sedan-ss32.png')}}"/></div>
        @endif
    </div>
</div>
