<div class="schema-lamp">
    <div class="image-schema-block" id="sedan-schema">
        <img src="{{Theme::asset('images/sedan-lamp-schema-front.png')}}" alt="Легковой автовобиль"/>
    </div>
    <div class="description-block">
            @if (isset($lamps['ft']['ft2']))
            <div nid="ft2" id="s-s-2"><p>Ближний свет</p><img src="{{Theme::asset('images/arrow-sedan-ss2.png')}}"/></div>
            @endif
            @if (isset($lamps['ft']['ft4']))
            <div nid="ft4" id="s-s-4"><p>Дальний свет</p><img src="{{Theme::asset('images/arrow-sedan-ss4.png')}}"/></div>
            @endif
            @if (isset($lamps['ft']['ft1']))
            <div nid="ft1" id="s-s-5"><p>Боковой указатель поворота</p><img src="{{Theme::asset('images/arrow-sedan-ss5.png')}}"/></div>
            @endif
            @if (isset($lamps['ft']['ft3']))
            <div nid="ft3" id="s-s-3"><p>Габаритные огни</p><img src="{{Theme::asset('images/arrow-sedan-ss3.png')}}"/></div>
            @endif
            @if (isset($lamps['ft']['ft5']))
            <div  nid="ft5"   id="s-s-6"><p>Освещение поворотов</p><img src="{{Theme::asset('images/arrow-sedan-ss6.png')}}"/></div>
            @endif
            @if (isset($lamps['ft']['ft7']))
            <div nid="ft7"  id="s-s-9"><p>Передний указатель поворота</p><img src="{{Theme::asset('images/arrow-sedan-ss9.png')}}"/></div>
            @endif
            @if (isset($lamps['ft']['ft6']))
            <div nid="ft6" id="s-s-8"><p>Дневные ходовые огни</p><img src="{{Theme::asset('images/arrow-sedan-ss8.png')}}"/></div>
            @endif
            @if (isset($lamps['ft']['ft8']))
            <div nid="ft8" id="s-s-10"><p>Противотуманные фары</p><img src="{{Theme::asset('images/arrow-sedan-ss10.png')}}"/></div>
            @endif

            @if (isset($lamps['ft']['ft9']))
                <div nid="ft9" id="s-s-29"><p>Окружающий свет</p><img src="{{Theme::asset('images/arrow-sedan-ss29.png')}}"/></div>
            @endif

            @if (isset($lamps['ft']['ft10']))
                <div nid="ft10" id="s-s-30"><p>Дополнительный передний свет</p><img src="{{Theme::asset('images/arrow-sedan-ss30.png')}}"/></div>
            @endif

            @if (isset($lamps['ft']['ft11']))
                <div nid="ft11" id="s-s-31"><p>Дополнительные фары дальнего света</p><img src="{{Theme::asset('images/arrow-sedan-ss31.png')}}"/></div>
            @endif

            @if (isset($lamps['ft']['ft12']))
                <div nid="ft12" id="s-s-20"><p>Боковые габаритные огни</p><img src="{{Theme::asset('images/arrow-sedan-ss20.png')}}"/></div>
            @endif
    </div>
</div>
