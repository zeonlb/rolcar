<div class="schema-lamp">
    <div class="image-schema-block" id="truck-schema">
        <img src="{{Theme::asset('images/truck-lamp-schema-front.png')}}" alt="Грузовой автовобиль"/>
    </div>
    <div class="description-block">
        @if (isset($lamps['ft']['ft2']))
            <div nid="ft2" id="s-t-2"><p>Ближний свет</p><img src="{{Theme::asset('images/arrow-truck-ss2.png')}}"/></div>
        @endif
        @if (isset($lamps['ft']['ft4']))
            <div nid="ft4" id="s-t-4"><p>Дальний свет</p><img src="{{Theme::asset('images/arrow-truck-ss4.png')}}"/></div>
        @endif
        @if (isset($lamps['ft']['ft1']))
            <div nid="ft1" id="s-t-1"><p>Боковой указатель поворота</p><img src="{{Theme::asset('images/arrow-truck-ss5.png')}}"/></div>
        @endif
        @if (isset($lamps['ft']['ft3']))
            <div nid="ft3" id="s-t-3"><p>Габаритные огни</p><img src="{{Theme::asset('images/arrow-truck-ss3.png')}}"/></div>
        @endif
        @if (isset($lamps['ft']['ft5']))
            <div  nid="ft5"  id="s-t-5"><p>Освещение поворотов</p><img src="{{Theme::asset('images/arrow-truck-ss6.png')}}"/></div>
        @endif
        @if (isset($lamps['ft']['ft7']))
            <div nid="ft7"  id="s-t-9"><p>Передний указатель поворота</p><img src="{{Theme::asset('images/arrow-truck-ss9.png')}}"/></div>
        @endif
        @if (isset($lamps['ft']['ft6']))
            <div nid="ft6" id="s-t-8"><p>Дневные ходовые огни</p><img src="{{Theme::asset('images/arrow-truck-ss8.png')}}"/></div>
        @endif
        @if (isset($lamps['ft']['ft8']))
            <div nid="ft8" id="s-t-10"><p>Противотуманные фары</p><img src="{{Theme::asset('images/arrow-truck-ss10.png')}}"/></div>
        @endif
        @if (isset($lamps['ft']['ft9']))
            <div nid="ft9" id="s-t-29"><p>Окружающий свет</p><img src="{{Theme::asset('images/arrow-truck-ss29.png')}}"/></div>
        @endif
        @if (isset($lamps['ft']['ft10']))
            <div nid="ft10" id="s-t-30"><p>Дополнительный передний свет</p><img src="{{Theme::asset('images/arrow-truck-ss30.png')}}"/></div>
        @endif
        @if (isset($lamps['ft']['ft11']))
            <div nid="ft11" id="s-t-31"><p>Дополнительные фары дальнего света</p><img src="{{Theme::asset('images/arrow-truck-ss31.png')}}"/></div>
        @endif
        @if (isset($lamps['ft']['ft12']))
            <div nid="ft12" id="s-t-32"><p>Боковые габаритные огни</p><img src="{{Theme::asset('images/arrow-truck-ss32.png')}}"/></div>
        @endif
        @if (isset($lamps['ft']['ft13']))
            <div nid="ft13" id="s-t-33"><p>Парковочные огни</p><img src="{{Theme::asset('images/arrow-truck-ss33.png')}}"/></div>
        @endif
    </div>
</div>
