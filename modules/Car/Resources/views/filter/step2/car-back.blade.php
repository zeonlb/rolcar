<div class="schema-lamp">
    <div class="image-schema-block" id="sedan-schema">
        <img src="{{Theme::asset('images/sedan-lamp-schema-back.png')}}" alt="Легковой автовобиль"/>
    </div>
    <div class="description-block">
        @if (isset($lamps['bt']['bt1']))
            <div nid="bt1" id="s-s-13"><p>Дополнительный стоп-сигнал</p><img src="{{Theme::asset('images/arrow-sedan-ss13.png')}}"/></div>
        @endif
        @if (isset($lamps['bt']['bt2']))
            <div nid="bt2" id="s-s-14"><p>Стоп-сигнал</p><img src="{{Theme::asset('images/arrow-sedan-ss14.png')}}"/></div>
        @endif
        @if (isset($lamps['bt']['bt3']))
            <div nid="bt3" id="s-s-15"><p>Задний ПТФ</p><img src="{{Theme::asset('images/arrow-sedan-ss15.png')}}"/></div>
        @endif
        @if (isset($lamps['bt']['bt4']))
            <div nid="bt4" id="s-s-16"><p>Подсветка номера</p><img src="{{Theme::asset('images/arrow-sedan-ss16.png')}}"/></div>
        @endif
        @if (isset($lamps['bt']['bt5']))
            <div nid="bt5" id="s-s-17"><p class="right-description">Задний ход</p><img src="{{Theme::asset('images/arrow-sedan-ss17.png')}}"/></div>
        @endif
        @if (isset($lamps['bt']['bt6']))
            <div nid="bt6" id="s-s-18"><p class="right-description">Дополнительный задний свет</p><img src="{{Theme::asset('images/arrow-sedan-ss18.png')}}"/></div>
        @endif
        @if (isset($lamps['bt']['bt8']))
            <div nid="bt8" id="s-s-19"><p class="right-description">Задний габарит</p><img src="{{Theme::asset('images/arrow-sedan-ss19.png')}}"/></div>
        @endif
        @if (isset($lamps['bt']['bt7']))
            <div nid="bt7" id="s-s-20"><p  class="right-description">Задний указатель поворота</p><img src="{{Theme::asset('images/arrow-sedan-ss20.png')}}"/></div>
        @endif
    </div>
</div>
