<div class="schema-lamp">
    <div class="image-schema-block" id="motorcycle-schema">
        <img src="{{Theme::asset('images/motorcycle-lamp-schema-back.png')}}" alt="Мотоцикл"/>
    </div>
    <div class="description-block">
        <p nid="bt6" id="s-m-11">Задний свет</p>
        <p nid="bt2" id="s-m-12">Стоп сигнал</p>
        <p nid="bt3" id="s-m-13">Противотуманный свет</p>
        <p nid="bt8" id="s-m-14">Резервный свет</p>
        <p nid="bt7" id="s-m-15">Задний указатель поворота</p>
        <p nid="bt4" id="s-m-16">Подсветка номера</p>
    </div>
</div>
