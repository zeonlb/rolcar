<div class="schema-lamp">
    <div class="image-schema-block" id="motorcycle-schema">
        <img src="{{Theme::asset('images/motorcycle-lamp-schema.png')}}" alt="Мотоцикл"/>
    </div>
    <div class="description-block">
        <p nid="ft1" id="s-m-1">Боковой указатель поворота</p>
        <p nid="ft2" id="s-m-2">Ближний свет</p>
        <p nid="ft3" id="s-m-3">Парковочный свет</p>
        <p nid="ft4" id="s-m-4">Дальний свет</p>
        <p nid="ft1" id="s-m-5">Боковой указатель поворота</p>
        <p nid="ft5" id="s-m-6">Габариты</p>
        <p nid="ft4" id="s-m-7">Дальний свет</p>
        <p nid="ft6" id="s-m-8">Дневной свет</p>
        <p nid="ft7" id="s-m-9">Передний указатель поворота</p>
        <p nid="ft8" id="s-m-10">Противотуманный свет</p>
    </div>
</div>
