<div class="container">
    <div id="chosen-slider-car-options">
        <div class="choose-select-block">
            <div  class="choose-lamp-header-block">
                <h3 class="choose-lamp-header">{{$title}}:</h3>
            </div>
            @foreach($selected as $select)
            <div class="choose-lamp-selects">
                <div class="select-wrapper wrapper-dropdown" defaultvalue='Выберите марку'>
                    <div class="chosen-value"><span>{{$select}}</span><i class="custom-i">X</i></div>
                </div>
            </div>
            @endforeach
        </div>
        <div class="clearfix"></div>
        {!! $content !!}
    </div>
</div>
<br class="clear" />
<div class="schema-navigation">
    <div class="nav-container">
        <div source="nav-left" class="schema-nav-left schema-nav navig"><img src="{{Theme::asset('images/schema-nav-arrow-left.jpg')}}" alt=""/></div>
        <div><a source="front" class="@if($side == 'front') active @endif navig" href="#">Передний свет</a></div>
        <div><a source="back" class="navig @if($side == 'back') active @endif"  href="#">Задний свет</a></div>
        <div><a source="inside"  @if ($insideDisabled) class="disabled" @else class="@if($side == 'inside') active @endif navig" @endif href="#">Свет в салоне</a></div>
        <div source="nav-right"  class="navig schema-nav-right schema-nav"><img src="{{Theme::asset('images/schema-nav-arrow-right.jpg')}}" alt=""/></div>
    </div>
</div>