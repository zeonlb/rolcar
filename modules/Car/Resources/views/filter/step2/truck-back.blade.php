<div class="schema-lamp">
    <div class="image-schema-block" id="truck-schema">
        <img src="{{Theme::asset('images/truck-lamp-schema-back.png')}}" alt="Грузовой автомобиль автовобиль"/>
    </div>
    <div class="description-block">
        <div class="description-block">
            @if (isset($lamps['bt']['bt1']))
                <div nid="bt1" id="s-t-13"><p>Дополнительный стоп-сигнал</p><img src="{{Theme::asset('images/arrow-truck-ss13.png')}}"/></div>
            @endif
            @if (isset($lamps['bt']['bt2']))
                <div nid="bt2" id="s-t-14"><p>Стоп-сигнал</p><img src="{{Theme::asset('images/arrow-truck-ss14.png')}}"/></div>
            @endif
            @if (isset($lamps['bt']['bt3']))
                <div nid="bt3" id="s-t-15"><p>Задний ПТФ</p><img src="{{Theme::asset('images/arrow-truck-ss15.png')}}"/></div>
            @endif
            @if (isset($lamps['bt']['bt4']))
                <div nid="bt4" id="s-t-16"><p>Подсветка номера</p><img src="{{Theme::asset('images/arrow-truck-ss16.png')}}"/></div>
            @endif
            @if (isset($lamps['bt']['bt5']))
                <div nid="bt5" id="s-t-17"><p class="right-description">Задний ход</p><img src="{{Theme::asset('images/arrow-truck-ss17.png')}}"/></div>
            @endif
            @if (isset($lamps['bt']['bt6']))
                <div nid="bt6" id="s-t-18"><p class="right-description">Дополнительный задний свет</p><img src="{{Theme::asset('images/arrow-truck-ss18.png')}}"/></div>
            @endif
            @if (isset($lamps['bt']['bt8']))
                <div nid="bt8" id="s-t-19"><p class="right-description">Задний габарит</p><img src="{{Theme::asset('images/arrow-truck-ss19.png')}}"/></div>
            @endif
            @if (isset($lamps['bt']['bt7']))
                <div nid="bt7" id="s-t-20"><p  class="right-description">Задний указатель поворота</p><img src="{{Theme::asset('images/arrow-truck-ss20.png')}}"/></div>
            @endif
        </div>
    </div>
</div>