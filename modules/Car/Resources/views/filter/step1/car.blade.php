<div class="container">
    <div id="chosen-slider-car-options">
        <div class="choose-select-block step2" id="car-select-block">
            <div class="choose-select-block">
                <h3 class="chosen-main-header">Выберите ваше легковое авто:</h3>
                <div id="mark" callback="model" class="select-wrapper wrapper-dropdown-medium">
                    <div class="chosen-value"><span>Выберите марку</span>
                        <img class="select-arrow" src="{{Theme::asset('images/select-arrow-btn.png')}}"/>
                    </div>

                    <ul class="dropdown" data-source="mark" >
                        <input type="text" class="form-control filter-search-input"  data-id="mark" name="filter" placeholder="поиск">
                        @foreach($marks as $mark)
                            <li class="name" value="{{$mark->id}}">{{$mark->name}}</li>
                        @endforeach
                    </ul>
                </div>
                <div callback="year" class="select-wrapper wrapper-dropdown-medium">
                    <div class="chosen-value"><span>Выберите модель</span>
                        <img class="select-arrow" src="{{Theme::asset('images/select-arrow-btn.png')}}"/>
                    </div>
                    <ul class="model dropdown" data-source="model" >
                        <input type="text" class="form-control filter-search-input" data-id="model" name="filter" placeholder="поиск">
                        <li>Нет данных</li>
                    </ul>
                </div>
                <div callback="modification" class="select-wrapper wrapper-dropdown-medium">
                    <div class="chosen-value"><span>Выберите год</span>
                        <img class="select-arrow" src="{{Theme::asset('images/select-arrow-btn.png')}}"/>
                    </div>
                    <ul class="year dropdown"  data-source="year" >
                        <input type="text" class="form-control filter-search-input" data-id="year" name="filter" placeholder="поиск">
                        <li>Нет данных</li>
                    </ul>
                </div>
                <div callback="motor" class="select-wrapper wrapper-dropdown-medium">
                    <div class="chosen-value"><span>Выберите модификацию</span>
                        <img class="select-arrow" src="{{Theme::asset('images/select-arrow-btn.png')}}"/>
                    </div>
                    <ul class="modification dropdown"   data-source="modification">
                        <input type="text" class="form-control filter-search-input" data-id="modification" name="filter" placeholder="поиск">
                        <li>Нет данных</li>
                    </ul>
                </div>
                <div callback="finish" class="hidden select-wrapper wrapper-dropdown-medium">
                    <div class="chosen-value"><span>Тип двигателя</span>
                        <img class="select-arrow" src="{{Theme::asset('images/select-arrow-btn.png')}}"/>
                    </div>
                    <ul class="motor dropdown"   data-source="motor">
                        <input type="text" class="form-control filter-search-input" data-id="motor" name="filter" placeholder="поиск">
                        <li>Нет данных</li>
                    </ul>
                </div>
            </div>
            <div class="big-transport-image" id="big-car-image"><img src="{{Theme::asset('images/big-car-image.png')}}" alt=""/></div>
        </div>

    </div>
</div>
<br class="clear" />
<script src="{{Theme::asset('js/select-wrapper.js')}}"></script>