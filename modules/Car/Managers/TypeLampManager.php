<?php namespace Modules\Car\Mangers;


use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Modules\Car\Entities\LampJoinQuestion;
use Modules\Car\Entities\Lamps;
use Modules\Car\Entities\Motor;
use Modules\Shop\Repositories\ProductRepository;

class TypeLampManager
{

    protected $productRepo;

    public function updateMotorOptions(array $motorIds, array $options, array $cases = [])
    {
        foreach ($motorIds as $id) {
            $motor = Motor::find($id);
            LampJoinQuestion::where('motor_id', $id)->delete();
            $motor->filled_percent = $options['filled_percent'];
            foreach ($cases as $lampType => $c) {
                foreach ($c as $case) {
                    $attributes = [
                        'lamp_type' => $lampType,
                        'lamp_type_question_id' => $case['qcode'],
                        'tsokol' => $case['tsokol'],
                        'motor_id' => $id
                    ];
                    $lampQuestion = new LampJoinQuestion();
                    $lampQuestion->fill($attributes);
                    $lampQuestion->save();
                    if (!array_key_exists($case['tsokol'], $options[$lampType])) {
                        $options[$lampType][$case['tsokol']] = [];
                    }
                    // add non existing lamps
//                    if (!isset($options[$lampType][$case['tsokol']])) {
//                        $options[$lampType] = [];
//                    } else {
//                        if (!in_array($case['tsokol'], $options[$lampType])) {
//                            array_push($options[$lampType], $case['tsokol']);
//                        }
//                    }
                }
            }
            $lampType = Lamps::where('lib_car_motor_id', $id)->first();
            if (!$lampType) {
                Lamps::create(['lib_car_motor_id' => $id, 'options' => json_encode($options)]);
            } else {
                $lampType->options = json_encode($options);
                $lampType->save();
            }
            $motor->save();
        }
    }

    /**
     * Synchronize Products with Cars by lamp types
     */
    public function syncMotorsWithProducts()
    {
        Cache::flush();
        $productRepo = new ProductRepository();

        DB::table('lib_car_shop_product')->delete();
        DB::table('lib_car_shop_product')->truncate();
        $products = $productRepo->getProductsWithOptionList();
        $products = $this->groupProductsWithOptions($products);
        $limit = 500;
        $offset = 0;
        while ($motorLamps = Lamps::with('motor.modification.model')->offset($offset)
            ->limit($limit)->get()) {
            if ($motorLamps->count() == 0) {
                break;
            }
            $insertData = [];
            foreach ($motorLamps as $motorLamp) {
                $lampOptions = json_decode($motorLamp->options, true);
                if (isset($lampOptions['filled_percent'])) {
                    unset($lampOptions['filled_percent']);
                }
                $ttCount = 0;
                foreach ($lampOptions as $c) {
                    if (!empty($c)) {
                        $ttCount++;
                    }
                }
                $fillPercent = ($ttCount * 100) / 28;

                $motorLamp->motor->filled_percent = $fillPercent;
                $motorLamp->motor->save();
//                foreach ($lampOptions as $category => $options) {
//                    foreach ($options as $option) {
//                        if (isset($products[$category . $option]) && $pIds = $products[$category . $option]) {
//                            foreach ($pIds as $pid) {
//                                if ($motorLamp->motor) {
//                                    $insertData[$pid . $motorLamp->motor->id . $category] = [
//                                        'transport_id' => $motorLamp->motor->modification->model->lib_car_transport_id,
//                                        'mark_id' => $motorLamp->motor->modification->model->lib_car_mark_id,
//                                        'model_id' => $motorLamp->motor->modification->model->id,
//                                        'modification_id' => $motorLamp->motor->modification->id,
//                                        'motor_id' => $motorLamp->motor->id,
//                                        'lamp_type' => $category,
//                                        'product_id' => $pid,
//                                        'created_at' => (new \DateTime())->format('Y-m-d H:i:s')
//                                    ];
//                                }
//
////                                if ((count($insertData) % 100) == 0) {
//                                if (1) {
//                                    DB::table('lib_car_shop_product')->insert($insertData);
//                                    $insertData = [];
//                                }
//                            }
//                        }
//                    }
//                }

            }
            $offset += $limit;
            echo $offset . PHP_EOL;
        }


    }

    public function groupProductsWithOptions($products)
    {
        $result = [];
        foreach ($products as $pr) {
            $result[$pr->category . $pr->attr][] = $pr->product_id;
        }
        return $result;
    }

}