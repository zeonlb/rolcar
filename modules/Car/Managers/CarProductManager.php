<?php namespace Modules\Car\Mangers;

use Modules\Car\Entities\CarProduct;
use Modules\Car\Entities\Motor;

class CarProductManager
{

    public function store($productId, $motorsList)
    {
        $motors = Motor::whereIn('id', $motorsList)
            ->with('modification')
            ->get();
        CarProduct::where('product_id', $productId)->delete();
        foreach ($motors as $motor) {
            $carProduct =  new CarProduct();
            $carProduct->product_id = $productId;
            $carProduct->transport_id = $motor->modification->model->transport->id;
            $carProduct->mark_id = $motor->modification->model->mark->id;
            $carProduct->model_id = $motor->modification->model->id;
            $carProduct->modification_id = $motor->modification->id;
            $carProduct->motor_id = $motor->id;
            $carProduct->save();
        }

    }

}