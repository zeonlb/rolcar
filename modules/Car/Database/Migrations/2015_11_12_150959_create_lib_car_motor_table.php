<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLibCarMotorTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lib_car_motor', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('lib_car_modification_id')->unsigned();
            $table->string('motor');
            $table->string('power')->nullable();
            $table->string('motor_capacity')->nullable();
            $table->string('speed_100')->nullable();
            $table->string('max_speed')->nullable();
            $table->string('year_start', 4);
            $table->string('year_end', 4);
            $table->string('consumption')->nullable();
            $table->timestamps();
            $table->foreign('lib_car_modification_id')->references('id')->on('lib_car_modification')->onDelete('cascade');;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lib_car_motor', function(Blueprint $table) {
            $table->dropForeign('lib_car_motor_lib_car_modification_id_foreign');
        });
        Schema::drop('lib_car_motor');
    }

}
