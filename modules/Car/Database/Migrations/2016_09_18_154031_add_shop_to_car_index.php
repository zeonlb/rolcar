<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddShopToCarIndex extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lib_car_shop_product', function(Blueprint $table)
        {
            $table->index(['lamp_type', 'motor_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lib_car_shop_product', function(Blueprint $table)
        {
            $table->dropIndex(['lamp_type', 'motor_id']);
        });
    }

}
