<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSeoData extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lib_car_mark', function(Blueprint $table)
        {
            $table->integer('cms_meta_id')->unsigned()->nullable();
            $table->foreign('cms_meta_id')->references('id')->on('cms_meta');
        });
        Schema::table('lib_car_model', function(Blueprint $table)
        {
            $table->integer('cms_meta_id')->unsigned()->nullable();
            $table->foreign('cms_meta_id')->references('id')->on('cms_meta')->onDelete('cascade');;
        });
        Schema::table('lib_car_modification', function(Blueprint $table)
        {
            $table->integer('cms_meta_id')->unsigned()->nullable();
            $table->foreign('cms_meta_id')->references('id')->on('cms_meta')->onDelete('cascade');;
        });
        Schema::table('lib_car_motor', function(Blueprint $table)
        {
            $table->integer('cms_meta_id')->unsigned()->nullable();
            $table->foreign('cms_meta_id')->references('id')->on('cms_meta')->onDelete('cascade');;
        });
        Schema::table('lib_car_transport', function(Blueprint $table)
        {
            $table->integer('cms_meta_id')->unsigned()->nullable();
            $table->foreign('cms_meta_id')->references('id')->on('cms_meta')->onDelete('cascade');;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lib_car_mark', function(Blueprint $table)
        {
            $table->dropForeign('lib_car_mark_cms_meta_id_foreign');
        });
        Schema::table('lib_car_model', function(Blueprint $table)
        {
            $table->dropForeign('lib_car_model_cms_meta_id_foreign');
        });
        Schema::table('lib_car_modification', function(Blueprint $table)
        {
            $table->dropForeign('lib_car_modification_cms_meta_id_foreign');
        });
        Schema::table('lib_car_motor', function(Blueprint $table)
        {
            $table->dropForeign('lib_car_motor_cms_meta_id_foreign');
        });
        Schema::table('lib_car_transport', function(Blueprint $table)
        {
            $table->dropForeign('lib_car_transport_cms_meta_id_foreign');
        });
    }

}
