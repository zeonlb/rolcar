<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddParentMotor extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lib_car_motor', function(Blueprint $table)
        {
            $table->integer('parent_id')->unsigned();
        });
        Schema::table('lib_car_motor', function(Blueprint $table)
        {
            $table->dropForeign('lib_car_motor_lib_car_modification_id_foreign');
            $table->foreign('lib_car_modification_id')->references('id')->on('lib_car_modification')
                ->onDelete('cascade')->onUpdate('no action');
        });
        $dbh = \DB::connection()->getPdo();
        $sth = $dbh->prepare('UPDATE `lib_car_motor` SET parent_id = id');
        $sth->execute();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('', function(Blueprint $table)
        {

        });
    }

}
