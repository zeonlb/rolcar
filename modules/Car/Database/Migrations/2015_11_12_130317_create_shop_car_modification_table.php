<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopCarModificationTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lib_car_modification', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('code');
            $table->string('year_start', 4);
            $table->string('year_end', 4);

            $table->integer('lib_car_type_id')->unsigned();
            $table->integer('lib_car_model_id')->unsigned();
            $table->timestamps();

            $table->foreign('lib_car_type_id')->references('id')->on('lib_car_type')->onDelete('cascade');;
            $table->foreign('lib_car_model_id')->references('id')->on('lib_car_model')->onDelete('cascade');;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lib_car_modification', function(Blueprint $table) {
            $table->dropForeign('lib_car_modification_lib_car_type_id_foreign');
        });
        Schema::drop('lib_car_modification');
    }

}
