<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCmsBasicPagesSecond extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $mainPageHtml = file_get_contents(base_path('source/seed').'/garanty');
        $page = new \Modules\Cms\Entities\Page();
        $page->name = 'Гарантия';
        $page->url = '/guarantee';
        $page->html = $mainPageHtml;
        $page->visible = 1;
        $page->save();

        $navigation =  new \Modules\Cms\Entities\Navigation();
        $navigation->menu_name = 'cms.main.navigation';
        $navigation->alias = 'Гарантия';
        $navigation->url = '/guarantee';
        $navigation->sort = 4;
        $navigation->save();

        $catalogPageHtml = file_get_contents(base_path('source/seed').'/delivery');
        $page = new \Modules\Cms\Entities\Page();
        $page->name = 'Доставка';
        $page->url = '/delivery';
        $page->visible = 1;
        $page->html = $catalogPageHtml;
        $page->save();

        $navigation =  new \Modules\Cms\Entities\Navigation();
        $navigation->menu_name = 'cms.main.navigation';
        $navigation->alias = 'Доставка';
        $navigation->url = '/delivery';
        $navigation->sort = 5;
        $navigation->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }

}
