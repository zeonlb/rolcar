<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCmsBasicPages extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $mainPageHtml = file_get_contents(base_path('source/seed').'/main_page');
        $page = new \Modules\Cms\Entities\Page();
        $page->name = 'Главная';
        $page->url = '/';
        $page->html = $mainPageHtml;
        $page->save();

        $navigation =  new \Modules\Cms\Entities\Navigation();
        $navigation->menu_name = 'cms.main.navigation';
        $navigation->alias = 'Главная';
        $navigation->url = '/';
        $navigation->sort = 0;
        $navigation->save();

        $catalogPageHtml = file_get_contents(base_path('source/seed').'/catalog');
        $page = new \Modules\Cms\Entities\Page();
        $page->name = 'Каталог';
        $page->url = '/catalog';
        $page->html = $catalogPageHtml;
        $page->save();

        $navigation =  new \Modules\Cms\Entities\Navigation();
        $navigation->menu_name = 'cms.main.navigation';
        $navigation->alias = 'Каталог';
        $navigation->url = '/catalog';
        $navigation->sort = 1;
        $navigation->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }

}
