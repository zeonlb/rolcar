<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCarProductColumn extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lib_car_shop_product', function(Blueprint $table)
        {
                $table->string('lamp_type')->after('product_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lib_car_shop_product', function(Blueprint $table)
        {
            $table->dropColumn('lamp_type');
        });
    }

}
