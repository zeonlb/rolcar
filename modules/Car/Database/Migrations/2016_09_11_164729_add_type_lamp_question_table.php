<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeLampQuestionTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lib_car_lamp_type_questions', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('code', 50);
            $table->string('message');

            $table->timestamps();
        });

        Schema::create('lib_car_motor_j_questions', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('lamp_type', 5);
            $table->integer('lamp_type_question_id')->unsigned();
            $table->integer('motor_id')->unsigned();
            $table->string('tsokol', 50);

            $table->timestamps();
        });

        Schema::table('lib_car_motor_j_questions', function(Blueprint $table){
            $table->foreign('lamp_type_question_id')->references('id')->on('lib_car_lamp_type_questions');
            $table->foreign('motor_id')->references('id')->on('lib_car_motor');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lib_car_motor_j_questions', function(Blueprint $table) {
            $table->dropForeign('lib_car_motor_j_questions_lamp_type_question_id_foreign');
        });
        Schema::drop('lib_car_motor_j_questions');
        Schema::drop('lib_car_lamp_type_questions');
    }

}
