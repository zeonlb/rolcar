<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLibCarTransportTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lib_car_transport', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('code')->unique();

            $table->timestamps();
        });
        DB::table('lib_car_transport')->insert(
            [
                'id' => 1,
                'name' => 'Легковая',
                'code' => 'sedan'
            ]
        );
        Schema::table('lib_car_model', function(Blueprint $table)
        {

            $table->integer('lib_car_transport_id')->unsigned()->default(1);

            $table->foreign('lib_car_transport_id')->references('id')->on('lib_car_transport')->onDelete('cascade');;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lib_car_model', function(Blueprint $table) {
            $table->dropForeign('lib_car_model_lib_car_transport_id_foreign');
            $table->dropColumn('lib_car_transport_id');
        });
        Schema::drop('lib_car_transport');
    }

}
