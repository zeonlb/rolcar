<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLibCarModificationTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lib_car_modification', function(Blueprint $table)
        {
            $table->string('osram_code')->after('year_end')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lib_car_modification', function(Blueprint $table)
        {
            $table->dropColumn('osram_code');
        });
    }

}
