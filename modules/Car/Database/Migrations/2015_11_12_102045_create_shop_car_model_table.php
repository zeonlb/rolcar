<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopCarModelTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lib_car_model', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('code');
            $table->integer('lib_car_mark_id')->unsigned();
            $table->timestamps();

            $table->foreign('lib_car_mark_id')->references('id')->on('lib_car_mark')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lib_car_model', function(Blueprint $table) {
            $table->dropForeign('lib_car_model_lib_car_mark_id_foreign');
        });
        Schema::drop('lib_car_model');
    }

}
