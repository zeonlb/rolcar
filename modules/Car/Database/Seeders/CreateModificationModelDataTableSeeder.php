<?php namespace Modules\Car\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Car\Entities\CModel;
use Modules\Car\Entities\Mark;
use Modules\Car\Entities\Type;

class CreateModificationModelDataTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();
		$mark = Mark::all();
		$carTypes = Type::all();
		$carTypes = array_column($carTypes->toArray(), 'id', 'name');
		$mark = array_column($mark->toArray(), 'id', 'name');

		$data =  json_decode(file_get_contents(base_path('source/seed').'/cars.json'), true);
		foreach ($data as $key => $dt) {
			$idMark = $mark[$dt['mark']];
			$pasrdedDesc = explode(" ", $dt['descr']);
			$typeName = trim($pasrdedDesc[0]);
			$model  = CModel::where('name', $dt['model'])->where('lib_car_mark_id', $idMark)->first();
			if (!$model) {
				echo "<pre>"; print_r($dt); echo "</pre>"; die;
			}
			$year_start = trim(strtr($pasrdedDesc[1], ['(' => '', ')' => '',' '=> '']));
			$year_finish = trim(strtr($pasrdedDesc[3], ['(' => '', ')' => '',' '=> '']));
			$idType = $carTypes[$typeName];
			$insertData[] = [
				'name' => $dt['modiffDeeper'],
				'code' => generate_code($dt['modiffDeeper'].'_'.rand(10,999)),
				'year_start' => $year_start,
				'year_end' => $year_finish,
				'lib_car_model_id' => $model->id,
				'lib_car_type_id' => $idType,

			];
		}
		DB::table('lib_car_modification')->insert($insertData);
	}

}