<?php namespace Modules\Car\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Car\Entities\Mark;


class CreateCarModelDataTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();
		$marks = Mark::all();
		$marks = array_column($marks->toArray(), 'id', 'name');
		$data =  json_decode(file_get_contents(base_path('source/seed').'/cars.json'), true);
		$insertData = [];
		foreach ($data as $dt) {
			$insertData = [
				'name' => $dt['model'],
				'code' => generate_code($dt['mark'].'_'.$dt['model']),
				'lib_car_mark_id' => $marks[$dt['mark']]
			];
			if (!DB::table('lib_car_model')->where('name', $dt['mark'])->where('lib_car_mark_id', $marks[$dt['mark']])->first()) {
				DB::table('lib_car_model')->insert($insertData);
			}
		}


	}

}