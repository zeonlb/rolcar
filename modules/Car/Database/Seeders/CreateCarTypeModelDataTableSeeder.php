<?php namespace Modules\Car\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class CreateCarTypeModelDataTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();
		$data =  json_decode(file_get_contents(base_path('source/seed').'/cars.json'), true);
		foreach ($data as  $dt) {
			$pasrdedDesc = explode(" ", $dt['descr']);
			$insertData[$pasrdedDesc[0]] = [
				'name' => trim($pasrdedDesc[0]),
				'code' => generate_code(trim($pasrdedDesc[0])),
			];
		}
		DB::table('lib_car_type')->insert($insertData);
	}

}