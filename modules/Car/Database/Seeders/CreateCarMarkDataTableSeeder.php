<?php namespace Modules\Car\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CreateCarMarkDataTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();
		$data =  json_decode(file_get_contents(base_path('source/seed').'/cars.json'), true);
		$marks = array_unique(array_column($data, 'mark'));
		$insertData = [];
		foreach ($marks as  $mark) {
			$insertData[] = [
				'name' => $mark,
				'code' => generate_code($mark),
				'created_at' => time()
			];
		}
		DB::table('lib_car_mark')->insert($insertData);
	}

}