<?php namespace Modules\Car\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class CarTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		/**
		 * Seed Cars data
		 *  */
		$this->call("Modules\\Car\\Database\\Seeders\\CreateCarMarkDataTableSeeder");
		$this->call("Modules\\Car\\Database\\Seeders\\CreateCarModelDataTableSeeder");
		$this->call("Modules\\Car\\Database\\Seeders\\CreateCarTypeModelDataTableSeeder");
		$this->call("Modules\\Car\\Database\\Seeders\\CreateModificationModelDataTableSeeder");
		$this->call("Modules\\Car\\Database\\Seeders\\CreateMotorModificationDataTableSeeder");
		$this->call("Modules\\Car\\Database\\Seeders\\CreateMotoAndTruckDataTableSeeder");
//		$this->call("Modules\\Car\\Database\\Seeders\\AddLampNaznachenoeFilterTableSeeder");

	}

}