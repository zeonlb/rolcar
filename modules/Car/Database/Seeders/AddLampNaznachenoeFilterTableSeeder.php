<?php namespace Modules\Car\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Shop\Entities\Attribute;
use Modules\Shop\Entities\AttributeOption;
use Modules\Shop\Entities\Category;

class AddLampNaznachenoeFilterTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $frCategory =  new Category();
        $frCategory->name = 'Передний свет';
        $frCategory->code =
        $optionsNames = [
            "ft1" => "Перед. Боковой указатель поворота",
            "ft2" => "Перед. Ближний свет",
            "ft3" => "Перед. Парковочный свет",
            "ft4" => "Перед. Дальний свет",
            "ft5" => "Перед. Габариты",
            "ft6" => "Перед. Дневной свет",
            "ft7" => "Перед. Передний указатель поворота",
            "ft8" => "Перед. Противотуманный свет",
            "ft9" => "Перед. Противотуманный свет низ",
            "bt1" => "Зад. Дополнительный стоп-сигнал",
            "bt2" => "Зад. Стоп-сигнал",
            "bt3" => "Зад. Противотуманный свет",
            "bt4" => "Зад. Подсветка номера",
            "bt5" => "Зад. Свет заднего хода",
            "bt6" => "Зад. Задний свет",
            "bt7" => "Зад. Задний указатель поворота",
            "bt8" => "Зад. Резервный свет",
            "it1" => "Салон. Освещение пространства для ног",
            "it2" => "Салон. Свет бардочка",
            "it3" => "Салон. Внутреннее освещение",
            "it4" => "Салон. Освещение салона",
            "it5" => "Салон. Свет салона",
            "it6" => "Салон. Свет безопасности двери",
            "it7" => "Салон. Свет багажника",
            "it8" => "Салон. Свет парога"
        ];

        $options = [];
        foreach ($optionsNames as $key => $name)
        {
            $option =  new AttributeOption();
            $option->code = generate_code($key);
            $option->name = $name;
            $options[] = $option;

        }
        $attribute->options()->saveMany($options);
    }

}