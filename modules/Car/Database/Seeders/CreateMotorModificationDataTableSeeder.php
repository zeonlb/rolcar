<?php namespace Modules\Car\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Car\Entities\CModel;
use Modules\Car\Entities\Mark;
use Modules\Car\Entities\Modification;
use Modules\Car\Entities\Type;


class CreateMotorModificationDataTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();
		$mark = Mark::all();
		$carTypes = Type::all();
		$carTypes = array_column($carTypes->toArray(), 'id', 'name');
		$mark = array_column($mark->toArray(), 'id', 'name');

		$data =  json_decode(file_get_contents(base_path('source/seed').'/cars.json'), true);
		foreach ($data as $key => $dt) {
			$idMark = $mark[$dt['mark']];
			$model  = CModel::where('name', $dt['model'])->where('lib_car_mark_id', $idMark)->first();
			$pasrdedDesc = explode(" ", $dt['descr']);
			$typeName = trim($pasrdedDesc[0]);
			$modificator  = Modification::where('name', $dt['modiffDeeper'])->where('lib_car_model_id', $model->id)->first();

			if (!$model) {
				echo "ERROR<pre>"; print_r($dt); echo "</pre>"; die;
			}
			$details =json_decode($dt['details']);
			foreach($details as $detail) {
				$motorData = explode('(',$detail[0]);
				$years = explode(' ',$detail[5]);
				$year_start = trim(strtr($years[0], ['(' => '', ')' => '',' '=> '']));
				$year_finish = trim(strtr($years[2], ['(' => '', ')' => '',' '=> '']));

				$insertData = [
					'lib_car_modification_id' => $modificator->id,
					'year_start' => $year_start,
					'year_end' => $year_finish,
					'motor' => trim($motorData[0]),
					'power' => $detail[1],
					'motor_capacity' => $detail[2],
					'speed_100' => $detail[3],
					'max_speed' => $detail[4],
					'consumption' => $detail[6],

				];
				DB::table('lib_car_motor')->insert($insertData);
			}
		}

	}

}