<?php namespace Modules\Car\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CreateMotoAndTruckDataTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();


		$transportData = [
			[
				'id' => 2,
				'name' => 'Мотоцикл',
				'code' => 'motorcycle',
				'created_at' => new \DateTime('now')
			],
			[
				'id' => 3,
				'name' => 'Грузовая',
				'code' => 'trucker',
				'created_at' => new \DateTime('now')

			]
		];
		$modelData = [
			[
				'name' => 'MAN',
				'code' => 'man',
				'created_at' => new \DateTime('now')
			]
		];

		DB::table('lib_car_transport')->insert( $transportData );

		DB::table('lib_car_mark')->insert( $modelData );
		$markID = DB::connection()->getPdo()->lastInsertId();

		$hondaMark = DB::table('lib_car_mark')
			->where('code', 'honda')
			->first();
		$modelData = [
			[
				'name' => '19',
				'code' => 'man_19',
				'lib_car_mark_id' => $markID,
				'lib_car_transport_id' => 3
			],
			[
				'name' => 'CRF',
				'code' => 'honda_crf_81',
				'lib_car_mark_id' => $hondaMark->id,
				'lib_car_transport_id' => 2
			]
		];
		DB::table('lib_car_model')->insert( $modelData );

		$transportType = [
			[
				'id' => 12,
				'name' => 'Еврофура',
				'code' => generate_code('еврофура'),

			],
			[
				'id' => 13,
				'name' => 'Спорт байк',
				'code' => generate_code('Спорт байк'),

			],
		];
		DB::table('lib_car_type')->insert( $transportType );

		$manModel = DB::table('lib_car_model')
			->where('code', 'man_19')
			->first();
		$hondaModel = DB::table('lib_car_model')
			->where('code', 'honda_crf_81')
			->first();

		$modiffData1 =
			[
				'name' => '19.464 FAS',
				'code' => '19_464_fas_29',
				'year_start' => '1999',
				'year_end' => '2002',
				'lib_car_type_id' => 12,
				'lib_car_model_id' => $manModel->id,
				'created_at' => new \DateTime('now')

			];
		$modiffData2 =
			[
					'name' => 'CRF 70 F',
					'code' => 'crf_70_f_83',
					'year_start' => '1999',
					'year_end' => '2005',
					'lib_car_type_id' => 13,
					'lib_car_model_id' => $hondaModel->id,
					'created_at' => new \DateTime('now')

			];
		DB::table('lib_car_modification')->insert( $modiffData1 );
		$mandModifId = DB::connection()->getPdo()->lastInsertId();
		DB::table('lib_car_modification')->insert( $modiffData2 );
		$hondsdModifId = DB::connection()->getPdo()->lastInsertId();
		$motorDat = [
			[
				'lib_car_modification_id' => $mandModifId,
				'motor' => '25.5V',
				'power' => '460',
				'motor_capacity' => '12816',
				'speed_100' => '',
				'max_speed' => '',
				'year_start' => '1998',
				'year_end' => '2002',
				'consumption' => '',
			],
			[
				'lib_car_modification_id' => $hondsdModifId,
				'motor' => '1.2',
				'power' => '72',
				'motor_capacity' => '',
				'speed_100' => '',
				'max_speed' => '',
				'year_start' => '2004',
				'year_end' => '2008',
				'consumption' => '',
			],

		];
		DB::table('lib_car_motor')->insert( $motorDat );
	}

}