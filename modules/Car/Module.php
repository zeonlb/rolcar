<?php

namespace Modules\Car;

use Modules\Cms\Seo\SiteMapGenerator;
use System\Module\ModuleInterface;
use Pingpong\Menus\MenuFacade as Menu;

class Module implements ModuleInterface
{

    public function getAdminNavigation()
    {
        $menu = Menu::instance('admin.navigation');
        $menu->dropdown('Cars', function ($sub) {
            $sub->url('admin/car/transport', 'Cars');
            $sub->url('admin/car/question', 'Questions');
        },
            6,
            ['icon' => 'fa fa fa-car']
        );
    }

    public function getFrontNavigation()
    {
        // TODO: Implement getFrontNavigation() method.
    }

    public function getDescription()
    {
        // TODO: Implement getDescription() method.
    }

    public function getName()
    {
        // TODO: Implement getName() method.
    }

    public function setSettingFields()
    {
        // TODO: Implement setSettings() method.
    }

    public function getSiteMapData(): SiteMapGenerator
    {
        return new SiteMapGenerator();
    }
}