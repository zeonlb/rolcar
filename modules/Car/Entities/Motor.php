<?php namespace Modules\Car\Entities;
   
use Illuminate\Database\Eloquent\Model;

class Motor extends Model {

    protected $table = 'lib_car_motor';
    protected $fillable = [
        'power',
        'motor',
        'year_start',
        'year_end',
        'power',
        'motor_capacity',
        'speed_100',
        'max_speed',
        'consumption',
        'lib_car_modification_id',
    ];

    /**
     * @return bool
     */
    public function isChild()
    {
        return $this->id !== $this->parent_id;
    }

    public function modification()
    {
        return $this->belongsTo('Modules\Car\Entities\Modification', 'lib_car_modification_id');
    }

    public function meta()
    {
        return $this->belongsTo('Modules\Cms\Entities\Meta', 'cms_meta_id', 'id');
    }

    public function lampOptions()
    {
        return $this->hasOne(Lamps::class, 'lib_car_motor_id', 'id');
    }
    public function questions()
    {
        return $this->hasOne(LampJoinQuestion::class, 'motor_id', 'id');
    }

    public function parent()
    {
        return $this->hasOne(Motor::class, 'id', 'parent_id');
    }



}