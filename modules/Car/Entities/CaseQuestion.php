<?php namespace Modules\Car\Entities;

use Illuminate\Database\Eloquent\Model;

class CaseQuestion extends Model
{
    protected $table = 'lib_car_lamp_type_questions';
    protected $fillable = ['code', 'message'];
}