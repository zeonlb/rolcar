<?php namespace Modules\Car\Entities;
   
use Illuminate\Database\Eloquent\Model;

class Lamps extends Model {

    protected $table = 'lib_car_motor_lamps';
    protected $fillable = ['lib_car_motor_id', 'options'];

    public function motor()
    {
        return $this->belongsTo(Motor::class, 'lib_car_motor_id', 'id');
    }

}