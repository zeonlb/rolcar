<?php namespace Modules\Car\Entities;
   
use Illuminate\Database\Eloquent\Model;

class Type extends Model {

    protected $table = 'lib_car_type';
    protected $fillable = ['name', 'code'];

}