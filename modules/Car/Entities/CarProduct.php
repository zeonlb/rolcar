<?php namespace Modules\Car\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Shop\Entities\Product;

class CarProduct extends Model
{

    protected $table = 'lib_car_shop_product';
    protected $fillable = [
        'transport_id',
        'mark_id',
        'model_id',
        'modification_id',
        'motor_id',
        'product_id'
    ];

    public function products()
    {
        return $this->hasMany(Product::class, 'id', 'product_id');
    }
}