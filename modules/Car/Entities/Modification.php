<?php namespace Modules\Car\Entities;
   
use Illuminate\Database\Eloquent\Model;

class Modification extends Model {

    protected $table = 'lib_car_modification';
    protected $fillable = [
        'name',
        'code',
        'lib_car_model_id',
        'year_start',
        'year_end',
        'osram_code',
        'lib_car_type_id',
    ];

    public function model()
    {
        return $this->belongsTo('Modules\Car\Entities\CModel', 'lib_car_model_id');
    }

    public function motors()
    {
        return $this->hasMany('Modules\Car\Entities\Motor', 'lib_car_modification_id');
    }

    public function type()
    {
        return $this->belongsTo('Modules\Car\Entities\Type', 'lib_car_type_id');
    }

    public function meta()
    {
        return $this->belongsTo('Modules\Cms\Entities\Meta', 'cms_meta_id', 'id');
    }




}