<?php namespace Modules\Car\Entities;

use Illuminate\Database\Eloquent\Model;

class LampJoinQuestion extends Model
{
    protected $table = 'lib_car_motor_j_questions';
    protected $fillable = ['lamp_type', 'lamp_type_question_id', 'tsokol', 'motor_id'];

    public function question()
    {
        return $this->belongsTo(CaseQuestion::class, 'lamp_type_question_id', 'id');
    }
}