<?php namespace Modules\Car\Entities;
   
use Illuminate\Database\Eloquent\Model;

class Mark extends Model {

    protected $table = 'lib_car_mark';
    protected $fillable = ['code','name','img'];


    public function meta()
    {
        return $this->belongsTo('Modules\Cms\Entities\Meta', 'cms_meta_id', 'id');
    }

    public function models()
    {
        return $this->hasMany(CModel::class, 'lib_car_mark_id');
    }

}