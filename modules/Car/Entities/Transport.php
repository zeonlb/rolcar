<?php namespace Modules\Car\Entities;
   
use Illuminate\Database\Eloquent\Model;

class Transport extends Model {

    protected $table = 'lib_car_transport';
    protected $fillable = ['code','name'];

    public function models()
    {
        return $this->hasMany('Modules\Car\Entities\CModel', 'lib_car_transport_id');
    }

    public function meta()
    {
        return $this->belongsTo('Modules\Cms\Entities\Meta', 'cms_meta_id', 'id');
    }
}