<?php namespace Modules\Car\Entities;
   
use Illuminate\Database\Eloquent\Model;

class CModel extends Model {

    protected $table = 'lib_car_model';
    protected $fillable = ['img','code','name','lib_car_mark_id', 'lib_car_transport_id'];

    public function mark()
    {
        return $this->belongsTo('Modules\Car\Entities\Mark', 'lib_car_mark_id');
    }

    public function transport()
    {
        return $this->belongsTo('Modules\Car\Entities\Transport', 'lib_car_transport_id');
    }

    public function modifications()
    {
        return $this->hasMany('Modules\Car\Entities\Modification', 'lib_car_model_id');
    }

    public function meta()
    {
        return $this->belongsTo('Modules\Cms\Entities\Meta', 'cms_meta_id', 'id');
    }


}