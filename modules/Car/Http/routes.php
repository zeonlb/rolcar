<?php

#Admin
Route::group(['prefix' => 'admin', 'middleware' => ['admin.auth', 'admin.menu.storing'], 'namespace' => 'Modules\Car\Http\Controllers'], function()
{
	Route::group(['prefix' => 'car', 'namespace' => 'Admin'], function() {
		Route::resource('transport', 'TransportController');
		Route::resource('transport_type', 'TransportTypeController');
		Route::resource('mark', 'MarkController');
		Route::controller('model', 'ModelController',
			[   'getShow' => 'admin.car.model.show',
				'getCreate' => 'admin.car.model.create',
				'putUpdate' => 'admin.car.model.update',
				'postStore' => 'admin.car.model.store',
				'deleteDestroy' => 'admin.car.model.destroy',
			]
		);
		Route::resource('modification', 'ModificationController');
		Route::resource('motor', 'MotorController');
		Route::resource('lamps', 'MotorLampsController', ['only' => ['edit', 'update']]);
		Route::get('lamps/sync', ['as' => 'admin.car.lamp.sync', 'uses' => 'MotorLampsController@sync']);
		Route::get('catalog/export', ['as' => 'admin.car.catalog.export', 'uses' => 'ExportController@catalog']);
        Route::get('catalog/mark/{id}/export', ['as' => 'admin.car.catalog.mark.export', 'uses' => 'ExportController@mark']);
		Route::post('motors/parent', ['as' => 'admin.car.catalog.motor.parent.update', 'uses' => 'MotorController@parent']);

        Route::resource('question', 'QuestionController');
	});
});

#Front
Route::group(['prefix' => 'catalog', 'namespace' => 'Modules\Car\Http\Controllers\Front'], function()
{
	Route::get('transports', ['as' => 'catalog.transport', 'uses' => 'CatalogController@transports']);
	Route::get('marks/{id}', ['as' => 'catalog.marks', 'uses' => 'CatalogController@marks']);
	Route::get('models/{id}/{transport_id}', ['as' => 'catalog.models', 'uses' => 'CatalogController@models']);
	Route::get('years/{id}', ['as' => 'catalog.years', 'uses' => 'CatalogController@years']);
	Route::get('modifications/{modelId}', ['as' => 'catalog.modifications', 'uses' => 'CatalogController@modifications']);
	Route::get('motors/{id}', ['as' => 'catalog.motors', 'uses' => 'CatalogController@motors']);
	Route::get('lamps/{id}', ['as' => 'catalog.lamps', 'uses' => 'CatalogController@lamps']);
});

Route::post('/sku/search', ['as' => 'sku.search','uses' => 'Modules\Car\Http\Controllers\Front\SkuController@search']);
# API
Route::group(['prefix' => 'api', 'namespace' => 'Modules\Car\Http\Controllers'],
	function () {
		Route::group(['prefix' => 'car', 'namespace' => 'API'], function () {
			Route::controller('transport', 'TransportController');
			Route::controller('mark', 'MarkController');
			Route::controller('model', 'ModelController');
			Route::controller('modification', 'ModificationController');
			Route::controller('motor', 'MotorController');
//			FILTER
			Route::controller('filter', 'FilterController');
		});
	});


