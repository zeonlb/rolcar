<?php
namespace Modules\Car\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Contracts\Validation\Validator;

class ProductListRequest extends Request{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'page' => 'integer',
            'filter' => 'min:1',
            'sortBy' => 'in:price,popular,discounts,count_ordered',
            'dest' => 'in:desc,asc'
        ];
    }

    public function response(array $errors)
    {

        return $this->redirector->to('/')
            ->withInput($this->except($this->dontFlash))
            ->withErrors($errors, $this->errorBag);
    }

}