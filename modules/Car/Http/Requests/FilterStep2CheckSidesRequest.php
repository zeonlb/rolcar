<?php
namespace Modules\Car\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Contracts\Validation\Validator;

class FilterStep2CheckSidesRequest extends Request{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'motorId' => 'required',
        ];
    }

}