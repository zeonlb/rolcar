<?php
namespace Modules\Car\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Contracts\Validation\Validator;

class FilterStep2Request extends Request{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'side' => 'required|max:255|in:front,back,inside',
            'names' => 'required|max:255',
            'transportId' => 'required',
        ];
    }

}