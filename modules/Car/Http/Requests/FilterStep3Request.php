<?php
namespace Modules\Car\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Contracts\Validation\Validator;

class FilterStep3Request extends Request{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'motorId' => 'required',
            'lampType' => 'required|max:255',
        ];
    }

}