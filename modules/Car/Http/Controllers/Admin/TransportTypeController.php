<?php namespace Modules\Car\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilder;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Modules\Car\Entities\Type;
use Modules\Car\EntitiesCModel;
use Illuminate\Support\Facades\View;
use Modules\Car\Forms\TransportTypeForm;
use Modules\Cms\Entities\Meta;
use Pingpong\Modules\Routing\Controller;

class TransportTypeController extends Controller
{
	use FormBuilderTrait;

	protected $formBuilder;

	public function __construct(FormBuilder $formBuilder)
	{
		$this->formBuilder = $formBuilder;
	}

	public function create(Request $request)
	{

		$entity = new Transport();
		$form = $this->formBuilder->create(TransportForm::class, [
			'model' => $entity,
			'novalidate' => '',
			'class' => 'form-horizontal form-label-left',
			'method' => 'POST',
			'url' => route('admin.car.transport.store')

		]);

		$backBtnUrl = route('admin.car.transport.index');
		$header = 'Create new transport';
		return View::make('car::admin.entity_edit', compact('backBtnUrl', 'entity', 'form', 'header'));

	}

	public function store(Request $request)
	{
		$form = $this->form(TransportTypeForm::class);

		// It will automatically use current request, get the rules, and do the validation
		if (!$form->isValid()) {
			return redirect()->back()->withErrors($form->getErrors())->withInput();
		}

		$transportType = Type::create($request->all());

		$transportType->save();

		return redirect()->back()->with('success', 'Transport type successfully added!');

	}

}