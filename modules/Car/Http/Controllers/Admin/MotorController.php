<?php namespace Modules\Car\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilder;
use Illuminate\Support\Facades\View;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Modules\Car\Entities\Modification;
use Modules\Car\Entities\Motor;
use Modules\Car\Forms\MotorForm;
use Modules\Car\Repositories\MotorRepository;
use Modules\Cms\Entities\Meta;
use Pingpong\Modules\Routing\Controller;

/**
 * @property mixed parent_id
 */
class MotorController extends Controller
{
	use FormBuilderTrait;

	protected $formBuilder;

	public function __construct(FormBuilder $formBuilder)
	{
		$this->formBuilder = $formBuilder;
	}

	/**
	 * @param $id
	 * @param MotorRepository $motorRepo
	 * @return mixed
	 */
	public function show($id, MotorRepository $motorRepo)
	{
		$motors  = $motorRepo->getModificationsByModificationId($id);
		$modification =  Modification::findOrFail($id);
		$backBtnUrl = route('admin.car.modification.show', ['id' => $modification->model->id]);

		return View::make('car::admin.motors',
			compact(
				'motors',
				'modification',
				'form',
				'backBtnUrl',
				'id'
			));
	}

    /**
     * @param \App\Http\Requests\Request $request
     * @return mixed
     */
	public function parent(Request $request)
	{
		$motors  = $motorRepo->getModificationsByModificationId($id);
		$modification =  Modification::findOrFail($id);
		$backBtnUrl = route('admin.car.modification.show', ['id' => $modification->model->id]);

		return View::make('car::admin.motors',
			compact(
				'motors',
				'modification',
				'form',
				'backBtnUrl',
				'id'
			));
	}

	public function create(Request $request)
	{

		if(!$id = $request->get('id')) abort(404);

		$entity = new Motor();
		$form = $this->formBuilder->create(MotorForm::class, [
			'model' => $entity,
			'novalidate' => '',
			'class' => 'form-horizontal form-label-left',
			'method' => 'POST',
			'url' => route('admin.car.motor.store')

		]);

		$form->add('lib_car_modification_id', 'hidden', ['value' => $id]);
		if (!$entity->meta) {
			$entity->meta =  new Meta();
		}
		$backBtnUrl = url('admin/car/motor', ['id' => $id]);
		$header = 'Create new motor';
		return View::make('car::admin.entity_edit', compact('backBtnUrl', 'entity', 'form', 'header'));

	}

	public function store(Request $request)
	{
		$form = $this->form(MotorForm::class);

		// It will automatically use current request, get the rules, and do the validation
		if (!$form->isValid()) {
			return redirect()->back()->withErrors($form->getErrors())->withInput();
		}
		$motor = Motor::create($request->all());
		if ($motor->meta) {
			$motor->meta->fill($request->all());
			$motor->meta->save();
		} else {
			$meta = Meta::create($request->all());
			$motor->meta()->associate($meta);
		}
		$motor->save();
        $motor->parent_id = $motor->id;
        $motor->save();

		return redirect()->route('admin.car.motor.show', ['id' => $motor->lib_car_modification_id])->with('success', 'Module successfully added!');

	}


	public function edit($id)
	{
		$entity = Motor::findOrFail($id);
		$form = $this->formBuilder->create(MotorForm::class, [
			'model' => $entity,
			'novalidate' => '',
			'class' => 'form-horizontal form-label-left',
			'method' => 'PUT',
			'url' => route('admin.car.motor.update', ['id' => $entity->id])

		]);
		if (!$entity->meta) {
			$entity->meta =  new Meta();
		}
		$backBtnUrl = url('admin/car/motor', ['id' => $entity->modification->id]);

		$header = 'Edit motor';
		return View::make('car::admin.entity_edit', compact('backBtnUrl','entity', 'form', 'header'));

	}

	public function update($id, Request $request)
	{
        $motor = Motor::findOrFail($id);
		$postData = $request->all();
		if ($postData['parent_id']??false) {
		    if ('delete' == $postData['parent_id']) {
                $motor->parent_id = $motor->id;
            } else {
                $motor->parent_id = $postData['parent_id'];
            }
            $motor->save();
		    return ['status' => 'success'];
        }
		$form = $this->form(MotorForm::class);

		if (!$form->isValid()) {
			return redirect()->back()->withErrors($form->getErrors())->withInput();
		}
		$motor = Motor::findOrFail($id);
		$motor->fill($request->all());
		if ($motor->meta) {
			$motor->meta->fill($postData);
			$motor->meta->save();
		} else {
			$meta = Meta::create($postData);
			$motor->meta()->associate($meta);
		}
		$motor->save();

		return redirect()->back()->with('success', 'Entity successfully updated!');
	}

	public function destroy($id, Request $request)
	{
		$motor = Motor::findOrFail($id);
        $lamps = $motor->lampOptions()->delete();
        $lamps = $motor->questions()->delete();
		$motor->delete();

		return redirect()->back()->with('success', 'Entity successfully deleted!');
	}
}