<?php namespace Modules\Car\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\View;
use Kris\LaravelFormBuilder\FormBuilder;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Modules\Car\Entities\CaseQuestion;
use Modules\Car\Entities\LampJoinQuestion;
use Modules\Car\Entities\Motor;
use Modules\Car\Mangers\TypeLampManager;
use Modules\Shop\Entities\Attribute;
use Modules\Shop\Entities\Category;
use Pingpong\Modules\Routing\Controller;
use Symfony\Component\Process\Exception\LogicException;
use Symfony\Component\Process\Process;

class MotorLampsController extends Controller
{
    use FormBuilderTrait;

    protected $formBuilder;

    public function __construct(FormBuilder $formBuilder)
    {
        $this->formBuilder = $formBuilder;
    }

    public function sync()
    {
        set_time_limit(-1);
        ini_set('max_execution_time', -1);
        $php = env('PHP_BIN', 'php');
        $command = "$php ".base_path().'/artisan car:products-sync';
        $process = new Process($command, null, null, null, 5000);
        $process->run();
        return redirect()->back()->with('success', 'Successfully synchronized!');
    }

    public function edit($id)
    {
        $entity = Motor::with('modification')->findOrFail($id);
        $selectedOptions = [];
        if ($entity->lampOptions && $entity->lampOptions->options) {
            $selectedOptions = json_decode($entity->lampOptions->options, true);
        }
        $cases = [];
        foreach (LampJoinQuestion::where('motor_id', $id)->get()->toArray() as $case) {
            $cases[$case['lamp_type']][] = $case;
        }
        $backBtnUrl = url('admin/car/motor', ['id' => $entity->modification->id]);
        $attribute = Attribute::with('options')->whereCode('tsokol')->first();
        $vAttributes = Attribute::with('options')->whereCode('voltazh')->first();
        $questions = CaseQuestion::all()->toArray();
        $categories = Category::where('code', 'LIKE', 'ft%')->orWhere('code', 'LIKE', 'bt%')->orWhere('code', 'LIKE', 'it%')->get();
//        echo "<pre>"; print_r($categories->toArray()); echo "</pre>"; die;
        if (!$attribute) {
            throw new LogicException('Could not find attribute tsokol');
        }
        return View::make('car::admin.lamps',
            compact('backBtnUrl', 'entity', 'attribute', 'categories', 'selectedOptions', 'questions', 'cases', 'vAttributes'));
    }

    public function update($motorId, Request $request, TypeLampManager $lampManager)
    {
        $fillPercent = (count($request->get('options')) * 100) / $request->get('count_positions');
        $options = $request->get('options');
        $voltages = $request->get('voltage');
        $newOptions = [];
        foreach ($options as $type => $option) {
            foreach ($option as $opt) {
                $newOptions[$type][$opt] =  (isset($voltages[$type][$opt]) && $voltages[$type][$opt]) ? $voltages[$type][$opt] : [];
            }
        }
        $options = $newOptions;
        $cases = $request->get('cases', []);
        $options['filled_percent'] = $fillPercent;
        $motor = Motor::findOrFail($motorId);
        if ($request->get('accept_for_all_motors')) {
            $motorsId = $motor->modification->motors->pluck('id')->toArray();
        } else {
            $motorsId = [$motor->id];
        }
        $lampManager->updateMotorOptions($motorsId, $options, $cases);
        return redirect()->back()->with('success', 'Successfully updated!');
    }

}