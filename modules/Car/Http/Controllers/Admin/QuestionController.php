<?php namespace Modules\Car\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Kris\LaravelFormBuilder\FormBuilder;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Modules\Car\Entities\CaseQuestion;
use Modules\Car\Forms\QuestionForm;
use Pingpong\Modules\Routing\Controller;

class QuestionController extends Controller
{
	use FormBuilderTrait;

	protected $formBuilder;

    /**
     * QuestionController constructor.
     * @param FormBuilder $formBuilder
     */
	public function __construct(FormBuilder $formBuilder)
	{
		$this->formBuilder = $formBuilder;
	}

	public function index()
	{
		$questions = CaseQuestion::all();

		return View::make('car::admin.question.list', compact('questions'));
	}

	public function edit($id)
	{
		$entity = CaseQuestion::findOrFail($id);
		$form = $this->formBuilder->create(QuestionForm::class, [
			'model' => $entity,
			'novalidate' => '',
			'class' => 'form-horizontal form-label-left',
			'method' => 'PUT',
			'url' => route('admin.car.question.update', ['id' => $entity->id])

		]);
		$backBtnUrl = route('admin.car.question.index');

		$header = 'Edit transport';
		return View::make('car::admin.entity_edit', compact('backBtnUrl','entity', 'form', 'header'));

	}
	public function update($id, Request $request)
	{
		$postData = $request->all();
		$form = $this->form(QuestionForm::class);

		if (!$form->isValid()) {
			return redirect()->back()->withErrors($form->getErrors())->withInput();
		}
		$question = CaseQuestion::findOrFail($id);
		$question->fill($request->all());
		$question->save();

		return redirect()->back()->with('success', 'Entity successfully updated!');
	}

	public function destroy($id, Request $request)
	{
		$motor = CaseQuestion::findOrFail($id);
		$motor->delete();

		return redirect()->back()->with('success', 'Entity successfully deleted!');
	}

	public function create(Request $request)
	{

		$entity = new CaseQuestion();
		$form = $this->formBuilder->create(QuestionForm::class, [
			'model' => $entity,
			'novalidate' => '',
			'class' => 'form-horizontal form-label-left',
			'method' => 'POST',
			'url' => route('admin.car.question.store')

		]);

		$backBtnUrl = route('admin.car.question.index');
		$header = 'Create new transport';
		return View::make('car::admin.entity_edit', compact('backBtnUrl', 'entity', 'form', 'header'));

	}

	public function store(Request $request)
	{
		$form = $this->form(QuestionForm::class);

		// It will automatically use current request, get the rules, and do the validation
		if (!$form->isValid()) {
			return redirect()->back()->withErrors($form->getErrors())->withInput();
		}
		$question = CaseQuestion::create($request->all());

		return redirect()->route('admin.car.question.index')->with('success', 'Question successfully added!');

	}

}