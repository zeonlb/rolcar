<?php namespace Modules\Car\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilder;
use Illuminate\Support\Facades\View;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Modules\Car\Entities\CModel;
use Modules\Car\Entities\Modification;
use Modules\Car\Entities\Type;
use Modules\Car\Forms\ModificationForm;
use Modules\Car\Forms\TransportTypeForm;
use Modules\Car\Http\Requests\ModificationCreateRequest;
use Modules\Car\Repositories\ModificationRepository;
use Modules\Cms\Entities\Meta;
use Pingpong\Modules\Routing\Controller;

class ModificationController extends Controller
{
	use FormBuilderTrait;

	protected $formBuilder;

	public function __construct(FormBuilder $formBuilder)
	{
		$this->formBuilder = $formBuilder;
	}

	/**
	 * @param $id
	 * @param ModificationRepository $modificationRepo
	 * @param Request $request
	 * @return mixed
	 */
	public function show($id, ModificationRepository $modificationRepo, Request $request)
	{
		$search = null;
		if ($request->get('search')) {
			$search = $request->get('search');
		}

		$modifications  = $modificationRepo
			->getUniqueModificationsByModelId($id, $search, 'type');
		$filledPercent  = $modificationRepo->calculateFilledPercent($id);

		$model = CModel::findOrFail($id);
		$types = Type::all();
		$transportType =  new Type();
		$form = $this->formBuilder->create(TransportTypeForm::class, [
			'model' => $transportType,
			'novalidate' => '',
			'class' => 'form-horizontal form-label-left',
			'method' => 'POST',
			'url' => route('admin.car.transport_type.store')

		]);

		$backBtnUrl = url('admin/car/model/show', ['id' => $model->mark->id, 'transport_id' => $model->lib_car_transport_id]);

		return View::make(
			'car::admin.modifications',
			compact('backBtnUrl','modifications', 'model', 'types', 'form', 'filledPercent', 'search')
		);
	}

	public function create(Request $request)
	{
		if(!$id = $request->get('id')) abort(404);

		$entity = new Modification();
		$form = $this->formBuilder->create(ModificationForm::class, [
			'model' => $entity,
			'novalidate' => '',
			'class' => 'form-horizontal form-label-left',
			'method' => 'POST',
			'url' => route('admin.car.modification.store')

		]);
		$backBtnUrl = url('admin/car/modification', ['id' => $id]);
		$form->add('lib_car_model_id', 'hidden', ['value' => $id]);
		if (!$entity->meta) {
			$entity->meta =  new Meta();
		}
		$header = 'Create new modification';
		return View::make('car::admin.entity_edit', compact('backBtnUrl','entity', 'form', 'header'));
	}


	public function store(ModificationCreateRequest $request)
	{
		$form = $this->form(ModificationForm::class);

		// It will automatically use current request, get the rules, and do the validation
		if (!$form->isValid()) {
			return redirect()->back()->withErrors($form->getErrors())->withInput();
		}
		$modification = Modification::create($request->all());
		if ($modification->meta) {
			$modification->meta->fill($request->all());
			$modification->meta->save();
		} else {
			$meta = Meta::create($request->all());
			$modification->meta()->associate($meta);
		}

		$modification->save();

		return redirect()->back()->with('success', 'Entity successfully added!');

	}

	public function edit($id, Request $request)
	{

		$entity =  Modification::findOrFail($id);

		$form = $this->formBuilder->create(ModificationForm::class, [
			'model' => $entity,
			'novalidate' => '',
			'class' => 'form-horizontal form-label-left',
			'method' => 'PUT',
			'url' => route('admin.car.modification.update', ['id' => $entity->id])

		]);
		if (!$entity->meta) {
			$entity->meta = new Meta();
		}
		$backBtnUrl = url('admin/car/modification', ['id' => $entity->model->id]);
		$header = 'Edit modification';
		return View::make('car::admin.entity_edit', compact('backBtnUrl', 'entity', 'form', 'header'));
	}

	public function update($id, Request $request)
	{
		$postData = $request->all();
		$form = $this->form(ModificationForm::class);

		if (!$form->isValid()) {
			return redirect()->back()->withErrors($form->getErrors())->withInput();
		}
		$modification = Modification::findOrFail($id);
		$modification->fill($request->all());
		if ($modification->meta) {
			$modification->meta->fill($postData);
			$modification->meta->save();
		} else {
			$meta = Meta::create($postData);
			$modification->meta()->associate($meta);
		}
		$modification->save();

		return redirect()->back()->with('success', 'Modification successfully updated!');
	}


	public function destroy($id, Request $request)
	{
		$modification = Modification::findOrFail($id);
		$modification->delete();

		return redirect()->back()->with('success', 'Entity successfully deleted!');
	}
}