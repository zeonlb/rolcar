<?php namespace Modules\Car\Http\Controllers\Admin;

use App\Traits\SaveFileTrait;
use Kris\LaravelFormBuilder\FormBuilder;
use Illuminate\Support\Facades\View;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Modules\Car\Entities\CModel;
use Modules\Car\Entities\Mark;
use Modules\Car\Entities\Transport;
use Modules\Car\Forms\MarkForm;
use Modules\Car\Forms\ModelForm;
use Modules\Car\Repositories\MarkRepository;
use Modules\Cms\Entities\Meta;
use Pingpong\Modules\Routing\Controller;
use \Illuminate\Http\Request;

class MarkController extends Controller
{
    use SaveFileTrait;
	use FormBuilderTrait;

	protected $formBuilder;

	public function __construct(FormBuilder $formBuilder)
	{
		$this->formBuilder = $formBuilder;
	}

	/**
	 * @param $id transport ID
	 * @return mixed
	 */
	public function show($id, MarkRepository $modelRepo, Request $request)
	{
		$search = null;
		if ($request->get('search')) {
			$search = $request->get('search');
		}

		$marks = $modelRepo->getUniqueMarksByTransportId($id, $search);
		$transport = Transport::findOrFail($id);

		$entity = new CModel();

		$form = $this->formBuilder->create(ModelForm::class, [
			'model' => $entity,
			'novalidate' => '',
			'class' => 'form-horizontal form-label-left',
			'method' => 'POST',
			'url' => route('admin.car.model.store')

		]);
		$backBtnUrl = route('admin.car.transport.index');

		return View::make('car::admin.marks',
			compact('marks', 'transport', 'id', 'form', 'backBtnUrl', 'search'));
	}

	public function create(Request $request)
	{
		if(!$id = $request->get('id')) abort(404);

		$entity = new Mark();
		$form = $this->formBuilder->create(MarkForm::class, [
			'model' => $entity,
			'novalidate' => '',
			'class' => 'form-horizontal form-label-left',
			'method' => 'POST',
			'url' => route('admin.car.mark.store')

		]);

		if (!$entity->meta) {
			$entity->meta =  new Meta();
		}
		$backBtnUrl = url('admin/car/mark', ['id' => $id]);
		$header = 'Create new mark';
		return View::make('car::admin.entity_edit', compact('backBtnUrl', 'entity', 'form', 'header'));

	}

	public function store(Request $request)
	{

		$form = $this->form(MarkForm::class);

		// It will automatically use current request, get the rules, and do the validation
		if (!$form->isValid()) {
			return redirect()->back()->withErrors($form->getErrors())->withInput();
		}
		$mark = new Mark();
		$mark->fill($request->all());
		if ($mark->meta) {
			$mark->meta->fill($request->all());
			$mark->meta->save();
		} else {
			$meta = Meta::create($request->all());
			$mark->meta()->associate($meta);
		}
		if ($request->get('img')) {
            $mark->img = $this->saveFile($request, 'img');

        }
		$mark->save();
		return redirect()->back()->with('success', 'Entity successfully added!');
	}

	public function edit($id, Request $request)
	{
		$entity = Mark::findOrFail($id);

		$form = $this->formBuilder->create(MarkForm::class, [
			'model' => $entity,
			'novalidate' => '',
			'class' => 'form-horizontal form-label-left',
			'method' => 'PUT',
			'url' => route('admin.car.mark.update', ['id' => $entity->id])

		]);
		if ($entity->img) {
            $form->add('Image', 'static', [
                'tag' => 'img', // Tag to be used for holding static data,
                'attr' => ['src' => '/'.$entity->img], // This is the default
            ]);
        }
		if (!$entity->meta) {
			$entity->meta =  new Meta();
		}
		if ($transport_id = $request->get('transport_id'))
			$backBtnUrl = route('admin.car.mark.show', ['id' => $transport_id]);

		$header = 'Edit mark';
		return View::make('car::admin.entity_edit', compact('backBtnUrl','entity', 'form', 'header'));

	}

	public function update($id, Request $request)
	{
		$postData = $request->all();
		$form = $this->form(MarkForm::class);

		if (!$form->isValid()) {
			return redirect()->back()->withErrors($form->getErrors())->withInput();
		}
		$mark = Mark::findOrFail($id);
		$mark->fill($request->all());
		if ($mark->meta) {
			$mark->meta->fill($postData);
			$mark->meta->save();
		} else {
			$meta = Meta::create($postData);
			$mark->meta()->associate($meta);
		}
        if ($request->file('img')) {
            $mark->img = $this->saveFile($request, 'img');
        }
		$mark->save();

		return redirect()->back()->with('success', 'Entity successfully updated!');
	}


	public function destroy($id)
	{
		$mark = Mark::findOrFail($id);
		$mark->delete();

		return redirect()->back()->with('success', 'Entity successfully deleted!');
	}
}