<?php namespace Modules\Car\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilder;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Modules\Car\EntitiesCModel;
use Modules\Car\Entities\Transport;
use Illuminate\Support\Facades\View;
use Modules\Car\Forms\TransportForm;
use Modules\Car\Mangers\TypeLampManager;
use Modules\Cms\Entities\Meta;
use Pingpong\Modules\Routing\Controller;

class TransportController extends Controller
{
	use FormBuilderTrait;

	protected $formBuilder;

	public function __construct(FormBuilder $formBuilder)
	{
		$this->formBuilder = $formBuilder;
	}

	public function index()
	{
		$transports = Transport::all();

		$form = $this->formBuilder->create(TransportForm::class, [
			'method' => 'GET',
			'url' => route('admin.car.transport.create'),
			'novalidate' => '',
			'class' => 'form-horizontal form-label-left'
		]);
		$form->add('lib_car_modification_id', 'hidden');

		return View::make('car::admin.transport', compact('transports', 'form'));
	}

	public function edit($id)
	{
		$entity = Transport::findOrFail($id);
		$form = $this->formBuilder->create(TransportForm::class, [
			'model' => $entity,
			'novalidate' => '',
			'class' => 'form-horizontal form-label-left',
			'method' => 'PUT',
			'url' => route('admin.car.transport.update', ['id' => $entity->id])

		]);
		if (!$entity->meta) {
			$entity->meta =  new Meta();
		}
		$backBtnUrl = route('admin.car.transport.index');

		$header = 'Edit transport';
		return View::make('car::admin.entity_edit', compact('backBtnUrl','entity', 'form', 'header'));

	}
	public function update($id, Request $request)
	{
		$postData = $request->all();
		$form = $this->form(TransportForm::class);

		if (!$form->isValid()) {
			return redirect()->back()->withErrors($form->getErrors())->withInput();
		}
		$motor = Transport::findOrFail($id);
		$motor->fill($request->all());
		if ($motor->meta) {
			$motor->meta->fill($postData);
			$motor->meta->save();
		} else {
			$meta = Meta::create($postData);
			$motor->meta()->associate($meta);
		}
		$motor->save();

		return redirect()->back()->with('success', 'Entity successfully updated!');
	}

	public function destroy($id, Request $request)
	{
		$motor = Transport::findOrFail($id);
		$motor->delete();

		return redirect()->back()->with('success', 'Entity successfully deleted!');
	}

	public function create(Request $request)
	{

		$entity = new Transport();
		$form = $this->formBuilder->create(TransportForm::class, [
			'model' => $entity,
			'novalidate' => '',
			'class' => 'form-horizontal form-label-left',
			'method' => 'POST',
			'url' => route('admin.car.transport.store')

		]);

		if (!$entity->meta) {
			$entity->meta =  new Meta();
		}
		$backBtnUrl = route('admin.car.transport.index');
		$header = 'Create new transport';
		return View::make('car::admin.entity_edit', compact('backBtnUrl', 'entity', 'form', 'header'));

	}

	public function store(Request $request)
	{
		$form = $this->form(TransportForm::class);

		// It will automatically use current request, get the rules, and do the validation
		if (!$form->isValid()) {
			return redirect()->back()->withErrors($form->getErrors())->withInput();
		}
		$transport = Transport::create($request->all());
		if ($transport->meta) {
			$transport->meta->fill($request->all());
			$transport->meta->save();
		} else {
			$meta = Meta::create($request->all());
			$transport->meta()->associate($meta);
		}
		$transport->save();

		return redirect()->route('admin.car.transport.index')->with('success', 'Transport successfully added!');

	}

}