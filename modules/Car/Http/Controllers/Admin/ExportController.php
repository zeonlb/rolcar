<?php namespace Modules\Car\Http\Controllers\Admin;

use Illuminate\Support\Facades\DB;
use Pingpong\Modules\Routing\Controller;

class ExportController extends Controller
{


    public function catalog()
    {
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=export_catalog.csv");
        header("Pragma: no-cache");
        header("Expires: 0");

        $this->exportMarks();
        $this->exportModels();
        $this->exportModifications();
        return ob_get_clean();
    }

    public function mark($id)
    {
        header("Content-type: text/csv;charset=UTF-8");
        header("Content-Disposition: attachment; filename=export_catalog.csv");
        header("Pragma: no-cache");
        header("Expires: 0");

        $this->exportLamps($id);
        return ob_get_clean();
    }


    private function exportMarks()
    {
        $data = DB::select('
            SELECT lib_car_model.*, lib_car_mark.name as mname, lib_car_mark.id as mid,
            lib_car_transport.name as tr_name
            FROM lib_car_model 
            INNER JOIN lib_car_mark ON lib_car_mark_id = lib_car_mark.id
            INNER JOIN lib_car_transport ON lib_car_transport_id = lib_car_transport.id
            GROUP BY lib_car_mark.id
        ');

        $out = fopen("php://output", 'w');
        foreach ($data as $code => $model) {
            fputcsv($out, [$model->tr_name . ' ' . $model->mname, route('catalog.models', [$model->mid, $model->lib_car_transport_id])]);
        }
        fclose($out);
    }

    private function exportModels()
    {
        $models = DB::select('
            SELECT lib_car_model.*, lib_car_mark.name as mark_name, 
            lib_car_model.name as model_name,
            lib_car_transport.name as tr_name,
            lib_car_model.id as model_id
            FROM lib_car_model 
            INNER JOIN lib_car_mark ON lib_car_mark_id = lib_car_mark.id
            INNER JOIN lib_car_modification ON lib_car_model_id = lib_car_model.id
            INNER JOIN lib_car_transport ON lib_car_transport_id = lib_car_transport.id
            GROUP BY lib_car_model_id
        ');
        $out = fopen("php://output", 'a+');
        foreach ($models as $code => $model) {
            fputcsv($out,
                [
                    $model->tr_name . ' ' . $model->mark_name . ' ' . $model->model_name,
                    route('catalog.modifications', [$model->model_id])
                ]);
        }
        fclose($out);
    }

    private function exportModifications()
    {
        $data = DB::select('
            SELECT lib_car_modification.*, lib_car_mark.name as mark_name, 
            lib_car_model.name as model_name,
            lib_car_model.id as model_id,
            lib_car_modification.id as modif_id,
            lib_car_transport.name as tr_name,
            lib_car_type.name as type_name,
            lib_car_modification.name as modif_name
            FROM lib_car_model 
            INNER JOIN lib_car_mark ON lib_car_mark_id = lib_car_mark.id
            INNER JOIN lib_car_modification ON lib_car_model_id = lib_car_model.id
            INNER JOIN lib_car_transport ON lib_car_transport_id = lib_car_transport.id
            INNER JOIN lib_car_type ON lib_car_type_id = lib_car_type.id
        ');
        $out = fopen("php://output", 'a+');
        foreach ($data as $code => $motor) {
            fputcsv($out,
                [
                    $motor->tr_name . ' ' . $motor->mark_name . ' ' .
                    $motor->model_name . ' ' . $motor->type_name . ' ' . $motor->modif_name,
                    route('catalog.motors', [$motor->id])
                ]);
        }
        fclose($out);
    }

    private function exportMotors()
    {
        $data = DB::select('
            SELECT lib_car_motor.*, lib_car_mark.name as mark_name, 
            lib_car_model.name as model_name,
            lib_car_model.id as model_id,
            lib_car_modification.id as modif_id,
            lib_car_transport.name as tr_name,
            lib_car_type.name as type_name,
            lib_car_modification.name as modif_name
            FROM lib_car_model 
            INNER JOIN lib_car_mark ON lib_car_mark_id = lib_car_mark.id
            INNER JOIN lib_car_modification ON lib_car_model_id = lib_car_model.id
            INNER JOIN lib_car_motor ON lib_car_modification_id = lib_car_modification.id
            INNER JOIN lib_car_transport ON lib_car_transport_id = lib_car_transport.id
            INNER JOIN lib_car_type ON lib_car_type_id = lib_car_type.id
            GROUP BY lib_car_motor.id
        ');
        $out = fopen("php://output", 'a+');
        foreach ($data as $code => $motor) {
            $name = $motor->tr_name . ' ' . $motor->mark_name . ' ' .
                $motor->model_name . ' ' . $motor->type_name . ' ' . $motor->modif_name . ' ' .
                $motor->motor . ' (' . $motor->power . ')';
            fputcsv($out,
                [
                    $name,
                    route('catalog.lamps', [$motor->id])
                ]);
        }
        fclose($out);
    }
    private function exportLamps(int $modelId)
    {
        $lamps = require_once(__DIR__.'/../../../Resources/lang/en/lamps.php');
        $lamps = array_map(function($v){ return [$v]; }, $lamps);
        $data = DB::select('
            SELECT lib_car_motor.*, lib_car_mark.name as mark_name, 
            lib_car_model.name as model_name,
            lib_car_model.id as model_id,
            lib_car_modification.id as modif_id,
            lib_car_transport.name as tr_name,
            lib_car_type.name as type_name,
            lib_car_modification.name as modif_name,
            lib_car_motor_lamps.options as lamp_options,
            Q.lamp_type as q_lamp_type,
            Q.tsokol as q_tsokol,
            TQ.message,
            TQ.id as message_id,
            GROUP_CONCAT(Q.lamp_type) as q_l_t,
            GROUP_CONCAT(TQ.message) as q_text
            FROM lib_car_model 
            INNER JOIN lib_car_mark ON lib_car_mark_id = lib_car_mark.id
            INNER JOIN lib_car_modification ON lib_car_model_id = lib_car_model.id
            INNER JOIN lib_car_motor ON lib_car_modification_id = lib_car_modification.id
            INNER JOIN lib_car_transport ON lib_car_transport_id = lib_car_transport.id
            INNER JOIN lib_car_type ON lib_car_type_id = lib_car_type.id
            LEFT JOIN lib_car_motor_j_questions Q ON Q.motor_id = lib_car_motor.id
            LEFT JOIN lib_car_motor_lamps ON lib_car_motor_id = lib_car_motor.id
            LEFT JOIN lib_car_lamp_type_questions TQ ON TQ.id = Q.lamp_type_question_id
            WHERE `lib_car_modification`.`lib_car_model_id` = '.$modelId.'
            GROUP BY lib_car_motor.id
        ');
        $out = fopen("php://output", 'a+');
        $marks = ['Марка'];
        $models =  ['Модель'];
        $years =  ['Год'];
        $modifs = ['Модификация'];
        $motors = ['Движок'];
        foreach ($data as $code => $motor) {
            $questResult = [];
            if ($motor->q_l_t && $motor->q_text) {
                $lampTypes = explode(',', $motor->q_l_t);
                $lampQuestions = explode(',', $motor->q_text);
                foreach ($lampTypes as $key =>  $type) {
                    $questResult[$type][] = $lampQuestions[$key] ?? '';
                }
            }
            $marks[] =  $motor->mark_name;
            $models[] =  $motor->model_name;
            $years[] =  $motor->year_start.' - '.$motor->year_end;
            $modifs[] = $motor->modif_name;
            $motors[] = $motor->motor."({$motor->power})";
            $motor->lamp_options = \json_decode($motor->lamp_options,  JSON_OBJECT_AS_ARRAY);
            foreach ($lamps  as $type => $lamp) {
                $lt = (array_key_exists($type, $motor->lamp_options)) ? implode('/', array_keys($motor->lamp_options[$type])) : '';
                if (isset($questResult[$type])) {
                    $lt .= "\n".implode('/', $questResult[$type]);
                }
                 $lamps[$type][] = $lt;
            }
        }
        fputcsv($out, $marks);
        fputcsv($out, $models);
        fputcsv($out, $years);
        fputcsv($out, $modifs);
        fputcsv($out, $motors);
        foreach ($lamps as $l) {
            fputcsv($out, $l);
        }
        fclose($out);
        $out = file_get_contents('php://output');
        $out = mb_convert_encoding($out, 'UTF-16LE', 'UTF-8');
        $out .= ( chr(0xEF) . chr(0xBB) . chr(0xBF) );
        file_put_contents('php://output', $out);
    }

}