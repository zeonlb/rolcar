<?php namespace Modules\Car\Http\Controllers\Admin;

use App\Traits\SaveFileTrait;
use Kris\LaravelFormBuilder\FormBuilder;
use Illuminate\Support\Facades\View;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Modules\Car\Entities\CModel;
use Modules\Car\Forms\MarkForm;
use Modules\Car\Forms\ModelForm;
use Modules\Car\Repositories\ModelRepository;
use Modules\Cms\Entities\Meta;
use Pingpong\Modules\Routing\Controller;
use \Illuminate\Http\Request;

class ModelController extends Controller
{
	use FormBuilderTrait;
	use SaveFileTrait;

	protected $formBuilder;

	public function __construct(FormBuilder $formBuilder)
	{
		$this->formBuilder = $formBuilder;
	}

	public function getShow($id, $transport_id, ModelRepository $modelRepo, Request $request)
	{
		$search = null;
		if ($request->get('search')) {
			$search = $request->get('search');
		}
		$models = $modelRepo->getUniqueModelsByMarkId($id, $transport_id, $search);
		$filledPercent = $modelRepo->calculateFilledPercent($id, $transport_id);
		$model_form = $this->formBuilder->create(ModelForm::class, [
			'method' => 'GET',
			'url' => route('admin.car.model.update'),
			'novalidate' => '',
			'class' => 'form-horizontal form-label-left'
		]);
		$backBtnUrl = route('admin.car.mark.show', ['id' => $transport_id]);


		return View::make('car::admin.models',
			compact('backBtnUrl', 'models', 'model_form', 'id', 'transport_id', 'filledPercent', 'search'));
	}

	public function getCreate($id, $transportId, Request $request)
	{
		if(!$id || !$transportId) abort(404);

		$entity = new CModel();
		$form = $this->formBuilder->create(ModelForm::class, [
			'model' => $entity,
			'novalidate' => '',
			'class' => 'form-horizontal form-label-left',
			'method' => 'POST',
			'url' => route('admin.car.model.store')

		]);

		$backBtnUrl = url('admin/car/model/show', ['id' => $id, 'transport_id' => $transportId]);

		$form->modify('lib_car_mark_id', 'hidden', ['value' => $id]);
		$form->modify('lib_car_transport_id', 'hidden', ['value' => $transportId]);

		$form->add('lib_car_modification_id', 'hidden', ['value' => $id]);
		if (!$entity->meta) {
			$entity->meta =  new Meta();
		}
		$header = 'Create new model';
		return View::make('car::admin.entity_edit', compact('backBtnUrl', 'entity', 'form', 'header'));

	}
	public function postStore(Request $request)
	{

		$form = $this->form(ModelForm::class);

		// It will automatically use current request, get the rules, and do the validation
		if (!$form->isValid()) {
			return redirect()->back()->withErrors($form->getErrors())->withInput();
		}
		CModel::create($request->all());
		return redirect()->back()->with('success', 'Module successfully added!');
	}


	public function getEdit($id, Request $request)
	{
		$entity = CModel::findOrFail($id);
		$form = $this->formBuilder->create(ModelForm::class, [
			'model' => $entity,
			'novalidate' => '',
			'class' => 'form-horizontal form-label-left',
			'method' => 'PUT',
			'url' => route('admin.car.model.update', ['id' => $entity->id])

		]);
        if ($entity->img) {
            $form->add('Image', 'static', [
                'tag' => 'img', // Tag to be used for holding static data,
                'attr' => ['src' => '/'.$entity->img], // This is the default
            ]);
        }
		if (!$entity->meta) {
			$entity->meta =  new Meta();
		}
		$backBtnUrl = url('admin/car/model/show', ['id' => $entity->lib_car_mark_id, 'transport_id' => $entity->lib_car_transport_id]);

		$header = 'Edit model';
		return View::make('car::admin.entity_edit', compact('backBtnUrl','entity', 'form', 'header'));
	}

	public function putUpdate($id, Request $request)
	{
		$form = $this->form(ModelForm::class);

		// It will automatically use current request, get the rules, and do the validation
		if (!$form->isValid()) {
			return redirect()->back()->withErrors($form->getErrors())->withInput();
		}
		$postData = $request->all();
		$model = CModel::findOrFail($id);
		$model->fill($postData);

		if ($model->meta) {
			$model->meta->fill($postData);
			$model->meta->save();
		} else {
			$meta = Meta::create($postData);
			$model->meta()->associate($meta);
		}
        if ($request->file('img')) {
            $model->img = $this->saveFile($request, 'img');
        }

		$model->save();
		return redirect()->back()->with('success', 'Module successfully updated!');
	}
	public function deleteDestroy($id, Request $request)
	{
		$model = CModel::findOrFail($id);
		$model->delete();

		return redirect()->back()->with('success', 'Entity successfully deleted!');
	}

	public function findModels($markId, Request $request)
	{
		if ($request->ajax())
		{
			echo $markId;
		}
	}

}