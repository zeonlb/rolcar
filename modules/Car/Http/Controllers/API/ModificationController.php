<?php namespace Modules\Car\Http\Controllers\API;

use Illuminate\Http\Request;
use Modules\Car\Repositories\ModelRepository;
use Modules\Car\Repositories\ModificationRepository;
use Pingpong\Modules\Routing\Controller;

class ModificationController extends Controller
{

	public function postFindByModelId(Request $request, ModificationRepository $modificationRepository)
	{
		if ($modelId = $request->get('modelId'))
			return $modificationRepository->getUniqueModificationsByModelId($modelId);

	}

	public function postFindByModelIdAndYear(Request $request, ModificationRepository $modificationRepo)
	{
		$modifications =  $modificationRepo->getModificationsWithYear(
			$request->get('modelId'),
			$request->get('year'));
		$result = [];
		foreach ($modifications as $key => $modification) {
			if ($modification->motors->sum('filled_percent') > 0) {
				$result[] = $modification;
			}
		}

		return $result;
	}


}