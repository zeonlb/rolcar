<?php namespace Modules\Car\Http\Controllers\API;

use Illuminate\Http\Request;
use Modules\Car\Repositories\ModelRepository;
use Modules\Car\Repositories\MotorRepository;
use Pingpong\Modules\Routing\Controller;

class MotorController extends Controller
{

	public function postFindByModificationId(Request $request, MotorRepository $motorRepository)
	{
		if ($modificationId = $request->get('modificationId'))
		{
			$filter = [];
			if ($year = $request->get('year')) $filter['year'] = $year;
			return $motorRepository->getModificationsByModificationIdOnlyFilled($modificationId, $filter);
		}
	}
	public function postFindByModelIdReturnYears(Request $request, ModelRepository $modelRepository)
	{

		$yearsData  = $modelRepository->getModelMotorsOnlyFilled($request->get('modelId'));
        $yearsParsed = [];
		foreach ($yearsData as $year) {
			if ($year->year_end == 0) {
				$year->year_end = (new \DateTime())->format('Y');
			}
            $reason = $year->year_end - $year->year_start;
            $years = [];
            for ($i = 0; $i <= $reason; $i++) {
                $currentYear = $year->year_start++;
                $yearsParsed[$currentYear] = $currentYear;
            }
		}
		sort($yearsParsed);
//		$first = array_shift($yearsParsed);
//		$last = array_pop($yearsParsed);
//		$reason = $last - $first;
//		$years = [];
//		for ($i = 0; $i <= $reason; $i++) {
//			$years[] = $first++;
//		}
		return $yearsParsed;
	}

	public function postFindMotorsUsingModificationIdAndYear(
		Request $request,
		MotorRepository $motorRepository)
	{
		return $motorRepository->getModificationsByModificationId(
			$request->get('modificationId'),
			['year' => $request->get('year')]
		);
	}
}