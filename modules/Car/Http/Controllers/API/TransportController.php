<?php namespace Modules\Car\Http\Controllers\API;

use Modules\Car\Entities\Transport;
use Pingpong\Modules\Routing\Controller;

class TransportController extends Controller
{
	public function postList()
	{
		return Transport::all()->toJson();
	}

}