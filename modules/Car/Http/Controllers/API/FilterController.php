<?php namespace Modules\Car\Http\Controllers\API;

use Illuminate\Http\Request;
use Modules\Car\Entities\LampJoinQuestion;
use Modules\Car\Entities\Lamps;
use Modules\Car\Entities\Motor;
use Modules\Car\Http\Requests\FilterStep1Request;
use Modules\Car\Http\Requests\FilterStep2CheckSidesRequest;
use Modules\Car\Http\Requests\FilterStep2Request;
use Modules\Car\Http\Requests\FilterStep3Request;
use Modules\Car\Mangers\TypeLampManager;
use Modules\Car\Mappers\FilterAttributeMapper;
use Modules\Car\Repositories\CarProductRepository;
use Modules\Car\Repositories\MarkRepository;
use Modules\Car\Repositories\ModelRepository;
use Modules\Car\Repositories\MotorRepository;
use Modules\Shop\Entities\AttributeOption;
use Modules\Shop\Entities\Category;
use Modules\Shop\Repositories\ProductRepository;
use Pingpong\Modules\Routing\Controller;
use Illuminate\Support\Facades\View;

class FilterController extends Controller
{

    public function postStep1(FilterStep1Request $request, MarkRepository $markRepository, ModelRepository $modelRepository)
    {
        if ($key = $request->get('id')) {
            $marks = $modelRepository->getUniqueMarksByTransportId($key);
            if (!$marks) abort(404);
            switch ($key) {
                case 1:
                    return View::make(
                        'car::filter.step1.car',
                        compact('marks')
                    );
                    break;
                case 2:
                    return View::make(
                        'car::filter.step1.motorcycle',
                        compact('marks')
                    );
                    break;
                case 3:
                    return View::make(
                        'car::filter.step1.truck',
                        compact('marks')
                    );
                    break;
            }
        }

    }

    public function postStep2(
        FilterStep2Request $request
    )
    {
        $content = null;
        $insideDisabled = false;
        $side = $request->get('side');
        $selected = explode('|', $request->get('names'));
        $lamps = $this->getEnalbeldTypeLampsList($request->get('motorId'));
        if ($key = $request->get('transportId')) {
            switch ($key) {
                case 1:
                    $content = View::make('car::filter.step2.car-' . $side, ['lamps' => $lamps])->render();
                    $title = "Ваш автомобиль";
                    break;
                case 2:
                    $content = View::make('car::filter.step2.motorcycle-' . $side, ['lamps' => $lamps])->render();
                    $title = "Ваш мотоцикл";
                    $insideDisabled = true;
                    break;
                case 3:
                    $content = View::make('car::filter.step2.truck-' . $side, ['lamps' => $lamps])->render();
                    $title = "Ваш грузовой автомобиль";
                    $insideDisabled = true;
                    break;
            }
        }

        return View::make(
            'car::filter.step2.general',
            compact('content', 'title', 'selected', 'insideDisabled', 'side')
        );
    }

    public function postStep3(FilterStep3Request $request)
    {
        $response = ['success' => false, 'message' => null];
        $motor = Motor::findOrFail($request->get('motorId'));
        $category = Category::where('code', $request->get('lampType'))->firstOrFail();
        $lamp = Lamps::where('lib_car_motor_id', $request->get('motorId'))->firstOrFail();
        $attributes = [
            'category' => $motor->modification->model->mark->code . '-' . $motor->modification->code . '-' . $motor->id,
            'naznachenie' => $category->code
        ];
        $options = json_decode($lamp->options, JSON_OBJECT_AS_ARRAY);
        if (!array_key_exists($request->get('lampType'), $options) || !$voltage = $options[$request->get('lampType')]) {
            $response['message'] = 'ERROR';
            return $response;
        }
        if ((count($voltage) === 1 && strtolower(key($voltage)) === 'led') || strtolower($request->get('questionAns')) === 'led') {
            $response['message'] = 'LED';
            return $response;
        }
        if ($request->get('questionAns')) {
            $attributes['tsokol'] = $request->get('questionAns');
            if (!array_key_exists($request->get('questionAns'), $voltage)) {
                $response['message'] = 'ERROR';
                return $response;
            }
            $attributes['voltage'] = implode('-', $voltage[$request->get('questionAns')]);
        } else {
            $attributes['tsokol'] = key($voltage);
            $attributes['voltage'] = implode('-', current($voltage));
        }
        $url = route('product.index', $attributes);
        $response['success'] = true;
        $response['message'] = $url;
        return $response;
    }

    public function postCheckEnabledSides(FilterStep2CheckSidesRequest $request)
    {
        return $this->getEnalbeldTypeLampsList($request->get('motorId'));
    }

    public function postFindQuestions(FilterStep3Request $request)
    {
        $questions = LampJoinQuestion::where(
            ['motor_id' => $request->get('motorId'), 'lamp_type' => $request->get('lampType')]
        )->with('question')->get();
        return $questions;
    }

    private function getEnalbeldTypeLampsList($motorId)
    {
        $productRepo = new ProductRepository();
        $motor = Motor::with('lampOptions')->findOrFail($motorId);
        $enabledOptions = $productRepo->getProductsFilterOptions();
        $lamps = [];
        $lamps['ft'] = [];
        $lamps['bt'] = [];
        $lamps['it'] = [];
        if ($motor->lampOptions->options) {
            foreach (json_decode($motor->lampOptions->options, true) as $type => $lampOptions) {
                if(!is_array($lampOptions)) {
                    continue;
                }
                foreach ($lampOptions as $tsokol => $voltages) {
                    $tsokol = strtolower($tsokol);
                    if ('led' == $tsokol) {
                        $lamps[substr($type, 0, 2)][$type] = $type;
                        continue;
                    }
                    if (is_array($voltages)) {
                        foreach ($voltages as $voltage) {
                            $voltage = strtr(strtolower($voltage), ['v' => '', 'V' =>'']);
                            $uCode = strtolower($tsokol . '_' . $voltage);
                            $revUCode = strtolower($voltage . '_' . $tsokol);
                            if (in_array($uCode, $enabledOptions) ||
                                in_array($revUCode, $enabledOptions) ||
                                in_array(strtolower($tsokol), ['led'])
                            ) {
                                $lamps[substr($type, 0, 2)][$type] = $type;
                            }
                        }
                    }

                }
            }
        }
        return $lamps;
    }
}