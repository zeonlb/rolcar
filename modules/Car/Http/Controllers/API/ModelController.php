<?php namespace Modules\Car\Http\Controllers\API;

use Illuminate\Http\Request;
use Modules\Car\Repositories\ModelRepository;
use Pingpong\Modules\Routing\Controller;

class ModelController extends Controller
{

	public function postFindByMarkId(Request $request, ModelRepository $modelRepository)
	{
		$markId = $request->get('markId');
		$transportId = $request->get('transportId');

		if ($markId && $transportId) {
			return 	$modelRepository->getUniqueModelsByMarkIdOnlyFilled($markId, $transportId);
		}
	}

}