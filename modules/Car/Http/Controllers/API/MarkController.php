<?php namespace Modules\Car\Http\Controllers\API;

use Illuminate\Http\Request;
use Modules\Car\Repositories\MarkRepository;
use Pingpong\Modules\Routing\Controller;

class MarkController extends Controller
{

	public function postFindByTransportId(Request $request, MarkRepository $markRepository)
	{
		if ($transportId = $request->get('transportId'))
			return $markRepository->getUniqueMarksByTransportId($transportId);
	}

}