<?php namespace Modules\Car\Http\Controllers\Front;

use Illuminate\Http\Request;
use Modules\Car\Core\BreadCrumbGenerator;
use Modules\Car\Entities\CModel;
use Modules\Car\Entities\Mark;
use Modules\Car\Entities\Modification;
use Modules\Car\Entities\Motor;
use Modules\Car\Entities\Transport;
use Modules\Car\Mangers\TypeLampManager;
use Modules\Car\Repositories\CarProductRepository;
use Modules\Car\Repositories\MarkRepository;
use Modules\Car\Repositories\ModelRepository;
use Modules\Car\Repositories\ModificationRepository;
use Modules\Car\Repositories\MotorRepository;
use Modules\Cms\Entities\Meta;
use Modules\Cms\Entities\Page;
use Modules\Cms\Entities\PageSettings;
use Modules\Cms\System\MetaGenerator;
use Modules\Cms\System\Navigation\NavigationGenerator;
use Modules\Shop\Repositories\ProductRepository;
use Pingpong\Modules\Routing\Controller;
use Illuminate\Support\Facades\View;
use Pingpong\Themes\ThemeFacade as Theme;

class CatalogController extends Controller
{
    public function __construct()
    {
        $navigationGenerator = new NavigationGenerator();
        $navigationGenerator->generate();
    }

    public function transports(Request $request)
    {
        $pageInfo = new Page();
        $pageInfo->meta = new Meta();
        $routeName = $request->getPathInfo();
        $transports = Transport::all();
        return Theme::view('catalog.transport',
            compact('transports', 'pageInfo')
        );
    }

    public function marks($id, MarkRepository $modelRepo)
    {
        $pageInfo = new Page();

        $marksPack = $this->getMarksPack($id);
        $transportId = $id;
        $transport = Transport::findOrFail($id);
        $pageInfo->meta = $transport->meta;
        $breadCrumbs = BreadCrumbGenerator::make([$transport]);
        return Theme::view('catalog.marks',
            compact('marksPack', 'pageInfo', 'transportId', 'breadCrumbs'));
    }

    public function models(
        $markId,
        $transportId,
        ModelRepository $modelRepo
    )
    {
        $pageInfo = new Page();
        $marksPack = $this->getMarksPack($transportId);
        $pageSettings = PageSettings::findOrNew(1);
        $models = $modelRepo->getUniqueModelsByMarkIdOnlyFilled($markId, $transportId);
        $selectedMark = Mark::findOrFail($markId);
        $transport = Transport::findOrFail($transportId);
        $breadCrumbs = BreadCrumbGenerator::make([$transport, $selectedMark]);
        $pageInfo->meta = MetaGenerator::make(
            $selectedMark->meta, 'catalog_model',
            [
                'transport' => $transport->name,
                'mark' => $selectedMark->name
            ]
        );

        return Theme::view('catalog.models',
            compact('pageSettings', 'marksPack', 'selectedMark', 'models', 'markId', 'pageInfo', 'transportId',
                'breadCrumbs'));
    }


    public function modifications($modelId, ModificationRepository $modificationRepo)
    {
        $pageInfo = new Page();
        $modifications = $modificationRepo->getUniqueModificationsByModelIdOnlyFilled($modelId);
        $model = CModel::with(['mark', 'transport'])->findOrFail($modelId);
        $transportId = $model->lib_car_transport_id;
        $markId = $model->lib_car_mark_id;
        $marksPack = $this->getMarksPack($transportId);
        $breadCrumbs = BreadCrumbGenerator::make([$model]);
        $pageSettings = PageSettings::findOrNew(1);
        $pageInfo->meta = MetaGenerator::make(
            $model->meta,
            'catalog_modifications',
            [
                'transport' => $model->transport->name,
                'mark' => $model->mark->name,
                'model' => $model->name
            ]
        );  
        $modificationsPack = $modifications->chunk(8);


        return Theme::view('catalog.modifications',
            compact('pageSettings', 'model', 'markId', 'breadCrumbs', 'transportId', 'marksPack', 'modificationsPack',
                'pageInfo', 'year'));
    }

    public function motors($id, MotorRepository $motorRepository)
    {
        $pageInfo = new Page();
        $motors = $motorRepository->getModificationsByModificationIdOnlyFilled($id);
        $modification = Modification::with(['meta', 'model.transport'])->findOrFail($id);
        $transportId = $modification->model->lib_car_transport_id;
        $pageSettings = PageSettings::findOrNew(1);
        $markId = $modification->model->lib_car_mark_id;
        $marksPack = $this->getMarksPack($transportId);
        $breadCrumbs = BreadCrumbGenerator::make([$modification]);

        $pageInfo->meta = MetaGenerator::make(
            $modification->meta,
            'catalog_motors',
            [
                'transport' => $modification->model->transport->name,
                'mark' => $modification->model->mark->name,
                'model' => $modification->model->name,
                'modification' => $modification->name
            ]
        );

        $motorsPack = $motors->chunk(8);


        return Theme::view('catalog.motors',
            compact('pageSettings', 'modification', 'markId', 'breadCrumbs', 'transportId', 'marksPack', 'motorsPack',
                'pageInfo', 'year'));
    }

    public function lamps($id, CarProductRepository $carProductRepository, ProductRepository $productRepo)
    {
        $pageInfo = new Page();

        $motor = Motor::with(['meta', 'lampOptions', 'modification.model.mark', 'modification.model.transport'])
            ->findOrFail($id);
        if ($motor->isChild()) {
            return redirect()->route('catalog.lamps',['id' => $motor->parent->id])->setStatusCode(301);
        }
        $productRepo = new ProductRepository();
        $enabledOptions = $productRepo->getProductsFilterOptions();
        $lamps = [];
        $lamps['ft'] = [];
        $lamps['bt'] = [];
        $lamps['it'] = [];
        if ($motor->lampOptions->options) {
            foreach (json_decode($motor->lampOptions->options, true) as $type => $lampOptions) {
                if(!is_array($lampOptions)) {
                    continue;
                }
                foreach ($lampOptions as $tsokol => $voltages) {
                    $tsokol = strtolower($tsokol);
                    foreach ($voltages as $voltage) {
                        $voltage = strtr(strtolower($voltage), ['v' => '', 'V' =>'']);
                        $uCode = strtolower($tsokol . '_' . $voltage);
                        $revUCode = strtolower($voltage . '_' . $tsokol);
                        if (in_array($uCode, $enabledOptions) ||
                            in_array($revUCode, $enabledOptions)
                        ) {
                            $lamps[substr($type, 0, 2)][$type] = $type;
                        }
                    }
                }

            }
        }
        $transportId = $motor->modification->model->lib_car_transport_id;
        $category = $motor->modification->model->mark->code . '-' . $motor->modification->code . '-' . $motor->id;
        $markId = $motor->modification->model->lib_car_mark_id;
        $marksPack = $this->getMarksPack($transportId);
        $breadCrumbs = BreadCrumbGenerator::make([$motor]);
        $pageInfo->meta = MetaGenerator::make(
            $motor->meta,
            'catalog_lamps',
            [
                'transport' => $motor->modification->model->transport->name,
                'mark' => $motor->modification->model->mark->name,
                'model' => $motor->modification->model->name,
                'modification' => $motor->modification->name,
                'motor' => $motor->motor,
                'power' => $motor->power
            ]
        );
        $pageSettings = PageSettings::findOrNew(1);
        return Theme::view('catalog.lamps',
            compact('pageSettings', 'marksPack', 'category', 'lamps', 'motor', 'markId', 'breadCrumbs', 'transportId',
                'pageInfo', 'year'));
    }

    private function getMarksPack($transportId)
    {
        $modelRepo = new ModelRepository();
        $marks = $modelRepo->getUniqueMarksByTransportId($transportId);
        return $marks->chunk(8);
    }

    public function years($id, ModelRepository $modelRepository)
    {

        $model = CModel::findOrFail($id);
        $pageInfo = new Page();
        $pageInfo->meta = $model->meta;
        $yearsData = $modelRepository->getModelMotors($id);

        $transportId = $model->lib_car_transport_id;
        $markId = $model->lib_car_mark_id;
        $marksPack = $this->getMarksPack($transportId);
        $yearsParsed = [];
        foreach ($yearsData as $year) {
            if (is_int(strpos($year->year_end, 'н.в'))) {
                $year->year_end = (new \DateTime())->format('Y');
            }

            $yearsParsed[$year->year_start] = $year->year_start;
            $yearsParsed[$year->year_end] = $year->year_end;

        }

        sort($yearsParsed);

        $first = array_shift($yearsParsed);
        $last = array_pop($yearsParsed);
        $reason = $last - $first;
        $years = [];
        for ($i = 0; $i <= $reason; $i++) {
            $years[] = $first++;
        }
        $yearsPack = array_chunk($years, 8);
        return Theme::view('catalog.years', compact('yearsPack', 'model', 'marksPack', 'markId', 'transportId'));
    }


}