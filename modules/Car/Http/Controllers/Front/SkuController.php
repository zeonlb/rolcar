<?php namespace Modules\Car\Http\Controllers\Front;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Blade;
use Modules\Cms\Entities\Meta;
use Modules\Cms\Entities\Page;
use Modules\Cms\Entities\PageSettings;
use Modules\Cms\Http\Requests\SkuSearchRequest;
use Modules\Cms\Repositories\PageRepository;
use Modules\Cms\System\Navigation\NavigationGenerator;
use Modules\Shop\Entities\Product;
use Pingpong\Modules\Routing\Controller;
use Pingpong\Themes\ThemeFacade as Theme;

class SkuController extends Controller
{
    public function __construct(PageRepository $pageRepository)
    {
        $navigationGenerator =  new NavigationGenerator();
        $navigationGenerator->generate();
        $this->pageRepo = $pageRepository;
    }

    public function index(Request $request)
    {
        $routeName = $request->getPathInfo();
        $pageInfo = $this->pageRepo->getPageInfo($routeName);
        $pageInfo->html = Blade::compileString($pageInfo->html);

        $products = Product::where('visible', true)->with('brand')->get();
        $productPack = [];
        foreach ($products as $product) {
            $productPack[$product->brand->name][] = $product;
        }
        $pageSettings = PageSettings::findOrNew(1);

        return Theme::view('page', compact('pageInfo', 'productPack', 'pageSettings'));
    }

    public function search(SkuSearchRequest $request)
    {


        $product = Product::where(['visible' =>  true])
            ->where('sku', 'like', '%'.trim($request->get('sku')).'%')
            ->first();
        if (!$product) {
            return redirect()->back()->withErrors(sprintf('Товар с артикулом %s в базе не найдет!', $request->get('sku')));
        }
        return redirect()->route('product.show', ['code' => $product->code]);
    }


}