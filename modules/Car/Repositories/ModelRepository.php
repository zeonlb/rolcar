<?php namespace Modules\Car\Repositories;

use Illuminate\Support\Facades\DB;
use Modules\Car\Entities\CModel;

class ModelRepository
{
    public function getUniqueMarksByTransportId($transportId)
    {
        return CModel::select('lib_car_mark.*', DB::raw('SUM(lib_car_motor.filled_percent) as total_filled'))
            ->join('lib_car_mark', 'lib_car_mark.id', '=', 'lib_car_model.lib_car_mark_id')
            ->join('lib_car_modification', 'lib_car_model.id', '=', 'lib_car_modification.lib_car_model_id')
            ->join('lib_car_motor', 'lib_car_modification.id', '=', 'lib_car_motor.lib_car_modification_id')
            ->where('lib_car_model.lib_car_transport_id', $transportId)
            ->having('total_filled', '>', 0)
            ->groupBy('lib_car_mark.name')
            ->get();

    }

    public function calculateFilledPercent($markID, $transportId)
    {
        $result = DB::table('lib_car_model as MOD')
            ->select('MOD.id as id',
                DB::raw('(((SELECT SUM(`filled_percent`) FROM `lib_car_motor` WHERE `lib_car_modification_id` = MN.id) * 0.01)/(SELECT COUNT(`filled_percent`) FROM `lib_car_motor` WHERE `lib_car_modification_id` = MN.id)*100)  as total_percent'),
                DB::raw('(SELECT COUNT(id) FROM `lib_car_modification` WHERE `lib_car_model_id` = MOD.id)  as count'))
            ->where('MOD.lib_car_mark_id', $markID)
            ->where('MOD.lib_car_transport_id', $transportId)
            ->join('lib_car_modification as MN', 'MN.lib_car_model_id', '=', 'MOD.id')
            ->get();
        $total = [];
        foreach ($result as $res) {
            if (!isset($total[$res->id])) {
                $total[$res->id]['percent'] = (int)$res->total_percent;
                $total[$res->id]['count'] = $res->count;
            } else {
                $total[$res->id]['percent'] += (int)$res->total_percent;
            }
        }

        return $total;
    }

    public function getUniqueModelsByMarkId($markID, $transportId, $search = null)
    {
        $query = CModel::where('lib_car_model.lib_car_mark_id', $markID)
            ->where('lib_car_model.lib_car_transport_id', $transportId);
        if ($search) {
            $query->where('name', 'like', "%$search%");
        }

        return $query->groupBy('lib_car_model.name')->get();

    }

    public function getUniqueModelsByMarkIdOnlyFilled($markID, $transportId)
    {
        $query = CModel::select('lib_car_model.*', DB::raw('SUM(lib_car_motor.filled_percent) as total_filled'))
            ->join('lib_car_modification', 'lib_car_model.id', '=', 'lib_car_modification.lib_car_model_id')
            ->join('lib_car_motor', 'lib_car_modification.id', '=', 'lib_car_motor.lib_car_modification_id')
            ->where('lib_car_model.lib_car_mark_id', $markID)
            ->where('lib_car_model.lib_car_transport_id', $transportId)
            ->having('total_filled', '>', 0);


        return $query->groupBy('lib_car_model.name')->get();

    }

    public function getModelMotors($modelId)
    {
        $result = DB::table('lib_car_modification as LCM')
            ->select('LCMOT.year_start', 'LCMOT.year_end')
            ->join('lib_car_motor as LCMOT', 'LCM.id', '=', 'LCMOT.lib_car_modification_id')
            ->where('LCM.lib_car_model_id', $modelId)
            ->groupBy('LCMOT.id');
        return $result->get();

    }


    public function getModelMotorsOnlyFilled($modelId)
    {
        $result = DB::table('lib_car_modification as LCM')
            ->select('LCMOT.year_start', 'LCMOT.year_end', DB::raw('SUM(LCMOT.filled_percent) as total_filled'))
            ->join('lib_car_motor as LCMOT', 'LCM.id', '=', 'LCMOT.lib_car_modification_id')
            ->where('LCM.lib_car_model_id', $modelId)
            ->having('total_filled', '>', 0)
            ->groupBy('LCMOT.id');
        return $result->get();

    }
}