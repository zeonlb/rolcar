<?php namespace Modules\Car\Repositories;

use Illuminate\Support\Facades\DB;
use Modules\Car\Entities\CarProduct;
use Modules\Car\Entities\Motor;

class CarProductRepository
{
    /**
     * @param $motorId
     * @return mixed
     */
    public function getActualLapmTypes($motorId)
    {
        return  DB::table('lib_car_shop_product as lcsp')
            ->select(['sc.code'])
            ->where('lcsp.motor_id', $motorId)
            ->join('shop_product as sp', 'lcsp.product_id', '=', 'sp.id')
            ->join('shop_product_categories as spc', 'lcsp.product_id', '=', 'spc.shop_product_id')
            ->join('shop_category as sc', 'sc.id', '=', 'spc.shop_category_id')
            ->where('sp.visible', '=' , true)

            ->orderBy('sc.code')
            ->groupBy('sc.code')
            ->get();
    }

    /**
     * @param $motorId
     * @return mixed
     */
    public function getTypeLamps($motorId)
    {
        return  DB::table('lib_car_motor_lamps as lcsp')
            ->select(['lcsp.options'])
            ->where('lcsp.lib_car_motor_id', $motorId)
            ->groupBy('lcsp.id')
            ->first();
    }
}