<?php namespace Modules\Car\Repositories;

use Illuminate\Support\Facades\DB;
use Modules\Car\Entities\Motor;

class MotorRepository
{
    /**
     * @param $modificationId
     * @param array $where
     */
    public function getModificationsByModificationId($modificationId, $where = [])
    {
        $query = Motor::where('lib_car_motor.lib_car_modification_id', $modificationId);
        if ($where) {
            foreach ($where as $key => $value) {
                switch ($key) {
                    case "year" :
                        $query->where('year_start', '<=', $value)
                              ->where('year_end', '>=', $value);

                        break;
                }
            }
        }
        $query->with(['modification.model.mark', 'modification.model.transport']);
        return $query->groupBy('lib_car_motor.id')
                     ->orderBy('lib_car_motor.motor')
                     ->orderBy('lib_car_motor.power')
                     ->get();
    }

    /**
     * @param $modificationId
     * @param array $where
     */
    public function getModificationsByModificationIdOnlyFilled($modificationId, $where = [])
    {
        $query = Motor::select('lib_car_motor.*', DB::raw('SUM(lib_car_motor.filled_percent) as total_filled'))
            ->where('lib_car_motor.lib_car_modification_id', $modificationId)
            ->whereRaw('lib_car_motor.id = lib_car_motor.parent_id')
        ;
        if ($where) {
            foreach ($where as $key => $value) {
                switch ($key) {
                    case "year" :
                        $query->where('year_start', '<=', $value)
                            ->where(function($q) use($value) {
                                $q->where('year_end', '>=', $value)
                                    ->orWhere('year_end', '=', 0)->orWhere('year_end', '=', 'н.в.');
                            });

                        break;
                }
            }
        }
        $query->with(['modification.model.mark', 'modification.model.transport'])
              ->having('total_filled', '>', 0);
        return $query->groupBy('lib_car_motor.id')
            ->orderBy('lib_car_motor.motor')
            ->get();
    }
}