<?php namespace Modules\Car\Repositories;

use Illuminate\Support\Facades\DB;
use Modules\Car\Entities\CModel;

class MarkRepository
{
    public function getUniqueMarksByTransportId($transportId, $search = null)
    {

        $query = CModel::join('lib_car_mark', 'lib_car_mark.id', '=', 'lib_car_model.lib_car_mark_id')
            ->where('lib_car_model.lib_car_transport_id', $transportId);
        if ($search) {
            $query->where('lib_car_mark.name', 'like', "%$search%");
        }
        return $query->groupBy('lib_car_mark.id')->get();

    }

    public function getUniqueModelsByMarkId($markID)
    {
        return CModel::where('lib_car_model.lib_car_mark_id', $markID)
            ->groupBy('lib_car_model.name')
            ->get();

    }
}