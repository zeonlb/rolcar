<?php namespace Modules\Car\Repositories;


use Illuminate\Support\Facades\DB;
use Modules\Car\Entities\Modification;

class ModificationRepository
{

    public function getUniqueModificationsByModelId($modelId, $search = null, $with = null)
    {
        $query = Modification::where('lib_car_modification.lib_car_model_id', $modelId)
            ->with('model', 'motors');
        if ($search) {
            $query->where('lib_car_modification.name', 'like', "%$search%");
        }
        if ($with) {
            $query->with($with);
        }

        return  $query->groupBy('lib_car_modification.id')->orderBy('lib_car_modification.id')->get();

    }

    public function getUniqueModificationsByModelIdOnlyFilled($modelId)
    {
//        $query = Modification::select('lib_car_modification.*', DB::raw('SUM(lib_car_motor.filled_percent) as total_filled'))
        $query = Modification::select('lib_car_modification.*')->with('type')
//            ->join('lib_car_motor', 'lib_car_modification.id', '=', 'lib_car_motor.lib_car_modification_id')
            ->where('lib_car_modification.lib_car_model_id', $modelId)
//            ->having('total_filled', '>', 0)
        ;
        return $query
//            ->groupBy('lib_car_modification.name')
            ->orderBy('lib_car_modification.id')->get();
    }

    public function calculateFilledPercent($modelId)
    {
        $result = DB::table('lib_car_modification as MOD')
            ->select('MOD.id as id', DB::raw('(((SELECT SUM(`filled_percent`) FROM `lib_car_motor` WHERE `lib_car_modification_id` = MOD.id) * 0.01)/(SELECT COUNT(`filled_percent`) FROM `lib_car_motor` WHERE `lib_car_modification_id` = MOD.id)*100)  as total_percent'))
            ->where('lib_car_model_id', $modelId)
            ->get();
        return array_column($result,  'total_percent', 'id');
    }

    public function getModificationsWithYear($modelId, $year)
    {
        $modifications =  Modification::where('lib_car_modification.lib_car_model_id', $modelId)
            ->with(['motors' => function($q) use ($year) {
                $q->where('year_start', '<=', $year)
                ->whereRaw('lib_car_motor.id = lib_car_motor.parent_id')
                ->where(function($query) use ($year) {
                    $query->where('year_end', '>=', $year)
                            ->orWhere('year_end', '=', 0)->orWhere('year_end', '=', 'н.в.');
                });
            }])
            ->with('type')
            ->orderBy('name')
            ->groupBy('id')
            ->get();

        return $modifications;
    }
}