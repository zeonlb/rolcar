<?php namespace Modules\Car\Repositories;

use Illuminate\Support\Facades\DB;
use Modules\Car\Entities\Motor;


class LampRepository
{
    public function getTotalFilledPercent()
    {
        $percent = \DB::table('lib_car_motor')->select(  DB::raw('(((SELECT SUM(`filled_percent`) FROM `lib_car_motor`) * 0.01)/(SELECT COUNT(`filled_percent`) FROM `lib_car_motor`)*100)  as total_percent'))->limit(1)->get();

        return $percent[0]->total_percent;
    }



}