@extends('admin::layouts.master')

@section('content')
    <h1>Settings</h1>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">

                <div class="clearfix"></div>
            </div>
            <div class="x_content">

                <form name="add-new-page" enctype="multipart/form-data"
                      action="{{route('admin.blog.settings.update')}}"
                      method="POST"
                      class="form-horizontal form-label-left" novalidate="">
                    <input type="hidden" name="_method" value="put"/>
                    <div class="" role="tabpanel" data-example-id="togglable-tabs">
                        <div id="myTabContent" class="tab-content">
                            <div>
                                <span class="section">Meta data</span>

                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="meta_title">Meta
                                        title</label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input id="meta_title" class="form-control col-md-7 col-xs-12"
                                               name="meta_title"
                                               placeholder="Title" type="text" value="{{$settings->meta->meta_title}}">
                                    </div>
                                </div>

                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="meta_keywords">Meta
                                        keys</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input id="tags_1" name="meta_keywords"
                                               type="text" class="tags form-control" value="{{$settings->meta->meta_keywords}}"/>
                                        <div id="suggestions-container"
                                             style="position: relative; float: left; width: 250px; margin: 10px;"></div>
                                    </div>
                                </div>
                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="meta_redirect_url">Meta
                                        redirect url</label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input placeholder="/new/url"
                                               type="text" id="meta_redirect_url" name="meta_redirect_url"
                                               class="form-control col-md-7 col-xs-12" value="{{$settings->meta->meta_redirect_url}}">
                                    </div>
                                </div>

                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="meta_description">Meta
                                        description</label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea placeholder="Your page description" id="meta_description"
                                                  name="meta_description"
                                                  class="form-control col-md-7 col-xs-12">{{$settings->meta->meta_description}}</textarea>
                                    </div>
                                </div>
                                <span class="section">Seo data</span>

                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="meta_description">Seo
                                        text</label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea placeholder="Your seo text" id="seo_text" name="seo_text"
                                                  class="form-control col-md-7 col-xs-12">{{$settings->meta->seo_text}}</textarea>
                                    </div>
                                </div>
                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="meta_description">Seo
                                        text 2</label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea placeholder="Your seo text" id="seo_text_2" name="seo_text_2"
                                                  class="form-control col-md-7 col-xs-12">{{$settings->meta->seo_text_2}}</textarea>
                                    </div>
                                </div>
                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="meta_description">Seo
                                        footer text</label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea placeholder="Your seo footer text" id="seo_footer_text"
                                                  name="seo_footer_text"
                                                  class="form-control col-md-7 col-xs-12">{{$settings->meta->seo_footer_text}}</textarea>
                                    </div>
                                </div>

                            </div>
                            <div role="tabpane3" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
                                <div class="col-md-12">

                                    <p>Drag multiple files to the box below for multi upload or click to select files.
                                        This is for demonstration purposes only, the files are not uploaded to any
                                        server.</p>

                                    <div class="dropzone dz-clickable" id="myDrop">
                                        <div class="dz-default dz-message" data-dz-message="">
                                            <span>Drop files here to upload</span>
                                        </div>
                                    </div>

                                    <br/>
                                    <br/>
                                    <br/>
                                    <br/>
                                </div>

                                <div class="clearfix"></div>
                                <div class="ln_solid"></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                            <button id="send" type="submit" class="btn btn-success">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    @include('admin::partials.js.form')

    <!-- input tags -->
    <script>
        function onAddTag(tag) {
            alert("Added a tag: " + tag);
        }

        function onRemoveTag(tag) {
            alert("Removed a tag: " + tag);
        }

        function onChangeTag(input, tag) {
            alert("Changed a tag: " + tag);
        }

        $(function () {
            $('#tags_1').tagsInput({
                width: 'auto'
            });
        });
    </script>
    <!-- /input tags -->

    <!-- Text editor -->
    <script>
        CKEDITOR.replace('short_content');
        CKEDITOR.config.htmlEncodeOutput = false;
        CKEDITOR.config.entities = false;
        CKEDITOR.config.basicEntities = false;
        CKEDITOR.config.configentities = false;
        CKEDITOR.config.forceSimpleAmpersand = true;
        CKEDITOR.config.allowedContent = true;
        CKEDITOR.config.extraPlugins = 'font';
        CKEDITOR.config.skin = 'bootstrapck';
    </script>
    <!-- /Text editor -->
    <!-- Text editor -->
    <script>
        CKEDITOR.replace('content');
        CKEDITOR.config.htmlEncodeOutput = false;
        CKEDITOR.config.entities = false;
        CKEDITOR.config.basicEntities = false;
        CKEDITOR.config.configentities = false;
        CKEDITOR.config.forceSimpleAmpersand = true;
        CKEDITOR.config.allowedContent = true;
        CKEDITOR.config.extraPlugins = 'font';
        CKEDITOR.config.skin = 'bootstrapck';
    </script>
    <!-- /Text editor -->
    <!-- Text editor -->
    <script>
        CKEDITOR.replace('seo_footer_text');
        CKEDITOR.config.htmlEncodeOutput = false;
        CKEDITOR.config.entities = false;
        CKEDITOR.config.basicEntities = false;
        CKEDITOR.config.configentities = false;
        CKEDITOR.config.forceSimpleAmpersand = true;
        CKEDITOR.config.allowedContent = true;
        CKEDITOR.config.extraPlugins = 'font';
        CKEDITOR.config.skin = 'bootstrapck';
    </script>
    <!-- /Text editor -->
    <!-- Text editor -->
    <script>
        CKEDITOR.replace('seo_text');
        CKEDITOR.config.htmlEncodeOutput = false;
        CKEDITOR.config.entities = false;
        CKEDITOR.config.basicEntities = false;
        CKEDITOR.config.configentities = false;
        CKEDITOR.config.forceSimpleAmpersand = true;
        CKEDITOR.config.allowedContent = true;
        CKEDITOR.config.extraPlugins = 'font';
        CKEDITOR.config.skin = 'bootstrapck';
    </script>
    <!-- /Text editor -->
    <!-- Text editor -->
    <script>
        CKEDITOR.replace('seo_text_2');
        CKEDITOR.config.htmlEncodeOutput = false;
        CKEDITOR.config.entities = false;
        CKEDITOR.config.basicEntities = false;
        CKEDITOR.config.configentities = false;
        CKEDITOR.config.forceSimpleAmpersand = true;
        CKEDITOR.config.allowedContent = true;
        CKEDITOR.config.extraPlugins = 'font';
        CKEDITOR.config.skin = 'bootstrapck';
    </script>
    <!-- /Text editor -->

@stop
@section('style')
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.0.2/css/font-awesome.css" rel="stylesheet">
    <link href="{{Module::asset('admin:css/editor/external/google-code-prettify/prettify.css')}}" rel="stylesheet">
    <link href="{{Module::asset('admin:css/editor/index.css')}}" rel="stylesheet">
@stop

