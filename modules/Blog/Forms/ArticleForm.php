<?php namespace Modules\Blog\Forms;

use Kris\LaravelFormBuilder\Form;
use Modules\Car\Entities\Mark;
use Modules\Car\Entities\Transport;


class ArticleForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('title', 'text', [
                'rules' => 'required|min:2',
                'label' => 'Title'
            ])
            ->add('code', 'text', [
                'rules' => 'required|min:2',
                'label' => 'Code'
            ])
            ->add('short_content', 'textarea', [
                'rules' => 'required|min:2',
                'label' => 'Short content'
            ])
            ->add('content', 'textarea', [
                'rules' => 'required|min:2',
                'label' => 'Content'
            ])
            ->add('code', 'text', [
                'rules' => 'required|min:2',
                'label' => 'Name'
            ])
            ->add('code', 'text',
                ['rules' => 'required|min:2'])

            ->add('submit', 'submit',
                [
                    'label' => 'Save',
                    'template' => 'admin::vendor.laravel-form-builder.button_modal',
                    'attr' => ['class' => 'btn btn-success']]);
    }
}