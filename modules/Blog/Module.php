<?php

namespace Modules\Blog;

use Modules\Blog\Entities\Article;
use Modules\Cms\Seo\SiteMapGenerator;
use Modules\Shop\Entities\Order;
use Modules\Shop\Entities\ProductComment;
use System\Module\ModuleInterface;
use Pingpong\Menus\MenuFacade as Menu;

class Module implements ModuleInterface
{

    public function getAdminNavigation()
    {
        $menu = Menu::instance('admin.navigation');
        $menu->dropdown('Blog', function ($sub) {
            $sub->url('admin/blog/article', 'Articles');
            $sub->url('admin/blog/settings', 'Settings');
        },
            3,
            ['icon' => 'fa fa-file']
        );


    }

    public function getFrontNavigation()
    {
        // TODO: Implement getFrontNavigation() method.
    }

    public function getDescription()
    {
        // TODO: Implement getDescription() method.
    }

    public function getName()
    {
        // TODO: Implement getName() method.
    }

    public function setSettingFields()
    {
        // TODO: Implement setSettings() method.
    }

    public function getSiteMapData() : SiteMapGenerator
    {
        $generator = new SiteMapGenerator('');
        $generator->sitemapFileName = "sitemap-blog.xml";
        $generator->addUrl(route('blog.list'), date('c'), 'daily', 0.8);

        $articles =   $articles = Article::where('is_enabled', 1)
            ->where('activate_date', '<=', (new \DateTime()))->get();
        foreach ($articles as $article) {
            $generator->addUrl(route('blog.view', $article->code),
                date('c'), 'daily', 0.9);
        }
        return $generator;

    }
}