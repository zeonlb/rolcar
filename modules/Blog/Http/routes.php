<?php

#Admin
Route::group(['prefix' => 'admin', 'middleware' => ['admin.auth', 'admin.menu.storing'],
    'namespace' => 'Modules\Blog\Http\Controllers'],
    function () {
        Route::group(['prefix' => 'blog', 'namespace' => 'Admin'], function () {
            Route::resource('article', 'ArticleController');
            Route::get('settings', 'ArticleController@settings');
            Route::put('settings', ['as' => 'admin.blog.settings.update', 'uses' => 'ArticleController@settingsUpdate']);;
        });

    });

Route::get('blog', ['as' => 'blog.list', 'uses' => 'Modules\Blog\Http\Controllers\Front\ArticleController@index']);
Route::get('blog/{code}', ['as' => 'blog.view', 'uses' => 'Modules\Blog\Http\Controllers\Front\ArticleController@view']);

