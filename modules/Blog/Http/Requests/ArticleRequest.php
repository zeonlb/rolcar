<?php

namespace Modules\Blog\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Contracts\Validation\Validator;

class ArticleRequest extends Request
{


    public function authorize()
    {
        return true;
    }

    public function rules(\Illuminate\Http\Request $request)
    {
        return [
            'title' => 'required|min:2',
            'code' => 'required|unique:blog_articles,code,'.$this->segment(4),
            'content' => 'required|min:2',
            'short_content' => 'required|min:2',
            'is_enabled' => 'required',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => '"Имя" обязательное поле',
            'email.required' => '"Email" обязательное поле',
            'email.email' => 'Поле "Email" имеет не верный формат',
            'message.required' => '"Сообщение" обязательное поле',
        ];
    }
}