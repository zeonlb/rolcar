<?php
/**
 * Created by PhpStorm.
 * User: rovel
 * Date: 24.06.17
 * Time: 18:21
 */

namespace Modules\Blog\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\SaveFileTrait;
use Modules\Blog\Entities\Article;
use Illuminate\Support\Facades\View;
use Modules\Blog\Entities\BlogSettings;
use Modules\Blog\Http\Requests\ArticleRequest;
use Modules\Cms\Entities\File;
use Modules\Cms\Entities\Meta;

class ArticleController extends Controller
{
    use SaveFileTrait;


    public function index()
    {
        $articles = Article::orderBy('created_at', 'desc')->get();
        return View::make('blog::admin.list', compact('articles'));
    }

    /**
     * @return mixed
     */
    public function create()
    {
        return View::make('blog::admin.create');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $article = Article::findOrFail($id);
        return View::make('blog::admin.edit', compact('article'));
    }

    /**
     * @return mixed
     */
    public function settings()
    {
        $settings = BlogSettings::firstOrCreate([]);
        if (!$settings->meta) {
            $settings->meta = new Meta();
        }
        return View::make('blog::admin.settings', compact('settings'));
    }

    /**
     * @return mixed
     */
    public function settingsUpdate(Request $request)
    {
        $settings = BlogSettings::firstOrCreate([]);
        if (!$settings->meta){
            $meta = Meta::create($request->all());
            $settings->cms_meta_id = $meta->id;
            $settings->save();
        } else {
            $settings->meta->fill($request->all());
            $settings->meta->save();
        }

        return redirect()->back()->with('success', 'Settings successfully saved!');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        $article = Article::findOrFail($id);
        $article->meta()->delete();
        if ($article->images()) {
            \DB::table('article_photos')->where('article_id', $article->id)->delete();
        }
        $article->delete();

        return redirect()->route('admin.blog.article.index')->with('success', 'Article successfully deleted!');
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ArticleRequest $request, $id)
    {
        $article = Article::findOrFail($id);
        $postData = $request->all();
        $postData['code'] = generate_code($postData['code']);
        if ($postData['activate_date']) {
            $postData['activate_date'] = new \DateTime($postData['activate_date']);
        }
        $postData['code'] = generate_code($postData['code']);
        $article->fill($postData);
        $article->meta->fill($postData);
        $article->meta->save();

        if ($request->file('file')) {
            $article->img = $this->saveFile($request);
        }

        $article->save();

        return redirect()->back()->with('success', 'Article successfully updated');
    }

    /**
     * @param ArticleRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ArticleRequest $request)
    {
        $postData = $request->all();
        $postData['owner_id'] = $request->user()->id;
        $postData['code'] = generate_code($postData['code']);
        if ($postData['activate_date']) {
            $postData['activate_date'] = new \DateTime($postData['activate_date']);
        }
        $article = Article::create($postData);
        $meta = Meta::create($request->all());
        $article->meta()->associate($meta);


        if ($request->file('file')) {
            $article->img = $this->saveFile($request);
        }

        $article->save();

        if (is_array($request->get('files'))) {
            $files = File::whereIn('id', $request->get('files'))->get();
            if ($files) {
                $filesArray = [];
                foreach ($files as $file) {
                    $filesArray[] = $file;
                }
                $article->images()->saveMany($filesArray);
            }
        }

        return redirect()->back()->with('success', 'Article successfully added');
    }


}