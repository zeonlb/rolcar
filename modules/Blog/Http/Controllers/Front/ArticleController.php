<?php

namespace Modules\Blog\Http\Controllers\Front;


use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Modules\Blog\Entities\Article;
use Modules\Blog\Entities\BlogSettings;
use Modules\Cms\Entities\Meta;
use Modules\Cms\Entities\Page;
use Modules\Cms\System\Navigation\NavigationGenerator;
use Modules\Shop\Utils\Checkout\CheckoutCartManager;
use Pingpong\Themes\ThemeFacade as Theme;

/**
 * Created by PhpStorm.
 * User: rovel
 * Date: 25.06.17
 * Time: 21:18
 */
class ArticleController extends Controller
{
    /**
     * ExportController constructor.
     */
    public function __construct()
    {
        $navigationGenerator = new NavigationGenerator();
        $this->cartManager = new CheckoutCartManager();
        $navigationGenerator->generate();
    }

    public function index()
    {
        $articles = Article::where('is_enabled', 1)
            ->where('activate_date', '<=', (new \DateTime()))
            ->orderBy('created_at', 'desc')->paginate(16);
        $pageInfo = new Page();
        $settings = BlogSettings::first();
        if ($settings && $settings->meta) {
            $pageInfo->meta = $settings->meta;
        } else {
            $pageInfo->meta = new Meta();
        }

        $breadCrumbs = [
            'Интернет-магазин автоламп' =>  url('/'),
            'Блог' =>  '',

        ];
        return Theme::view('blog.list', compact('articles', 'pageInfo', 'breadCrumbs')
        );
    }

    public function view($code)
    {
        $article = Article::where('is_enabled', 1)->where('code', $code)->first();
        if (!$article) {
            abort(404);
        }
        $pageInfo = new Page();
        if ($article->meta) {
            $pageInfo->meta = $article->meta;
        } else {
            $pageInfo->meta = new Meta();
        }
        $breadCrumbs = [
            'Интернет-магазин автоламп' =>  url('/'),
            'Блог' =>  route('blog.list'),
            $article->title => ''

        ];
        return Theme::view('blog.view', compact('article', 'pageInfo', 'breadCrumbs')
        );
    }

}