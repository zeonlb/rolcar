<?php namespace Modules\Blog\Entities;
   
use Illuminate\Database\Eloquent\Model;
use Modules\Cms\Entities\File;
use Modules\Cms\Entities\Meta;
use Modules\Cms\Entities\User;

class BlogSettings extends Model {

    protected $table = 'blog_settings';


    protected $fillable = [
        'cms_meta_id',
    ];
    protected $dates = ['created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function meta()
    {
        return $this->belongsTo(Meta::class, 'cms_meta_id');
    }
}