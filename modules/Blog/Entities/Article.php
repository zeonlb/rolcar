<?php namespace Modules\Blog\Entities;
   
use Illuminate\Database\Eloquent\Model;
use Modules\Cms\Entities\File;
use Modules\Cms\Entities\Meta;
use Modules\Cms\Entities\User;

class Article extends Model {

    protected $table = 'blog_articles';


    protected $fillable = [
        'code',
        'title',
        'short_content',
        'content',
        'owner_id',
        'is_enabled',
        'img',
        'cms_meta_id',
        'activate_date',
        'img'
    ];
    protected $dates = ['created_at', 'updated_at', 'activate_date'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function meta()
    {
        return $this->belongsTo(Meta::class, 'cms_meta_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function owner()
    {
        return $this->belongsTo(User::class, 'owner_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function images()
    {
        return $this->belongsToMany(File::class, 'article_photos', 'article_id', 'photo_id');
    }


}