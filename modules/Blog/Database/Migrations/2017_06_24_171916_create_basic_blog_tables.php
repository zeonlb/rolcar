<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBasicBlogTables extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog_articles', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('code')->unique();
            $table->string('title');
            $table->string('short_content')->default(null);
            $table->longText('content');
            $table->integer('owner_id')->unsigned();
            $table->string('img')->default(null);
            $table->boolean('is_enabled')->default(null);
            $table->integer('cms_meta_id')->unsigned()->nullable();


            $table->timestamps();

            $table->foreign('owner_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('blog_articles', function(Blueprint $table) {
            $table->dropForeign('blog_articles_owner_id_foreign');
        });
        Schema::drop('blog_articles');
    }

}
