<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogPhotosTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('article_photos', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('article_id')->unsigned();
            $table->integer('photo_id')->unsigned();

            $table->timestamps();

            $table->foreign('article_id')->references('id')->on('blog_articles');
            $table->foreign('photo_id')->references('id')->on('cms_files');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('article_photos', function(Blueprint $table) {
            $table->dropForeign('article_photos_article_id_foreign');
            $table->dropForeign('article_photos_photo_id_foreign');
        });
        Schema::drop('article_photos');
    }
}
