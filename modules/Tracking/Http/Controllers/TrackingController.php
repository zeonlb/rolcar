<?php namespace Modules\Tracking\Http\Controllers;

use DB;
use Exception;
use Illuminate\Http\Request;
use Modules\Log\Entities\File;
use Pingpong\Modules\Routing\Controller;

class TrackingController extends Controller
{

    /**
     * @return mixed
     */
    public function getIndex(Request $request)
    {
        $content = $request->get('content', '');
        try {
            $decoded = json_decode(base64_decode($content), true);
            if (json_last_error()) {
                return response('', 204);
            }
            $data = array_merge($decoded, session('utm_data', []));
            $trackData = [
                'id' => md5(\request()->getClientIp().date('Y-m-d').$data['uniq'] . implode(array_values(session('utm_data', [])))),
                'identity' => $data['uniq'],
                'user_agent' => $data['user_agent'],
                'path' => $data['path'],
                'ip' => request()->getClientIp(),
                'gclid' => $data['gclid'] ?? '',
                'yclid' => $data['yclid'] ?? '',
                'ymclid' => $data['ymclid'] ?? '',
                'fbclid' => $data['fbclid'] ?? '',
                'utm_source' => $data['utm_source'] ?? '',
                'utm_medium' => $data['utm_medium'] ?? '',
                'utm_campaign' => $data['utm_campaign'] ?? '',
                'utm_term' => $data['utm_term'] ?? '',
                'utm_content' => $data['utm_content'] ?? '',
                'created_at' => date('Y-m-d H:i:s'),
            ];
            $pdo = DB::connection()->getPdo();
            $sql = sprintf('INSERT IGNORE INTO tracking_log (%s) VALUE (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', implode(',', array_keys($trackData)));
            $q = $pdo->prepare($sql);
            $q->bindParam(1, $trackData['id']);
            $q->bindParam(2, $trackData['identity']);
            $q->bindParam(3, $trackData['user_agent']);
            $q->bindParam(4, $trackData['path']);
            $q->bindParam(5, $trackData['ip']);
            $q->bindParam(6, $trackData['gclid']);
            $q->bindParam(7, $trackData['yclid']);
            $q->bindParam(8, $trackData['ymclid']);
            $q->bindParam(9, $trackData['fbclid']);
            $q->bindParam(10, $trackData['utm_source']);
            $q->bindParam(11, $trackData['utm_medium']);
            $q->bindParam(12, $trackData['utm_campaign']);
            $q->bindParam(13, $trackData['utm_term']);
            $q->bindParam(14, $trackData['utm_content']);
            $q->bindParam(15, $trackData['created_at']);
            $q->execute();

        } catch (Exception $e) {
            throw $e;
        }
        return response('', 204);
    }

}