<?php

namespace Modules\Tracking;

use Modules\Cms\Seo\SiteMapGenerator;
use Modules\Cms\Services\SiteMap\SiteMapObject;
use Pingpong\Menus\MenuFacade as Menu;
use System\Module\ModuleInterface;

class Module implements ModuleInterface
{
    public function getAdminNavigation()
    {
//        $menu = Menu::instance('admin.navigation');
//        $menu->url(route('admin.log.log.index'), 'Logs ', 99, ['icon' => 'fa fa-exclamation-triangle']);
    }

    public function getFrontNavigation()
    {
    }

    public function getDescription()
    {
        // TODO: Implement getDescription() method.
    }

    public function getName()
    {
        // TODO: Implement getName() method.
    }

    public function setSettingFields()
    {
        // TODO: Implement setSettings() method.
    }

    public function getSiteMapHtmlData(): SiteMapObject
    {
        return new SiteMapObject('log');
    }

    public function getSiteMapData(): SiteMapGenerator
    {
        // TODO: Implement getSiteMapData() method.
    }
}