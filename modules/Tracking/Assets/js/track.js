import Fingerprint2 from 'fingerprintjs2';

if (window.requestIdleCallback) {
    requestIdleCallback(function () {
        Fingerprint2.get(handleClientFunc)
    })
} else {
    setTimeout(function () {
        Fingerprint2.get(handleClientFunc)
    }, 500)
}

var handleClientFunc = function (components) {
    var values = components.map(function (component) {
        return component.value
    });

    var uniq = Fingerprint2.x64hash128(values.join(), 31);
    var data = {
        uniq,
        title: unescape(encodeURIComponent(document.title)),
        path: window.location.pathname,
        user_agent: navigator.userAgent,
    };
    console.log(data);
    const xhr = new XMLHttpRequest();
    xhr.open('GET', '/tracking?content=' + encodeURIComponent(btoa(JSON.stringify(data))), true);
    xhr.withCredentials = true;
    xhr.send();

};