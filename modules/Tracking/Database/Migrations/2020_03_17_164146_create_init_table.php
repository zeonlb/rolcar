<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInitTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tracking_log', function (Blueprint $table) {
            $table->string('id', 32);
            $table->string('identity');
            $table->string('user_agent');
            $table->string('path');
            $table->string('gclid');
            $table->string('yclid');
            $table->string('ymclid');
            $table->string('fbclid');
            $table->string('utm_source');
            $table->string('utm_medium');
            $table->string('utm_campaign');
            $table->string('utm_term');
            $table->string('utm_content');
            $table->timestamp('created_at');
            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('init');
    }

}
