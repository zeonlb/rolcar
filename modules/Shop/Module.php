<?php

namespace Modules\Shop;

use Modules\Cms\Seo\SiteMapGenerator;
use Modules\Shop\Entities\Order;
use Modules\Shop\Entities\ProductComment;
use System\Module\ModuleInterface;
use Pingpong\Menus\MenuFacade as Menu;

class Module implements ModuleInterface
{

    public function getAdminNavigation()
    {
        $totalCountHtml = $countOrdersHtml = $countCommentsHtml = '';
        $countComments = $countOrders = 0;
        $menu = Menu::instance('admin.navigation');
        $countComments = ProductComment::where('processed', false)->count();
        $countOrders = Order::where('status', 'new')->count();
        $total = $countOrders + $countComments;
        if ($total > 0) {
            $totalCountHtml = '&nbsp&nbsp<span class="badge bg-green">' . $total . '</span>';
            if ($countComments)
                $countCommentsHtml = '&nbsp&nbsp<span class="badge bg-green">' . $countComments . '</span>';
            if ($countOrders)
                $countOrdersHtml = '&nbsp&nbsp<span class="badge bg-green">' . $countOrders . '</span>';
        }
        $menu->dropdown('Shop' . $totalCountHtml, function ($sub) use ($countCommentsHtml, $countOrdersHtml) {
            $sub->url('admin/shop/attribute', 'Attributes');
            $sub->url('admin/shop/category', 'Categories');
            $sub->url('admin/shop/product_type', 'Product type');
            $sub->url('admin/shop/product', 'Products');
            $sub->url('admin/shop/brand', 'Brands');
            $sub->url('admin/shop/comment', 'Comments' . $countCommentsHtml);
            $sub->url('admin/shop/order', 'Orders' . $countOrdersHtml);
            $sub->url('admin/shop/banner', 'Banners');
            $sub->url('admin/shop/icon', 'Icons');
            $sub->url('admin/shop/config/settings', 'Settings')->order(999);
        },
            3,
            ['icon' => 'fa fa-shopping-cart']
        );


    }

    public function getFrontNavigation()
    {
        // TODO: Implement getFrontNavigation() method.
    }

    public function getDescription()
    {
        // TODO: Implement getDescription() method.
    }

    public function getName()
    {
        // TODO: Implement getName() method.
    }

    public function setSettingFields()
    {
        // TODO: Implement setSettings() method.
    }


    public function getSiteMapData(): SiteMapGenerator
    {
        return new SiteMapGenerator();
    }
}