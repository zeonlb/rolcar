<?php namespace Modules\Shop\Entities;
   
use Illuminate\Database\Eloquent\Model;

class ProductOptions extends Model {
    protected $table = 'shop_product_options';
    protected $fillable = [
        'value'
    ];

    public function products()
    {
        return $this->belongsTo('Modules\Shop\Entities\Product', 'shop_product_id', 'id');
    }
    public function attribute()
    {
        return $this->belongsTo('Modules\Shop\Entities\Attribute', 'shop_attribute_id', 'id');
    }

    public function productOptions()
    {
        return $this->belongsTo('Modules\Shop\Entities\AttributeOption', 'shop_product_option_id', 'id');
    }
}