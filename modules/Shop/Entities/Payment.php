<?php namespace Modules\Shop\Entities;


use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    const STATUS_NEW = 1;
    const STATUS_SUCCESS = 2;
    const STATUS_FAILED = 3;
    const STATUS_UNPAID = 4;
    const STATUS_WAIT = 5;

    const PAYMENT_TYPE_VISA_MASTER_NAME = 'visa_master';
    const PAYMENT_TYPE_PRIVAT_24_NAME = 'privat24';
    const PAYMENT_TYPE_PART_INSTALLMENTS_NAME = 'part_in_installments';
    const PAYMENT_TYPE_INSTANT_INSTALLMENTS_NAME = 'instant_installments';
    const PAYMENT_TYPE_BEZNAL_NAME = 'beznal';
    const PAYMENT_TYPE_NALICHNIE_NAME = 'nalichnie';
    const PAYMENT_TYPE_PROM_PAY_NAME = 'prom_pay';
    const PAYMENT_TYPE_SEND_BANK_CARD_NAME = 'to_bank_card';

    const PAYMENT_TYPE_VISA_MASTER_TYPE = 1;
    const PAYMENT_TYPE_PRIVAT_24_TYPE = 2;
    const PAYMENT_TYPE_NALICHNIE_TYPE = 3;
    const PAYMENT_TYPE_BEZNAL_TYPE = 4;
    const PAYMENT_TYPE_PROM_PAY = 5;
    const PAYMENT_TYPE_PART_INSTALLMENTS = 6;
    const PAYMENT_TYPE_INSTANT_INSTALLMENTS = 7;
    const PAYMENT_TYPE_SEND_BANK_CARD = 8;


    const PAYMENT_NAME_TYPE_MAP = [
        self::PAYMENT_TYPE_SEND_BANK_CARD_NAME => self::PAYMENT_TYPE_SEND_BANK_CARD,
        self::PAYMENT_TYPE_VISA_MASTER_NAME => self::PAYMENT_TYPE_VISA_MASTER_TYPE,
        self::PAYMENT_TYPE_PRIVAT_24_NAME => self::PAYMENT_TYPE_PRIVAT_24_TYPE,
        self::PAYMENT_TYPE_NALICHNIE_NAME => self::PAYMENT_TYPE_NALICHNIE_TYPE,
        self::PAYMENT_TYPE_BEZNAL_NAME => self::PAYMENT_TYPE_BEZNAL_TYPE,
        self::PAYMENT_TYPE_PROM_PAY_NAME => self::PAYMENT_TYPE_PROM_PAY,
        self::PAYMENT_TYPE_PART_INSTALLMENTS_NAME => self::PAYMENT_TYPE_PART_INSTALLMENTS,
        self::PAYMENT_TYPE_INSTANT_INSTALLMENTS_NAME => self::PAYMENT_TYPE_INSTANT_INSTALLMENTS,
    ];

    const PAYMENT_CODE_TYPE_MAP = [
        0 => self::PAYMENT_TYPE_NALICHNIE_NAME,
        self::PAYMENT_TYPE_SEND_BANK_CARD => self::PAYMENT_TYPE_SEND_BANK_CARD_NAME,
        self::PAYMENT_TYPE_VISA_MASTER_TYPE => self::PAYMENT_TYPE_VISA_MASTER_NAME,
        self::PAYMENT_TYPE_PRIVAT_24_TYPE => self::PAYMENT_TYPE_PRIVAT_24_NAME,
        self::PAYMENT_TYPE_NALICHNIE_TYPE => self::PAYMENT_TYPE_NALICHNIE_NAME,
        self::PAYMENT_TYPE_BEZNAL_TYPE => self::PAYMENT_TYPE_BEZNAL_NAME,
        self::PAYMENT_TYPE_PROM_PAY => self::PAYMENT_TYPE_PROM_PAY_NAME,
        self::PAYMENT_TYPE_PART_INSTALLMENTS => self::PAYMENT_TYPE_PART_INSTALLMENTS_NAME,
        self::PAYMENT_TYPE_INSTANT_INSTALLMENTS => self::PAYMENT_TYPE_INSTANT_INSTALLMENTS_NAME,
    ];

    protected $table = 'shop_payment';


    protected $fillable = [
        'order_id',
        'code',
        'status',
        'payment_type',
        'message',
        'access_code',
        'status_code',
    ];


    public function order()
    {
        return $this->belongsTo(Order::class);
    }

}