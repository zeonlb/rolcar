<?php namespace Modules\Shop\Entities;
   
use Illuminate\Database\Eloquent\Model;

class Configuration extends Model {
    protected $table = 'shop_configs';


    protected $fillable = [
        'name',
        'code',
        'value',
        'group',
        'type',
    ];

}