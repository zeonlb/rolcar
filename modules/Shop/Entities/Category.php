<?php namespace Modules\Shop\Entities;
   
use Illuminate\Database\Eloquent\Model;

/**
 * @property int|string code
 * @property mixed name
 */
class Category extends Model {

    protected $table = 'shop_category';
    protected $fillable = [
        'name',
        'code',
        'img',
        'description',
        'cms_meta_id',
        'filter',
        'visible',
        'parent_id',
        'show_in_main',
        'show_in_top',
        'sort',
    ];

    public function parents()
    {
        return $this->hasMany('Modules\Shop\Entities\Category', 'parent_id');

    }
    public function cjp()
    {
        return $this->hasMany(CategoryJoinProduct::class, 'shop_category_id')->orderBy('sort');;

    }

    public function meta()
    {
        return $this->belongsTo('Modules\Cms\Entities\Meta', 'cms_meta_id');
    }

    public function products()
    {
        return $this->belongsToMany(Product::class, 'shop_product_categories', 'shop_category_id', 'shop_product_id')
            ->orderBy('sort');
    }

    public function getChild()
    {
        return Category::where('parent_id', $this->id)->get();
    }

    public function getParent()
    {
        return Category::where('id', $this->parent_id)->first();
    }

}