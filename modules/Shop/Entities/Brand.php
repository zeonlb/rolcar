<?php namespace Modules\Shop\Entities;
   
use Illuminate\Database\Eloquent\Model;

class Brand extends Model {
    protected $table = 'shop_brands';
    protected $fillable = [
        'name',
        'code',
        'cms_meta_id',
        'description',
        'shop_photo_id',
    ];


    public function meta()
    {
        return $this->belongsTo('Modules\Cms\Entities\Meta', 'cms_meta_id');
    }

    public function logo()
    {
        return $this->belongsTo('Modules\Cms\Entities\File', 'shop_photo_id', 'id');
    }

}