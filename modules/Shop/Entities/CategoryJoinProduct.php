<?php namespace Modules\Shop\Entities;
   
use Illuminate\Database\Eloquent\Model;

/**
 * @property int|string code
 * @property mixed name
 */
class CategoryJoinProduct extends Model {

    protected $table = 'shop_product_categories';
}