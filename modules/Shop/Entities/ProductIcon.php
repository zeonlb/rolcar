<?php namespace Modules\Shop\Entities;
   
use Illuminate\Database\Eloquent\Model;

class ProductIcon extends Model {

    protected $table = 'shop_product_icons';


    protected $fillable = [
        'path',
        'top',
        'bottom',
        'left',
        'right',
    ];

    public function product()
    {
        return $this->belongsTo(Product::class, 'shop_product_id', 'id');
    }

}