<?php namespace Modules\Shop\Entities;
   
use Illuminate\Database\Eloquent\Model;

class Currency extends Model {
    protected $table = 'shop_currency';
    protected $fillable = ['name', 'code'];

}