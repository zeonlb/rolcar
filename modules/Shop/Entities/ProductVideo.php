<?php namespace Modules\Shop\Entities;
   
use Illuminate\Database\Eloquent\Model;

class ProductVideo extends Model {
    protected $table = 'shop_product_video';


    protected $fillable = [
        'youtube_code',
        'title',
        'description',
    ];

    public function product()
    {
        return $this->belongsTo('Modules\Shop\Entities\Product', 'shop_product_id', 'id');
    }
}