<?php namespace Modules\Shop\Entities;
   
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed invoice
 * @property mixed total_price
 */
class Order extends Model {
    protected $table = 'shop_orders';

    protected $casts = [
        'utm' => 'json',
    ];

    protected $fillable = [
        'first_name',
        'last_name',
        'invoice',
        'resource_id',
        'phone',
        'city',
        'email',
        'utm',
        'street',
        'status',
        'total_price',
        'delivery',
        'promocode',
        'description',
        'payment'
    ];

    public function products()
    {
        return $this->belongsToMany(Product::class, 'shop_products_orders', 'shop_order_id', 'shop_product_id');
    }

    public function paymentRel()
    {
        return $this->hasOne(Payment::class);
    }


    public function orderDetails()
    {
        return $this->hasMany(ProductOrder::class, 'shop_order_id', 'id');
    }
}