<?php namespace Modules\Shop\Entities;
   
use Illuminate\Database\Eloquent\Model;

class Banner extends Model {
    protected $table = 'shop_banner';


    protected $fillable = [
        'title',
        'url',
        'img'
    ];


}