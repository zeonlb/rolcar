<?php namespace Modules\Shop\Entities;
   
use Illuminate\Database\Eloquent\Model;
use Modules\Cms\Entities\File;

class ProductComment extends Model {
    protected $table = 'shop_product_comments';


    protected $fillable = [
        'name',
        'comment',
        'processed',
        'advantages',
        'disadvantages',
        'video_link',
        'rating',
        'shop_product_id',
        'parent_id',
        'is_admin',
    ];

    public function product()
    {
        return $this->belongsTo(Product::class, 'shop_product_id', 'id');
    }

    public function getYoutubeCode() {
        if ($this->video_link) {
            $a =parse_url($this->video_link);
            parse_str($a['query']??'', $get_array);

            return $get_array['v'] ?? null;
        }
        return null;

    }

    public function answers()
    {
        return $this->hasMany(ProductComment::class, 'parent_id');
    }

    public function photos()
    {
        return $this->belongsToMany(File::class, 'shop_product_comment_photos', 'comment_id', 'photo_id');
    }
}