<?php namespace Modules\Shop\Entities;
   
use Illuminate\Database\Eloquent\Model;
use Modules\Cms\Entities\File;

class ProductCommentJoinPhoto extends Model {
    protected $table = 'shop_product_comment_photos';


    protected $fillable = [
        'comment_id',
        'photo_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function comment()
    {
        return $this->belongsTo(ProductComment::class, 'comment_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function photo()
    {
        return $this->belongsTo(File::class, 'photo_id', 'id');
    }

}