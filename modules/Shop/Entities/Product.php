<?php namespace Modules\Shop\Entities;

use DateTime;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Product
 * @package Modules\Shop\Entities
 * @property  DateTime pre_order_date
 */
class Product extends Model
{
    protected $table = 'shop_product';
    protected $dates = ['created_at', 'updated_at', 'pre_order_date'];
    protected $fillable = [
        'name',
        'code',
        'sku',
        'romax_id',
        'short_description',
        'description',
        'count',
        'price',
        'is_present',
        'price_for',
        'shop_currency_id',
        'visible',
        'shop_product_type_id',
        'cms_meta_id',
        'lending_html',
        'lending_styles',
        'pre_order_date',
        'is_rozetka',
        'is_merchant'
    ];

    public function getThumb()
    {
        if ($this->photos->count() > 0) {
            return $this->photos[0]->path;
        }
        return false;
    }

    public function categories()
    {
        return $this->belongsToMany('Modules\Shop\Entities\Category', 'shop_product_categories', 'shop_product_id', 'shop_category_id');
    }

    public function photos()
    {
        return $this->belongsToMany('Modules\Cms\Entities\File', 'shop_product_photos', 'shop_product_id', 'shop_photo_id')
            ->wherePivot('is_rozetka', false);
    }

    public function rozetkaPhotos()
    {
        return $this->belongsToMany('Modules\Cms\Entities\File', 'shop_product_photos', 'shop_product_id', 'shop_photo_id')
            ->wherePivot('is_rozetka', true);
    }

    public function icon()
    {
        return $this->belongsToMany(ProductIcon::class, 'shop_product_j_icon', 'product_id', 'icon_id');
    }

    public function comments()
    {
        return $this->hasMany('Modules\Shop\Entities\ProductComment', 'shop_product_id', 'id')->where('is_admin', false);
    }

    public function productType()
    {
        return $this->belongsTo('Modules\Shop\Entities\ProductType', 'shop_product_type_id', 'id');
    }

    public function currency()
    {
        return $this->belongsTo('Modules\Shop\Entities\Currency', 'shop_currency_id', 'id');
    }

    public function brand()
    {
        return $this->belongsTo('Modules\Shop\Entities\Brand', 'shop_brand_id', 'id');
    }

    public function meta()
    {
        return $this->belongsTo('Modules\Cms\Entities\Meta', 'cms_meta_id', 'id');
    }

    public function options()
    {
        return $this->hasMany('Modules\Shop\Entities\ProductOptions', 'shop_product_id', 'id');
    }

    public function video()
    {
        return $this->hasOne('Modules\Shop\Entities\ProductVideo', 'shop_product_id', 'id');
    }

    public function getPreOrderDiffHours()
    {
        $days = 0;
        if ($this->pre_order_date && $this->pre_order_date->getTimestamp() > time()) {
            $diff = $this->pre_order_date->diff(new DateTime());
            $days = $diff->days;
            if (0 == $days && $diff->h > 0) {
                $days = 1;
            }
        }
        return $days;
    }


    public function discount()
    {
        return $this->hasOne('Modules\Shop\Entities\ProductDiscount', 'shop_product_id', 'id');
    }

    public function crossSales()
    {
        return $this->belongsToMany('Modules\Shop\Entities\Product', 'shop_cross_sale', 'shop_product_id', 'shop_cross_sale_product_id');
    }

    public function cjp()
    {
        return $this->hasOne(CategoryJoinProduct::class, 'shop_product_id');;
    }

    /**
     * FOR CAR MODULE RELATION
     * @return HasMany
     */
    public function motors()
    {
        return $this->belongsToMany('Modules\Car\Entities\Motor', 'lib_car_shop_product', 'product_id', 'motor_id');
    }

}