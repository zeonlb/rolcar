<?php namespace Modules\Shop\Entities;
   
use Illuminate\Database\Eloquent\Model;

class Attribute extends Model {

    protected $table = 'shop_attributes';
    protected $fillable = ['name', 'type', 'code', 'filter', 'required', 'visible', 'sort', 'option'];

    public function options()
    {
        return $this->hasMany('Modules\Shop\Entities\AttributeOption', 'shop_attribute_id');
    }

    public function shopProductOptions()
    {
        return $this->hasMany(ProductOptions::class, 'shop_attribute_id');
    }

    public function scopeOnlyOptions($query)
    {
        return $query->where('option', 1);
    }

    public function scopeOnlyAttributes($query)
    {
        return $query->where('option', 0);
    }

}