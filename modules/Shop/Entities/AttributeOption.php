<?php namespace Modules\Shop\Entities;
   
use Illuminate\Database\Eloquent\Model;

class AttributeOption extends Model {

    protected $table = 'shop_attribute_options';
    protected $fillable = ['name', 'code', 'shop_attribute_id'];

    public function option()
    {
        return $this->belongsTo('Modules\Shop\Entities\Attribute', 'shop_attribute_id');
    }

}