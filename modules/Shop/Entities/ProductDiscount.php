<?php namespace Modules\Shop\Entities;
   
use Illuminate\Database\Eloquent\Model;

class ProductDiscount extends Model {
    protected $table = 'shop_discounts';


    protected $fillable = [
        'date_start',
        'date_end',
        'type',
        'value',
        'active'
    ];
    protected $dates = ['created_at', 'updated_at', 'date_start', 'date_end'];
    public function product()
    {
        return $this->belongsTo(Product::class, 'shop_product_id', 'id');
    }

    public function isActive()
    {
        return $this->active && $this->date_start <= new \DateTime('now') && $this->date_end >= new \DateTime('now');
    }

}