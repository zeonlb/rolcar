<?php namespace Modules\Shop\Entities;
   
use Illuminate\Database\Eloquent\Model;

class ProductType extends Model {
    protected $table = 'shop_product_type';
    protected $fillable = [
        'name',
        'code'
    ];

    public function attributes()
    {
        return $this->belongsToMany('Modules\Shop\Entities\Attribute', 'shop_product_type_attributes', 'shop_product_type_id', 'shop_attribute_id');
    }
}