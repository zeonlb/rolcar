<?php namespace Modules\Shop\Entities;


use Illuminate\Database\Eloquent\Model;

class ProductOrder extends Model {
    protected $table = 'shop_products_orders';


    protected $fillable = [
        'price',
        'currency_id',
        'count',
        'selected',
    ];

    public function product()
    {
        return $this->hasOne(Product::class, 'id', 'shop_product_id');
    }
}