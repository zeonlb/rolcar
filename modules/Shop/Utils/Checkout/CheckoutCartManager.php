<?php
namespace Modules\Shop\Utils\Checkout;


use Illuminate\Support\Facades\Session;
use Irazasyed\LaravelGAMP\Facades\GAMP;
use Modules\Shop\Entities\Product;
use Modules\Shop\Utils\GA;
use Modules\Shop\Utils\Price\Processor;
use TheIconic\Tracking\GoogleAnalytics\Analytics;

class CheckoutCartManager
{
    public function generateUniqueCode($productId, $selectedOptions)
    {
        if (!is_array($selectedOptions)) {
            $selectedOptions = [];
        }
        ksort($selectedOptions);
        return md5(implode(array_keys($selectedOptions), ',') . implode($selectedOptions, ',') . $productId);
    }

    public function addProductToCart($productId, array $selectedOptions)
    {
        $product = Product::find($productId);
        if (!$product) {
            return false;
        }

        $shopCart = Session::get('shop.cart') ?  Session::get('shop.cart') : [];
        $uniqueCode = $this->generateUniqueCode($productId, $selectedOptions);
        if (isset($shopCart[$uniqueCode])) {
            $shopCart[$uniqueCode]['count'] = $shopCart[$uniqueCode]['count'] + 1;
        } else {
            $shopCart[$uniqueCode] = [
                'uniqueCode' => $uniqueCode,
                'product_id' => $productId,
                'selected_options' => $selectedOptions,
                'count' => 1,
                'created_at' => new \DateTime
            ];
        }
        Session::put('shop.cart', $shopCart);
    }

    public function deleteProductFromCart($uniqueCode)
    {
        $shopCart = Session::get('shop.cart');
        if (isset($shopCart[$uniqueCode])) {
            $product  = Product::find($shopCart[$uniqueCode]['product_id']);
            GA::deleteFromCartProduct($product);
            unset($shopCart[$uniqueCode]);
            Session::put('shop.cart', $shopCart);
            return true;
        }
        return false;
    }


    public function changeProductCartCount($uniqueKey, $type, $count)
    {
        if ($carts = Session::get('shop.cart')) {
            if (isset($carts[$uniqueKey])) {
                $product = Product::findOrFail($carts[$uniqueKey]['product_id']);
                switch ($type) {
                    case "plus":
                        $carts[$uniqueKey]['count'] = $carts[$uniqueKey]['count'] + 1;
                        Session::put('shop.cart', $carts);
                        GA::changeCartProductsCount($product);
                        return true;
                        break;
                    case "minus":
                        $carts[$uniqueKey]['count'] = $carts[$uniqueKey]['count'] - 1;
                        if ($carts[$uniqueKey]['count'] <= 0) {
                            unset($carts[$uniqueKey]);
                        }
                        GA::changeCartProductsCount($product, 'remove');
                        Session::put('shop.cart', $carts);
                        return true;
                        break;
                    case "manual":
                        if ($count == 0) {
                            GA::changeCartProductsCount($product, 'remove', $carts[$uniqueKey]['count']);
                            unset($carts[$uniqueKey]);
                        } elseif ($count > 0) {
                            $diff = $carts[$uniqueKey]['count'] - $count;
                            if ($diff < 0) {
                                GA::changeCartProductsCount($product, 'add', abs($diff));
                            } else {
                                GA::changeCartProductsCount($product, 'remove', $diff);
                            }
                            $carts[$uniqueKey]['count'] = $count;
                        }
                        Session::put('shop.cart', $carts);
                        return true;
                        break;
                }
            }
        }
    }


}