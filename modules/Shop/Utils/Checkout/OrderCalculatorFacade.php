<?php
namespace Modules\Shop\Utils\Checkout;


use Illuminate\Support\Facades\Session;
use Modules\Shop\Entities\Product;
use Modules\Shop\Utils\Price\Processor;

class OrderCalculatorFacade
{
    protected $cartCollection;
    protected $activeAttrs;


    protected $selectedOptions;

    public function __construct()
    {
        $this->generateCollection();
    }
    public function generateCollection()
    {
        $cartCollection = $selected_options = [];
        $processor = new Processor();
        if ($carts = Session::get('shop.cart')) {
            $uniqueIdsOfProducts = array_unique(array_column($carts, 'product_id'));

            $activeProducts =  Product::with('photos', 'brand')->whereIn('id', $uniqueIdsOfProducts)->get()->keyBy('id');
            $carManager = new CheckoutCartManager();
            foreach ($carts as $id => $cart) {
                $uKey = $carManager->generateUniqueCode($cart['product_id'], $cart['selected_options']);
                $acProduct = $activeProducts[$cart['product_id']];
                $processor = $processor->calculate($acProduct);
                $cartCollection[$id] = $cart;
                $pr = new Product();
                $pr->id = $acProduct->id;
                $pr->sku = $acProduct->sku;
                $pr->name = $acProduct->name;
                $pr->price_actual = $processor->actualPrice();
                $pr->price = $processor->actualPrice();
                $pr->brand = $acProduct->brand;
                $pr->photos = $acProduct->photos;
                $pr->single_price =  $processor->actualPrice() / $cart['count'];
                $pr->price_for = $acProduct->price_for;
                $pr->uKey = $uKey;
                $cartCollection[$id]['product'] = $pr;
                $this->activeAttrs[] = array_keys($cart['selected_options']);
                $selected_options[$uKey] = $cart['selected_options'];
            }
        }
        $this->cartCollection = $cartCollection;
        $this->selectedOptions = $selected_options;
    }
    /**
     * @return mixed
     */
    public function getActiveAttrs()
    {
        return $this->activeAttrs;
    }
    public function getOrderCalculation()
    {
        $orderCalculator =  new OrderSumCalculator();
        $orderCalculator->calculate($this->cartCollection);
        return $orderCalculator;
    }

    public function getCartCollection()
    {
        return $this->cartCollection;
    }

    public function getSelectedOptions()
    {
        return $this->selectedOptions;
    }


}