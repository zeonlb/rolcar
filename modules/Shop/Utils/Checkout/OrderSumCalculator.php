<?php

namespace Modules\Shop\Utils\Checkout;

class OrderSumCalculator
{
    private $totalSum = 0;
    
    public function calculate($cartsCollection)
    {
        foreach ($cartsCollection as $id => $cart) {


            $count = $cart['count'];
            $cart['product']->price = $cart['product']->price * $count;
            $cartsCollection[$id]['product'] = $cart['product'];
            $this->totalSum += $cart['product']->price;
        }

        return $cartsCollection;
    }

    public function getTotalSum()
    {
        return $this->totalSum;
    }
}