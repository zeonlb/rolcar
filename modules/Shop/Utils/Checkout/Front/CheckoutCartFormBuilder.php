<?php
namespace Modules\Shop\Utils\Checkout\Front;


use Kris\LaravelFormBuilder\Facades\FormBuilder;
use Modules\Shop\Entities\Product;

class CheckoutCartFormBuilder
{
    public static function make($productOptions = array(), $productId)
    {   
        $form = FormBuilder::plain([
            'method' => 'POST',
            'url' => route('shop.checkout.cart.add', ['id' => $productId])
        ]);
        
        foreach ($productOptions as $key => $option) {
            if ($option['values']) {
                $choices = [];
                foreach ($option['values'] as $value) {
                    $choices[$value->code] = $value->name;
                }

                $rules = $option['required'] ? 'required|min:5000' : '';
                $form->add('options[' . $key . ']', 'choice', [
                    'choices' => $choices,
                    'expanded' => false,
                    'multiple' => false,
                    'rules' => $rules,
                    'label' => $option['name'],
                    'empty_value' => ' ',
                    'template' => 'laravel-form-builder::select_front',
                ]);
            }

        }
        $product = Product::findOrFail($productId);
        $btnText = ($product->pre_order_date && $product->getPreOrderDiffHours() > 0)  ? 'Предзаказ' : 'Купить';
        $form->add('<span class="fa fa-shopping-cart">  </span>    '.$btnText, 'submit', [
            'attr' => ['class' => 'hbtn btn-buy', 'onclick' => "ga('send', 'pageview', '/add_Korzina'); return true;"],
            'template' => 'laravel-form-builder::submit_front'
        ]);

        return $form;
    }

    public static function getValidationRules($productOptions)
    {
        $validateAttrs = [];
        foreach ($productOptions as $key => $option) {
            if ($option['required']) $validateAttrs['options.'.$key] = 'required';
        }
        return $validateAttrs;
    }

}