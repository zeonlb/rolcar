<?php
namespace Modules\Shop\Utils\Price;


use DateTime;
use Modules\Shop\Entities\Product;

class Processor implements IPriceProcessor
{
    private $actualPrice;
    private $originPrice;
    private $discountProcessor;

    /**
     * Processor constructor.
     */
    public function __construct()
    {
        $this->discountProcessor = new DiscountProcessor();
    }


    /**
     * @return mixed
     */
    public function actualPrice()
    {
        return $this->actualPrice;
    }

    /**
     * @return mixed
     */
    public function originPrice()
    {
        return $this->originPrice;
    }

    

    public function calculate(Product $product)
    {
        $this->originPrice = $product->price;
        $this->actualPrice = $product->price;

        if ($product->discount && $product->discount->isActive()) {
            $this->actualPrice = round($this->discountProcessor->calculate($product));
        }
        return $this;
    }
}