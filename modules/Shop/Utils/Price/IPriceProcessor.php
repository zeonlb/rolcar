<?php
namespace modules\Shop\Utils\Price;


use Modules\Shop\Entities\Product;

interface IPriceProcessor
{
        public function calculate(Product $product);
}