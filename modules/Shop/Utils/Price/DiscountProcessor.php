<?php
namespace Modules\Shop\Utils\Price;


use Modules\Shop\Entities\Product;

class DiscountProcessor implements IPriceProcessor
{

    public function calculate(Product $product)
    {
        $price = $product->price;
        $discount = $product->discount;

        if ($discount->active && $discount->date_end > new \DateTime('now')) {
            switch ($discount->type) {
                case "fixed":
                    $price = $price - $discount->value;
                    break;
                case "percent":
                    $price = $price - ($price * $discount->value/100);
                    break;
            }
        } else {
            $product->discount->active = false;
            $product->discount->save();
        }
        return $price;
    }
}