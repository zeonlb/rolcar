<?php
namespace Modules\Shop\Utils;



use Illuminate\Support\Facades\Route;

class UrlHelper {

    public static function getFullUrl($category, $typeLamp)
    {
       return  strtr(Route::getCurrentRoute()->getPath(), ["{category}" => $category, "{naznachenie}" => $typeLamp]);

    }
}