<?php
/**
 * Created by PhpStorm.
 * User: dat
 * Date: 18.06.17
 * Time: 9:06
 */

namespace Modules\Shop\Utils;


use Illuminate\Support\Facades\Session;
use Modules\Shop\Entities\Order;
use Modules\Shop\Entities\Product;
use Modules\Shop\Utils\Price\Processor;

class GA
{
    private $transactionResult = '';

    public function addToCartProduct(Product $product){
        $price = (new Processor())->calculate($product)->actualPrice();

        $script = "
            window.dataLayer = window.dataLayer || [];
            dataLayer.push({
             'ecommerce': {
               'currencyCode': 'UAH',
               'add': {
               'actionField': {'step': 1},
                 'products': [{
                   'name': '{$product->name}',
                   'id': '{$product->sku}',
                   'price': '{$price}',
                   'brand': '{$product->brand->name}',
                   'quantity': '1'
                  }]
               }
             },
             'event': 'gtm-ee-event',
             'gtm-ee-event-category': 'Enhanced Ecommerce',
             'gtm-ee-event-action': 'Adding a Product to a Shopping',
             'gtm-ee-event-non-interaction': 'False',
            });
            
            var google_tag_params = {
            'ecomm_prodid': '{$product->sku}',
            'ecomm_pagetype': 'add_cart',
            'ecomm_totalvalue': {$price}
            };

            ga('send', 'event', 'Add_to_Cart', 'Send'); return true;
        ";

	$script_fb = "
fbq('track', 'AddToCart', {
      content_ids: '{$product->sku}',
      content_type: 'product',
      value: {$price},
      currency: 'UAH' 
    });
	";
	Session::put('fb_add_to_cart_script', $script_fb);
        Session::put('ga_add_to_cart_script', $script);
    }

    public static function changeCartProductsCount(Product $product, $type = 'add', $quantity = 1)
    {
        $price = (new Processor())->calculate($product)->actualPrice();
        $script = "
            window.dataLayer = window.dataLayer || [];
            dataLayer.push({
             'ecommerce': {
               'currencyCode': 'UAH',
               'add': {
               'actionField': {'step': 1},
                 'products': [{
                   'name': '{$product->name}',
                   'id': '{$product->sku}',
                   'price': '{$price}',
                   'brand': '{$product->brand->name}',
                   'quantity': '{$quantity}'
                  }]
               }
             },
             'event': 'gtm-ee-event',
             'gtm-ee-event-category': 'Enhanced Ecommerce',
             'gtm-ee-event-action': '{$type} 1 count product to a Shopping',
             'gtm-ee-event-non-interaction': 'False',
            });
        ";
        Session::put('ga_script', $script);
    }

    public static function deleteFromCartProduct(Product $product){
        $price = (new Processor())->calculate($product)->actualPrice();

        $script = "
            window.dataLayer = window.dataLayer || [];
            dataLayer.push({
             'ecommerce': {
               'currencyCode': 'UAH',
               'remove': {
               'actionField': {'step': 1},
                 'products': [{
                   'name': '{$product->name}',
                   'id': '{$product->sku}',
                   'price': '{$price}',
                   'brand': '{$product->brand->name}',
                   'quantity': '1'
                  }]
               }
             },
             'event': 'gtm-ee-event',
             'gtm-ee-event-category': 'Enhanced Ecommerce',
             'gtm-ee-event-action': 'Removing a Product from a Shopping Cart',
             'gtm-ee-event-non-interaction': 'False',
            });
            
            var google_tag_params = {
            'ecomm_prodid': '{$product->sku}',
            'ecomm_pagetype': 'add_cart',
            'ecomm_totalvalue': '{$price}'
            }; 
            ga('send', 'event', 'Delete_Cart', 'Send');  return true;
        ";

	$script_fb = "
fbq('track', 'DeleteCart', {
  content_ids: '{$product->sku}',
  content_type: 'product',
  value: '{$price}',
  currency: 'UAH'
});
";
	Session::put('fb_delete_from_cart_script', $script_fb);
        Session::put('ga_delete_from_cart_script', $script);
    }      
}
