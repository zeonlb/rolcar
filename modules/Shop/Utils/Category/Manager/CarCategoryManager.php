<?php

namespace Modules\Shop\Utils\Category\Manager;


use Modules\Car\Core\BreadCrumbGenerator;
use Modules\Cms\Entities\Meta;
use Modules\Cms\Entities\Page;
use Modules\Cms\System\MetaGenerator;
use Modules\Shop\Entities\AttributeOption;
use Modules\Shop\Entities\Category;

class CarCategoryManager implements Manager
{

    protected $id;
    protected $motor;
    protected $breadCrumbs;
    public  $category;

    function __construct($motor)
    {
        $this->motor = $motor;
        $this->breadCrumbs = BreadCrumbGenerator::make([$motor]);
        $this->category = new Category();
    }

    public function getId()
    {
        return $this->motor->id;
    }

    public function getBreadCrumbs()
    {
        return $this->breadCrumbs;
    }

    public function getFirstSeoText()
    {
        if (isset($this->motor->meta->seo_text)) {
            return $this->motor->meta->seo_text;
        }

        return null;
    }

    public function getSecondSeoText()
    {
        if (isset($this->motor->meta->seo_text_2)) {
            return $this->motor->meta->seo_text_2;
        }

        return null;
    }

    public function getMetaTitle()
    {
        return $this->motor->modification->model->mark->name . ' ' . $this->motor->modification->model->name;

        return null;
    }

    public function getMetaData($filterData)
    {
        return MetaGenerator::make(
            $this->motor->meta,
            'product_list_light',
            [
                'transport' => $this->motor->modification->model->transport->name,
                'mark' => $this->motor->modification->model->mark->name,
                'model' => $this->motor->modification->model->name,
                'modification' => $this->motor->modification->name,
                'motor' => $this->motor->motor,
                'light' => trans('car::lamps.' . $filterData),
            ]
        );
    }

    public function getPageInfo(Page $pageInfo, $filterData)
    {
        if (isset($filterData) && $filterData) {

            $pageInfo->name = trans('car::lamps.' . $filterData);
            $this->breadCrumbs[$pageInfo->name] = null;

        }

        $pageInfo->name .= ' для ' . $this->getMetaTitle();
        $pageInfo->meta = $this->getMetaData($filterData);

        return $pageInfo;
    }


    public function addBreadCrumb($name, $url)
    {
        if ($name) {
            $this->breadCrumbs[$name] = $url;
        }
    }

    /**
     * @return mixed
     */
    public function getMotor()
    {
        return $this->motor;
    }


}