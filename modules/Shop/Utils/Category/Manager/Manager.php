<?php
namespace Modules\Shop\Utils\Category\Manager;


use Modules\Cms\Entities\Page;

interface Manager {
    public function getId();
    public function getBreadCrumbs();
    public function addBreadCrumb($name, $url);
    public function getMetaTitle();
    public function getMetaData($filterData);
    public function getFirstSeoText();
    public function getSecondSeoText();
    public function getPageInfo(Page $pageInfo, $filterData);
}