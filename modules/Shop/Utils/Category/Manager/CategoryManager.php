<?php

namespace Modules\Shop\Utils\Category\Manager;



use Modules\Cms\Entities\Page;
use Modules\Cms\System\MetaGenerator;
use Modules\Shop\Entities\Category;

class CategoryManager implements Manager
{
    /** @var  Category */
    public $category;
    private $breadCrumbs = [];

    function __construct($category)
    {
        $this->breadCrumbs = [
            'Интернет-магазин' => url('/')
        ];
        if ($category->code === 'main') {
            $this->breadCrumbs[$category->name] = '';
        }
        $this->category = $category;
    }

    public function getBreadCrumbs()
    {
        return $this->breadCrumbs;
    }

    public function getId()
    {
        return null;
    }

    public function addBreadCrumb($name, $url)
    {
        $this->breadCrumbs[$name] = $url;
    }

    public function getFirstSeoText()
    {
        if (isset($this->category->meta->seo_text)) {
            return $this->category->meta->seo_text;
        }

        return null;
    }

    public function getSecondSeoText()
    {
        if (isset($this->category->meta->seo_text_2)) {
            return $this->category->meta->seo_text_2;
        }

        return null;
    }

    public function getMetaTitle()
    {
        return $this->category->name;
    }


    public function getPageInfo(Page $pageInfo, $filterData)
    {
        $pageInfo->name = $this->category->name;
        $pageInfo->meta = $this->category->meta;
        return $pageInfo;
    }

    public function getMetaData($filterData)
    {
        return $this->category->meta;
    }
}