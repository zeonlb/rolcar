<?php

namespace Modules\Shop\Utils\Category;


use Modules\Car\Entities\Modification;
use Modules\Car\Entities\Motor;
use Modules\Cms\Entities\Page;
use Modules\Shop\Entities\AttributeOption;
use Modules\Shop\Entities\Category;
use Modules\Shop\Utils\Category\Manager\CarCategoryManager;
use Modules\Shop\Utils\Category\Manager\CategoryManager;
use Modules\Shop\Utils\Category\Manager\Manager;

class CategoryManagerFactory
{
    /** @var  Manager */
    protected static $manager;

    public static function getManager($code = null){

        if (is_int(strpos($code,'-'))) {
            $p = explode('-', $code);
            $id = array_pop($p);
            array_shift($p);

            if ($id) {
                $motor = Motor::where('id', $id)->with(['meta','modification.model.mark', 'modification.model.transport', 'lampOptions'])->first();
                if ($motor && $motor->modification->code == implode('-', $p))
                    return self::$manager = new CarCategoryManager($motor);
            }

        }
        if (null !== $code) {
            $category =  Category::where('code', $code)->with('meta')->firstOrFail();

            return self::$manager =  new CategoryManager($category);
        }

        return  self::$manager =  new CategoryManager(Category::where('code', 'main')->with('meta')->first());
    }



}