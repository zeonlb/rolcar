<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopCrossSaleTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_cross_sale', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('shop_product_id')->unsigned();
            $table->integer('shop_cross_sale_product_id')->unsigned();

            $table->timestamps();

            $table->foreign('shop_product_id')->references('id')->on('shop_product');
            $table->foreign('shop_cross_sale_product_id')->references('id')->on('shop_product');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('shop_cross_sale');
    }

}
