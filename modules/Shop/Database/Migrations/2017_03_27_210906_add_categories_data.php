<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use \Modules\Shop\Entities\Category;

class AddCategoriesData extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        $optionsNames = [
            "ft1" => "Боковой указатель поворота",
            "ft2" => "Ближний свет",
            "ft3" => "Парковочный свет",
            "ft4" => "Дальний свет",
            "ft5" => "Габариты",
            "ft6" => "Дневной свет",
            "ft7" => "Передний указатель поворота",
            "ft8" => "Противотуманный свет",
            "ft9" => "Противотуманный свет низ",
            "ft10" => "Дополнительный передний свет",
            "ft11" => "Дополнительные фары дальнего света",
            "ft12" => "Боковые габаритные огни",
            "ft13" => "Боковой свет",
            ];
        foreach ($optionsNames as $key => $o) {
            $category = new Category();
            $category->name = $o;
            $category->code = $key;
            $category->visible = true;
            $category->parent_id = null;
            $category->save();
        }
        $optionsNames = [
            "bt1" => "Дополнительный стоп-сигнал",
            "bt2" => "Стоп-сигнал",
            "bt3" => "Задний ПТФ",
            "bt4" => "Подсветка номера",
            "bt5" => "Свет заднего хода",
            "bt6" => "Дополнительный задний свет",
            "bt7" => "Задний указатель поворота",
            "bt8" => "Задний габарит",
            ];
        foreach ($optionsNames as $key => $o) {
            $category = new Category();
            $category->name = $o;
            $category->code = $key;
            $category->visible = true;
            $category->parent_id = null;
            $category->save();
        }

        $optionsNames = [
            "it1" => "Освещение пространства для ног",
            "it2" => "Свет бардочка",
            "it3" => "Лампа для чтения",
            "it4" => "Освещение салона",
            "it5" => "Дверной свет",
            "it6" => "Свет безопасности двери",
            "it7" => "Свет багажника",
            "it8" => "Свет парога",
            "it9" => "Подсветка мотора",
        ];
        foreach ($optionsNames as $key => $o) {
            $category = new Category();
            $category->name = $o;
            $category->code = $key;
            $category->visible = true;
            $category->parent_id = null;
            $category->save();
        }

      
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }

}
