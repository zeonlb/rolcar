<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProductOrder extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shop_products_orders', function(Blueprint $table)
        {
            $table->double('price');
            $table->integer('currency_id')->nullable()->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shop_products_orders', function(Blueprint $table)
        {
            $table->dropColumn('price');
            $table->dropColumn('currency_id');
        });
    }

}
