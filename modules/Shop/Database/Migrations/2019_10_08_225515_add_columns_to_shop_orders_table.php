<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToShopOrdersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shop_orders', function(Blueprint $table)
        {
            $table->text('delivery')->default('');
            $table->text('description')->default('');
            $table->string('payment')->default('');
            $table->string('promocode')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shop_orders', function(Blueprint $table)
        {
            $table->dropColumn('delivery');
            $table->dropColumn('description');
            $table->dropColumn('payment');
            $table->dropColumn('promocode');
        });
    }
}
