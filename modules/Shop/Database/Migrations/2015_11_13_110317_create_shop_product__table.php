<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopProductTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_product', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('code')->unique();
            $table->string('sku')->unique();
            $table->string('short_description');
            $table->longText('description');
            $table->integer('count');

            $table->integer('price');
            $table->integer('shop_currency_id')->unsigned();
            $table->boolean('visible')->default(false);

            $table->integer('shop_product_type_id')->unsigned();

            $table->integer('cms_meta_id')->unsigned()->nullable();

            $table->timestamps();

            $table->foreign('cms_meta_id')->references('id')->on('cms_meta')->onDelete('cascade');
            $table->foreign('shop_product_type_id')->references('id')->on('shop_product_type');
            $table->foreign('shop_currency_id')->references('id')->on('shop_currency');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shop_product', function(Blueprint $table) {
            $table->dropForeign('shop_product_shop_currency_id_foreign');
            $table->dropForeign('shop_product_shop_product_type_id_foreign');
            $table->dropForeign('shop_product_shop_category_id_foreign');
        });
        Schema::drop('shop_product');
    }

}
