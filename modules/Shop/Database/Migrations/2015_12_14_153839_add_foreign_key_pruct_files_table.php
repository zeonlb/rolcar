<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyPructFilesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shop_product_photos', function(Blueprint $table)
        {
            $table->dropForeign('shop_product_photos_shop_photo_id_foreign');
        });

        Schema::table('shop_product_photos', function(Blueprint $table)
        {
            $table->foreign('shop_photo_id')->references('id')->on('cms_files')->onDelete('cascade');;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shop_product_photos', function(Blueprint $table)
        {
            $table->dropForeign('shop_product_photos_shop_photo_id_foreign');
        });

        Schema::table('shop_product_photos', function(Blueprint $table)
        {
            $table->foreign('shop_photo_id')->references('id')->on('cms_files');
        });
    }

}
