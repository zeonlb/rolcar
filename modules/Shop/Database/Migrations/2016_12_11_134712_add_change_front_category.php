<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use \Illuminate\Support\Facades\DB;
use \Modules\Shop\Entities\Category;

class AddChangeFrontCategory extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('shop_category')->where('code', 'ft3')->update(['name' => 'Габаритные огни']);
        DB::table('shop_category')->where('code', 'ft5')->update(['name' => 'Освещение поворотов']);
        DB::table('shop_category')->where('code', 'ft6')->update(['name' => 'Дневные ходовые огни']);
        DB::table('shop_category')->where('code', 'ft7')->update(['name' => 'Передний указатель поворота']);
        DB::table('shop_category')->where('code', 'ft8')->update(['name' => 'Противотуманные фары']);
        DB::table('shop_category')->where('code', 'ft9')->update(['name' => 'Окружающий свет']);

        $frCategory = Category::where('code', 'peredniy_svet')->first();
        if(!$frCategory) {
            $frCategory = Category::create([
                'name' => 'Передний свет',
                'code' => 'peredniy_svet'
            ]);
        }
        $optionsNames = [
            "ft10" => "Дополнительный передний свет",
            "ft11" => "Дополнительные фары дальнего света",
            "ft5" => "Освещение поворотов",
            "ft9" => "Окружающий свет",
        ];
        foreach ($optionsNames as $key => $o) {
            $category = new Category();
            $category->name = $o;
            $category->code = $key;
            $category->visible = true;
            $category->parent_id = $frCategory->id;
            $category->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('', function(Blueprint $table)
        {

        });
    }

}
