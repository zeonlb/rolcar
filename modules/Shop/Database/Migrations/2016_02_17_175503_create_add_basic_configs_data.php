<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use \Modules\Shop\Entities\Configuration;

class CreateAddBasicConfigsData extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        $config = new Configuration();
        $config->code = 'delivery_text';
        $config->name = 'Delivery text';
        $config->type = 'textarea';
        $config->value = '<p>Доставка в отделения Новой Почты по всей Украине</p>
                    <p>С помощью доставки Новой Почтой, Вы можете получить товар даже в самых отдаленных уголках Украины</p>
                    <p>В среднем, доставка занимает 1-3 дня, во время заказа наши менеджеры согласуют с Вами дату доставки перед отправкой товара.</p>
                    <p>Стоимость доставки товаров Новой почтой в отделение составляет 35 грн</p>
                    <p>Стоимость доставки крупногабаритных товаров составляет 80 грн</p>
                    <p>Стоимость доставки товаров из раздела «Бытовая техника и интерьер» рассчитывается индивидуально.</p>';
        $config->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('');
    }

}
