<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopOrdersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_orders', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name')->nullable();
            $table->string('phone');
            $table->string('city')->nullable();
            $table->string('street')->nullable();
            $table->enum('status', ['new', 'in_progress', 'processed', 'send_to_client', 'delivered', 'sold'])->default('new');

            $table->timestamps();
        });

        Schema::table('shop_product', function(Blueprint $table)
        {
            $table->integer('count_ordered')->after('visible')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('shop_orders');
        Schema::table('shop_product', function(Blueprint $table)
        {
            $table->dropColumn('count_ordered');
        });
    }

}
