<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use Modules\Shop\Entities\Category;

class AddChangeSedanInsideCategory extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('shop_category')->where('code', 'it1')->update(['name' => 'Подсветка для ног']);
        DB::table('shop_category')->where('code', 'it2')->update(['name' => 'Свет бардочка']);
        DB::table('shop_category')->where('code', 'it3')->update(['name' => 'Лампа для чтения']);
        DB::table('shop_category')->where('code', 'it6')->update(['name' => 'Габаритный огонь двери']);
        DB::table('shop_category')->where('code', 'it7')->update(['name' => 'Подсветка багажника']);
        DB::table('shop_category')->where('code', 'it8')->update(['name' => 'Подсветка порогов']);

        $bCategory = Category::where('code', 'svet_v_salone')->first();
        if (!$bCategory) {
            return;
        }
        $optionsNames = [
            "it4" => "Внутрисалонный свет",
            "it5" => "Подсветка двери",
            "it9" => "Подсветка мотора",
        ];
        foreach ($optionsNames as $key => $o) {
            $category = new Category();
            $category->name = $o;
            $category->code = $key;
            $category->visible = true;
            $category->parent_id = $bCategory->id;
            $category->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('', function(Blueprint $table)
        {

        });
    }

}
