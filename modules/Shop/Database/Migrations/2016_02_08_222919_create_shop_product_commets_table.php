<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopProductCommetsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_product_comments', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->longText('comment');
            $table->boolean('processed')->default(false);
            $table->integer('rating');
            $table->integer('shop_product_id')->unsigned();
            $table->integer('parent_id')->unsigned()->nullable();
            $table->boolean('is_admin')->delault(false);

            $table->timestamps();

            $table->foreign('shop_product_id')->references('id')->on('shop_product');
            $table->foreign('parent_id')->references('id')->on('shop_product_comments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('shop_product_comments');
    }

}
