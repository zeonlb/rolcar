<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOptionIdColumn extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shop_product_options', function(Blueprint $table)
        {
            $table->integer('shop_product_option_id')->unsigned()->nullable()->after('shop_attribute_id');
            $table->foreign('shop_product_option_id')->references('id')->on('shop_attribute_options');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shop_product_options', function(Blueprint $table)
        {
            $table->dropForeign('shop_product_options_shop_product_option_id_foreign');
            $table->dropColumn('shop_product_option_id');
        });
    }

}
