<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Modules\Cms\Entities\Navigation;

class AddFixNavigation extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Illuminate\Support\Facades\DB::table('cms_navigation')->delete();
        \Illuminate\Support\Facades\DB::table('cms_navigation')->insert([
            [
                'menu_name' => 'cms.main.navigation',
                'alias' => 'Главная',
                'url' => '/',
                'icon' => NULL,
                'sort' => 0,
                'lang' => 'en',
                'parent_id' => NULL,
            ],
            [
                'menu_name' => 'cms.main.navigation',
                'alias' => 'Поиск по артикулу',
                'url' => '/sku',
                'icon' => NULL,
                'sort' => 1,
                'lang' => 'en',
                'parent_id' => NULL,
            ],
            [
                'menu_name' => 'cms.main.navigation',
                'alias' => 'Каталог автомобилей',
                'url' => '/catalog',
                'icon' => NULL,
                'sort' => 2,
                'lang' => 'en',
                'parent_id' => NULL,
            ],
            [
                'menu_name' => 'cms.main.navigation',
                'alias' => 'Каталог ламп',
                'url' => '/products',
                'icon' => NULL,
                'sort' => 3,
                'lang' => 'en',
                'parent_id' => NULL,
            ],
            [
                'menu_name' => 'cms.main.navigation',
                'alias' => 'Контактная информация',
                'url' => '/contact_us',
                'icon' => NULL,
                'sort' => 4,
                'lang' => 'en',
                'parent_id' => NULL,
            ],
            [
                'menu_name' => 'cms.main.navigation',
                'alias' => 'Гарантия',
                'url' => '/guarantee',
                'icon' => NULL,
                'sort' => 5,
                'lang' => 'en',
                'parent_id' => NULL,
            ],
            [
                'menu_name' => 'cms.main.navigation',
                'alias' => 'Доставка',
                'url' => '/delivery',
                'icon' => NULL,
                'sort' => 6,
                'lang' => 'en',
                'parent_id' => NULL,
            ],
            [
                'menu_name' => 'cms.footer.navigation',
                'alias' => 'Главная',
                'url' => '/',
                'icon' => NULL,
                'sort' => 0,
                'lang' => 'en',
                'parent_id' => NULL,
            ],
            [
                'menu_name' => 'cms.footer.navigation',
                'alias' => 'Поиск по артикулу',
                'url' => '/sku',
                'icon' => NULL,
                'sort' => 1,
                'lang' => 'en',
                'parent_id' => NULL,
            ],
            [
                'menu_name' => 'cms.footer.navigation',
                'alias' => 'Каталог автомобилей',
                'url' => '/catalog',
                'icon' => NULL,
                'sort' => 2,
                'lang' => 'en',
                'parent_id' => NULL,
            ],
            [
                'menu_name' => 'cms.footer.navigation',
                'alias' => 'Каталог ламп',
                'url' => '/products',
                'icon' => NULL,
                'sort' => 3,
                'lang' => 'en',
                'parent_id' => NULL,
            ],
            [
                'menu_name' => 'cms.footer.navigation',
                'alias' => 'Контактная информация',
                'url' => '/contact_us',
                'icon' => NULL,
                'sort' => 4,
                'lang' => 'en',
                'parent_id' => NULL,
            ],
            [
                'menu_name' => 'cms.footer.navigation',
                'alias' => 'Гарантия',
                'url' => '/guarantee',
                'icon' => NULL,
                'sort' => 5,
                'lang' => 'en',
                'parent_id' => NULL,
            ],
            [
                'menu_name' => 'cms.footer.navigation',
                'alias' => 'Доставка',
                'url' => '/delivery',
                'icon' => NULL,
                'sort' => 6,
                'lang' => 'en',
                'parent_id' => NULL,
            ],

        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Illuminate\Support\Facades\DB::table('cms_navigation')->delete();
    }

}
