<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAttrCombinationIdColumn extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shop_product_options', function(Blueprint $table)
        {
            $table->renameColumn('attr_combination_id', 'value');
        });
        Schema::table('shop_product_options', function(Blueprint $table)
        {
            $table->mediumText('value')->change();;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shop_product_options', function(Blueprint $table)
        {
            $table->renameColumn('value', 'attr_combination_id');
        });
    }

}
