<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopProductOptionsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_product_options', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('attr_combination_id');
            $table->integer('shop_product_id')->unsigned();
            $table->integer('shop_attribute_id')->unsigned();

            $table->timestamps();

            $table->foreign('shop_product_id')->references('id')->on('shop_product')->onDelete('cascade');
            $table->foreign('shop_attribute_id')->references('id')->on('shop_attributes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shop_product_options', function(Blueprint $table) {
            $table->dropForeign('shop_product_options_shop_product_id_foreign');
            $table->dropForeign('shop_product_options_shop_attribute_id_foreign');
        });
        Schema::drop('shop_product_options');
    }

}
