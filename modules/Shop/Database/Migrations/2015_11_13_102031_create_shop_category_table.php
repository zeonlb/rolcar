<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopCategoryTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_category', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('code');
            $table->string('img')->nullable();
            $table->longText('description')->nullable();
            $table->integer('cms_meta_id')->unsigned()->nullable();

            $table->boolean('filter')->default(true);
            $table->boolean('visible')->default(true);

            $table->integer('parent_id')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('cms_meta_id')->references('id')->on('cms_meta');
            $table->foreign('parent_id')->references('id')->on('shop_category');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shop_category', function(Blueprint $table) {
            $table->dropForeign('shop_category_cms_meta_id_foreign');
            $table->dropForeign('shop_category_parent_id_foreign');
        });

        Schema::drop('shop_category');
    }

}
