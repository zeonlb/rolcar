<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAttributeColumnIsOption extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shop_attributes', function(Blueprint $table)
        {
             $table->boolean('option')->after('visible')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shop_attributes', function(Blueprint $table)
        {
            $table->dropColumn('option');
        });
    }

}
