<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopProdictPhotosTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_product_photos', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('shop_product_id')->unsigned();
            $table->integer('shop_photo_id')->unsigned();

            $table->timestamps();

            $table->foreign('shop_product_id')->references('id')->on('shop_product');
            $table->foreign('shop_photo_id')->references('id')->on('cms_files');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shop_product_photos', function(Blueprint $table) {
            $table->dropForeign('shop_product_photos_shop_product_id_foreign');
            $table->dropForeign('shop_product_photos_shop_photo_id_foreign');
        });
        Schema::drop('shop_product_photos');
    }

}
