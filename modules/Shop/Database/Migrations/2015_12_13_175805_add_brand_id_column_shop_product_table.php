<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBrandIdColumnShopProductTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shop_product', function(Blueprint $table)
        {
            $table->integer('shop_brand_id')->unsigned()->nullable()->after('code');


            $table->foreign('shop_brand_id')->references('id')->on('shop_brands');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shop_product', function(Blueprint $table)
        {
            $table->dropForeign('shop_product_shop_brand_id_foreign');
            $table->dropColumn('shop_brand_id');

        });
    }

}
