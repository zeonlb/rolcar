<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopAttributeOptionsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_attribute_options', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('code');

            $table->integer('shop_attribute_id')->unsigned();
            $table->timestamps();

            $table->foreign('shop_attribute_id')->references('id')->on('shop_attributes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shop_attribute_options', function(Blueprint $table) {
            $table->dropForeign('shop_attribute_options_shop_attribute_id_foreign');
        });

        Schema::drop('shop_attribute_options');
    }

}
