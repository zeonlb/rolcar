<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopBrandsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_brands', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('code')->unique();
            $table->longText('description');

            $table->integer('cms_meta_id')->unsigned()->nullable();
            $table->integer('shop_photo_id')->unsigned()->nullable();

            $table->timestamps();

            $table->foreign('cms_meta_id')->references('id')->on('cms_meta');
            $table->foreign('shop_photo_id')->references('id')->on('cms_files');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('shop_brands', function(Blueprint $table)
        {
            $table->dropForeign('shop_brands_cms_meta_id_foreign');
        });
        Schema::drop('shop_brands');
    }

}
