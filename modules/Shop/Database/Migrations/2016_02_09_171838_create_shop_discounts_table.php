<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopDiscountsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_discounts', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('shop_product_id')->unsigned();
            $table->timestamp('date_start');
            $table->timestamp('date_end')->nullable();
            $table->enum('type', ['fixed', 'percent'])->default('percent');
            $table->double('value')->nullable();
            $table->boolean('active')->default(true);


            $table->foreign('shop_product_id')->references('id')->on('shop_product');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('shop_discounts');
    }

}
