<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCommnetsExpanded extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shop_product_comments', function(Blueprint $table)
        {
            $table->text('advantages');
            $table->text('disadvantages');
            $table->string('video_link');
        });

        Schema::create('shop_product_comment_photos', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';

            $table->integer('comment_id')->unsigned();
            $table->integer('photo_id')->unsigned();
            $table->primary(['comment_id', 'photo_id'])->unique();

            $table->foreign('photo_id')->references('id')->on('cms_files')->onDelete('cascade')->onUpdate('no action');
            $table->foreign('comment_id')->references('id')->on('shop_product_comments')->onDelete('cascade')->onUpdate('no action');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('', function(Blueprint $table)
        {

        });
    }

}
