<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProductIcons extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_product_icons', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('path')->unique();
            $table->integer('top')->default(0);
            $table->integer('left')->default(0);
            $table->integer('right')->default(0);
            $table->integer('bottom')->default(0);

            $table->timestamps();
        });

        Schema::create('shop_product_j_icon', function(Blueprint $table)
        {
            $table->integer('product_id')->unsigned();
            $table->integer('icon_id')->unsigned();

            $table->foreign('icon_id')->references('id')->on('shop_product_icons')->onDelete('cascade')->onUpdate('no action');
            $table->foreign('product_id')->references('id')->on('shop_product')->onDelete('cascade')->onUpdate('no action');
        });

        Schema::table('shop_product', function(Blueprint $table)
        {
            $table->dropColumn('icon');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('', function(Blueprint $table)
        {

        });
    }

}
