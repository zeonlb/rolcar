<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductVideoTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_product_video', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('youtube_code', 150);
            $table->string('title');
            $table->longText('description');
            $table->integer('shop_product_id')->unsigned();

            $table->timestamps();
            $table->foreign('shop_product_id')->references('id')->on('shop_product');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shop_product_video', function(Blueprint $table)
        {
            $table->dropForeign('shop_product_video_shop_photo_id_foreign');
        });
        Schema::drop('shop_product_video');
    }

}
