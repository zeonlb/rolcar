<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use Modules\Shop\Entities\Category;

class AddChangeSedanBackCategory extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('shop_category')->where('code', 'bt3')->update(['name' => 'Задний ПТФ']);
        DB::table('shop_category')->where('code', 'bt5')->update(['name' => 'Задний ход']);
        DB::table('shop_category')->where('code', 'bt6')->update(['name' => 'Дополнительный задний свет']);
        DB::table('shop_category')->where('code', 'bt7')->update(['name' => 'Задний указатель поворота']);
        DB::table('shop_category')->where('code', 'bt8')->update(['name' => 'Задний габарит']);

        $bCategory = Category::where('code', 'zadniy_svet')->first();
        if (!$bCategory) {
            return;
        }
        $optionsNames = [
            "bt8" => "Задний габарит",
        ];
        foreach ($optionsNames as $key => $o) {
            $category = new Category();
            $category->name = $o;
            $category->code = $key;
            $category->visible = true;
            $category->parent_id = $bCategory->id;
            $category->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }

}
