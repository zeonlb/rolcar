<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDefaultCategory extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shop_category', function(Blueprint $table)
        {
            $table->boolean('is_deletable')->default(true)->after('show_in_top');
        });

        \Modules\Shop\Entities\Category::create([
            'name' => 'Каталог',
            'code' => 'main',

        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('', function(Blueprint $table)
        {

        });
    }

}
