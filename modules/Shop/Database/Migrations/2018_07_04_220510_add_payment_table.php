<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPaymentTable extends Migration {

    /**
     * Run the migrations.
     *delivery_type
     * @return void
     */
    public function up()
    {
        Schema::create('shop_payment', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('order_id')->unsigned();
            $table->string('code');
            $table->string('access_code');
            $table->string('status_code')->nullable();
            $table->integer('status');
            $table->integer('payment_type');
            $table->text('message')->nullable();
            $table->timestamps();
        });

        Schema::table('shop_orders', function(Blueprint $table)
        {
            $table->integer('payment_type');
            $table->string('delivery_type');
            $table->string('delivery_info');
            $table->float('sale_price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('', function(Blueprint $table)
        {

        });
    }

}
