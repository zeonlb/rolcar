<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopAttributesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_attributes', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('code')->unique();
            $table->enum('type', [
                'text',
                'check_box',
                'radio',
                'text_area',
                'drop_down',
                'drop_down_multiple',
                'date',
            ]);
            $table->integer('sort')->default(0);
            $table->boolean('filter')->default(true);
            $table->boolean('required')->default(true);
            $table->boolean('visible')->default(true);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('shop_attributes');
    }

}
