<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Modules\Shop\Entities\Category;

class AddFrontTruckLamps extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $frCategory = Category::where('code', 'peredniy_svet')->first();
        $optionsNames = [
            "ft12" => "Боковые габаритные огни",
            "ft13" => "Парковочные огни",
        ];
        foreach ($optionsNames as $key => $o) {
            $category = new Category();
            $category->name = $o;
            $category->code = $key;
            $category->visible = true;
            $category->parent_id = $frCategory->id;
            $category->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('', function(Blueprint $table)
        {

        });
    }

}
