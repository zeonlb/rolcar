<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopProductTypeAttributesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_product_type_attributes', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->integer('shop_product_type_id')->unsigned();
            $table->integer('shop_attribute_id')->unsigned();
            $table->timestamps();

            $table->foreign('shop_product_type_id')->references('id')->on('shop_product_type');
            $table->foreign('shop_attribute_id')->references('id')->on('shop_attributes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shop_product_type_attributes', function(Blueprint $table) {
            $table->dropForeign('shop_product_type_attributes_shop_attribute_id_foreign');
            $table->dropForeign('shop_product_type_attributes_shop_product_type_id_foreign');
        });

        Schema::drop('shop_product_type_attributes');
    }

}
