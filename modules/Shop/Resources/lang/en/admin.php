<?php
return [
    'new' => 'New',
    'in_progress' => 'In progress',
    'processed' => 'Processed',
    'send_to_client' => 'Send to client',
    'delivered' => 'Delivered',
    'sold' => 'Sold',
    'canceled' => 'Canceled'
];