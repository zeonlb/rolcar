@extends('admin::layouts.master')

@section('content')
    <h1>Product</h1>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-bars"></i> Create new product</h2>

                <div class="clearfix"></div>
            </div>
            <div class="x_content">

                <form name="add-new-page" action="{{route('admin.shop.product.store')}}" method="POST"
                      class="form-horizontal form-label-left" novalidate="">

                    <div class="" role="tabpanel" data-example-id="togglable-tabs">
                        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab"
                                                                      data-toggle="tab" aria-expanded="true">
                                    General
                                </a>
                            </li>
                            <li role="presentation"><a href="#tab_content2" id="home-tab" role="tab"
                                                       data-toggle="tab" aria-expanded="true">
                                    Category
                                </a>
                            </li>
                            <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab"
                                                                data-toggle="tab" aria-expanded="false">
                                    Attributes
                                </a>
                            </li>
                            <li role="presentation" class=""><a href="#tab_content4" role="tab" id="profile-tab"
                                                                data-toggle="tab" aria-expanded="false">
                                    SEO
                                </a>
                            </li>

                            <li role="presentation" class=""><a href="#tab_content5" role="tab" id="profile-tab"
                                                                data-toggle="tab" aria-expanded="false">
                                    Photos
                                </a>
                            </li>
                            <li role="presentation" class="">
                                <a href="#tab_content7" role="tab" id="profile-tab"
                                   data-toggle="tab" aria-expanded="false">
                                    Video
                                </a>
                            </li>
                            <li role="presentation" class="">
                                <a href="#tab_content8" role="tab" id="profile-tab"
                                   data-toggle="tab" aria-expanded="false">
                                    Cross sale
                                </a>
                            </li>
                            <li role="presentation" class="">
                                <a href="#tab_content9" role="tab" id="profile-tab"
                                   data-toggle="tab" aria-expanded="false">
                                    Special price
                                </a>
                            </li>
                            <li role="presentation" class="">
                                <a href="#tab_content10" role="tab" id="profile-tab"
                                   data-toggle="tab" aria-expanded="false">
                                    Lending page
                                </a>
                            </li>
                            <li role="presentation" class=""><a href="#tab_content11" role="tab" id="profile-tab"
                                                                data-toggle="tab" aria-expanded="false">
                                    Rozetka photos
                                </a>
                            </li>
                        </ul>
                        <div id="myTabContent" class="tab-content">
                            <div role="tabpanel" class="tab-pane fade active in" id="tab_content1"
                                 aria-labelledby="home-tab">

                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="productType" value="{{$productType->id}}">

                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Name <span
                                                class="required">*</span>
                                    </label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input id="name" name="name" class="form-control col-md-7 col-xs-12"
                                               required="required" type="text">
                                    </div>
                                </div>

                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="code">Code <span
                                                class="required">*</span>
                                    </label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input id="code" name="code" class="form-control col-md-7 col-xs-12"
                                               required="required" type="text">
                                    </div>
                                </div>

                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="sku">SKU <span
                                                class="required">*</span>
                                    </label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input id="sku" name="sku" class="form-control col-md-7 col-xs-12"
                                               required="required" type="text">
                                    </div>
                                </div>
                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="brand">Brand <span
                                                class="required">*</span>
                                    </label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select required="" name="brand_id" class="form-control">
                                            <option></option>
                                            @foreach($brands as $brand)
                                                <option value="{{$brand->id}}">{{$brand->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description">Description<span
                                                class="required">*</span>
                                    </label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea  id="description" required="" name="description" class="form-control" id="" cols="20"
                                                  rows="5"></textarea>
                                    </div>
                                </div>
                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="short_description">Short
                                        description<span
                                                class="required">*</span>
                                    </label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea required="" name="short_description" class="form-control" id="short_description"
                                                  cols="20" rows="5"></textarea>
                                    </div>
                                </div>
                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="count">Count <span
                                                class="required">*</span>
                                    </label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input id="count" name="count" class="form-control col-md-7 col-xs-12"
                                               placeholder="10"
                                               required="required" type="text">
                                    </div>
                                </div>
                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="price">Price <span
                                                class="required">*</span>
                                    </label>

                                    <div class="col-md-2 col-sm-2 col-xs-4">
                                        <input id="price" name="price" class="form-control col-md-7 col-xs-12"
                                               placeholder="999"
                                               required="required" type="text">
                                    </div>
                                    <div class="col-md-2 col-sm-2 col-xs-4">
                                        <select required="" name="currency" id="currency" class="form-control">
                                            <option></option>
                                            @foreach($currencies as $currency)
                                                <option value="{{$currency->id}}">{{$currency->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-2 col-sm-2 col-xs-4">
                                        <select name="price_for" id="price_for" class="form-control">
                                            <option value="one">One</option>
                                            <option value="pair">Pair</option>
                                            <option value="set">Set</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Visible</label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div id="visible" class="btn-group" data-toggle="buttons">
                                            <label class="btn btn-default active" data-toggle-class="btn-default"
                                                   data-toggle-passive-class="btn-default">
                                                <input checked="" type="radio" name="visible" value="1"> &nbsp; On
                                                &nbsp;
                                            </label>
                                            <label class="btn btn-danger " data-toggle-class="btn-default"
                                                   data-toggle-passive-class="btn-default">
                                                <input type="radio" name="visible" value="0"> Off
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Present</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div id="is_present" class="btn-group" data-toggle="buttons">
                                            <label class="btn btn-default" data-toggle-class="btn-default"
                                                   data-toggle-passive-class="btn-default">
                                                <input checked="" type="radio" name="is_present" value="1"> &nbsp; On
                                                &nbsp;
                                            </label>
                                            <label class="btn btn-danger active" data-toggle-class="btn-default"
                                                   data-toggle-passive-class="btn-default">
                                                <input type="radio" name="is_present" value="0"> Off
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">For rozetka</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div id="is_rozetka" class="btn-group" data-toggle="buttons">
                                            <label class="btn btn-default" data-toggle-class="btn-default"
                                                   data-toggle-passive-class="btn-default">
                                                <input checked="" type="radio" name="is_rozetka" value="1"> &nbsp; On
                                                &nbsp;
                                            </label>
                                            <label class="btn btn-danger active" data-toggle-class="btn-default"
                                                   data-toggle-passive-class="btn-default">
                                                <input type="radio" name="is_rozetka" value="0"> Off
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Merchant</label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div id="is_merchant" class="btn-group" data-toggle="buttons">
                                            <label class="btn btn-default" data-toggle-class="btn-default"
                                                   data-toggle-passive-class="btn-default">
                                                <input checked="" type="radio" name="is_merchant" value="1"> &nbsp; On
                                                &nbsp;
                                            </label>
                                            <label class="btn btn-danger active" data-toggle-class="btn-default"
                                                   data-toggle-passive-class="btn-default">
                                                <input type="radio" name="is_merchant" value="0"> Off
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                @foreach($icons as  $icon)
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Icon</label>

                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input  value="{{$icon->id}}" name="icon" type="radio"
                                                   id="icon-{{$icon}}">
                                            <img src="/{{$icon->path}}" alt="">

                                        </div>
                                    </div>
                                @endforeach

                                <div class="col-md-12 col-sm-12 col-xs-12">

                                    <br/>

                                    <div class="ln_solid"></div>
                                </div>
                            </div>
                            <div role="tabpane2" class="tab-pane fade in" id="tab_content2" aria-labelledby="home-tab">

                                <ul>
                                    @foreach($categories as $category)
                                        <?php addCategoryView($category, true); ?>
                                    @endforeach
                                </ul>

                                <div class="col-md-12 col-sm-12 col-xs-12">

                                    <br/>

                                    <div class="ln_solid"></div>
                                </div>
                            </div>
                            <div role="tabpane3" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
                                <span class="section">Attributes</span>

                                <div class="col-md-12">
                                    @foreach ($productType->attributes as $attribute)
                                        @if (!$attribute->option)
                                            <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12"
                                                       for="{{$attribute->code}}">{{$attribute->name}}</label>

                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    {!! generateProductAttributesForm($attribute, 'form-control col-md-7
                                                    col-xs-12') !!}
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                                <span class="section">Options</span>

                                <div class="col-md-12">
                                    @foreach ($productType->attributes as $attribute)
                                        @if ($attribute->option)
                                            <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12"
                                                       for="{{$attribute->code}}">{{$attribute->name}}</label>

                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    {!! generateProductAttributesForm($attribute, 'form-control col-md-7
                                                    col-xs-12') !!}
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                                <div class="clearfix"></div>
                                <div class="ln_solid"></div>
                            </div>
                            <div role="tabpane4" class="tab-pane fade" id="tab_content4" aria-labelledby="profile-tab">
                                <div class="col-md-12">
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="meta_title">Meta
                                            title</label>

                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input id="meta_title" class="form-control col-md-7 col-xs-12"
                                                   name="meta_title"
                                                   placeholder="Title" type="text">
                                        </div>
                                    </div>

                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="meta_keywords">Meta
                                            keys</label>

                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input id="tags_1" name="meta_keywords" type="text"
                                                   class="tags form-control" value=""/>

                                            <div id="suggestions-container"
                                                 style="position: relative; float: left; width: 250px; margin: 10px;"></div>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12"
                                               for="meta_redirect_url">Meta redirect url</label>

                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input placeholder="/new/url" type="text" id="meta_redirect_url"
                                                   name="meta_redirect_url" class="form-control col-md-7 col-xs-12">
                                        </div>
                                    </div>

                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="meta_description">Meta
                                            description</label>

                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <textarea placeholder="Your page description" id="meta_description"
                                                      name="meta_description"
                                                      class="form-control col-md-7 col-xs-12"></textarea>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="meta_keywords">SEO H1</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input id="seo_h1" name="seo_h1"
                                                   type="text"
                                                   class="form-control"
                                                   value=""
                                            />
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <div class="ln_solid"></div>
                            </div>

                            <div role="tabpane5" class="tab-pane fade" id="tab_content5" aria-labelledby="profile-tab">
                                <div class="col-md-12">

                                    <p>Drag multiple files to the box below for multi upload or click to select files.
                                        This is for demonstration purposes only, the files are not uploaded to any
                                        server.</p>

                                    <div class="dropzone dz-clickable" id="myDrop">
                                        <div class="dz-default dz-message" data-dz-message="">
                                            <span>Drop files here to upload</span>
                                        </div>
                                    </div>

                                    <br/>
                                    <br/>
                                    <br/>
                                    <br/>
                                </div>

                                <div class="clearfix"></div>
                                <div class="ln_solid"></div>
                            </div>
                            <div role="tabpane6" class="tab-pane fade" id="tab_content6" aria-labelledby="profile-tab">
                                <div class="col-md-12">

                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="meta_description">Car</label>

                                        <div class="col-md-2 col-sm-2 col-xs-6">
                                            <select class="form-control" name="transport">
                                                <option></option>
                                                @foreach($transports as $transport)
                                                    <option value="{{$transport->id}}">{{$transport->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-2 col-sm-2 col-xs-6">
                                            <select class="form-control" name="mark"></select>
                                        </div>
                                        <div class="col-md-2 col-sm-2 col-xs-6">
                                            <select class="form-control" name="model"></select>
                                        </div>
                                        <div class="col-md-2 col-sm-2 col-xs-6">
                                            <select class="form-control" name="modification"></select>
                                        </div>
                                    </div>

                                    <table id="modification-list"
                                           class="table table-striped responsive-utilities jambo_table bulk_action">
                                        <thead>
                                        <tr class="headings">
                                            <th>
                                                <input type="checkbox" id="check-all" class="tablecheckbox">
                                            </th>
                                            <th class="column-title">ID</th>
                                            <th class="column-title">Motor</th>
                                            <th class="column-title">Power</th>
                                            <th class="column-title">Year start</th>
                                            <th class="column-title">Year end</th>
                                            <th class="bulk-actions" colspan="7">
                                                <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions (
                                                    <span class="action-cnt"> </span> )</a>
                                            </th>
                                        </tr>
                                        </thead>

                                        <tbody>


                                        </tbody>

                                    </table>
                                </div>

                                <div class="clearfix"></div>
                                <div class="ln_solid"></div>
                            </div>
                            <div role="tabpane7" class="tab-pane fade" id="tab_content7" aria-labelledby="profile-tab">
                                <div class="col-md-12">
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="youtube_code">Video
                                            code</label>

                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input id="youtube_code" name="video[youtube_code]"
                                                   type="text"
                                                   class="form-control"
                                                   value=""
                                                    />
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="video_title">Video
                                            title</label>

                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input id="video_title" name="video[title]"
                                                   type="text"
                                                   class="form-control"
                                                   value=""
                                                    />
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12"
                                               for="video[description]">Video description</label>

                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <textarea class="form-control" name="video[description]"
                                                      id="video_description"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="ln_solid"></div>
                            </div>
                            <div role="tabpane8" class="tab-pane fade" id="tab_content8" aria-labelledby="profile-tab">
                                <div class="col-md-12">
                                    <table id="cross_sale_table" class="table table-striped responsive-utilities jambo_table">
                                        <thead>
                                        <tr class="headings">
                                            <th>
                                                <input type="checkbox" id="check-all" class="tablecheckbox">
                                            </th>
                                            <th class="column-title"># </th>
                                            <th class="column-title">SKU</th>
                                            <th class="column-title">Name</th>
                                            <th class="column-title">Price</th>
                                            <th class="column-title">Status</th>
                                            <th class="bulk-actions" colspan="7">
                                                <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> )</a>
                                            </th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        @foreach($products as $pr)
                                            <tr class="even pointer">
                                                <td class="a-center">
                                                        <input type="checkbox" class="tablecheckbox innercheck" name="selected_cross_sales[]" value="{{$pr->id}}">
                                                </td>

                                                <td>{{$pr->id}}</td>
                                                <td>{{$pr->sku}}</td>
                                                <td>{{$pr->name}}</td>
                                                <td>{{$pr->price}} {{$pr->currency->name}}</td>
                                                <td>@if ($pr->visible)
                                                        <span class="text-success">active</span>
                                                    @else
                                                        <span class="text-danger">inactive</span>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>

                                    </table>
                                </div>

                                <div class="clearfix"></div>
                                <div class="ln_solid"></div>
                            </div>
                            <div role="tabpane9" class="tab-pane fade" id="tab_content9" aria-labelledby="profile-tab">
                                <div class="col-md-12">
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="discount-date_start">Date start</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input id="discount-date_start" name="discount[date_start]"
                                                   type="datetime-local"
                                                   class="form-control"
                                            />
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="discount-date_end">Date end</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input id="discount-date_end" name="discount[date_end]"
                                                   type="datetime-local"
                                                   class="form-control"
                                            />

                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="discount-type">Type</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select name="discount[type]" class="form-control" id="discount-type">
                                                <option></option>
                                                <option  value="fixed">Fixed</option>
                                                <option  value="percent">Percent</option>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="discount-value">Sale amount</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input id="discount-value" name="discount[value]"
                                                   type="text"
                                                   class="form-control"
                                            />

                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <div class="ln_solid"></div>
                            </div>
                            <div role="tabpane10" class="tab-pane fade" id="tab_content10"
                                 aria-labelledby="profile-tab">
                                <div class="col-md-12">
                                    <div class="item form-group">
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <textarea name="lending_html" id="" cols="30"
                                                      rows="10"></textarea>
                                        </div>
                                    </div>
                                    <
                                </div>

                                <div class="clearfix"></div>
                                <div class="ln_solid"></div>
                            </div>
                            <div role="tabpane11" class="tab-pane fade" id="tab_content11" aria-labelledby="profile-tab">
                                <div class="col-md-12">

                                    <p>Drag multiple files to the box below for multi upload or click to select files.
                                        This is for demonstration purposes only, the files are not uploaded to any
                                        server.</p>
                                    <div class="dropzone dz-clickable" id="myDropRozetka">
                                        <div class="dz-default dz-message" data-dz-message="">
                                            <span>Drop files here to upload</span>
                                        </div>
                                    </div>

                                    <br/>
                                    <br/>
                                    <br/>
                                    <br/>
                                </div>

                                <div class="clearfix"></div>
                                <div class="ln_solid"></div>
                            </div>
                        </div>

                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                            <button id="send" type="submit" class="btn btn-success">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>
@stop

@section('javascript')
    @include('admin::partials.js.form')
    <script src="{{Module::asset('admin:js/datatables/js/jquery.dataTables.js')}}"></script>
    <script src="{{Module::asset('admin:js/datatables/tools/js/dataTables.tableTools.js')}}"></script>

    <!-- /form validation -->
    <script src="{{Module::asset('shop:js/admin/product.js')}}"></script>
    <!-- dropzone -->
    <script src="{{Module::asset('admin:js/dropzone/dropzone.js')}}"></script>

    <script type="text/javascript">
        var uploadUrl = "{{ route('cms.image.store') }}";
        var token = "{{ csrf_token() }}";
        Dropzone.autoDiscover = false;
        var myDropzone = new Dropzone("div#myDrop", {
            url: uploadUrl,
            addRemoveLinks: true,
            dictRemoveFile: 'Remove',
            maxFilesize: 2,
            addRemoveLinks: true,
            params: {
                _token: token
            }
        });
        myDropzone.on("success", function (file, resp) {
            if (resp.fileId) {
                var input = "<input name='files[]' type='hidden' value='" + resp.fileId + "' />";
                file.uniqueId = resp.fileId;
                $('.dz-success:last').append(input);
            }
        });
        myDropzone.on("removedfile", function (file) {
            $.ajax({
                type: 'delete',
                url: '{{route('cms.image.destroy')}}',
                data: {id: file.uniqueId}
            });

        });
    </script>
    <script type="text/javascript">
        var uploadUrl = "{{ route('cms.image.store') }}";
        var token = "{{ csrf_token() }}";
        Dropzone.autoDiscover = false;
        var rozDropzone = new Dropzone("div#myDropRozetka", {
            url: uploadUrl,
            addRemoveLinks: true,
            dictRemoveFile: 'Remove',
            maxFilesize: 20000,
            addRemoveLinks: true,
            params: {
                _token: token
            }
        });
        rozDropzone.on("success", function (file, resp) {
            if (resp.fileId) {
                var input = "<input name='rozetka_files[]' type='hidden' value='" + resp.fileId + "' />";
                file.uniqueId = resp.fileId;
                $('.dz-success:last').append(input);
            }
        });
        rozDropzone.on("removedfile", function (file) {
            $.ajax({
                type: 'delete',
                url: '{{route('cms.image.destroy')}}',
                data: {id: file.uniqueId}
            });

        });
    </script>

@stop
@section('style')
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.0.2/css/font-awesome.css" rel="stylesheet">
    <link href="{{Module::asset('admin:css/editor/external/google-code-prettify/prettify.css')}}" rel="stylesheet">
    <link href="{{Module::asset('admin:css/dropzone.css')}}" rel="stylesheet">
    <link href="{{Module::asset('admin:css/editor/index.css')}}" rel="stylesheet">
    <link href="{{Module::asset('shop:css/admin/product.css')}}" rel="stylesheet">
    <link href="{{Module::asset('admin:css/datatables/tools/css/dataTables.tableTools.css')}}" rel="stylesheet">
    <style>
        .tablecheckbox {
            width: 20px;
            height: 15px;
            cursor: pointer;
        }
    </style>

    <script>
        $('document').ready(function(){
            <!-- Text editor -->
            CKEDITOR.replace( 'description' );
            CKEDITOR.config.htmlEncodeOutput = false;
            CKEDITOR.config.entities = false;
            CKEDITOR.config.basicEntities = false;
            CKEDITOR.config.configentities = false;
            CKEDITOR.config.forceSimpleAmpersand = true;
            CKEDITOR.config.allowedContent = true;
            CKEDITOR.config.skin = 'bootstrapck';
            CKEDITOR.config.extraPlugins = 'font';
            <!-- /Text editor -->
        });
        $('document').ready(function(){
            <!-- Text editor -->
            CKEDITOR.replace( 'short_description' );
            CKEDITOR.config.htmlEncodeOutput = false;
            CKEDITOR.config.entities = false;
            CKEDITOR.config.basicEntities = false;
            CKEDITOR.config.configentities = false;
            CKEDITOR.config.forceSimpleAmpersand = true;
            CKEDITOR.config.allowedContent = true;
            CKEDITOR.config.skin = 'bootstrapck';
            CKEDITOR.config.extraPlugins = 'font';
            <!-- /Text editor -->
        });
    </script>
@stop

