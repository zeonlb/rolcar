@extends('admin::layouts.master')

@section('content')
    <h1>Product</h1>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-bars"></i> Update product {{$product->name}}</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form name="add-new-page" action="{{$formRoute}}" method="POST"
                      class="form-horizontal form-label-left" novalidate="" enctype="multipart/form-data">

                    <div class="" role="tabpanel" data-example-id="togglable-tabs">
                        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab"
                                                                      data-toggle="tab" aria-expanded="true">
                                    General
                                </a>
                            </li>
                            <li role="presentation"><a href="#tab_content2" id="home-tab" role="tab"
                                                       data-toggle="tab" aria-expanded="true">
                                    Category
                                </a>
                            </li>
                            <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab"
                                                                data-toggle="tab" aria-expanded="false">
                                    Attributes
                                </a>
                            </li>
                            <li role="presentation" class=""><a href="#tab_content4" role="tab" id="profile-tab"
                                                                data-toggle="tab" aria-expanded="false">
                                    SEO
                                </a>
                            </li>

                            <li role="presentation" class=""><a href="#tab_content5" role="tab" id="profile-tab"
                                                                data-toggle="tab" aria-expanded="false">
                                    Photos
                                </a>
                            </li>
                            <li role="presentation" class="">
                                <a href="#tab_content7" role="tab" id="profile-tab"
                                   data-toggle="tab" aria-expanded="false">
                                    Video
                                </a>
                            </li>
                            <li role="presentation" class="">
                                <a href="#tab_content8" role="tab" id="profile-tab"
                                   data-toggle="tab" aria-expanded="false">
                                    Cross sale
                                </a>
                            </li>
                            <li role="presentation" class="">
                                <a href="#tab_content9" role="tab" id="profile-tab"
                                   data-toggle="tab" aria-expanded="false">
                                    Special price
                                </a>
                            </li>
                            <li role="presentation" class="">
                                <a href="#tab_content10" role="tab" id="profile-tab"
                                   data-toggle="tab" aria-expanded="false">
                                    Lending page
                                </a>
                            </li>
                            <li role="presentation" class=""><a href="#tab_content11" role="tab" id="profile-tab"
                                                                data-toggle="tab" aria-expanded="false">
                                    Rozetka photos
                                </a>
                            </li>
                        </ul>
                        <div id="myTabContent" class="tab-content">
                            <div role="tabpanel" class="tab-pane fade active in" id="tab_content1"
                                 aria-labelledby="home-tab">

                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                @if(isset($formMethod) && $formMethod == 'put')
                                    <input type="hidden" name="_method" value="put">
                                @endif
                                <input type="hidden" name="productType" value="{{$product->productType->id}}">

                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Name <span
                                                class="required">*</span>
                                    </label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input id="name" name="name" class="form-control col-md-7 col-xs-12"
                                               required="required" type="text" value="{{ $product->name }}">
                                    </div>
                                </div>

                                  <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="productType">    Тип продукта <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select class="form-control" id="productType" name="productType">
                                            @foreach ($productTypes as $productType)
                                                <option @if ($product->shop_product_type_id == $productType->id) selected
                                                            @endif value="{{$productType->id}}"> 
                                                    {{$productType->name}} 
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                  </div>                                

                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="romax_id">Romax ID <span
                                                class="required">*</span>
                                    </label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input id="code" name="romax_id" class="form-control col-md-7 col-xs-12"
                                            type="text" value="{{ $product->romax_id }}">
                                    </div>
                                </div>

                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="code">Code <span
                                                class="required">*</span>
                                    </label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input id="code" name="code" class="form-control col-md-7 col-xs-12"
                                               required="required" type="text" value="{{ $product->code }}">
                                    </div>
                                </div>

                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="sku">SKU <span
                                                class="required">*</span>
                                    </label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input id="sku" name="sku" class="form-control col-md-7 col-xs-12"
                                               required="required" type="text" value="{{ $product->sku }}">
                                    </div>
                                </div>
                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="brand">Brand <span
                                                class="required">*</span>
                                    </label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select required="" name="brand_id" class="form-control">
                                            <option></option>
                                            @foreach($brands as $brand)
                                                <option @if ($product->shop_brand_id == $brand->id) selected
                                                        @endif value="{{$brand->id}}">{{$brand->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description">Description<span
                                                class="required">*</span>
                                    </label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea id="description" required="" name="description" class="form-control"
                                                  id="" cols="20" rows="5">{{ $product->description }}</textarea>
                                    </div>
                                </div>
                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="short_description">Short
                                        description<span
                                                class="required">*</span>
                                    </label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea required="" name="short_description" class="form-control" id=""
                                                  cols="20" rows="5">{{ $product->short_description }}</textarea>
                                    </div>
                                </div>
                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="count">Count <span
                                                class="required">*</span>
                                    </label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input id="count" name="count" class="form-control col-md-7 col-xs-12"
                                               placeholder="10"
                                               required="required" type="text" value="{{ $product->count }}">
                                    </div>
                                </div>
                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="pre_order_date">Pre order day
                                    </label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input id="pre_order_date" name="pre_order_date" class="form-control col-md-7 col-xs-12" type="date" value="@if ($product->pre_order_date){{$product->pre_order_date->format('Y-m-d')}}@endif">
                                    </div>
                                </div>
                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="price">Price <span
                                                class="required">*</span>
                                    </label>

                                    <div class="col-md-2 col-sm-2 col-xs-4">
                                        <input id="price" name="price" class="form-control col-md-7 col-xs-12"
                                               placeholder="999"
                                               required="required" type="text" value="{{ $product->price }}">
                                    </div>
                                    <div class="col-md-2 col-sm-2 col-xs-4">
                                        <select required="" name="currency" id="currency" class="form-control">
                                            <option></option>
                                            @foreach($currencies as $currency)
                                                <?php $selected = ($product->currency->id == $currency->id) ? 'selected' : ''; ?>
                                                <option {{$selected}} value="{{$currency->id}}">{{$currency->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-2 col-sm-2 col-xs-4">
                                        <select required="" name="price_for" id="price_for" class="form-control">
                                            <option @if($product->price_for == 'one') selected @endif value="one">One
                                            </option>
                                            <option @if($product->price_for == 'pair') selected @endif value="pair">
                                                Pair
                                            </option>
                                            <option @if($product->price_for == 'set') selected @endif value="set">Set
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Visible</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div id="visible" class="btn-group" data-toggle="buttons">
                                            <label class="btn btn-default  @if ($product->visible) active @endif"
                                                   data-toggle-class="btn-default"
                                                   data-toggle-passive-class="btn-default">
                                                <input @if ($product->visible) checked="" @endif type="radio"
                                                       name="visible" value="1"> &nbsp; On &nbsp;
                                            </label>
                                            <label class="btn btn-danger @if (!$product->visible) active @endif"
                                                   data-toggle-class="btn-default"
                                                   data-toggle-passive-class="btn-default">
                                                <input @if (!$product->visible) checked="" @endif type="radio"
                                                       name="visible" value="0"> Off
                                            </label>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Present</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div id="is_present" class="btn-group" data-toggle="buttons">
                                            <label class="btn btn-default  @if ($product->is_present) active @endif"
                                                   data-toggle-class="btn-default"
                                                   data-toggle-passive-class="btn-default">
                                                <input @if ($product->is_present) checked="" @endif type="radio"
                                                       name="is_present" value="1"> &nbsp; On &nbsp;
                                            </label>
                                            <label class="btn btn-danger @if (!$product->is_present) active @endif"
                                                   data-toggle-class="btn-default"
                                                   data-toggle-passive-class="btn-default">
                                                <input @if (!$product->is_present) checked="" @endif type="radio"
                                                       name="is_present" value="0"> Off
                                            </label>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">For rozetka</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div id="is_rozetka" class="btn-group" data-toggle="buttons">
                                            <label class="btn btn-default  @if ($product->is_rozetka) active @endif"
                                                   data-toggle-class="btn-default"
                                                   data-toggle-passive-class="btn-default">
                                                <input @if ($product->is_rozetka) checked="" @endif type="radio"
                                                       name="is_rozetka" value="1"> &nbsp; On &nbsp;
                                            </label>
                                            <label class="btn btn-danger @if (!$product->is_rozetka) active @endif"
                                                   data-toggle-class="btn-default"
                                                   data-toggle-passive-class="btn-default">
                                                <input @if (!$product->is_rozetka) checked="" @endif type="radio"
                                                       name="is_rozetka" value="0"> Off
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Merchant</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div id="is_merchant" class="btn-group" data-toggle="buttons">
                                            <label class="btn btn-default  @if ($product->is_merchant) active @endif"
                                                   data-toggle-class="btn-default"
                                                   data-toggle-passive-class="btn-default">
                                                <input @if ($product->is_merchant) checked="" @endif type="radio"
                                                       name="is_merchant" value="1"> &nbsp; On &nbsp;
                                            </label>
                                            <label class="btn btn-danger @if (!$product->is_merchant) active @endif"
                                                   data-toggle-class="btn-default"
                                                   data-toggle-passive-class="btn-default">
                                                <input @if (!$product->is_merchant) checked="" @endif type="radio"
                                                       name="is_merchant" value="0"> Off
                                            </label>
                                        </div>
                                    </div>
                                </div>                                

                                @foreach($icons as  $icon)
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Icon</label>

                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input @if($product->icon->count() > 0  && $product->icon[0]->id == $icon->id) checked
                                                   @endif value="{{$icon->id}}" name="icon" type="radio"
                                                   id="icon-{{$icon}}">
                                            <img src="/{{$icon->path}}" alt="">
                                            @if($product->icon->count() > 0  && $product->icon[0]->id == $icon->id)
                                                <a href="#" class="delete-icon" data-id="{{$product->id}}">delete</a>
                                            @endif

                                        </div>
                                    </div>
                                @endforeach


                                <div class="col-md-12 col-sm-12 col-xs-12">

                                    <br/>
                                    <div class="ln_solid"></div>
                                </div>
                            </div>
                            <div role="tabpane2" class="tab-pane fade in" id="tab_content2" aria-labelledby="home-tab">

                                <ul>
                                    @foreach($categories as $category)
                                        <?php addCategoryView($category, true, array_column($product->categories->toArray(), 'id')); ?>
                                    @endforeach
                                </ul>

                                <div class="col-md-12 col-sm-12 col-xs-12">

                                    <br/>
                                    <div class="ln_solid"></div>
                                </div>
                            </div>
                            <div role="tabpane3" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
                                <span class="section">Attributes</span>
                                <div class="col-md-12">
                                    @foreach ($product->productType->attributes as $attribute)
                                        @if (!$attribute->option)
                                            <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12"
                                                       for="{{$attribute->code}}">{{$attribute->name}}</label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    {!! generateProductAttributesForm($attribute, 'form-control col-md-7 col-xs-12', $product) !!}
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                                <span class="section">Options</span>
                                <div class="col-md-12">
                                    @foreach ($product->productType->attributes as $attribute)
                                        @if ($attribute->option)
                                            <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12"
                                                       for="{{$attribute->code}}">{{$attribute->name}}</label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    {!! generateProductAttributesForm($attribute, 'form-control col-md-7 col-xs-12', $product) !!}
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                                <div class="clearfix"></div>
                                <div class="ln_solid"></div>
                            </div>
                            <div role="tabpane4" class="tab-pane fade" id="tab_content4" aria-labelledby="profile-tab">
                                <div class="col-md-12">
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="meta_title">Meta
                                            title</label>

                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input id="meta_title" class="form-control col-md-7 col-xs-12"
                                                   name="meta_title"
                                                   @if ($product->meta)
                                                   value="{{$product->meta->meta_title}}"
                                                   @endif
                                                   placeholder="Title" type="text">
                                        </div>
                                    </div>

                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="meta_keywords">Meta
                                            keys</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input id="tags_1" name="meta_keywords"
                                                   type="text"
                                                   class="tags form-control"
                                                   @if ($product->meta)
                                                   value="{{$product->meta->meta_keywords}}"
                                                    @endif
                                            />
                                            <div id="suggestions-container"
                                                 style="position: relative; float: left; width: 250px; margin: 10px;"></div>
                                        </div>
                                    </div>

                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12"
                                               for="meta_redirect_url">Meta redirect url</label>

                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input placeholder="/new/url"
                                                   type="text" id="meta_redirect_url"
                                                   name="meta_redirect_url"
                                                   class="form-control col-md-7 col-xs-12"
                                                   @if ($product->meta)
                                                   value="{{$product->meta->meta_redirect_url}}"
                                                    @endif
                                            >
                                        </div>
                                    </div>

                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="meta_description">Meta
                                            description</label>

                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <textarea placeholder="Your page description" id="meta_description"
                                                      name="meta_description"
                                                      class="form-control col-md-7 col-xs-12">@if ($product->meta){{$product->meta->meta_description}}@endif</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="meta_keywords">SEO
                                        H1</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input id="seo_h1" name="seo_h1"
                                               type="text"
                                               class="form-control"
                                               @if ($product->meta)
                                               value="{{$product->meta->seo_h1}}"
                                                @endif
                                        />
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <div class="ln_solid"></div>
                            </div>

                            <div role="tabpane5" class="tab-pane fade" id="tab_content5" aria-labelledby="profile-tab">
                                <div class="col-md-12">

                                    <p>Drag multiple files to the box below for multi upload or click to select files.
                                        This is for demonstration purposes only, the files are not uploaded to any
                                        server.</p>
                                    <div class="dropzone dz-clickable" id="myDrop">
                                        <div class="dz-default dz-message" data-dz-message="">
                                            <span>Drop files here to upload</span>
                                        </div>
                                    </div>

                                    <br/>
                                    <br/>
                                    <br/>
                                    <br/>
                                </div>

                                <div class="clearfix"></div>
                                <div class="ln_solid"></div>
                            </div>

                            <div role="tabpane7" class="tab-pane fade" id="tab_content7" aria-labelledby="profile-tab">
                                <div class="col-md-12">
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="youtube_code">Video
                                            code</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input id="youtube_code" name="video[youtube_code]"
                                                   type="text"
                                                   class="form-control"
                                                   @if ($product->video)
                                                   value="{{$product->video->youtube_code}}"
                                                    @endif
                                            />
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="video_title">Video
                                            title</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input id="video_title" name="video[title]"
                                                   type="text"
                                                   class="form-control"
                                                   @if ($product->video)
                                                   value="{{$product->video->title}}"
                                                    @endif
                                            />
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12"
                                               for="video_description">Video description</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <textarea class="form-control" name="video[description]"
                                                      id="video_description">@if ($product->video){{$product->video->description}}@endif</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="ln_solid"></div>
                            </div>
                            <div role="tabpane8" class="tab-pane fade" id="tab_content8" aria-labelledby="profile-tab">
                                <div class="col-md-12">
                                    <table id="cross_sale_table"
                                           class="table table-striped responsive-utilities jambo_table">
                                        <thead>
                                        <tr class="headings">
                                            <th>
                                                <input type="checkbox" id="check-all" class="tablecheckbox">
                                            </th>
                                            <th class="column-title">#</th>
                                            <th class="column-title">SKU</th>
                                            <th class="column-title">Name</th>
                                            <th class="column-title">Price</th>
                                            <th class="column-title">Status</th>
                                            <th class="bulk-actions" colspan="7">
                                                <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions (
                                                    <span class="action-cnt"> </span> )</a>
                                            </th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        @foreach($products as $pr)
                                            <tr class="even pointer @if(isset($crossSales[$pr->id])) selected @endif">
                                                <td class="a-center">
                                                    @if(isset($crossSales[$pr->id]))
                                                        <input type="checkbox" checked class="tablecheckbox innercheck"
                                                               name="selected_cross_sales[]" value="{{$pr->id}}">
                                                    @else
                                                        <input type="checkbox" class="tablecheckbox innercheck"
                                                               name="selected_cross_sales[]" value="{{$pr->id}}">
                                                    @endif
                                                </td>

                                                <td>{{$pr->id}}</td>
                                                <td>{{$pr->sku}}</td>
                                                <td>{{$pr->name}}</td>
                                                <td>{{$pr->price}} {{$pr->currency->name}}</td>
                                                <td>@if ($pr->visible)
                                                        <span class="text-success">active</span>
                                                    @else
                                                        <span class="text-danger">inactive</span>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>

                                    </table>
                                </div>
                                <div class="selected_cross_sales_block">
                                    @foreach($crossSales as $productId => $data)
                                        <input type="hidden" pr-id="{{$productId}}" name="selected_cross_sales[]"
                                               value="{{$productId}}">
                                    @endforeach
                                </div>


                                <div class="clearfix"></div>
                                <div class="ln_solid"></div>
                            </div>
                            <div role="tabpane9" class="tab-pane fade" id="tab_content9" aria-labelledby="profile-tab">
                                <div class="col-md-12">
                                    <div class="col-lg-offset-9">
                                        <button id="reset-discount" class="btn btn-warning">Reset</button>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12"
                                               for="discount-date_start">Date start</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input id="discount-date_start" name="discount[date_start]"
                                                   type="datetime-local"
                                                   class="form-control"
                                                   @if ($product->discount)
                                                   value="{{$product->discount->date_start->format('Y-m-d')}}T{{$product->discount->date_end->format('H:i')}}"
                                                    @endif
                                            />
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12"
                                               for="discount-date_end">Date end</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input id="discount-date_end" name="discount[date_end]"
                                                   type="datetime-local"
                                                   class="form-control"
                                                   @if ($product->discount)
                                                   value="{{$product->discount->date_end->format('Y-m-d')}}T{{$product->discount->date_end->format('H:i')}}"
                                                    @endif
                                            />

                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="discount-type">Type</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <?php $selectedValue = ($product->discount && $product->discount->type) ? $product->discount->type : ''; ?>
                                            <select name="discount[type]" class="form-control" id="discount-type">
                                                <option></option>
                                                <option @if($selectedValue == "fixed") selected="" @endif value="fixed">
                                                    Fixed
                                                </option>
                                                <option @if($selectedValue == "percent") selected=""
                                                        @endif value="percent">Percent
                                                </option>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="discount-value">Sale
                                            amount</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input id="discount-value" name="discount[value]"
                                                   type="text"
                                                   class="form-control"
                                                   @if ($product->discount)
                                                   value="{{$product->discount->value}}"
                                                    @endif
                                            />

                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <div class="ln_solid"></div>
                            </div>
                            <div role="tabpane10" class="tab-pane fade" id="tab_content10"
                                 aria-labelledby="profile-tab">
                                <div class="col-md-12">
                                    <div class="item form-group">
	
                                        <div class="col-md-8">
                                    	    <label for="lending_styles"> css and js block </label>
                                            <textarea class="form-control" name="lending_styles" id="lending_styles" cols="20"  rows="5">{!! html_entity_decode($product->lending_styles) !!}</textarea>
                                        </div>
                                        <div class="col-md-12">
                                    	    <label for="lending_html"> Code block </label>
                                            <textarea class="form-control" name="lending_html" id="lending_html" cols="30"  rows="10">{!! html_entity_decode($product->lending_html) !!}</textarea>
                                        </div>
                                    </div>

                                </div>

                                <div class="clearfix"></div>
                                <div class="ln_solid"></div>
                            </div>
                            <div role="tabpane11" class="tab-pane fade" id="tab_content11" aria-labelledby="profile-tab">
                                <div class="col-md-12">

                                    <p>Drag multiple files to the box below for multi upload or click to select files.
                                        This is for demonstration purposes only, the files are not uploaded to any
                                        server.</p>
                                    <div class="dropzone dz-clickable" id="myDropRozetka">
                                        <div class="dz-default dz-message" data-dz-message="">
                                            <span>Drop files here to upload</span>
                                        </div>
                                    </div>

                                    <br/>
                                    <br/>
                                    <br/>
                                    <br/>
                                </div>

                                <div class="clearfix"></div>
                                <div class="ln_solid"></div>
                            </div>

                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                            <button id="send" type="submit" class="btn btn-success">Save</button>
                            {{--<button id="save_close" type="submit" class="btn btn-success">Save & close</button>--}}
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>
@stop

@section('javascript')
    @include('admin::partials.js.form')
    <script src="{{Module::asset('admin:js/datatables/js/jquery.dataTables.js')}}"></script>
    <script src="{{Module::asset('admin:js/datatables/tools/js/dataTables.tableTools.js')}}"></script>

    <!-- /form validation -->
    <script src="{{Module::asset('shop:js/admin/product.js')}}"></script>

    <!-- dropzone -->
    <script src="{{Module::asset('admin:js/dropzone/dropzone.js')}}"></script>

    <script src="{{Module::asset('admin:js/chartjs/chart.min.js')}}"></script>
    <!-- bootstrap progress js -->
    <script src="{{Module::asset('admin:js/progressbar/bootstrap-progressbar.min.js')}}"></script>
    <script src="{{Module::asset('admin:js/nicescroll/jquery.nicescroll.min.js')}}"></script>


    <script type="text/javascript">
        var uploadUrl = "{{ route('cms.image.store') }}";
        var token = "{{ csrf_token() }}";
        Dropzone.autoDiscover = false;
        var myDropzone = new Dropzone("div#myDrop", {
            url: uploadUrl,
            dictRemoveFile: 'Remove',
            maxFilesize: 20000,
            addRemoveLinks: true,
            params: {
                _token: token
            }
        });
        myDropzone.on("success", function (file, resp) {
            if (resp.fileId) {
                var input = "<input name='files[]' type='hidden' value='" + resp.fileId + "' />";
                file.uniqueId = resp.fileId;
                $('.dz-success:last').append(input);
            }
        });
        myDropzone.on("removedfile", function (file) {
            $.ajax({
                type: 'delete',
                url: '{{route('cms.image.destroy')}}',
                data: {id: file.uniqueId}
            });

        });
    </script>
    <script>
        var photos = JSON.parse('{!! $product->photos->toJson() !!}');
        $.each(photos, function (index, val) {
            var mockFile = {name: val.name, size: 2000, type: 'image/jpeg', status: Dropzone.ADDED, uniqueId: val.id};
            myDropzone.emit("addedfile", mockFile);

            myDropzone.createThumbnailFromUrl(mockFile, '{{url()}}/' + val.path);
            myDropzone.emit("complete", mockFile);
            myDropzone.files.push(mockFile);
            var input = "<input name='files[]' type='hidden' value='" + val.id + "' />";
            $('.dz-preview:last').append(input);
        });
    </script>

    <script type="text/javascript">
        $('#reset-discount').on('click', function (e) {
            e.preventDefault();
            $("#discount-date_start, #discount-date_end,#discount-value").val("");
            $('#discount-type option').removeAttr('selected')
        });
        var uploadUrl = "{{ route('cms.image.store') }}";
        var token = "{{ csrf_token() }}";
        Dropzone.autoDiscover = false;
        var rozDropzone = new Dropzone("div#myDropRozetka", {
            url: uploadUrl,
            dictRemoveFile: 'Remove',
            maxFilesize: 20000,
            addRemoveLinks: true,
            params: {
                _token: token
            }
        });
        rozDropzone.on("success", function (file, resp) {
            if (resp.fileId) {
                var input = "<input name='rozetka_files[]' type='hidden' value='" + resp.fileId + "' />";
                file.uniqueId = resp.fileId;
                $('.dz-success:last').append(input);
            }
        });
        rozDropzone.on("removedfile", function (file) {
            $.ajax({
                type: 'delete',
                url: '{{route('cms.image.destroy')}}',
                data: {id: file.uniqueId}
            });

        });
    </script>

    <script>
        var rphotos = JSON.parse('{!! $product->rozetkaPhotos->toJson() !!}');
        $.each(rphotos, function (index, val) {
            var mockFile = {name: val.name, size: 2000, type: 'image/jpeg', status: Dropzone.ADDED, uniqueId: val.id};
            rozDropzone.emit("addedfile", mockFile);

            rozDropzone.createThumbnailFromUrl(mockFile, '{{url()}}/' + val.path);
            rozDropzone.emit("complete", mockFile);
            rozDropzone.files.push(mockFile);
            var input = "<input name='rozetka_files[]' type='hidden' value='" + val.id + "' />";
            $('.dz-preview:last').append(input);
        });
    </script>
    <script>
        var asInitVals = [];
        $(document).ready(function () {
            var oTable = $('#cross_sale_table').dataTable({
                "oLanguage": {
                    "sSearch": "Search:"
                },
                "aoColumnDefs": [
                    {
                        'bSortable': false,
                        'aTargets': [0]
                    } //disables sorting for column one
                ],
                'iDisplayLength': 12,
                "sPaginationType": "full_numbers",
                "dom": 'T<"clear">lfrtip',
                "tableTools": {}
            });
            $("tfoot input").keyup(function () {
                /* Filter on the column based on the index of this element's parent <th> */
                oTable.fnFilter(this.value, $("tfoot th").index($(this).parent()));
            });
            $("tfoot input").each(function (i) {
                asInitVals[i] = this.value;
            });
            $("tfoot input").focus(function () {
                if (this.className == "search_init") {
                    this.className = "";
                    this.value = "";
                }
            });
            $("tfoot input").blur(function (i) {
                if (this.value == "") {
                    this.className = "search_init";
                    this.value = asInitVals[$("tfoot input").index(this)];
                }
            });
            $('#cross_sale_table').on('click', '.tablecheckbox', function () {
                var productId = $(this).val();
                var hasSelected = $(this).parent().parent().hasClass("selected");
                if (hasSelected) {
                    $('.selected_cross_sales_block input[value="' + productId + '"]').remove()
                } else {

                    var html = '<input type="hidden" pr-id="' + productId + '" name="selected_cross_sales[]" value="' + productId +
                        '">';
                    if (!$('.selected_cross_sales_block input[value="' + productId + '"]').length) {
                        $('.selected_cross_sales_block').append(html);
                    }
                }
            });
        });
    </script>
    <script>
        $('document').ready(function () {
            $('#save_close').on('click', function (e) {
                e.preventDefault();
                $(this).parent().parent().submit();
                var thisWindow = window.open("Listing3.html", '_self');
                thisWindow.close();
            });
        });
    </script>
    <script src="{{Module::asset('admin:js/chartjs/chart.min.js')}}"></script>
    <!-- bootstrap progress js -->
    <script src="{{Module::asset('admin:js/progressbar/bootstrap-progressbar.min.js')}}"></script>
    <script src="{{Module::asset('admin:js/nicescroll/jquery.nicescroll.min.js')}}"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.1/js/bootstrap.min.js"></script>


    <script src="//cdnjs.cloudflare.com/ajax/libs/ace/1.1.3/ace.js"></script>
    <script src="{{Module::asset('admin:vendor/textarea-as-ace-editor-master/dist/textarea-as-ace-editor.js')}}"></script>
    {{--<script>--}}
        {{--$( document ).ready(function() {--}}
            {{--$("#lending_html").asAceEditor();--}}
            {{--var editor = $('#lending_html').data('ace-editor');--}}
            {{--$(".ace_editor").css({"max-width" : "100%"});--}}
            {{--editor.getSession().on('change', function(){--}}
                {{--$('#lending_html').val(editor.getSession().getValue());--}}
            {{--});--}}
        {{--});--}}
    {{--</script>--}}
    <script>
        $('document').ready(function () {
            $('.delete-icon').on('click', function(e){
                e.preventDefault();
                var id = $(this).attr('data-id');
                if (confirm('Delete icon ?')) {
                    $.ajax({
                        type: 'delete',
                        url: '{{route('admin.shop.product.delete_icon', ['id' => $product->id])}}',
                        success: function(){
                            $('input[name="icon"]').removeAttr('checked');
                            $('.delete-icon').remove()
                        }
                    });
                }


            })

        });
    </script>
    <script>
        $('document').ready(function () {
            <!-- Text editor -->
            CKEDITOR.replace('description');
            CKEDITOR.replace('short_description');
            CKEDITOR.config.htmlEncodeOutput = false;
            CKEDITOR.config.entities = false;
            CKEDITOR.config.basicEntities = false;
            CKEDITOR.config.configentities = false;
            CKEDITOR.config.forceSimpleAmpersand = true;
            CKEDITOR.config.allowedContent = true;
            CKEDITOR.config.skin = 'bootstrapck';
            CKEDITOR.config.extraPlugins = 'font';
            <!-- /Text editor -->
        });
    </script>
@stop
@section('style')
    <link href="{{Module::asset('admin:css/editor/index.css')}}" rel="stylesheet">
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.0.2/css/font-awesome.css" rel="stylesheet">
    <link href="{{Module::asset('admin:css/editor/external/google-code-prettify/prettify.css')}}" rel="stylesheet">
    <link href="{{Module::asset('admin:css/dropzone.css')}}" rel="stylesheet">
    <link href="{{Module::asset('admin:css/editor/index.css')}}" rel="stylesheet">
    <link href="{{Module::asset('shop:css/admin/product.css')}}" rel="stylesheet">
    <link href="{{Module::asset('shop:css/admin/product.css')}}" rel="stylesheet">
    <link href="{{Module::asset('admin:css/datatables/tools/css/dataTables.tableTools.css')}}" rel="stylesheet">
    <style>
        .tablecheckbox {
            width: 20px;
            height: 15px;
            cursor: pointer;
        }
    </style>
@stop

