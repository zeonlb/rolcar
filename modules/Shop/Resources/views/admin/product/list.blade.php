@extends('admin::layouts.master')

@section('content')

    <h1>Product</h1>
    <div>
        <div class="col-md-12">
            <div class="col-md-6"></div>

            <div class="col-md-2 text-right">
                <a href="{{route('admin.shop.product.export')}}"><button class="btn btn-info">Export</button></a>
            </div>
            <div class="col-md-2 text-left">
                <form class="entity-import" method="POST" action="{{route('admin.shop.product.import')}}" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="file" class="hidden" name="import_products" id="import_products">
                    <button class="btn btn-warning btn-import-products" type ="submit">Import</button>
                </form>
            </div>
            <div class="col-md-2 text-right">
                <a href="{{route('admin.shop.product.create')}}"><button class="btn btn-success">Create new product</button></a>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>List of products</h2>
                        <div class="dataTables_length" style="float: right">
                            <form>
                            <label>Product types
                                <select  style="padding: 6px;" name="product_type" aria-controls="example">
                                    <option></option>
                                    @foreach($productTypes as $type)
                                        <option @if(Request::get('product_type') == $type->id) selected @endif value="{{$type->id}}">{{$type->name}}</option>
                                    @endforeach
                                </select>
                                <button class="btn btn-primary">Filter</button>
                            </label>
                            </form>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <table id="example" class="table table-striped responsive-utilities jambo_table">
                            <thead>
                            <tr class="headings">
                                <th>ID </th>
                                <th>ID R</th>
                                <th>Sku </th>
                                <th>Name </th>
                                <th>Attributes </th>
                                <th>Price </th>
                                <th>Count </th>
                                <th>Visible </th>
                                <th>Created </th>
                                <th class=" no-link last"><span class="nobr">Action</span>
                                </th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($products as $product)
                                <tr class="even pointer">
                                    <td>{{$product->id}}</td>
                                    <td>{{$product->romax_id}}</td>
                                    <td>{{$product->sku}}</td>
                                    <td>{{$product->name}}</td>
                                    <td>
                                        @foreach($product->options as $option)
                                            @if ($option->productOptions)
                                                {{$option->productOptions->name}}
                                            @endif
                                     
                                        @endforeach
                                    </td>
                                    <td>{{$product->price}} {{$product->currency->name}}</td>
                                    <td>{{$product->count}}</td>
                                    @if ($product->visible)
                                        <td class="text-success">active</td>
                                    @else
                                        <td class="text-danger">inactive</td>
                                    @endif

                                    <td>{{$product->created_at}}</td>
                                    <td class="entity-action last">
                                        <a href="{{route('admin.shop.product.copy', ['id' => $product->id])}}"><button type="button" class="btn btn-default btn-xs">copy</button></a>
                                        <a target="_blank" href="{{route('admin.shop.product.edit', ['id' => $product->id])}}"><button type="button" class="btn btn-success btn-xs">edit</button></a>
                                            <form class="entity-delete-form" method="POST" action="{{route('admin.shop.product.destroy', ['id' => $product->id])}}">
                                                <input type="hidden" name="_method" value="DELETE">
                                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                                <button  type="submit" class="entity-delete btn btn-danger btn-xs">delete</button>
                                            </form>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <br />
            <br />
            <br />

        </div>
    </div>
@stop

@section('javascript')
    <!-- chart js -->
    <script src="{{Module::asset('admin:js/chartjs/chart.min.js')}}"></script>
    <!-- bootstrap progress js -->
    <script src="{{Module::asset('admin:js/progressbar/bootstrap-progressbar.min.js')}}"></script>
    <script src="{{Module::asset('admin:js/nicescroll/jquery.nicescroll.min.js')}}"></script>
    <!-- icheck -->
    <script src="{{Module::asset('admin:js/icheck/icheck.min.js')}}"></script>

    <!-- Datatables -->
    <script src="{{Module::asset('admin:js/datatables/js/jquery.dataTables.js')}}"></script>
    <script src="{{Module::asset('admin:js/datatables/tools/js/dataTables.tableTools.js')}}"></script>
    <script>
        $(document).ready(function () {
            $('input.tableflat').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });
        });

        var asInitVals = new Array();
        $(document).ready(function () {
            var oTable = $('#example').dataTable({
                "oLanguage": {
                    "sSearch": "Search all columns:"
                },
                "aoColumnDefs": [
                    {
                        'bSortable': true,
                        'aTargets': [0]
                    } //disables sorting for column one
                ],
                'iDisplayLength': 12,
                "sPaginationType": "full_numbers",
                "dom": 'T<"clear">lfrtip',
                "tableTools": {
                }
            });
            $("tfoot input").keyup(function () {
                /* Filter on the column based on the index of this element's parent <th> */
                oTable.fnFilter(this.value, $("tfoot th").index($(this).parent()));
            });
            $("tfoot input").each(function (i) {
                asInitVals[i] = this.value;
            });
            $("tfoot input").focus(function () {
                if (this.className == "search_init") {
                    this.className = "";
                    this.value = "";
                }
            });
            $("tfoot input").blur(function (i) {
                if (this.value == "") {
                    this.className = "search_init";
                    this.value = asInitVals[$("tfoot input").index(this)];
                }
            });
        });
    </script>

    <script>
        $('document').ready(function(){
            $('.btn-import-products').on('click', function (e) {
                e.preventDefault()
                $(this).parent().find('input#import_products').trigger('click');
            })
            $('#import_products').on('change', function () {
                $(this).parent().submit();
            })
        })
    </script>
@stop
@section('style')
    <link href="{{Module::asset('admin:css/custom.css')}}" rel="stylesheet">
    <link href="{{Module::asset('admin:css/icheck/flat/green.css')}}" rel="stylesheet">
    <link href="{{Module::asset('admin:css/datatables/tools/css/dataTables.tableTools.css')}}" rel="stylesheet">
@stop

