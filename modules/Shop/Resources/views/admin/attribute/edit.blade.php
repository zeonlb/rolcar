@extends('admin::layouts.master')

@section('content')
    <h1>Attributes</h1>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-bars"></i> Create new attribute</h2>
                <div class="nav navbar-right panel_toolbox">
                    <form method="POST" action="{{route('admin.shop.attribute.destroy', ['id' => $attribute->id])}}">
                        <input type="hidden" name="_method" value="delete">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <button id="send" type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form name="add-new-page" action="{{route('admin.shop.attribute.update', ['id' => $attribute->id])}}" method="POST"
                      class="form-horizontal form-label-left" novalidate="">

                    <div class="" role="tabpanel" data-example-id="togglable-tabs">
                        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab"
                                                                      data-toggle="tab" aria-expanded="true">
                                    Attribute
                                </a>
                            </li>
                            <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab"
                                                                data-toggle="tab" aria-expanded="false">
                                    Options
                                </a>
                            </li>
                        </ul>
                        <div id="myTabContent" class="tab-content">
                            <div role="tabpanel" class="tab-pane fade active in" id="tab_content1"
                                 aria-labelledby="home-tab">

                                <input type="hidden" name="_method" value="put">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Name <span
                                                class="required">*</span>
                                    </label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input id="name" name="name" class="form-control col-md-7 col-xs-12"
                                               value="{{$attribute->name}}" placeholder="color" required="required"
                                               type="text">
                                    </div>
                                </div>

                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="code">Code <span
                                                class="required">*</span>
                                    </label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input id="code" name="code" class="form-control col-md-7 col-xs-12"
                                               value="{{$attribute->code}}" placeholder="color" required="required"
                                               type="text">
                                    </div>
                                </div>
                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="sort">Sort <span
                                                class="required">*</span>
                                    </label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input id="sort" name="sort" class="form-control col-md-7 col-xs-12"
                                               value="{{$attribute->sort}}" placeholder="0" required="required"
                                               type="text">
                                    </div>
                                </div>

                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="url">Type<span
                                                class="required">*</span>
                                    </label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select required="required" class="form-control col-md-7 col-xs-12" name="type"
                                                id="">
                                            <option></option>
                                            <option @if($attribute->type == 'text') selected @endif value="text">Text
                                            </option>
                                            <option @if($attribute->type == 'text_area') selected
                                                                                         @endif value="text_area">
                                                Textarea
                                            </option>
                                            <option @if($attribute->type == 'check_box') selected
                                                                                         @endif value="check_box">
                                                Checkbox
                                            </option>
                                            <option @if($attribute->type == 'drop_down_multiple') selected
                                                                                         @endif value="drop_down_multiple">Multiple Drop Down
                                            </option>
                                            <option @if($attribute->type == 'drop_down') selected
                                                                                         @endif value="drop_down">Drop
                                                Down
                                            </option>
                                            <option @if($attribute->type == 'date') selected @endif value="date">Date
                                            </option>
                                        </select>

                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Required</label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div id="required" class="btn-group" data-toggle="buttons">
                                            <label class="btn btn-default @if ($attribute->required) active @endif"
                                                   data-toggle-class="btn-default"
                                                   data-toggle-passive-class="btn-default">
                                                <input @if ($attribute->required) checked @endif type="radio"
                                                                                  name="required" value="1"> &nbsp; True
                                                &nbsp;
                                            </label>
                                            <label class="btn btn-danger @if (!$attribute->required) active @endif"
                                                   data-toggle-class="btn-default"
                                                   data-toggle-passive-class="btn-default">
                                                <input @if (!$attribute->required) checked @endif type="radio"
                                                                                   name="required" value="0" > False
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Visible</label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div id="visible" class="btn-group" data-toggle="buttons">
                                            <label class="btn btn-default  @if ($attribute->visible) active @endif"
                                                   data-toggle-class="btn-default"
                                                   data-toggle-passive-class="btn-default">
                                                <input @if ($attribute->visible) checked @endif type="radio"
                                                                                 name="visible" value="1"> &nbsp; On
                                                &nbsp;
                                            </label>
                                            <label class="btn btn-danger @if (!$attribute->visible) active @endif"
                                                   data-toggle-class="btn-default"
                                                   data-toggle-passive-class="btn-default">
                                                <input  @if (!$attribute->visible) checked @endif type="radio"
                                                                                   name="visible" value="0" > Off
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Show in filter</label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div id="filter" class="btn-group" data-toggle="buttons">
                                            <label class="btn btn-default @if ($attribute->filter) active @endif"
                                                   data-toggle-class="btn-default"
                                                   data-toggle-passive-class="btn-default">
                                                <input @if ($attribute->filter) checked @endif type="radio"name="filter"
                                                                                value="1"> &nbsp; Show &nbsp;
                                            </label>
                                            <label class="btn btn-danger @if (!$attribute->filter) active @endif"
                                                   data-toggle-class="btn-default"
                                                   data-toggle-passive-class="btn-default">
                                                <input @if (!$attribute->filter) checked @endif type="radio"
                                                                                 name="filter" value="0" > Hide
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Option</label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div id="option" class="btn-group" data-toggle="buttons">
                                            <label class="btn btn-default @if ($attribute->option) active @endif"
                                                   data-toggle-class="btn-default"
                                                   data-toggle-passive-class="btn-default">
                                                <input @if ($attribute->option) checked @endif type="radio"name="option"
                                                                                value="1"> &nbsp; On &nbsp;
                                            </label>
                                            <label class="btn btn-danger @if (!$attribute->option) active @endif"
                                                   data-toggle-class="btn-default"
                                                   data-toggle-passive-class="btn-default">
                                                <input @if (!$attribute->option) checked @endif type="radio"
                                                                                 name="option" value="0" > Off
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">

                                    <br/>

                                    <div class="ln_solid"></div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
                                <div class="col-md-12">
                                    <div class="col-md-8"></div>
                                    <div class="col-md-4">
                                        <button class="add-new-option-btn btn btn-primary">Add option</button>
                                    </div>
                                </div>
                                <div class="option-blocks">
                                    @foreach($attribute->options as $key => $option)

                                        <div class="new-option-block">
                                            <span class="section">Option {{++$key}}</span>
                                            <input id="option_name" class="form-control col-md-7 col-xs-12"
                                                   name="options[{{$key}}][id]" value="{{$option->id}}" type="hidden">
                                            <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12"
                                                       for="option_name">Code</label>

                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <input id="option_name" class="form-control col-md-7 col-xs-12"
                                                           name="options[{{$key}}][code]" value="{{$option->code}}" type="text"></div>
                                            </div>
                                            <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12"
                                                       for="option_value">Name</label>

                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <input id="option_value" class="form-control col-md-7 col-xs-12"
                                                           name="options[{{$key}}][name]" value="{{$option->name}}" type="text">

                                                </div>
                                                <button class="delete-attr-option btn btn-danger" eid="{{$option->id}}">Delete</button>
                                            </div>
                                        </div>
                                        @endforeach
                                        <div class="clearfix"></div>
                                </div>


                                <div class="ln_solid"></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                            <button id="send" type="submit" class="btn btn-success">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
@stop

@section('javascript')
    @include('admin::partials.js.form')

    </script>
    <!-- input tags -->
    <script>
        function onAddTag(tag) {
            alert("Added a tag: " + tag);
        }

        function onRemoveTag(tag) {
            alert("Removed a tag: " + tag);
        }

        function onChangeTag(input, tag) {
            alert("Changed a tag: " + tag);
        }

        $(function () {
            $('#tags_1').tagsInput({
                width: 'auto'
            });
        });
    </script>
    <!-- /input tags -->
    <!-- form validation -->
    <script type="text/javascript">
        $(document).ready(function () {
            // initialize the validator function
            validator.message['date'] = 'not a real date';

            // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
            $('form')
                    .on('blur', 'input[required], input.optional, select.required', validator.checkField)
                    .on('change', 'select.required', validator.checkField)
                    .on('keypress', 'input[required][pattern]', validator.keypress);

            $('.multi.required')
                    .on('keyup blur', 'input', function () {
                        validator.checkField.apply($(this).siblings().last()[0]);
                    });
            $('form').submit(function (e) {
                e.preventDefault();
                $('#html').val($('#editor').html());
                var submit = true;
                // evaluate the form using generic validaing
                if (!validator.checkAll($(this))) {
                    submit = false;
                }

                if (submit)
                    this.submit();
                return false;
            });
        });
    </script>
    <!-- /form validation -->

    <script src="{{Module::asset('shop:js/admin/attribute.js')}}"></script>

@stop
@section('style')
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.0.2/css/font-awesome.css" rel="stylesheet">
    <link href="{{Module::asset('admin:css/editor/external/google-code-prettify/prettify.css')}}" rel="stylesheet">
    <link href="{{Module::asset('admin:css/editor/index.css')}}" rel="stylesheet">
@stop

