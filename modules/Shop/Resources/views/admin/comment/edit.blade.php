@extends('admin::layouts.master')

@section('content')
    <h1>Comments</h1>
    <div class="">
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Comment</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div><img src="{{ \Modules\Cms\Core\Images\ThumbBuilder::resize($comment->product->getThumb(), 'auto', 198) }}" title="{{$comment->product->name}}" alt="{{$comment->product->name}}"/></div><br/>
                        <div>Product SKU: {{$comment->product->sku}}</div><br/>
                        <div>Name: {{$comment->name}}</div><br/>
                        <div>Product name: {{$comment->product->name}}</div><br/>
                        <div>Advantages: {{$comment->advantages}}</div><br/>
                        <div>Disadvantages: {{$comment->disadvantages}}</div><br/>
                        @if ($comment->photos->count() > 0)
                            <div class="comment-block">
                                <div class="comment-container">
                                    @foreach($comment->photos as $key => $photo)
                                        <div class="comment-img-thumb">
                                            <a class="video gallery-item"   href="/{!! $photo->path !!}">
                                                <img
                                                      src="{{\Modules\Cms\Core\Images\ThumbBuilder::resize($photo->path, 200, 200)}}"/>
                                            </a>
                                        </div>
                                    @endforeach

                                    <br clear="both">
                                </div>
                            </div>

                        @endif
                        @if ($comment->video_link)
                            <div class="comment-video-thumb">
                                <a class="mfp-iframe" href="{{$comment->video_link}}">
                                    <i style="font-size: 40px; color: red" class="fa fa-youtube-play " aria-hidden="true"></i>
                                </a>
                            </div>
                        @endif
                        <div>Comment: {{$comment->comment}}</div><br/>

                        <div>Processed:
                            @if ($comment->processed)
                                <lable class="text-success">true</lable>
                            @else
                                <lable class="text-danger">false</lable>
                            @endif</div><br/>
                        <br/>
                    </div>
                    @if ($comment->answers)
                        @foreach($comment->answers as $answ)
                            <div>Manager answer: {{$answ->comment}}</div>
                            <form class="entity-delete-form" method="POST" action="{{route('admin.shop.comment.destroy', ['id' => $answ->id])}}">
                                <input type="hidden" name="_method" value="DELETE">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <button  type="submit" class="entity-delete btn btn-danger btn-xs">delete</button>
                            </form>
                            <br/>
                            <br/>
                            @endforeach
                        @endif

                    <form action="{{route('admin.shop.comment.answer', ['id' => $comment->id])}}" method="POST">
                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                        <textarea name="answer" id="" style="width: 500px" rows="10"></textarea>
                        <br>
                        <br>
                        <button type="submit" class="btn-info btn">Save</button>
                    </form>
                    <br>
                    <form action="{{route('admin.shop.comment.update', ['id' => $comment->id])}}" method="POST">
                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                        <input type="hidden" name="_method" value="put"/>
                        @if ($comment->processed)
                            <input type="hidden" name="processed" value="0"/>
                            <button class="btn btn-danger" type="submit">Mark as unprocessed</button>
                        @else
                            <input type="hidden" name="processed" value="1"/>
                            <button class="btn btn-primary" type="submit">Mark as processed</button>
                        @endif

                    </form>
                </div>

            </div>


        </div>
    </div>
@stop

@section('javascript')
    <!-- chart js -->
    <script src="{{Module::asset('admin:js/chartjs/chart.min.js')}}"></script>
    <!-- bootstrap progress js -->
    <script src="{{Module::asset('admin:js/progressbar/bootstrap-progressbar.min.js')}}"></script>
    <script src="{{Module::asset('admin:js/nicescroll/jquery.nicescroll.min.js')}}"></script>
    <!-- icheck -->
    <script src="{{Module::asset('admin:js/icheck/icheck.min.js')}}"></script>

    <!-- Datatables -->
    <script src="{{Module::asset('admin:js/datatables/js/jquery.dataTables.js')}}"></script>
    <script src="{{Module::asset('admin:js/datatables/tools/js/dataTables.tableTools.js')}}"></script>
    <script>
        $(document).ready(function () {
            $('input.tableflat').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });
        });

        var asInitVals = new Array();
        $(document).ready(function () {
            var oTable = $('#example').dataTable({
                "oLanguage": {
                    "sSearch": "Search all columns:"
                },
                "aoColumnDefs": [
                    {
                        'bSortable': false,
                        'aTargets': [0]
                    } //disables sorting for column one
                ],
                'iDisplayLength': 12,
                "sPaginationType": "full_numbers",
                "dom": 'T<"clear">lfrtip',
                "tableTools": {
                }
            });
            $("tfoot input").keyup(function () {
                /* Filter on the column based on the index of this element's parent <th> */
                oTable.fnFilter(this.value, $("tfoot th").index($(this).parent()));
            });
            $("tfoot input").each(function (i) {
                asInitVals[i] = this.value;
            });
            $("tfoot input").focus(function () {
                if (this.className == "search_init") {
                    this.className = "";
                    this.value = "";
                }
            });
            $("tfoot input").blur(function (i) {
                if (this.value == "") {
                    this.className = "search_init";
                    this.value = asInitVals[$("tfoot input").index(this)];
                }
            });
        });
    </script>
@stop
@section('style')
    <link href="{{Module::asset('admin:css/custom.css')}}" rel="stylesheet">
    <link href="{{Module::asset('admin:css/icheck/flat/green.css')}}" rel="stylesheet">
    <link href="{{Module::asset('admin:css/datatables/tools/css/dataTables.tableTools.css')}}" rel="stylesheet">
@stop

