@extends('admin::layouts.master')

@section('content')
    <h1>Product type</h1>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-bars"></i> Create new product type</h2>

                <div class="clearfix"></div>
            </div>
            <div class="x_content">

                <form name="add-new-page" action="{{route('admin.shop.product_type.store')}}" method="POST"
                      class="form-horizontal form-label-left" novalidate="">

                    <div class="" role="tabpanel" data-example-id="togglable-tabs">
                        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab"
                                                                      data-toggle="tab" aria-expanded="true">
                                    General
                                </a>
                            </li>
                            <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab"
                                                                data-toggle="tab" aria-expanded="false">
                                    Attributes
                                </a>
                            </li>
                            <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab"
                                                                data-toggle="tab" aria-expanded="false">
                                    Options
                                </a>
                            </li>
                        </ul>
                        <div id="myTabContent" class="tab-content">
                            <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">

                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Name <span
                                                class="required">*</span>
                                    </label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input id="name"  name="name" class="form-control col-md-7 col-xs-12"
                                               required="required" type="text">
                                    </div>
                                </div>

                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="code">Code <span
                                                class="required">*</span>
                                    </label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input id="code"  name="code" class="form-control col-md-7 col-xs-12"
                                               required="required" type="text">
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12">

                                    <br /><div class="ln_solid"></div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
                                <div class="col-md-12">
                                        <div class="col-md-6">
                                            <span class="section">Product attributes</span>
                                            <ul id="attribute" class="droptrue product-enable">

                                            </ul>
                                        </div>
                                        <div class="col-md-6">
                                            <span class="section">Available attributes</span>
                                            <ul class="droptrue">
                                                @foreach($attributes as $attribute)
                                                    <li etype="attribute" eid="{{$attribute->id}}">{{$attribute->name}}</li>
                                                @endforeach
                                            </ul>
                                        </div>

                                </div>
                                <div class="option-blocks"></div>


                                <div class="ln_solid"></div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <span class="section">Product options</span>
                                        <ul id="option" class="droptrue product-enable">

                                        </ul>
                                    </div>
                                    <div class="col-md-6">
                                        <span class="section">Available options</span>
                                        <ul class="droptrue">
                                            @foreach($options as $option)
                                                <li etype="option" eid="{{$option->id}}">{{$option->name}}</li>
                                            @endforeach
                                        </ul>
                                    </div>

                                </div>
                                <div class="option-blocks"></div>


                                <div class="ln_solid"></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                            <button id="send" type="submit" class="btn btn-success">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    @include('admin::partials.js.form')

    <!-- input tags -->
    <script>
        function onAddTag(tag) {
            alert("Added a tag: " + tag);
        }

        function onRemoveTag(tag) {
            alert("Removed a tag: " + tag);
        }

        function onChangeTag(input, tag) {
            alert("Changed a tag: " + tag);
        }

        $(function () {
            $('#tags_1').tagsInput({
                width: 'auto'
            });
        });
    </script>
    <!-- /input tags -->
    <!-- form validation -->
    <script type="text/javascript">
        $(document).ready(function () {
            // initialize the validator function
            validator.message['date'] = 'not a real date';

            // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
            $('form')
                    .on('blur', 'input[required], input.optional, select.required', validator.checkField)
                    .on('change', 'select.required', validator.checkField)
                    .on('keypress', 'input[required][pattern]', validator.keypress);

            $('.multi.required')
                    .on('keyup blur', 'input', function () {
                        validator.checkField.apply($(this).siblings().last()[0]);
                    });

        });
    </script>
    <!-- /form validation -->
    {{--Attributes UI sortable--}}
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script src="{{Module::asset('shop:js/admin/product_type.js')}}"></script>

  @stop
@section('style')
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.0.2/css/font-awesome.css" rel="stylesheet">
    <link href="{{Module::asset('admin:css/editor/external/google-code-prettify/prettify.css')}}" rel="stylesheet">
    <link href="{{Module::asset('admin:css/editor/index.css')}}" rel="stylesheet">
    <style>
        .droptrue {
            cursor: pointer;
            list-style: none;
            padding: 0;
            margin: 0;
            font-size: 15px;
            text-align: center;
            min-height: 300px;
            border: 1px solid #E6E9ED;
        }
        .droptrue li{
            padding: 3px 0 3px 0;
            margin: 5px;
            border: 1px solid #E6E9ED;
            background-color: #F5F7FA;
        }
    </style>
    @stop

