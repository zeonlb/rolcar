@extends('admin::layouts.master')

@section('content')
    <h1>Order</h1>
    <div class="col-md-12">
        <div class="col-md-9"></div>
        <div c class="col-md-3 text-right">

            <a href="{{route('admin.shop.order.index')}}">
                <button type="button" class="btn btn-default">
                    Back
                </button>
            </a>


        </div>
    </div>
    <div class="">
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Order #{{$order->invoice}}</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content row">
                        <div class="col-md-6">
                            <div>Name: {{$order->last_name}} {{$order->first_name}}</div><br/>
                            <div>Phone: {{$order->phone}}</div><br/>
                            <div>E-mail: <a href="mailto:{{$order->email}}">{{$order->email}}</a></div><br/>                            
                        </div>
                        <div class="col-md-6">
                            <div>
                                <div><b>Доставка:</b></div>
                                <div style="padding-left: 20px">
                                    @if ( isset($delivery->type) )
                                        <div>Тип: {{$delivery->type}}</div> 
                                    @endif
                                    @if ( isset($delivery->city) )
                                        <div>Город: {{$delivery->city}}</div> 
                                    @endif
                                    @if ( isset($delivery->warehouse) )
                                        <div>склад НП: {{$delivery->warehouse}}</div> 
                                    @endif  
                                    @if ( isset($delivery->adress) )
                                        <div>Адрес: {{$delivery->adress}}</div> 
                                    @endif                                      
                                    </div>                                                             
                            </div>
                            <div><b>Оплата:</b> {{$order->payment}}</div>
                            <div><b>Промо код:</b> {{$order->promocode}}</div>
                            <div><b>Комментарий:</b> {{$order->description}}</div>
                        </div>


                        <table class="table" cellpadding="0" border="0" cellspacing="0">
                            @foreach( $order->orderDetails as $od )
                                <tr>
                                    <?php $photo = $od->product->photos ? $od->product->photos[0]->path : '';
                                    ?>
                                    <td><img class="product-thumb" src="{{ \Modules\Cms\Core\Images\ThumbBuilder::resize($photo , 'auto', 70) }}"/></td>
                                    <td  valign="top">
                                        <a href="{{route('product.show', ['code' => $od->product->code])}}" target="_blank">SKU: {{$od->product->sku}}
                                            <br />
                                            {{$od->product->name}} <br />
                                            Кол-во: {{$od->count}} <br />
                                            {{$od->selected}}
                                        </a>
                                    </td>
                                    <td  align="right" valign="top"><b>{{$od->product->price}} грн</b></td>
                                </tr>
                            @endforeach
                            <tr class="total">
                                <td></td>
                                <td width="80%" align="right" valign="top">Всего</td>
                                <td width="20%"  align="right" valign="top"><b>{{$order->total_price}} грн</b> </td>
                            </tr>
                        </table>
                    </div>

                    <form action="{{route('admin.shop.order.update', ['id' => $order->id])}}" method="POST">
                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                        <input type="hidden" name="_method" value="put"/>
                        <div class="col-md-6">
                            <label>Order status</label>
                            <select required name="order[status]" class="form-control">
                                <option></option>
                                @foreach($status as $s)
                                    <option @if($order->status == $s) selected @endif value="{{$s}}">{{trans("shop::admin.".$s)}}</option>
                                @endforeach
                            </select>
                            <br>
                            <button class="btn btn-primary" type="submit">Save</button>
                        </div>
                    </form>
                </div>

            </div>


        </div>
    </div>
@stop

@section('javascript')
    <!-- chart js -->
    <script src="{{Module::asset('admin:js/chartjs/chart.min.js')}}"></script>
    <!-- bootstrap progress js -->
    <script src="{{Module::asset('admin:js/progressbar/bootstrap-progressbar.min.js')}}"></script>
    <script src="{{Module::asset('admin:js/nicescroll/jquery.nicescroll.min.js')}}"></script>
    <!-- icheck -->
    <script src="{{Module::asset('admin:js/icheck/icheck.min.js')}}"></script>

    <!-- Datatables -->
    <script src="{{Module::asset('admin:js/datatables/js/jquery.dataTables.js')}}"></script>
    <script src="{{Module::asset('admin:js/datatables/tools/js/dataTables.tableTools.js')}}"></script>
    <script>
        $(document).ready(function () {
            $('input.tableflat').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });
        });

        var asInitVals = new Array();
        $(document).ready(function () {
            var oTable = $('#example').dataTable({
                "oLanguage": {
                    "sSearch": "Search all columns:"
                },
                "aoColumnDefs": [
                    {
                        'bSortable': false,
                        'aTargets': [0]
                    } //disables sorting for column one
                ],
                'iDisplayLength': 12,
                "sPaginationType": "full_numbers",
                "dom": 'T<"clear">lfrtip',
                "tableTools": {
                }
            });
            $("tfoot input").keyup(function () {
                /* Filter on the column based on the index of this element's parent <th> */
                oTable.fnFilter(this.value, $("tfoot th").index($(this).parent()));
            });
            $("tfoot input").each(function (i) {
                asInitVals[i] = this.value;
            });
            $("tfoot input").focus(function () {
                if (this.className == "search_init") {
                    this.className = "";
                    this.value = "";
                }
            });
            $("tfoot input").blur(function (i) {
                if (this.value == "") {
                    this.className = "search_init";
                    this.value = asInitVals[$("tfoot input").index(this)];
                }
            });
        });
    </script>
@stop
@section('style')
    <link href="{{Module::asset('admin:css/custom.css')}}" rel="stylesheet">
    <link href="{{Module::asset('admin:css/icheck/flat/green.css')}}" rel="stylesheet">
    <link href="{{Module::asset('admin:css/datatables/tools/css/dataTables.tableTools.css')}}" rel="stylesheet">
@stop

