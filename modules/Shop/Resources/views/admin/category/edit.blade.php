@extends('admin::layouts.master')

@section('content')
    <h1>Category</h1>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-bars"></i> Edit "{{$category->name}}" category</h2>
                <div class="nav navbar-right panel_toolbox">
                    <form method="POST" action="{{route('admin.shop.category.destroy', ['id' => $category->id])}}">
                        <input type="hidden" name="_method" value="delete">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <button id="send" type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </div>

                <div class="clearfix"></div>
            </div>
            <div class="x_content">

                <form name="add-new-page" action="{{route('admin.shop.category.update', ['id' => $category->id])}}"
                      method="POST"
                      class="form-horizontal form-label-left" enctype="multipart/form-data">

                    <div class="" role="tabpanel" data-example-id="togglable-tabs">
                        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab"
                                                                      data-toggle="tab" aria-expanded="true">
                                    Category
                                </a>
                            </li>
                            <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab"
                                                                data-toggle="tab" aria-expanded="false">
                                    SEO
                                </a>
                            </li>
                            <li role="presentation" class=""><a href="#tab_content3" role="tab" id="products"
                                                                data-toggle="tab" aria-expanded="false">
                                    Products
                                </a>
                            </li>
                        </ul>
                        <div id="myTabContent" class="tab-content">
                            <div role="tabpanel" class="tab-pane fade active in" id="tab_content1"
                                 aria-labelledby="home-tab">

                                <input type="hidden" name="_method" value="put">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="url">Parent
                                    </label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select class="form-control col-md-7 col-xs-12" name="parent_id" id="">
                                            <option></option>
                                            @foreach($categories as $cat)
                                                <option @if ($cat->id == $category->parent_id) selected
                                                        @endif value="{{$cat->id}}">{{$cat->name}}</option>
                                            @endforeach
                                        </select>

                                    </div>
                                </div>
                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Name <span
                                                class="required">*</span>
                                    </label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input id="name" name="name" class="form-control col-md-7 col-xs-12"
                                               value="{{$category->name}}" required="required" type="text">
                                    </div>
                                </div>

                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="code">Code <span
                                                class="required">*</span>
                                    </label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input id="code" name="code" class="form-control col-md-7 col-xs-12"
                                               value="{{$category->code}}" required="required" type="text">
                                    </div>
                                </div>
                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="code">Description<span
                                                class="required">*</span>
                                    </label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea name="description" class="form-control" id="" cols="20"
                                                  rows="5">{{$category->description}}</textarea>
                                    </div>
                                </div>
                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="sort">Sort<span
                                                class="required">*</span>
                                    </label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input id="sort" name="sort" class="form-control col-md-7 col-xs-12"
                                               value="{{$category->sort}}"  type="text">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Visible</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div id="visible" class="btn-group" data-toggle="buttons">
                                            <label class="btn btn-default @if($category->visible) active @endif"
                                                   data-toggle-class="btn-default"
                                                   data-toggle-passive-class="btn-default">
                                                <input @if($category->visible) checked @endif type="radio"
                                                       name="visible" value="1"> &nbsp; On &nbsp;
                                            </label>
                                            <label class="btn btn-danger @if(!$category->visible) active @endif"
                                                   data-toggle-class="btn-default"
                                                   data-toggle-passive-class="btn-default">
                                                <input @if(!$category->visible) checked @endif type="radio"
                                                       name="visible" value="0"> Off
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Show in main
                                        catalog</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div id="show_in_main" class="btn-group" data-toggle="buttons">
                                            <label class="btn btn-default @if($category->show_in_main) active @endif"
                                                   data-toggle-class="btn-default"
                                                   data-toggle-passive-class="btn-default">
                                                <input @if($category->show_in_main) checked @endif type="radio"
                                                       name="show_in_main" value="1"> &nbsp; Show &nbsp;
                                            </label>
                                            <label class="btn btn-danger @if(!$category->show_in_main) active @endif"
                                                   data-toggle-class="btn-default"
                                                   data-toggle-passive-class="btn-default">
                                                <input @if(!$category->show_in_main) checked @endif type="radio"
                                                       name="show_in_main" value="0"> Hide
                                            </label>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Show in main top</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div id="show_in_top" class="btn-group" data-toggle="buttons">
                                            <label class="btn btn-default @if($category->show_in_top) active @endif"
                                                   data-toggle-class="btn-default"
                                                   data-toggle-passive-class="btn-default">
                                                <input @if($category->show_in_top) checked @endif type="radio"
                                                       name="show_in_top" value="1"> &nbsp; Show &nbsp;
                                            </label>
                                            <label class="btn btn-danger @if(!$category->show_in_top) active @endif"
                                                   data-toggle-class="btn-default"
                                                   data-toggle-passive-class="btn-default">
                                                <input @if(!$category->show_in_top) checked @endif type="radio"
                                                       name="show_in_top" value="0"> Hide
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Show in filter</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div id="filter" class="btn-group" data-toggle="buttons">
                                            <label class="btn btn-default @if($category->filter) active @endif"
                                                   data-toggle-class="btn-default"
                                                   data-toggle-passive-class="btn-default">
                                                <input @if($category->filter) checked @endif type="radio" name="filter"
                                                       value="1"> &nbsp; Show &nbsp;
                                            </label>
                                            <label class="btn btn-danger @if(!$category->filter) active @endif"
                                                   data-toggle-class="btn-default"
                                                   data-toggle-passive-class="btn-default">
                                                <input @if(!$category->filter) checked @endif type="radio" name="filter"
                                                       value="0"> Hide
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Image</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input name="file" type="file" id="exampleInputFile">
                                        @if($category->img)
                                            <br>
                                            <img src="{{ \Modules\Cms\Core\Images\ThumbBuilder::resize($category->img , 145, 'auto') }}"
                                                 alt="">
                                        @endif
                                    </div>

                                </div>


                                <div class="col-md-12 col-sm-12 col-xs-12">

                                    <br/>
                                    <div class="ln_solid"></div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
                                <span class="section">Meta data</span>

                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="meta_title">Meta
                                        title</label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input id="meta_title" class="form-control col-md-7 col-xs-12"
                                               name="meta_title"
                                               value="{{$category->meta->meta_title}}"
                                               placeholder="Title" type="text">
                                    </div>
                                </div>

                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="meta_keywords">Meta
                                        keys</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input value="{{$category->meta->meta_keywords}}" id="tags_1"
                                               name="meta_keywords"
                                               type="text" class="tags form-control" value=""/>
                                        <div id="suggestions-container"
                                             style="position: relative; float: left; width: 250px; margin: 10px;"></div>
                                    </div>
                                </div>
                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="meta_redirect_url">Meta
                                        redirect url</label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input placeholder="/new/url" value="{{$category->meta->meta_redirect_url}}"
                                               type="text" id="meta_redirect_url" name="meta_redirect_url"
                                               class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>

                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="meta_description">Meta
                                        description</label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea placeholder="Your page description" id="meta_description"
                                                  name="meta_description"
                                                  class="form-control col-md-7 col-xs-12">{{$category->meta->meta_description}}</textarea>
                                    </div>
                                </div>
                                <span class="section">Seo data</span>

                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="meta_description">Seo
                                        text</label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea placeholder="Your seo text" id="seo_text" name="seo_text"
                                                  class="form-control col-md-7 col-xs-12">{{$category->meta->seo_text}}</textarea>
                                    </div>
                                </div>
                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="meta_description">Seo
                                        text 2</label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea placeholder="Your seo text" id="seo_text_2" name="seo_text_2"
                                                  class="form-control col-md-7 col-xs-12">{{$category->meta->seo_text_2}}</textarea>
                                    </div>
                                </div>
                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="meta_description">Seo
                                        footer text</label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea placeholder="Your seo footer text" id="seo_footer_text"
                                                  name="seo_footer_text"
                                                  class="form-control col-md-7 col-xs-12">{{$category->meta->seo_footer_text}}</textarea>
                                    </div>
                                </div>

                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="products">

                                <div class="x_content">
                                    <table id="example" class="table table-striped responsive-utilities jambo_table">
                                        <thead>
                                        <tr class="headings">
                                            <th>#</th>
                                            <th>Sku</th>
                                            <th>Name</th>
                                            <th>Price</th>
                                            <th>Visible</th>
                                            <th class=" no-link last"><span class="nobr">Action</span>
                                            <th>Sort</th>
                                            </th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        @foreach($category->products as $product)
                                            <tr class="even pointer">
                                                <td>{{$product->id}}</td>
                                                <td>{{$product->sku}}</td>
                                                <td>{{$product->name}}</td>
                                                <td>{{$product->price}} {{$product->currency->name}}</td>
                                                @if ($product->visible)
                                                    <td class="text-success">active</td>
                                                @else
                                                    <td class="text-danger">inactive</td>
                                                @endif

                                                <td>{{$product->created_at}}</td>
                                                <td class="entity-action last">
                                                    <input size="1" class="form-control" value="{{$category->cjp[$product->id]->sort}}" name="category[sort][{{$product->id}}]">
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-3">
                                <button id="send" type="submit" class="btn btn-success">Save</button>
                            </div>
                        </div>
                </form>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    @include('admin::partials.js.form')


    <script type="text/javascript">
        $(document).ready(function () {
            // initialize the validator function
            validator.message['date'] = 'not a real date';

            // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
            $('form')
                .on('blur', 'input[required], input.optional, select.required', validator.checkField)
                .on('change', 'select.required', validator.checkField)
                .on('keypress', 'input[required][pattern]', validator.keypress);

            $('.multi.required')
                .on('keyup blur', 'input', function () {
                    validator.checkField.apply($(this).siblings().last()[0]);
                });
            $('form').submit(function (e) {
                e.preventDefault();
                $('#html').val($('#editor').html());
                var submit = true;
                // evaluate the form using generic validaing
                if (!validator.checkAll($(this))) {
                    submit = false;
                }

                if (submit)
                    this.submit();
                return false;
            });
        });

    </script>
    <!-- /form validation -->

    <!-- Text editor -->
    <script>
        CKEDITOR.replace('seo_footer_text');
        CKEDITOR.config.htmlEncodeOutput = false;
        CKEDITOR.config.entities = false;
        CKEDITOR.config.basicEntities = false;
        CKEDITOR.config.configentities = false;
        CKEDITOR.config.forceSimpleAmpersand = true;
        CKEDITOR.config.allowedContent = true;
        CKEDITOR.config.extraPlugins = 'uploadwidget';
//        CKEDITOR.config.skin = 'bootstrapck';
//        CKEDITOR.config.extraPlugins = 'uploadwidget';
    </script>
    <!-- /Text editor -->
    <!-- Text editor -->
    <script>
        CKEDITOR.replace('seo_text');
        CKEDITOR.config.htmlEncodeOutput = false;
        CKEDITOR.config.entities = false;
        CKEDITOR.config.basicEntities = false;
        CKEDITOR.config.configentities = false;
        CKEDITOR.config.forceSimpleAmpersand = true;
        CKEDITOR.config.allowedContent = true;
        CKEDITOR.config.extraPlugins = 'font';
        CKEDITOR.config.skin = 'bootstrapck';
    </script>
    <!-- /Text editor -->
    <!-- Text editor -->
    <script>
        CKEDITOR.replace('seo_text_2');
        CKEDITOR.config.htmlEncodeOutput = false;
        CKEDITOR.config.entities = false;
        CKEDITOR.config.basicEntities = false;
        CKEDITOR.config.configentities = false;
        CKEDITOR.config.forceSimpleAmpersand = true;
        CKEDITOR.config.allowedContent = true;
        CKEDITOR.config.extraPlugins = 'font';
        CKEDITOR.config.skin = 'bootstrapck';
    </script>
    <!-- /Text editor -->
@stop
@section('style')
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.0.2/css/font-awesome.css" rel="stylesheet">
    <link href="{{Module::asset('admin:css/editor/external/google-code-prettify/prettify.css')}}" rel="stylesheet">
    <link href="{{Module::asset('admin:css/editor/index.css')}}" rel="stylesheet">
@stop

