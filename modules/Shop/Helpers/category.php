<?php

function addCategoryView($category, $withCheckboxes = false, $productCategoriesList = []){
    $checked = '';
    if ($productCategoriesList) {
        if (in_array($category->id, $productCategoriesList)) {
            $checked = 'checked';
        }
    }
    $checkBox = $withCheckboxes ? '<input name="categories[]" '.$checked.' type="checkbox" value="'.$category->id.'" eid="'.$category->id.'" class="tableflat">' : '';
    echo "<li>$checkBox <a href='".route('admin.shop.category.edit', ['id' => $category->id])."'>{$category->name}</a></li>";
    $category->with('parents');
    if ($category->parents()->count()) {
        foreach ($category->parents as $cat) {
            echo "<ul class='child'>";
            addCategoryView($cat, $withCheckboxes, $productCategoriesList);
            echo "</ul>";

        }
    }
}

function addCategoryShopView($category, $withCheckboxes = false, $productCategoriesList = []){
    $checked = '';
    if ($productCategoriesList) {
        if (in_array($category->id, $productCategoriesList)) {
            $checked = 'checked';
        }
    }
    $checkBox = $withCheckboxes ? '<input name="categories[]" '.$checked.' type="checkbox" value="'.$category->id.'" eid="'.$category->id.'" class="tableflat">' : '';
    echo "<li>$checkBox <a href='".route('admin.shop.category.edit', ['id' => $category->id])."'>{$category->name}</a></li>";
    $category->with('parents');
    if ($category->parents()->count()) {
        foreach ($category->parents as $cat) {
            echo "<ul class='child'>";
            addCategoryView($cat, $withCheckboxes, $productCategoriesList);
            echo "</ul>";

        }
    }
}

function groupCategories($categories) {
   foreach ($categories as $key => $category) {
       if (is_int($category['parent_id']) && array_key_exists($category['parent_id'], $categories)) {
           $categories[$category['parent_id']]['children'][] = $category;
           unset($categories[$key]);
       } else {
           $category['children'] = [];
       }
   }
   return $categories;
}