<?php

function generateProductAttributesForm(
    \Modules\Shop\Entities\Attribute $attribute,
    $class = null,
    $product = false
){
    return \Modules\Shop\Forms\Product\ManualGenerator::generate($attribute, $class, $product);
}

function convertDate(\DateTime $dateTime)
{
    // Вывод даты на русском
    $monthes = array(
        1 => 'января', 2 => 'февраля', 3 => 'марта', 4 => 'апреля',
        5 => 'мая', 6 => 'июня', 7 => 'июля', 8 => 'августа',
        9 => 'сентября', 10 => 'октября', 11 => 'ноября', 12 => 'декабря'
    );
    return strtolower($dateTime->format('d ') . $monthes[($dateTime->format('n'))] . $dateTime->format(' Y'));

}
