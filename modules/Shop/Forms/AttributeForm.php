<?php namespace Modules\Shop\Forms;

use Kris\LaravelFormBuilder\Form;
use Modules\Car\Entities\Mark;
use Modules\Car\Entities\Transport;


class AttributeForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('name', 'text', [
                'rules' => 'required|min:2',
                'label' => 'Name'
            ])
            ->add('type', 'choice', [
                'choices' => [
                    'text' => 'Text',
                    'check_box' => 'Check box',
                    'radio' => 'Radio',
                    'drop_down' => 'Drop down',
                    'date' => 'Date'
                ],
                'expanded' => false,
                'multiple' => false,
                'rules' => 'required',
            ])

            ->add('filter', 'choice', [
                'choices' => ['1' => 'On', '0' => 'Off'],
                'selected' => ['1'],
                'expanded' => true,
                'multiple' => false
            ])
            ->add('filter', 'choice', [
                'choices' => ['1' => 'On', '0' => 'Off'],
                'selected' => ['1'],
                'expanded' => true,
                'multiple' => false
            ])
            ->add('required', 'choice', [
                'choices' => ['1' => 'On', '0' => 'Off'],
                'selected' => ['1'],
                'expanded' => true,
                'multiple' => false
            ])
            ->add('submit', 'submit',
                [
                    'label' => 'Create user',
                    'template' => 'vendor.laravel-form-builder.button_modal',
                    'attr' => ['class' => 'btn btn-success']]);
    }
}