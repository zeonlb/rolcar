<?php namespace Modules\Shop\Forms;

use Kris\LaravelFormBuilder\Form;


class CategoryForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('name', 'text', [
                  'rules' => 'required|min:2|max:255',
                  'label' => 'Name'
            ])
            ->add('code', 'text', [
                  'rules' => 'required|min:2|max:255',
                  'label' => 'Code'
            ])
            ->add('img', 'text', [
                  'rules' => 'required|min:2|max:255',
                  'label' => 'Image'
            ])
            ->add('description', 'textarea', [
                  'rules' => 'required|min:2',
                  'label' => 'Description'
            ])
            ->add('submit', 'submit',
                [
                    'label' => 'Create user',
                    'template' => 'vendor.laravel-form-builder.button_modal',
                    'attr' => ['class' => 'btn btn-success']]);
    }
}