<?php namespace Modules\Shop\Forms;

use Kris\LaravelFormBuilder\Form;


class AttributeOptionsForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('name', 'text', [
                'rules' => 'required|min:2',
                'label' => 'Name'
            ])
            ->add('code', 'text', [
                'rules' => 'required|min:2',
                'label' => 'Code'
            ])
            ->add('shop_attribute_id', 'entity', [
                'class' => 'Modules\Shop\Entities\AttributeOption',
                'label' => 'Mark',
                'rules' => 'required',
                'property' => 'name',
                'query_builder' => function (AttributeOption $entity) {
                    return $entity->all();
                }
            ])
            ->add('submit', 'submit',
                [
                    'label' => 'Create user',
                    'template' => 'vendor.laravel-form-builder.button_modal',
                    'attr' => ['class' => 'btn btn-success']]);
    }
}