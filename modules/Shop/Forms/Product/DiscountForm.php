<?php namespace Modules\Shop\Forms\Product;

use Kris\LaravelFormBuilder\Form;
use Modules\Car\Entities\Mark;
use Modules\Car\Entities\Transport;


class DiscountForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('date_start', 'datetime', [
                'rules' => 'required|min:2',
                'label' => 'Start date'
            ])
            ->add('date_end', 'datetime', [
                'rules' => 'required|min:2',
                'label' => 'End date'
            ])

            ->add('type', 'choice', [
                'choices' => ['fixed' => 'Fixed', 'percent' => 'Percent'],
                'expanded' => true,
                'multiple' => false
            ])

            ->add('submit', 'submit',
                [
                    'label' => 'Create user',
                    'template' => 'vendor.laravel-form-builder.button_modal',
                    'attr' => ['class' => 'btn btn-success']]);
    }
}