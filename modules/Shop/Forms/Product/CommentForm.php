<?php  namespace Modules\Shop\Forms\Product;


class CommentForm
{
    public function buildForm()
    {
        $this
            ->add('name', 'text', [
                'rules' => 'required|min:2',
                'label' => 'Name'
            ])
            ->add('comment', 'textarea', [
                'rules' => 'required|min:2',
                'label' => 'Comment'
            ])

            ->add('submit', 'submit',
                [
                    'label' => 'Create user',
                    'template' => 'vendor.laravel-form-builder.button_modal',
                    'attr' => ['class' => 'btn btn-success']]);
    }
}