<?php  namespace Modules\Shop\Forms\Product;

use Modules\Shop\Entities\Attribute;

class ManualGenerator
{
    public static function generate(Attribute $attribute, $class, $product){
        $html = '';
        $params = self::inputAttributeParams($attribute, $class);
        switch ($attribute['type']) {
            case "text" :
                $value = '';

                if ($product) {
                    $productOption = $product->options->where('shop_attribute_id', $attribute->id)->first();
                    if ($productOption && $productOption->value) {
                        $value = "value='". json_decode($productOption->value). "'";
                    }
                }
                $html .= "<input $params $value  type='text' name='attributes[{$attribute->id}]' placeholder='{$attribute->name}' />";
                break;
            case "text_area" :
                $value = '';

                if ($product) {
                    $productOption = $product->options->where('shop_attribute_id', $attribute->id)->first();
                    if ($productOption && $productOption->value) {
                        $value = json_decode($productOption->value);
                    }
                }
                $html .= "<textarea $params  name='attributes[{$attribute->id}]' placeholder='{$attribute->name}'>{$value}</textarea>";
                break;
            case "drop_down" :
                $selectedPOId = false;

                if ($product) {
                    $productOption = $product->options->where('shop_attribute_id', $attribute->id)->first();
                    if ($productOption) {
                        $selectedPOId = $productOption->shop_product_option_id;
                    }
                }

                $html .= "<select $params name='attributes[{$attribute->id}]'><option></option>";
                foreach ($attribute->options as $option) {
                    $selected = ($selectedPOId && $selectedPOId == $option->id) ? "selected" : '';
                    $html .= "<option $selected value='{$option->id}'>{$option->name}</option>";
                }
                $html .= "</select>";
                break;
            case "drop_down_multiple" :
                $selectedPOIdList = [];
                if ($product) {
                    $productOption = $product->options->where('shop_attribute_id', $attribute->id);
                    $selectedPOIdList = array_column($productOption->toArray(), 'shop_product_option_id');
                }
                $params .= " multiple";
                $html .= "<select $params name='attributes[{$attribute->id}][]'>";
                foreach ($attribute->options as $option) {
                    $selected = (in_array($option->id, $selectedPOIdList)) ? "selected" : '';
                    $html .= "<option $selected value='{$option->id}'>{$option->name}</option>";
                }
                $html .= "</select>";
                break;
        }
        return $html;
    }

    public static function inputAttributeParams($attribute, $class)
    {
        $params = '';
        if ($attribute['required']) {
            $params .= 'required="required"';
        }
        if ($class) {
            $params .= "class='$class'";
        }
        return $params;
    }
}