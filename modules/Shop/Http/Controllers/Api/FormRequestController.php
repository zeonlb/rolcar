<?php namespace Modules\Shop\Http\Controllers\Api;


use Exception;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Log;
use Pingpong\Modules\Routing\Controller;
use Session;
use stdClass;
use URL;

/**
 * Class TildaController
 * @package Modules\Shop\Http\Controllers\Api
 */
class FormRequestController extends Controller
{

    public function postForm(Request $request)
    {

        $client = new Client();
        $data = $request->all();

        $sources = [
            'source' => $data['source'],
            'pages_type' => $data['pages_type'],
            'formName' => $data['form_name'],
            'utm' => session('utm_data', [])
        ];

        unset($data['source']);
        unset($data['pages_type']);
        unset($data['formname']);
        unset($data['form_name']);
        unset($data['formid']);
        unset($data['tranid']);

        if (array_key_exists('Phone', $data)) {
            $data['phone'] = convertPhone($data['Phone']);
            unset($data['Phone']);
        }

        if (array_key_exists('Name', $data)) {
            $data['name'] = $data['Name'];
            unset($data['Name']);
        }

        if (array_key_exists('Email', $data)) {
            $data['email'] = $data['Email'];
            unset($data['Email']);
        }
        if (URL::previous()) {
            $parts = parse_url(URL::previous());
            parse_str($parts['query']??'', $queryParams);
            if ($queryParams) {
                $data = array_merge($data, $queryParams);
            }
        }
        
        $client->request('POST', env('MAX_SERVER') . '/api/v2/shop/form-request/create', [
            'exceptions' => false,
            'headers' => [
                'api-token' => env('MAX_TOKEN')
            ],
            'form_params' => [
                'fields' => json_encode($data),
                'sources' => json_encode($sources),
                'status' => 0
            ]
        ]);

        Log::info('new Request ' . $sources['source'] . $sources['formName'] . ' send to Romax');

        Session::flash('message', "Спасибо за заявку. В ближайшее время с вами свяжется наш менеджер.");

        if ($sources['formName'] == 'заказ в 1 клик') {
            try {
                $api = app('esputnik');
                $api->startEventAutomation360('rolcar_one_click', [
                    "phone" => $data['phone'],
                ]);
            } catch (Exception $e) {

            }
            Session::flash('landing_one_click', "");
        }

        if ($sources['formName'] == 'Перезвони мне') {
            try {
                $api = app('esputnik');
                $api->startEventAutomation360('rolcar_recall_me', [
                    "phone" => $data['phone'],
                ]);
            } catch (Exception $e) {

            }
            Session::flash('recall_ga', "");
        }

        if ($sources['formName'] == 'Стать партнером') {
            try {
                $api = app('esputnik');
                $api->startEventAutomation360('rolcar_partnership', [
                    "phone" => $data['phone'],
                    "email" => $data['email'],
                    "name" => $data['name']
                ]);
            } catch (Exception $e) {

            }
            Session::flash('partner_ga', "");
        }


        return redirect()->back();
    }

    public function postSubscribe(Request $request) {

        $this->validate($request, [
            'email' => 'required|email',
        ]);

        $user = 'user@esputnik.com';
        $password = env('ESPUTNIK_PASSWORD');
        $subscribe_contact_url = 'https://esputnik.com/api/v1/contact/subscribe';

        $json_contact_value = new stdClass();
        $contact = new stdClass();
        $contact->channels = array(
            array('type'=>'email', 'value' => $request->get('email')),
        );
        $json_contact_value->contact = $contact;
        $json_contact_value->groups = array('rolcar');	// группы, в которые будет помещен контакт
        $this->send_request($subscribe_contact_url, $json_contact_value, $user, $password);
        Session::flash('message', "Вы успешно подписались на рассылку.");
        return redirect()->back();

        $json_contact_value = [];
        $contact = [];
        $contact['firstName'] = '';
        $contact['channels'] = array(
            ['type'=>'email', 'value' => $request->get('email')],
        );
        $json_contact_value['contact'] = $contact;
        $json_contact_value['groups'] = array('rolcar');
        $api = app('esputnik');
        $api->subscribe($json_contact_value);

        Session::flash('message', "Вы успешно подписались на рассылку.");
        return redirect()->back();
    }

    function send_request($url, $json_value, $user, $password) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($json_value));
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json', 'Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_USERPWD, $user.':'.$password);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1);
        curl_setopt ($ch, CURLOPT_SSLVERSION, 6);
        $output = curl_exec($ch);
        echo($output);
        curl_close($ch);
    }
}
