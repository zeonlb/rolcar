<?php namespace Modules\Shop\Http\Controllers\Api;


use App\Repositories\NovaPoshtaRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Log;
use Pingpong\Modules\Routing\Controller;
use Session;
use URL;

/**
 * Class NpController
 * @package Modules\Shop\Http\Controllers\Api
 */
class NpController extends Controller
{

    public function city(Request $request, NovaPoshtaRepository $repository)
    {
        return $repository->getCityData($request->get('search', 'all'));
    }

    public function warhouse(Request $request, NovaPoshtaRepository $repository)
    {
        return $repository->getWarhouse($request->get('city', ''));
    }

}
