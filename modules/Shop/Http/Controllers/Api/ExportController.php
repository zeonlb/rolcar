<?php namespace Modules\Shop\Http\Controllers\Api;


use Carbon\Carbon;
use DB;
use Modules\Shop\Entities\Product;
use Modules\Shop\Utils\Price\Processor;
use Pingpong\Modules\Collection;
use Pingpong\Modules\Routing\Controller;
use SimpleXMLElement;

/**
 * Class ExportController
 * @package Modules\Shop\Http\Controllers\Api
 */
class ExportController extends Controller
{
    const TSOKOL_ATTR_ID = 2;
    const MODEL_RYAD_ATTR_ID = 1;
    const VOLTAG_RYAD_ATTR_ID = 7;
    const POWER_RYAD_ATTR_ID = 4;

    const ROZETKA_OPTIOTNS_MAP = [
        'tsokol' => 'Тип лампы',
        'tekhnologiya' => 'Вид',
        'category' => 'Назначение',
    ];

    public function hotlineProducts()
    {
        $now = Carbon::now();
        Header('Content-type: text/xml;charset=utf-8');
        $products = Product::with(['categories', 'brand', 'options.productOptions'])->where(['visible' => true])->get();
        $categories = DB::table('shop_category')->where('code', '!=', 'main')->get();
        $pXml = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><price></price>');
        $pXml->addChild("date", date('Y-m-d H:i'));
        $pXml->addChild("firmName", "rolcar.COM.UA");
        $pXml->addChild("firmId", "31594");
        $categoriesXml = $pXml->addChild("categories");
        foreach ($categories as $category) {
            $cat = $categoriesXml->addChild('category');
            $cat->addChild('id', $category->id);
            $cat->addChild('name', $category->name);
        }
        $items = $pXml->addChild('items');
        $priceManager = new Processor();
        foreach ($products as $product) {
            $item = $items->addChild('item');
            $item->addChild('id', $product->id);
            if ($product->categories->count() > 0) {
                $item->addChild('categoryId', $product->categories[count($product->categories) - 1]->id);
            }
            $item->addChild('code', $product->sku);
            $item->addChild('vendor', $product->brand->name);
            $param = $product->brand->name;
            if ($attr = $product->options->keyBy('shop_attribute_id')[self::TSOKOL_ATTR_ID] ?? false) {
                $param .= ' ' . $attr->productOptions->getAttributeValue('name');
            }
            if ($attr = $product->options->keyBy('shop_attribute_id')[self::MODEL_RYAD_ATTR_ID] ?? false) {
                $param .= ' ' . $attr->productOptions->getAttributeValue('name');
            }
            if ($attr = $product->options->keyBy('shop_attribute_id')[self::VOLTAG_RYAD_ATTR_ID] ?? false) {
                $param .= ' ' . $attr->productOptions->getAttributeValue('name') . 'V';
            }
            if ($attr = $product->options->keyBy('shop_attribute_id')[self::POWER_RYAD_ATTR_ID] ?? false) {
                $param .= ' ' . $attr->productOptions->getAttributeValue('name') . 'W';
            }
            $param .= ' (' . trim($product->sku) . ')';
            $item->addChild('name', $param);


            $item->addChild('url', route('product.show', ['code' => $product->code]));
            if ($product->photos->count() > 0) {
                $item->addChild('image', url($product->photos[0]->path));
            }
            $item->addChild('priceRUAH', $priceManager->calculate($product)->actualPrice());
            if ($product->pre_order_date && $now->diffInHours($product->pre_order_date, false) > 0) {
                $stock = $item->addChild('stock', 'Под заказ');
                $stock->addAttribute('days', $product->getPreOrderDiffHours());
            } else {
                $mess = 'В наличии';
                if ($product->count == 0) {
                    $mess = 'Нет в наличии';
                }
                $item->addChild('stock', $mess);
            }
            $description = [];
            foreach ($product->options as $option) {
                if ($option->productOptions && $option->attribute->visible) {
                    $param = $option->productOptions->getAttributeValue('name');
                    $xattr = $item->addChild('param', $param);
                    $xattr->addAttribute('name', $option->attribute->name);
                    $description[] = $option->attribute->name . ': ' . $param;
                }
            }
            $item->addChild('description', (strip_tags(implode('; ', $description))));
//            $item->addAttribute('description', $product->description);
//            $item->addAttribute('description', $product->description);
//            $item->addAttribute('description', $product->description);
        }

//        echo "<pre>"; print_r($products[0]); echo "</pre>"; die;
        echo $pXml->asXML();
        die;
        var_dump($pXml->asXML());
        die;
    }


    public function rozetkaProducts(Processor $priceManager)
    {
        Header('Content-type: text/xml;charset=UTF-8');
        $products = Product::with(['categories', 'brand', 'options.productOptions', 'options.attribute', 'rozetkaPhotos'])->where(['visible' => true, 'is_rozetka' => true])->get();
        $categories = DB::table('shop_category')->get();
        $pXml = '<?xml version="1.0" encoding="utf-8"?><!DOCTYPE yml_catalog SYSTEM "shops.dtd">
        <yml_catalog date="' . date('Y-m-d H:i') . '"><shop><name>rolcar</name><company>rolcar</company><url>rolcar</url><currencies><currency id="UAH" rate="1"/></currencies>
<categories>';
        foreach ($categories as $category) {
            $p = $category->parent_id ? 'parent_id = "' . $category->parent_id . '"' : '';
            $pXml .= '<category ' . $p . ' id="' . $category->id . '">' . $category->name . '</category>';
        }

        $pXml .= '</categories>';
        $pXml .= '<offers>';

        foreach ($products as $product) {
            $pXml .= '
                <offer id="' . $product->id . '" available="' . (($product->count > 0) ? 'true' : 'false') . '">
                <url>' . route('product.show', ['code' => $product->code]) . '</url>
                <price>' . $priceManager->calculate($product)->actualPrice() . '</price>
                <currencyId>UAH</currencyId>';
            if ($product->categories->count() > 0) {
                $pXml .= '<categoryId>' . $product->categories[count($product->categories) - 1]->id . '</categoryId>';
            }
            if ($product->rozetkaPhotos->count() > 0) {
                foreach ($product->rozetkaPhotos as $ph) {
                    $pXml .= '<picture>' . url($ph->path) . '</picture>';
                }

            }
            $name = 'Автолампы ' . $product->brand->name;
            if ($attr = $product->options->keyBy('shop_attribute_id')[self::TSOKOL_ATTR_ID] ?? false) {
                $name .= ' ' . $attr->productOptions->getAttributeValue('name');
            }
            if ($attr = $product->options->keyBy('shop_attribute_id')[self::MODEL_RYAD_ATTR_ID] ?? false) {
                $name .= ' ' . $attr->productOptions->getAttributeValue('name');
            }
            if ($attr = $product->options->keyBy('shop_attribute_id')[self::VOLTAG_RYAD_ATTR_ID] ?? false) {
                $name .= ' ' . $attr->productOptions->getAttributeValue('name') . 'V';
            }
            if ($attr = $product->options->keyBy('shop_attribute_id')[self::POWER_RYAD_ATTR_ID] ?? false) {
                $name .= ' ' . $attr->productOptions->getAttributeValue('name') . 'W';
            }
            $name .= ' (' . trim($product->sku) . ')';
            $html = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $product->description);
            $pXml .= '<vendor>' . $product->brand->name . '</vendor>
                <name>' . $name . '</name>
                <description><![CDATA[' . trim($html) . ']]></description>';
            $options = new Collection();

            foreach ($product->options as $option) {
                $options[$option->attribute->code] = $option;
            }
            $this->getRozetkaOption('Тип лампы', 'tsokol', $options, $pXml);
            $this->getRozetkaOption('Вид', 'vid_dlya_rozetka', $options, $pXml);
            $this->getRozetkaOption('Цоколь', 'tsokol_dlya_rozetka', $options, $pXml);
            $this->getRozetkaOption('Назначение', 'naznachenie_dlya_rozetka', $options, $pXml);
            $this->getRozetkaOption('Мощность', 'moschnost_dlya_rozetka', $options, $pXml);
            $this->getRozetkaOption('Напряжение', 'napryazhenie_dlya_rozetki', $options, $pXml);
            $this->getRozetkaOption('Цветовая температура', 'tsvetovaya_temperatura_dlya_rozetka', $options, $pXml);
            $this->getRozetkaOption('Цвет свечения', 'tsvet_svecheniya_dlya_rozetka', $options, $pXml);
            $this->getRozetkaOption('Количество ламп в упаковке', 'kolichestvo_lamp_v_upakovke', $options, $pXml);
            $this->getRozetkaOption('Гарантия', 'garantiya_dlya_rozetki', $options, $pXml);

            $pXml .= '</offer>';
        }
        $pXml .= '</offers></shop></yml_catalog>
        ';


        echo $pXml;
        die;
        var_dump($pXml->asXML());
        die;
    }

    public function merchantProducts()
    {
        Header('Content-type: text/xml;charset=UTF-8');

        $products = Product::with(['brand', 'options.productOptions', 'options.attribute', 'photos'])
            ->where(['is_merchant' => true])->get();

        $pXml = '<?xml version="1.0"?>
        <rss xmlns:g="http://base.google.com/ns/1.0" version="2.0">
            <channel>
                <title>rolcar | Интернет-сервис №1 в Мире</title>
                <link>rolcar</link>
                <description>Интернет-сервис №1 rolcar</description>
                ';
        $priceManager = new Processor();
        foreach ($products as $product) {

            $categoryPath = [];
            foreach ($product->categories as $category) {
                $categoryPath[] = $category->name;
            }

            if (isset($product->short_description)) {
                $description = $this->remEntities($product->short_description);

            } else {
                $description = '';
            }
            $pr = $priceManager->calculate($product);


            $pXml .= '<item>';
            $availability = $product->count > 0 ? '[in stock]' : '[out of stock]';
            $pXml .= '
                <g:id>' . $product->sku . '</g:id>
                <g:title>' . $product->name . '</g:title>
                <g:description>' . strip_tags($description) . '</g:description>
                <g:link>' . route('product.show', ['code' => $product->code]) . '</g:link>
                <g:ads_redirect>' . htmlentities(route('product.show', ['code' => $product->code, 'utm_medium' => 'cpc', 'utm_source' => 'google', 'utm_campaign' => 'shopping', 'utm_id'=>'{campaignid}', 'utm_productid' => '{product_id}', 'device' => '{device}'])) . '</g:ads_redirect>
                <g:google_product_category>3318</g:google_product_category>
                <g:condition>new</g:condition>
                <g:availability><![CDATA' . $availability . ']></g:availability>';
            if ($pr->originPrice() > $pr->actualPrice()) {
                $pXml .= '<g:price>' . $pr->originPrice() . ' UAH</g:price>';
                $pXml .= '<g:sale_price>' . $pr->actualPrice() . ' UAH</g:sale_price>';
                $pXml .= '<g:sale_price_effective_date>' . $product->discount->date_start->format('c') . '/' . $product->discount->date_end->format('c') . ' UAH</g:sale_price_effective_date>';
            } else {
                $pXml .= '<g:price>' . $pr->actualPrice() . ' UAH</g:price>';
            }
            $pXml .= '<g:brand>' . $product->brand->name . '</g:brand>
                ';
            if ($product->photos->count() > 0) {
                $ph = $product->photos[0];
                $pXml .= '<g:image_link>' . url($ph->path) . '</g:image_link>';
            }
            if ($product->photos->count() >= 1) {
                $length = ($product->photos->count()) > 10 ? 10 : ($product->photos->count());
                for ($i = 1; $i < $length; $i++) {
                    $ph = $product->photos[$i];
                    $pXml .= '<g:additional_image_link>' . url($ph->path) . '</g:additional_image_link>';
                }
            }
            $pXml .= '</item>';
        }
        $pXml .= '</channel></rss>
        ';


        echo $pXml;
        die;
    }

    private function getRozetkaOption(string $name, string $code, Collection $options, &$pXml)
    {
        if ($options->has($code)) {
            $pXml .= '<param name="' . $name . '">' . $options[$code]->productOptions->getAttributeValue('name') . '</param>';
        }
    }

    private function remEntities($str)
    {

        if (substr_count($str, '&') && substr_count($str, ';')) {
            // Find amper
            $amp_pos = strpos($str, '&');
            //Find the ;
            $semi_pos = strpos($str, ';');
            // Only if the ; is after the &
            if ($semi_pos > $amp_pos) {
                //is a HTML entity, try to remove
                $tmp = substr($str, 0, $amp_pos);
                $tmp = $tmp . substr($str, $semi_pos + 1, strlen($str));
                $str = $tmp;
                //Has another entity in it?
                if (substr_count($str, '&') && substr_count($str, ';'))
                    $str = remEntities($tmp);
            }
        }
        $str = str_replace('&', 'and', $str);
        return $str;
    }

}
