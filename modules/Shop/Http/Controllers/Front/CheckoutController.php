<?php namespace Modules\Shop\Http\Controllers\Front;


use Illuminate\Http\Request;
use Modules\Cms\System\Navigation\NavigationGenerator;
use Modules\Shop\Entities\Attribute;
use Modules\Shop\Entities\Product;
use Modules\Shop\Repositories\ProductRepository;
use Modules\Shop\Utils\Checkout\CheckoutCartManager;
use Modules\Shop\Utils\Checkout\Front\CheckoutCartFormBuilder;
use Modules\Shop\Utils\Checkout\OrderCalculatorFacade;
use Modules\Shop\Utils\GA;
use Pingpong\Modules\Routing\Controller;
use Pingpong\Themes\ThemeFacade as Theme;
use TheIconic\Tracking\GoogleAnalytics\Analytics;

/**
 * Class ExportController
 * @package Modules\Shop\Http\Controllers\Front
 */
class CheckoutController extends Controller
{
    /**
     * @var CheckoutCartManager
     */
    private $cartManager;

    /**
     * ExportController constructor.
     */
    public function __construct()
    {
        $navigationGenerator = new NavigationGenerator();
        $this->cartManager = new CheckoutCartManager();
        $navigationGenerator->generate();
    }

    /**
     * @param $id
     * @param Request $request
     * @param ProductRepository $productRepository
     * @return \Illuminate\Http\RedirectResponse
     */
    public function addProductToCart($id, Request $request, ProductRepository $productRepository)
    {
        $ga = new GA();
        $product = Product::findOrFail($id);
        $ga->addToCartProduct($product);
        $productOptions = $productRepository->getProductOptions($id);
        $validRules = CheckoutCartFormBuilder::getValidationRules($productOptions);
        $this->validate($request, $validRules);
        $selectedOptions = $request->get('options') ? $request->get('options') : [];
        $this->cartManager->addProductToCart($id, $selectedOptions);
        $request->getSession()->flash('show_cart', 'Success');
        return redirect()->back();
    }


    /**
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $cartsCollection = $groupedCartProducts = $activeAttrs = $attributes = $countable = $selectedOptions = [];

        $orderCalcFacade = new OrderCalculatorFacade();
        $orderCalculator = $orderCalcFacade->getOrderCalculation();
        $cartsCollection = $orderCalcFacade->getCartCollection();
        $selectedOptions = $orderCalcFacade->getSelectedOptions();

        if ($activeAttrs = $orderCalcFacade->getActiveAttrs()) {
            $uniqueAttr = [];
            foreach ($activeAttrs as $atr) {
                foreach ($atr as $item) {
                    $uniqueAttr[$item] = $item;
                }
            }
            $attributes = Attribute::whereIn('code', array_values($uniqueAttr))->get()->keyBy('code');

        }
        $breadCrumbs = ['Главная' => url('/'), 'Оформление заказа' => ''];

        return Theme::view('shop.checkout.cart', compact('orderCalculator', 'orderCalcFacade', 'cartsCollection', 'groupedCartProducts', 'attributes', 'countable', 'breadCrumbs', 'selectedOptions'));
    }


    /**
     * @param Request $request
     * @return mixed
     */
    public function cartModalBody(Request $request)
    {
        $groupedCartProducts = $activeAttrs = $attributes = $countable = $selectedOptions = [];

        $orderCalcFacade = new OrderCalculatorFacade();
        $orderCalculator = $orderCalcFacade->getOrderCalculation();
        $cartsCollection = $orderCalcFacade->getCartCollection();
        $selectedOptions = $orderCalcFacade->getSelectedOptions();

        if ($activeAttrs = $orderCalcFacade->getActiveAttrs()) {
            $uniqueAttr = [];
            foreach ($activeAttrs as $atr) {
                foreach ($atr as $item) {
                    $uniqueAttr[$item] = $item;
                }
            }
            $attributes = Attribute::whereIn('code', array_values($uniqueAttr))->get()->keyBy('code');

        }
        $breadCrumbs = ['Главная' => url('/'), 'Оформление заказа' => ''];

        return Theme::view('shop.checkout.modal_body', compact('orderCalculator', 'cartsCollection', 'groupedCartProducts', 'attributes', 'countable', 'breadCrumbs', 'selectedOptions'));
    }


    /**
     * @param $uniqueKey
     * @param $type
     * @param $count
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function changeProductCount($uniqueKey, $type, $count, Request $request)
    {
        if ($carts = $request->session()->get('shop.cart')) {
            $this->cartManager->changeProductCartCount($uniqueKey, $type, $count);
        }


        $orderCalcFacade = new OrderCalculatorFacade();
        $cartsCollection = $orderCalcFacade->getCartCollection();

        $orderCalculator = $orderCalcFacade->getOrderCalculation();
        $totalSum = $orderCalculator->getTotalSum();

        return [
            'status' => 'success',
            'data' => [],
            'cartItem' => [$cartsCollection[$uniqueKey]['count'],
                $cartsCollection[$uniqueKey]['uniqueCode'],
                $cartsCollection[$uniqueKey]['product']->price
            ],
            'totalSum' => $totalSum
        ];

    }

    /**
     * @param $uniqueKey
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteProductFromCart($uniqueKey, Request $request)
    {
        if ($carts = $request->session()->get('shop.cart')) {
            $this->cartManager->deleteProductFromCart($uniqueKey);
        }
        return redirect()->route('shop.checkout.list');
    }
}
