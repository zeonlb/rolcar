<?php namespace Modules\Shop\Http\Controllers\Front;


use App\Jobs\SendOrdersRomax;
use Carbon\Carbon;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Modules\Cms\Entities\Meta;
use Modules\Cms\Entities\Page;
use Modules\Shop\Entities\Attribute;
use Modules\Shop\Entities\Order;
use Modules\Shop\Entities\Payment;
use Modules\Shop\Entities\Product;
use Modules\Shop\Http\Requests\OrderCreateRequest;
use Modules\Shop\Services\LiqPayService;
use Modules\Shop\Services\Private24Service;
use Modules\Shop\Utils\GA;
use Modules\Shop\Utils\Price\Processor;
use Pingpong\Modules\Routing\Controller;
use Pingpong\Themes\ThemeFacade;
use Pingpong\Themes\ThemeFacade as Theme;
use Sendpulse\RestApi\ApiClient;
use stdClass;

class OrderController extends Controller
{
    /**
     * @param OrderCreateRequest $request
     * @return $this|RedirectResponse
     */
    public function create(OrderCreateRequest $request)
    {
        try {
            $ga = new GA();
            if ($carts = $request->session()->get('shop.cart')) {
                $totalPrice = 0;
                $postData = $request->all();
                $postData['payment_type'] = Payment::PAYMENT_NAME_TYPE_MAP[$request->get('payment_type')] ?? Payment::PAYMENT_TYPE_NALICHNIE_TYPE;
                $delivery = new stdClass();
                switch ($postData['delivery']) {
                    case 's_np':
                        $delivery->type = 'novaya_pochta';
                        $delivery->city = $postData["s_np_city"];
                        $delivery->warehouse = $postData["s_np_warehouse"];
                        break;
                    case 'k_np':
                        $delivery->type = 'nova_poshta_address';
                        $delivery->city = $postData["k_np_city"];
                        $delivery->adress = $postData["k_np_adress"];
                        break;
                    default:
                        $delivery->type = 'samovivoz';
                        $delivery->city = $postData["s_shop_city"];
                        break;
                }
                $postData['delivery'] = json_encode($delivery);
                $postData['invoice'] = substr(strtoupper(uniqid()), 6);
                $postData['status'] = 'new';
                $order = Order::create($postData);
                $skus = [];
                foreach ($carts as $cart) {
                    $product = Product::with('brand')->findOrFail($cart['product_id']);
                    $selected = '';
                    if (!$cart['selected_options']) $cart['selected_options'] = [];
                    foreach ($cart['selected_options'] as $code => $value) {
                        $attribute = Attribute::where('code', $code)->first();
                        $selected .= $attribute->name . ': ' . $value . ' ';
                    }
                    $actualPrice = (new Processor())->calculate($product)->actualPrice();
                    $totalPrice += $actualPrice * $cart['count'];
                    $productsArray[] =
                        [
                            'shop_product_id' => $product->id,
                            'shop_order_id' => $order->id,
                            'price' => $actualPrice,
                            'count' => $cart['count'],
                            'selected' => $selected
                        ];
                    $p = new stdClass();

                    $productSendArray[] = (object)['product_id' => $product->romax_id, 'price' => $actualPrice * $cart['count'], 'count' => $cart['count']];

                    $gaProducts[] = [
                        'name' => $product->name,
                        'id' => $product->sku,
                        'price' => $actualPrice,
                        'brand' => $product->brand->name,
                        'quantity' => $cart['count'],
                    ];
                    $skus[] = $product->sku;
                    //add ordered
                    $product->count_ordered = $product->count_ordered + $cart['count'];
                    $product->save();
                }
                $order->total_price = $totalPrice;
                $order->save();
                $payment = null;
                if (in_array($request->get('payment'), [
                    Payment::PAYMENT_TYPE_PRIVAT_24_NAME,
                    Payment::PAYMENT_TYPE_VISA_MASTER_NAME,
                    Payment::PAYMENT_TYPE_INSTANT_INSTALLMENTS_NAME,
                    Payment::PAYMENT_TYPE_PART_INSTALLMENTS_NAME,

                ])) {
                    $payment = Payment::create([
                        'order_id' => $order->id,
                        'code' => md5(uniqid()),
                        'access_code' => microtime(true) . '_' . md5(uniqid()),
                        'payment_type' => Payment::PAYMENT_NAME_TYPE_MAP[$request->get('payment')],
                        'status' => Payment::STATUS_NEW,
                        'status_code' => 'no_status'
                    ]);
                }
                #$ga->addTransaction($productsArray,$order);
                DB::table('shop_products_orders')->insert(
                    $productsArray
                );
                //Payment
                if ($payment) {
                    if (in_array($payment->payment_type, [Payment::PAYMENT_TYPE_VISA_MASTER_TYPE, Payment::PAYMENT_TYPE_PRIVAT_24_TYPE])) {
                        $message = 'Вы успешно оформили заказ! Осталось только оплатить ваш заказ.';
                        $this->sendPuchaseMail($request, $postData);
                        $job = $this->dispatch(new SendOrdersRomax($order, $productSendArray));
                        $request->session()->remove('shop.cart');
                        return redirect()->route('shop.order.payment.liqpay', ['code' => $payment->access_code])
                            ->with('success', $message);
                    } elseif (in_array($payment->payment_type, [Payment::PAYMENT_TYPE_PART_INSTALLMENTS, Payment::PAYMENT_TYPE_INSTANT_INSTALLMENTS])) {
                        /** @var Private24Service $privateManager */
                        try {
                            $privateManager = app('private24');
                            $response = $privateManager->create($order, $request->get('payment_part_count', 2));
                            $payment->message = json_encode($response);
                            $payment->save();
                            $job = $this->dispatch(new SendOrdersRomax($order, $productSendArray));
                            $request->session()->remove('shop.cart');
                            return redirect()->away('https://payparts2.privatbank.ua/ipp/v2/payment?token=' . $response['token']);
                        } catch (Exception $e) {
                            throw $e;
                        }
                    }
                }
                event('shop.order.created', $order);

                $job = $this->dispatch(new SendOrdersRomax($order, $productSendArray));

                $request->session()->remove('shop.cart');
                $message = 'Ожидайте в ближайшее время звонка от оператора для подтверждения информации! Наш оператор обязательно перезвонит Вам для уточнения всех деталей!';
                if ($postData['payment'] == 'privat') {
                    $message = $message . '<p>Пока Вы ожидаете, Вы можете оплатить Ваш заказ на карточку ПриватБанка</p>';
                }
                if ($postData['payment'] == 'beznal') {
                    $message = $message . '<p>Безналичный способ оплаты подразумевает оплату на расчетный счет нашей компании. После связи с оператором Вам будет выставлен счет на оплату со всеми необходимыми документами.</p>';
                }

                try {
                    $api = app('esputnik');
                    $api->startEventAutomation360('rolcar_new_order', [
                        "phone" => $postData['phone'],
                        "email" => $postData['email'],
                        "name" => $postData['first_name'],
                        "payment" => $postData['payment'],
                        'order_invoice' => $order->invoice
                    ]);
                } catch (Exception $e) {

                }

                return Theme::view('shop.checkout.thank_you_page')
                    ->with('message', $message)
                    ->with('products', $gaProducts)
                    ->with('skus', $skus)
                    ->with('order', $order);
            }
        } catch (Exception $exception) {
            throw $exception;
            return redirect()->back()->withErrors('Ошибка при оформлении заказа.');
        }


    }

    public function liqPayPayment(string $code)
    {
        $payment = Payment::where([
            'access_code' => $code,
            'status' => Payment::STATUS_NEW
        ])->firstOrFail();
        $public_key = env('LP_PUBLIC_KEY');
        $private_key = env('LP_PRIVATE_KEY');
        $successUrl = route('shop.order.payment.payment_info', ['code' => $payment->access_code]);

        $liqpay = new LiqPayService($public_key, $private_key);
        $params = [
            'version' => 3,
            'action' => 'pay',
            'amount' => number_format($payment->order->total_price, 2, '.', ''),
            'currency' => 'UAH',
            'sender_first_name' => $payment->order->first_name,
            'info' => 'External information for payments #' . $payment->order->invoice,
            'description' => 'Оплата заказа № ' . $payment->order->invoice . '',
            'order_id' => 'Заказ №' . $payment->order->invoice,
            'result_url' => $successUrl,
            'server_url' => '/shop/order/liqpay-status',
            'language' => 'ru',
            'paytypes' => Payment::PAYMENT_TYPE_VISA_MASTER_TYPE === $payment->payment_type ? 'card' : 'privat24'
        ];
        $form = $liqpay->cnbForm($params);

        $pageInfo = new Page();
        $pageInfo->meta = new Meta([
                'meta_title' => $params['description'],
            ]
        );


        return Theme::view('shop.order.payment', compact('pageInfo',
                'form',
                'payment'
            )
        );
    }


    public function privat24Payment(string $code, Request $request)
    {
        file_put_contents('/tmp/privat_resp.json', json_encode(request()->all()), FILE_APPEND);
        $payment = Payment::where([
            'code' => $code,
            'status' => Payment::STATUS_NEW
        ])->firstOrFail();
        $payment->message = json_encode($request->all());
        if ('SUCCESS' === $request->get('paymentState')) {
            $payment->status = Payment::STATUS_SUCCESS;
            $payment->status_code = 'success';
        } else {
            $payment->status = Payment::STATUS_FAILED;
            $payment->status_code = 'failure';
        }
        $payment->save();

        $client = new Client();
        $respData = $client->request('PUT', env('MAX_SERVER') . '/api/v2/shop/orders/' . $payment->order->resource_id, [
            'exceptions' => false,
            'headers' => [
                'api-token' => env('MAX_TOKEN')
            ],
            'form_params' => [
                'payment_status' => $payment->status,
            ]
        ]);
        $data = ['module' => 'payment', 'action' => 'update order', 'params' => ['request' => [
            'payment_status' => $payment->status,
        ], 'response' => $respData->getBody()->getContents()]];
        Log::info("updating order", $data);
    }

    public function paymentInfo(string $code)
    {
        $payment = Payment::where([
            'access_code' => $code,
        ])->firstOrFail();
        $deliveryAddress = ['г. ' . $payment->order->city];
        foreach (is_array($payment->order->delivery_info) ? $payment->order->delivery_info : [] as $index => $val) {
            $deliveryAddress[] = $val['DescriptionRu'] ?? $val;
        }
        $breadCrumbs = [
            'Главная' => url('/'),
            'Завершение покупок' => ''
        ];
        $pageInfo = new Page();
        $pageInfo->meta = new Meta(
            [
                'meta_title' => 'Оформление заказа на сайте компании.',
            ]
        );
        $deliveryAddress = implode(' ', $deliveryAddress);
        /** @var $payment Payment */
        if (Payment::STATUS_SUCCESS === $payment->status) {
            $message = 'Оплата нами получена. Ожидайте в течении ближайшего времени звонка от оператора.';
        } else {
            $message = 'К сожалению, мы не получили оплату. Возможно произошел какой-то технический сбой. Но это не страшно. В ближайшее время с Вами свяжется наш менеджер и мы решим Вашу проблему. Пожалуйста, ожидайте!';
        }
        return ThemeFacade::view('shop.payment.info', compact(
                'breadCrumbs',
                'pageInfo',
                'deliveryAddress',
                'payment',
                'message'
            )
        );
    }

    public function liqpayStatus(Request $request)
    {
        file_put_contents(storage_path('payment.log'), json_encode($request->all()), FILE_APPEND);
        $data = $request->get('data');
        $private_key = env('LP_PRIVATE_KEY');

        $signature = base64_encode(sha1(
            $private_key .
            $data .
            $private_key
            , 1));
        if ($request->get('signature') !== $signature) {
            file_put_contents(storage_path('payment_err.log'), json_encode($request->all()), FILE_APPEND);
            return ['error'];
        }

        $lpData = json_decode(base64_decode($data));
        $orderId = trim(strtr($lpData->order_id, ['Заказ №' => '']));
        $order = Order::where('invoice', $orderId)->with('paymentRel')->firstOrFail();
        $payment = $order->paymentRel;
        $payment->status_code = $lpData->status;
        $payment->message = json_encode($lpData);
        if ('success' === $lpData->status) {
            $payment->status = Payment::STATUS_SUCCESS;
        } else {
            $payment->status = Payment::STATUS_FAILED;
        }
        $payment->save();
        $client = new Client();
        $respData = $client->request('PUT', env('MAX_SERVER') . '/api/v2/shop/orders/' . $payment->order->resource_id, [
            'exceptions' => false,
            'headers' => [
                'api-token' => env('MAX_TOKEN')
            ],
            'form_params' => [
                'payment_status' => $payment->status,
            ]
        ]);
        $data = ['module' => 'payment', 'action' => 'update order', 'params' => ['request' => [
            'payment_status' => $payment->status,
        ], 'response' => $respData->getBody()->getContents()]];
        Log::info("updating order", $data);
        return ['ok'];
    }

    public function oneClick(Request $request)
    {
        $postData = $request->all();
        $product = Product::with('brand')->findOrFail($request->get('product_id'));
        $actualPrice = (new Processor())->calculate($product)->actualPrice();
        $productSendArray[] = (object)['product_id' => $postData['romax_id'], 'count' => 1, 'price' => $actualPrice];

        $order = new stdClass;
        $order->phone = $postData['phone'];

        $job = $this->dispatch(new SendOrdersRomax($order, $productSendArray));

        try {
            $api = app('esputnik');
            $api->startEventAutomation360('rolcar_one_click', [
                "phone" => $postData['phone']
            ]);
        } catch (Exception $e) {

        }

        $message = 'Благодарим Вас за вашу заявку. Ожидайте, в течении ближайшего времени с вами свяжется менеджер.';

        return redirect()->back()
            ->with('success', $message)
            ->with('one_click_ga', '');
    }

    private function addScriptGA(Product $product, $type = 'add', $quantity = 1)
    {
        $price = (new Processor())->calculate($product)->actualPrice();
        $script = "
            ga('ec:addProduct', {
            'id': '{$product->sku}',
            'name': '{$product->name}',
            'brand': '{$product->brand->name}',
            'price': '{$price}',
            'quantity': '{$quantity}'
          });
          ga('ec:setAction', '{$type}');
          ga('send', 'event', 'UX', 'click', '{$type} to cart');
        ";
        Session::put('ga_script', $script);
    }

    /**
     * @param OrderCreateRequest $request
     * @param $postData
     * @return Exception
     */
    private function sendPuchaseMail(OrderCreateRequest $request, $postData)
    {
        try {
            /** @var ApiClient $api */
            $api = app('esputnik');
            $api->startEventAutomation360('purchase', [
                "email" => $request->get('email'),
                "phone" => $request->get('phone'),
                "first_name" => $request->get('first_name'),
                "order_number" => $postData['invoice'],
                "order_date" => Carbon::now()->format('dd.mm.yy H:i:s'),
            ]);
        } catch (Exception $e) {
        }
    }
}
