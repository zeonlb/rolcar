<?php namespace Modules\Shop\Http\Controllers\Front;

use Illuminate\Http\RedirectResponse;
use Modules\Cms\Entities\File;
use Modules\Shop\Entities\Product;
use Modules\Shop\Entities\ProductComment;
use Modules\Shop\Http\Requests\ProductCommentRequest;
use Pingpong\Modules\Routing\Controller;
use Pingpong\Themes\ThemeFacade as Theme;


class CommentController extends Controller
{
    /**
     * @param $id
     * @param ProductCommentRequest $commentRequest
     * @return RedirectResponse
     */
    public function create($id, ProductCommentRequest $commentRequest)
    {
        $product = Product::findOrFail($id);
        $comment = new ProductComment();
        $comment->fill($commentRequest->get('comment'));
        if ($comment->video_link) {
            preg_match('/(youtu\.be\/|youtube\.com\/(watch\?(.*&)?v=|(embed|v)\/))([^\?&"\'>]+)/', $comment->video_link, $out);
            if (!$out[5]??false) {
                return redirect()->back()->withErrors('Не правильная ссылка на youtube видео.');
            }
            $comment->video_link = 'https://www.youtube.com/watch?v='.$out[5];
        }
        $product->comments()->save($comment);


        if ($fileIds = $commentRequest->get('files')) {
            $files = File::whereIn('id', $fileIds)->get();
            if ($files) {
                $filesArray = [];
                foreach ($files as $file) {
                    $filesArray[] = $file;
                }
                $comment->photos()->saveMany($filesArray);
            }
        }

        return redirect()->back()->with('success', 'Спасибо за ваш коментарий к товару "' . $product->name . '".');
    }

    /**
     * @param int $offset
     * @return mixed
     */
    public function index($offset = 0)
    {
        $limit = 3;
        $comments = ProductComment::where('processed', 1)
            ->groupBy('shop_product_id')
            ->with('product')
            ->offset($offset)
            ->take($limit)
            ->orderBy('id', 'desc')->get();

        $totalComments = ProductComment::where('processed', 1)->count();
        $offsetComments = $offset + $limit;

        return Theme::view('shop.partials.all_comment', compact('comments', 'totalComments', 'offsetComments'));
    }

    static function topComments()
    {
        $comments = ProductComment::where('parent_id', null)->orderBy('created_at', 'desc')->take(10)->get();
        $commentsArray = [];

        foreach ($comments as $comment) {
            $commentProduct = $comment->product;

            if ($commentProduct->visible == 1) {
                $commentsArray[] = [
                    'comment' => $comment,
                    'product' => $commentProduct,
                    'thumb' => $commentProduct->getThumb()
                ];
            }
        }

        $commentsArray = array_slice($commentsArray, 0, 5, TRUE);

        return Theme::view('shop.partials.top_comments', compact('commentsArray'));
    }

}
