<?php namespace Modules\Shop\Http\Controllers\Front;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Pagination\AbstractPaginator;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Input;
use Irazasyed\LaravelGAMP\Facades\GAMP;
use Modules\Blog\Entities\Article;
use Modules\Car\Core\BreadCrumbGenerator;
use Modules\Car\Entities\Motor;
use Modules\Car\Http\Requests\ProductListRequest;
use Modules\Cms\Entities\Page;
use Modules\Cms\System\MetaGenerator;
use Modules\Cms\System\Navigation\NavigationGenerator;
use Modules\Shop\Entities\Banner;
use Modules\Shop\Entities\Category;
use Modules\Shop\Entities\Product;
use Modules\Shop\Entities\ProductComment;
use Modules\Shop\Entities\ProductIcon;
use Modules\Shop\Mangers\AttributeManager;
use Modules\Shop\Mangers\ProductManager;
use Modules\Shop\Repositories\ProductRepository;
use Modules\Shop\Utils\Category\CategoryManagerFactory;
use Modules\Shop\Utils\Category\Manager\CarCategoryManager;
use Modules\Shop\Utils\Checkout\Front\CheckoutCartFormBuilder;
use Pingpong\Modules\Routing\Controller;
use Pingpong\Themes\ThemeFacade as Theme;

class ProductController extends Controller
{
    protected $productRepo;
    protected $attributeManager;

    const COOKIE_PR_SEEN = 'pr_seen';

    public function __construct(ProductRepository $productRepository, AttributeManager $attributeManager)
    {
        $navigationGenerator = new NavigationGenerator();
        $navigationGenerator->generate();
        $this->productRepo = $productRepository;
        $this->attributeManager = $attributeManager;
    }


    public function index(ProductListRequest $request,
                          $category,
                          $typeLamp = null,
                          $tsokol = null,
                          $voltage = null)
    {
        $pageInfo = new Page();
        $categoryManager = CategoryManagerFactory::getManager($category);
        $filterData = $this->attributeManager->parseFilterString($request->get('filter'));
        if ($this->isItFirstPage($request)) {
            return redirect()->route('product.index', compact('category', 'typeLamp', 'tsokol', 'voltage'), 301);
        }


        $request->session()->set('shop.last_visited', compact('category', 'typeLamp', 'tsokol', 'voltage'));
        $cat = $category;
        if ('main' === $category) {
            return redirect()->route('product.index', compact('typeLamp', 'tsokol', 'voltage'), 301);
        }
        if ($categoryManager instanceof CarCategoryManager) {
            $cat = $typeLamp;
            $motor = $categoryManager->getMotor();
            if ($motor->isChild()) {
                return redirect()->route('product.index', [
                    'category' => strtr($category, ['-' . $motor->id => '-' . $motor->parent_id]),
                    'naznachenie' => $typeLamp,
                    'tsokol' => $tsokol,
                    'voltage' => $voltage,
                ])->setStatusCode(301);
            }
        }
        // Generate Meta data
        $pageInfo = $categoryManager->getPageInfo($pageInfo, $cat);
        $productsObj = $this->productRepo->getActive($filterData, $cat, $categoryManager,
            $request->get('sortBy'), $request->get('dest'), $tsokol, $voltage);
        // Get Min and Max prices
        list($minPrice, $maxPrice) = ProductManager::getMinMaxProductsPrices($productsObj->get());
        $actualMin = $filterData['pf'][0] ?? $minPrice;
        $actualMax = $filterData['pt'][0] ?? $maxPrice;
        $banners = Banner::all();
        $productsObj = $this->productRepo->getActive($filterData, $cat, $categoryManager,
            $request->get('sortBy'), $request->get('dest'), $tsokol, $voltage, false);


        $seenProductData = Cookie::get(self::COOKIE_PR_SEEN, false);
        $seenProducts = [];
        if ($seenProductData) {
            $ids = explode(',', $seenProductData);
            if (is_array($ids) && $ids) {
                $seenProducts = $this->productRepo->getProductsByIds(array_slice($ids, 0, 20));
            }

        }
        $withOutFilters = $productsObj->get();
        $products = $productsObj->paginate(24);
        $categoryId = $categoryManager->getId();
        $filters = $this->attributeManager->getFilteredAttributes($withOutFilters);
        $filterObjects = $this->attributeManager->getFilterObject($filterData);
        $brands = $this->attributeManager->getActiveBrands();
        $aOptions = $this->addEnabledFilterOptions($withOutFilters);
        $attributes = [
            'category' => $category,
            'naznachenie' => $typeLamp
        ];

        // Add breadcrumbs
        $attributes = $this->addBreadCrumbs($typeLamp, $tsokol, $voltage, $categoryManager, $attributes);
        $routeUrl = route('product.index', $attributes);
        if ($this->requestHasAnyFilters($request) || $request->get('page')) {
            $pageInfo->meta->meta_robot_index = false;
        }

        $icons = ProductIcon::all();

        /** @var AbstractPaginator $products */
        $products = $products->appends(Input::except('page'));
        return Theme::view('shop.product.list',
            compact('pageInfo', 'products', 'brands', 'filters', 'routeUrl', 'filterData', 'filterObjects', 'categoryManager', 'icons',
                'typeLamp', 'categoryId', 'aOptions', 'minPrice', 'maxPrice', 'actualMax', 'actualMin', 'seenProducts', 'banners'));
    }

    public function main(Request $request)
    {
        $pageInfo = new Page();
        $categoryManager = CategoryManagerFactory::getManager();
        $categories = Category::where(['visible' => true, 'show_in_main' => true])->orderBy('name', 'asc')->get()->keyBy('id')->toArray();
        $groupedCategories = groupCategories($categories);
        $topCategories = Category::where(['visible' => true, 'show_in_top' => true])->orderBy('sort')->get();
        $banners = Banner::all();
        $topProducts = $this->productRepo->getTopProducts();
        $icons = ProductIcon::all();
        $seenProductData = Cookie::get(self::COOKIE_PR_SEEN, false);
        $comments = ProductComment::where('processed', 1)
            ->groupBy('shop_product_id')
            ->with('product')
            ->orderBy('id', 'desc')->offset(0)->take(3)->get();
        $totalComments = ProductComment::where('processed', 1)->count();
        $offsetComments = 3;
        $seenProducts = [];
        if ($seenProductData) {
            $ids = explode(',', $seenProductData);
            if (is_array($ids) && $ids) {
                $seenProducts = $this->productRepo->getProductsByIds(array_slice($ids, 0, 20));
            }

        }

        $articles = Article::where('is_enabled', 1)->orderBy('created_at', 'desc')->take(4)->get();
        // Generate Meta data
        $pageInfo = $categoryManager->getPageInfo($pageInfo, 'fb2');
        return Theme::view('shop.product.main',
            compact('pageInfo', 'articles', 'banners', 'icons', 'offsetComments', 'totalComments', 'categoryManager', 'groupedCategories', 'topCategories', 'topProducts', 'seenProducts', 'comments'));
    }

    public function show($code, Request $request, ProductRepository $productRepository, Response $response)
    {
        $product = $productRepository->getProductByCode($code, true);
        if (!$product) {
            abort(404);
        }
        $product->with('comment.answers');
        if ($product->meta && strlen($product->meta->meta_redirect_url) > 0) {
            return redirect($product->meta->meta_redirect_url, 301);
        }
        $this->setSeenProductToCookie($product);

        $breadCrumbs = [];
        $pageInfo = new Page();
        $pageInfo->meta = $product->meta;
        $productOptions = $productRepository->getProductOptions($product->id);
        $seenProductData = Cookie::get(self::COOKIE_PR_SEEN, false);
        $seenProducts = [];
        if ($seenProductData) {
            $ids = explode(',', $seenProductData);
            if (is_array($ids) && $ids) {
                if ($key = array_search($product->id, $ids)) {
                    unset($ids[$key]);
                }
                $seenProducts = $this->productRepo->getProductsByIds(array_slice($ids, 0, 20));
            }

        }

        $hasBreadCrumbs = false;
        $options = [
            'brand' => $product->brand->name
        ];
        foreach ($product->options as $option) {
            if ($option->productOptions) {
                $options[$option->attribute->code] = $option->productOptions->getAttributeValue('name');
            }

        }
        $seoOptionsData = $options;
        $seoOptionsData['product'] = $product->name;
        if ($request->get('ck')) {
            $motor = Motor::where('id', $request->get('ck'))->with([
                'meta',
                'modification.model.mark',
                'modification.model.transport'
            ])->first();
            if ($motor) {
                $seoOptionsData = array_merge($seoOptionsData, [
                    'transport' => $motor->modification->model->transport->name,
                    'mark' => $motor->modification->model->mark->name,
                    'model' => $motor->modification->model->name,
                    'modification' => $motor->modification->name,
                    'motor' => $motor->motor,
                    'product' => $product->name
                ]);


                $hasBreadCrumbs = true;
                $breadCrumbs = BreadCrumbGenerator::make([$motor]);
                if ($filterType = $request->get('lt')) {
                    $breadCrumbs[trans('car::lamps.' . $filterType)] = route('product.index',
                        [
                            'category' => $category = $motor->modification->model->mark->code . '-' . $motor->modification->code . '-' . $motor->id,
                            'naznachenie' => $filterType
                        ]);
                    $seoOptionsData['light'] = trans('car::lamps.' . $filterType);
                }
            } else {
                $seoOptionsData = array_merge($seoOptionsData, [
                    'transport' => '',
                    'mark' => '',
                    'model' => '',
                    'modification' => '',
                    'motor' => '',
                ]);
            }
        }

        if (!$hasBreadCrumbs) {
            $breadCrumbs['Интернет-магазин автоламп'] = url('/');
            $categories = $product->categories->keyBy('id')->sortBy('parent_id')->toArray();
            $categories = $this->groupCategories($categories);

            if ($categories) {
                $breadCrumbs[$categories['name']] = route('product.index', ['category' => $categories['code']]);
                $children = $categories['child']??[];
                if (1 === count($children)) {
                    $breadCrumbs = $this->addCategoryBreadCrumbs($children, $breadCrumbs);
                } else {
                    $cat = current($children);
                    $breadCrumbs[$cat['name']] = route('product.index', ['category' => $cat['code']]);
                }

            }
        }
        $breadCrumbs[$product->name] = '';
        $pageInfo->meta = MetaGenerator::make(
            $product->meta,
            'product_details',
            $seoOptionsData

        );
        if ($_GET) {
            $pageInfo->meta->meta_robot_index = false;
        }
        $form = CheckoutCartFormBuilder::make($productOptions, $product->id);
        return Theme::view('shop.product.show',
            compact('pageInfo', 'product', 'breadCrumbs', 'productOptions', 'form', 'seenProducts'));
    }


    private function groupCategories($categories)
    {
        $res = [];
        foreach ($categories as $key => $category) {
            $category = $this->findChildCategories($category, $categories);
            if (array_key_exists('child', $category)) {
                $res[$category['id']] = $category;
            }
        }

        return array_shift($res);
    }


    private function findChildCategories($parent, $categories)
    {
        foreach ($categories as $category) {
            if ($category['parent_id'] == $parent['id']) {
                $category['child'] = $this->findChildCategories($category, $categories);
                $parent['child'][] = $category;
            }
        }
        return $parent;
    }


    private function addCategoryBreadCrumbs($categories, $breadCrumbs)
    {
        foreach ($categories as $child) {
            $children = $child['child']??[];
            if (!array_key_exists(1, $child)) {
                $breadCrumbs[$child['name']] = route('product.index', ['category' => $child['code']]);
                if (array_key_exists('child', $children)) {
                    if (count($children['child']) == 1) {
                        $breadCrumbs = $this->addCategoryBreadCrumbs($children['child'], $breadCrumbs);
                    }
                }
            }
        }
        return $breadCrumbs;
    }

    public function showLendingAjax ($code, ProductRepository $productRepository)
    {
        $product = $productRepository->getProductByCode($code, true);

        $output = '';

        if ( $product->lending_html )
        {
            $output .=  '<div class="lending col-md-12">' . $product->lending_html . '</div>';
        }

        return $output;
    }
    /**
     * @param ProductListRequest $request
     * @return bool
     */
    private function requestHasAnyFilters(ProductListRequest $request): bool
    {
        return $request->get('filter') || $request->get('sortBy') || $request->get('dest');
    }

    /**
     * @param $typeLamp
     * @param $tsokol
     * @param $voltage
     * @param $categoryManager
     * @param $attributes
     * @return mixed
     */
    private function addBreadCrumbs($typeLamp, $tsokol, $voltage, $categoryManager, $attributes)
    {
        if (isset($categoryManager->category)) {
            $cat = $categoryManager->category;
            $bc = [];
            while (true) {
                $parent = $cat->getParent();
                if (!$parent) break;
                if ($parent->code === 'main') {
                    array_unshift($bc, [$parent->name, route('product.main')]);
                } else {
                    array_unshift($bc, [$parent->name, route('product.index', $parent->code)]);
                }
                $cat = $parent;
            }
            foreach ($bc as list($n, $u)) {
                $categoryManager->addBreadCrumb($n, $u);
            }
            $categoryManager->addBreadCrumb($categoryManager->category->name, '');
        }
        if ($typeLamp) {
            $categoryManager->addBreadCrumb(trans('car::lamps.' . $typeLamp), route('product.index', $attributes));
        }
        if ($tsokol) {
            $attributes['tsokol'] = $tsokol;
            $categoryManager->addBreadCrumb($tsokol, route('product.index', $attributes));
        }
        if ($voltage) {
            $attributes['voltage'] = $voltage;
            $categoryManager->addBreadCrumb($voltage, route('product.index', $attributes));
            return $attributes;
        }
        return $attributes;
    }

    /**
     * @param $withOutFilters
     * @return array
     */
    private function addEnabledFilterOptions($withOutFilters): array
    {
        $aOptions = [];
        foreach ($withOutFilters as $pr) {
            foreach ($pr->options as $option) {
                if ($option->shop_attribute_id !== null) {
                    $aOptions[$option->shop_attribute_id][] = $option->shop_product_option_id;
                }
            }
        }
        return $aOptions;
    }

    /**
     * @param ProductListRequest $request
     * @return bool
     */
    private function isItFirstPage(ProductListRequest $request): bool
    {
        return 1 == $request->get('page');
    }


    private function setSeenProductToCookie(Product $product)
    {
        $seenProductId = $product->id;
        if ($seen = Cookie::get(self::COOKIE_PR_SEEN)) {
            $seenArr = explode(',', $seen);
            if (!in_array($seenProductId, $seenArr)) {
                //delete lat
                if (count($seenArr) > 20) {
                    unset($seenArr[count($seenArr) - 1]);
                }
                $seenProductId = $seenProductId . ',' . implode(',', $seenArr);
            } else {
                $seenProductId = $seen;
            }
        }
        Cookie::queue(self::COOKIE_PR_SEEN, $seenProductId, 30);
    }
}
