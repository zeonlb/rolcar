<?php namespace Modules\Shop\Http\Controllers\Front;

use App\Api\ListResource;
use App\Api\Request\ListActionRequest;
use Modules\Cms\Core\Images\ThumbBuilder;
use Modules\Cms\Entities\Meta;
use Modules\Cms\Entities\Page;
use Modules\Cms\Http\Requests\SkuSearchRequest;
use Modules\Cms\System\Navigation\NavigationGenerator;
use Modules\Shop\Entities\Brand;
use Modules\Shop\Entities\Category;
use Modules\Shop\Entities\Product;
use Modules\Shop\Http\Controllers\Api\DataWrapper\SearchDataWrapper;
use Modules\Shop\Utils\Price\Processor;
use Pingpong\Modules\Routing\Controller;
use Pingpong\Themes\ThemeFacade as Theme;

class SearchController extends Controller
{
    public function __construct()
    {
        $navigationGenerator = new NavigationGenerator();
        $navigationGenerator->generate();
    }

    public function search(SkuSearchRequest $request)
    {
        $strippedSearch = stripslashes($request->get('search'));
        $product = Product::where('sku', $strippedSearch)->first();
        if ($product) {
            return redirect()->route('product.show', ['code' => $product->code]);
        }
        $search = '%' . trim($strippedSearch) . '%';
        $category = Category::where(['name' => $strippedSearch])->first();
        if ($category) {
            if ( $category->products->count() > 0)
            {
                return redirect()->route('product.index', ['category' => $category->code]);
            }      
        }
        $categories = [];
        $query = Product::where(['visible' => 1])
            ->with(['photos', 'productType', 'options.productOptions', 'brand',
                'categories' => function ($q) {
                    $q->where('code', '!=', 'main')->orderBy('parent_id');
                }])
            ->where(function ($q) use ($search) {
                $q->where('sku', 'like', $search)
                    ->orWhere('name', 'like', $search);
            })->orderBy('count', 'desc');

        $products = $query->take(500)->get();
        foreach ($products as $product) {
            if ($product->categories->count() > 0) {
                $category = $product->categories->last();
                if (isset($categories[$category->id])) {
                    $categories[$category->id]['count']++;
                } else {
                    $categories[$category->id] = [
                        'name' => $category->name,
                        'code' => $category->code,
                        'count' => 1
                    ];
                }

            }
        }
        usort($categories, function($a, $b) {
            return $b['name'] < $a['name'];
        });

        $pageInfo = new Page();
        $pageInfo->meta = new Meta();
        $pageInfo->meta->meta_robot_index =  false;

        if ( $product = $products->first() )
        {       
            $pageOg = [
                'image' => url('/') . '/' . $product->photos()->first()->path,
                'title' => 'Результат поиска: ' . $strippedSearch,
                'description' => $product->name
            ];
        }
        if ( $products->count() == 0 )
        {
            abort(404);
        }      
        else
        {
            return Theme::view('shop.search', compact('products', 'categories', 'pageInfo', 'pageOg'));
        }        
    }

    

}