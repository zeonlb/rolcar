<?php namespace Modules\Shop\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Traits\SaveFileTrait;
use Illuminate\Support\Facades\View;
use Modules\Shop\Entities\ProductIcon;
use Modules\Shop\Http\Requests\ProductIconRequest;
use Pingpong\Modules\Routing\Controller;

class IconController extends Controller
{

    use SaveFileTrait;

    /**
     * @return mixed
     */
    public function index()
    {
        $icons = ProductIcon::all();
        return View::make('shop::admin.icon.list', compact('icons'));
    }

    /**
     * @return mixed
     */
    public function create()
    {
        return View::make('shop::admin.icon.create');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $icon = ProductIcon::findOrFail($id);
        return View::make('shop::admin.icon.edit', compact('icon'));
    }

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        $icon = ProductIcon::findOrFail($id);
        $icon->delete();

        return redirect()->route('admin.shop.icon.index')->with('success', 'Icon successfully deleted!');
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $icon = ProductIcon::findOrFail($id);
        $icon->fill($request->all());

        if ($request->file('path')) {
            $icon->img = $this->saveFile($request);
        }
        $icon->save();

        return redirect()->back()->with('success', 'Icon successfully updated');
    }

    /**
     * @param ProductIconRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ProductIconRequest $request)
    {
        $postData = $request->all();
        $icon = new  ProductIcon();
        $icon->fill($postData);
        if ($request->file('path')) {
            $icon->path = $this->saveFile($request, 'path');
        }
        $icon->save();

        return redirect()->route('admin.shop.icon.index')->with('success', 'Icon successfully added');
    }


}