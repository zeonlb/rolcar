<?php namespace Modules\Shop\Http\Controllers\Admin;

use App\Traits\SaveFileTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Modules\Car\Entities\Transport;
use Modules\Cms\Entities\Meta;
use Modules\Shop\Entities\Brand;
use Modules\Shop\Entities\Category;
use Modules\Shop\Entities\Currency;
use Modules\Shop\Entities\Product;
use Modules\Shop\Entities\ProductIcon;
use Modules\Shop\Entities\ProductType;
use Modules\Shop\Http\Requests\CreateProductStepOneRequest;
use Modules\Shop\Http\Requests\ProductRequest;
use Modules\Shop\Mangers\ProductManager;
use Pingpong\Modules\Routing\Controller;

class ProductController extends Controller
{
    use SaveFileTrait;

    /**
     * @return mixed
     */
    public function index(Request $request)
    {
        $products = Product::with('currency', 'options.productOptions', 'options.attribute');
        if ($request->get('product_type')) {
            $products->where('shop_product_type_id', $request->get('product_type'));
        }
        $products = $products->get();
        $productTypes = ProductType::all();
        return View::make('shop::admin.product.list', compact('products', 'productTypes'));
    }

    /**
     * @return mixed
     */
    public function create()
    {
        $productTypes = ProductType::all();
        return View::make('shop::admin.product.create', compact('productTypes'));
    }

    /**
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $product = Product::with('productType', 'currency', 'meta', 'crossSales', 'categories', 'productType.attributes', 'icon')->findOrFail($id);
        $crossSales = $product->crossSales->keyBy('id')->toArray();
        $categories = Category::orderBy('parent_id', 'etc')->where('parent_id', null)->get();
        $currencies = Currency::all();
        $icons = ProductIcon::all();
        $products = Product::where('id', '!=', $id)->with('currency')->get();
        $brands = Brand::all();
        $formRoute = route('admin.shop.product.update', ['id' => $id]);
        $formMethod = 'put';
        $productTypes = ProductType::all();

        return View::make('shop::admin.product.edit',
            compact('products', 'crossSales', 'product', 'currencies', 'icons', 'categories', 'brands', 'formRoute', 'formMethod', 'productTypes'));
    }

    /**
     * @param CreateProductStepOneRequest $request
     * @return mixed
     */
    public function add(CreateProductStepOneRequest $request)
    {
        $productType = ProductType::findOrFail($request->get('productType'));
        $currencies = Currency::all();
        $brands = Brand::all();
        $icons = ProductIcon::all();
        $categories = Category::orderBy('parent_id', 'etc')->where('parent_id', null)->get();
        $products = Product::with('currency')->get();
        $transports = Transport::all();
        return View::make('shop::admin.product.add',
            compact('productType', 'currencies', 'categories', 'transports', 'brands', 'products', 'icons')
        );
    }

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);

        \DB::table('shop_product_photos')->where('shop_product_id', $id)->delete();
        \DB::table('shop_product_video')->where('shop_product_id', $id)->delete();
        \DB::table('shop_discounts')->where('shop_product_id', $id)->delete();
        if ($product->crossSales->count() > 0) {
            \DB::table('shop_cross_sale')->where('shop_product_id', $id)->delete();
        }
        if ($product->options->count() > 0) {
            \DB::table('shop_product_options')->where('shop_product_id', $id)->delete();
        }
        if ($product->comments->count() > 0) {
            \DB::table('shop_product_comments')->where('shop_product_id', $id)->delete();
        }
        $product->delete();
        return redirect()->route('admin.shop.product.index')->with('success', 'Product type successfully deleted!');
    }


    /**
     * @param ProductRequest $request
     * @param $id
     * @param ProductManager $productManager
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ProductRequest $request, $id, ProductManager $productManager)
    {
        $product = Product::findOrFail($id);
        $productManager->update($product, $request);

        return redirect()->back()->with('success', 'Product type successfully updated');
    }

    /**
     * @param ProductRequest $request
     * @param ProductManager $productManager
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ProductRequest $request, ProductManager $productManager)
    {
        $productManager->createNewProduct($request);
        return redirect()->route('admin.shop.product.index')->with('success', 'Product type successfully added');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteIcon($id)
    {
        $product = Product::findOrFail($id);
        $product->icon()->sync([]);
        return ['ok'];
    }

    /**
     *
     */
    public function export()
    {
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=products-' . date('m-d_H_i') . '.csv');
        $products = \DB::table('shop_product')->select(['sku', 'count'])->where('visible', true)->get();
        $out = fopen('php://output', 'w');
        foreach ($products as $product) {
            fputcsv($out, [$product->sku, $product->count]);
        }
        exit;
    }

    /**
     *
     */
    public function import(Request $request)
    {

        if (!$request->hasFile('import_products')) {
            return redirect()->back()->withErrors('Error to import product');
        }
        $file = $request->file('import_products');
        if (($handle = fopen($file->getPathname(), "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                list($sku, $count) = $data;
                $product = Product::where('sku', $sku)->first();
                if ($product) {
                    $product->count = (int)$count;
                    $product->save();
                }
            }
            fclose($handle);
        }
        return redirect()->back()->with('success', 'Successfully imported');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function copy($id)
    {
        $productCopied = Product::with('productType', 'currency', 'meta', 'crossSales')->findOrFail($id);
        $product = new Product();
        $product->meta = new Meta();
        $product->shop_brand_id = $productCopied->shop_brand_id;
        $product->motors = $productCopied->motors;
        $product->visible = false;
        $product->options = $productCopied->options;
        $product->description = $productCopied->description;
        $product->short_description = $productCopied->short_description;
        $product->count = $productCopied->count;
        $product->categories = $productCopied->categories;
        $product->price = $productCopied->price;
        $products = Product::with('currency')->get();

        $product->productType = $productCopied->productType;
        $product->currency = $productCopied->currency;
        $productMotors = json_encode(array_column($product->motors->toArray(), 'id', 'id'));

        $categories = Category::orderBy('parent_id', 'etc')->where('parent_id', null)->get();
        $currencies = Currency::all();
        $transports = Transport::all();
        $brands = Brand::all();
        $formRoute = route('admin.shop.product.store');
        $crossSales = $productCopied->crossSales->keyBy('id')->toArray();
        $icons = ProductIcon::all();

        return View::make('shop::admin.product.edit',
            compact('product', 'products', 'currencies', 'categories', 'transports', 'brands', 'productMotors', 'formRoute', 'crossSales', 'icons'));
    }
}