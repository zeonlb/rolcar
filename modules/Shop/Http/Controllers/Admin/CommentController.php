<?php namespace Modules\Shop\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Modules\Shop\Entities\ProductComment;
use Pingpong\Modules\Routing\Controller;

class CommentController extends Controller {

	/**
	 * @return mixed
     */
	public function index()
	{
		$comments = ProductComment::with('product')->get();
		return View::make('shop::admin.comment.list', compact('comments'));
	}

	/**
	 * @return mixed
     */
	public function create()
	{
		return View::make('shop::admin.brand.create');
	}

	/**
	 * @param $id
	 * @return mixed
     */
	public function edit($id)
	{
		$comment = ProductComment::findOrFail($id);
		return View::make('shop::admin.comment.edit', compact('comment'));
	}

	/**
	 * @param $id
	 * @return mixed
     */
	public function answer($id, Request $request)
	{
		$comment = ProductComment::findOrFail($id);
		$cData = ['name' => 'Manager',
            'comment' => $request->get('answer'),
            'processed' => 1,
            'advantages' => '',
            'disadvantages' => '',
            'shop_product_id' => $comment->shop_product_id,
            'parent_id' => $comment->id,
            'is_admin' => 1
        ];
		ProductComment::create($cData);
		return redirect()->back()->with('Successfully answered');
	}

	/**
	 * @param $id
	 * @return mixed
     */
	public function destroy($id)
	{
		$comment = ProductComment::findOrFail($id);
		$comment->answers()->delete();
		$comment->delete();

		return redirect()->back()->with('success', 'Comment successfully deleted!');
	}

	/**
	 * @param Request $request
	 * @param $id
	 * @return \Illuminate\Http\RedirectResponse
     */
	public function update(Request $request, $id)
	{
		$comment = ProductComment::findOrFail($id);
		$comment->fill($request->all());
		$comment->save();

		return redirect()->back()->with('success', 'Comment successfully updated');
	}
}