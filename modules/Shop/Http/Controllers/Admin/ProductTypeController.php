<?php namespace Modules\Shop\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Modules\Shop\Entities\Attribute;
use Modules\Shop\Entities\ProductType;
use Modules\Shop\Http\Requests\ProductTypeRequest;
use Modules\Shop\Repositories\AttributeRepository;
use Pingpong\Modules\Routing\Controller;

class ProductTypeController extends Controller {

	private $attrRepo;

	public function __construct(AttributeRepository $attributeRepository)
	{
		$this->attrRepo = $attributeRepository;
	}

	/**
	 * @return mixed
     */
	public function index()
	{
		$productTypes = ProductType::all();
		return View::make('shop::admin.product_type.list', compact('productTypes'));
	}

	/**
	 * @return mixed
     */
	public function create()
	{
		$attributes = Attribute::onlyAttributes()->get();
		$options = Attribute::onlyOptions()->get();
		return View::make('shop::admin.product_type.create', compact('attributes', 'options'));
	}

	/**
	 * @param $id
	 * @return mixed
	 */
	public function edit($id)
	{
		$productType = ProductType::findOrFail($id);
		$attributeUsedIds = array_column($productType->attributes->toArray(), 'id');
		$notUsedAttributes = $this->attrRepo->getNotUsedAttributes($attributeUsedIds);

		return View::make('shop::admin.product_type.edit', compact('productType', 'notUsedAttributes'));
	}

	/**
	 * @param $id
	 * @return mixed
     */
	public function destroy($id)
	{
		$productType = ProductType::findOrFail($id);
		$productType->attributes()->detach();
		$productType->delete();

		return redirect()->route('admin.shop.product_type.index')->with('success', 'Product type successfully deleted!');
	}


	/**
	 * @param ProductTypeRequest $request
	 * @param $id
	 * @return \Illuminate\Http\RedirectResponse
     */
	public function update(ProductTypeRequest $request, $id)
	{
		$productType = ProductType::findOrFail($id);
		$productType->fill($request->all());
		$productType->attributes()->detach();
		if ($request->has('attributes')) {
			foreach($request->get('attributes') as $attributeId) {
				$productType->attributes()->attach($attributeId);
			}
		}
		$productType->save();

		return redirect()->back()->with('success', 'Product type successfully updated');
	}

	/**
	 * @param ProductTypeRequest $request
	 * @return \Illuminate\Http\RedirectResponse
     */
	public function store(ProductTypeRequest $request)
	{
		$productType = ProductType::create($request->all());

		if ($request->has('attributes')) {
			foreach($request->get('attributes') as $attributeId) {
				$productType->attributes()->attach($attributeId);
			}
		}

		return redirect()->back()->with('success', 'Product type successfully added');
	}
	
	
}