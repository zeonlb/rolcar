<?php namespace Modules\Shop\Http\Controllers\Admin;

use App\Traits\SaveFileTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Modules\Shop\Entities\Banner;
use Pingpong\Modules\Routing\Controller;

class BannerController extends Controller {

    use SaveFileTrait;
	/**
	 * @return mixed
     */
	public function index()
	{
		$banners =  Banner::all();
		return View::make('shop::admin.banner.list', compact('banners'));
	}

	/**
	 * @return mixed
     */
	public function create()
	{
		return View::make('shop::admin.banner.create');
	}

	/**
	 * @param $id
	 * @return mixed
     */
	public function edit($id)
	{
		$banner = Banner::findOrFail($id);
		return View::make('shop::admin.banner.edit', compact('banner'));
	}

	/**
	 * @param $id
	 * @return mixed
     */
	public function destroy($id)
	{
		$banner = Banner::findOrFail($id);
		$banner->delete();

		return redirect()->route('admin.shop.banner.index')->with('success', 'Banner successfully deleted!');
	}

	/**
	 * @param Request $request
	 * @param $id
	 * @return \Illuminate\Http\RedirectResponse
     */
	public function update(Request $request, $id)
	{
		$banner = Banner::findOrFail($id);
		$banner->fill($request->all());

        if ($request->file('file')) {
            $banner->img = $this->saveFile($request);
        }
		$banner->save();

		return redirect()->route('admin.shop.banner.index')->with('success', 'Banner successfully updated');
	}

	/**
	 * @param Request $request
	 * @return \Illuminate\Http\RedirectResponse
     */
	public function store(Request $request)
	{

		$banner = Banner::create($request->all());

        if ($request->file('file')) {
            $banner->img = $this->saveFile($request);
        }
		$banner->save();

		return redirect()->route('admin.shop.banner.index')->with('success', 'Banner successfully added');
	}
	
	
}