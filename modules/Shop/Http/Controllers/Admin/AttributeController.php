<?php namespace Modules\Shop\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Modules\Shop\Entities\Attribute;
use Modules\Shop\Entities\AttributeOption;
use Modules\Shop\Http\Requests\AttributeRequest;
use Pingpong\Modules\Routing\Controller;

class AttributeController extends Controller {

	/**
	 * @return mixed
     */
	public function index()
	{
		$attributes =  Attribute::all();
		return View::make('shop::admin.attribute.list', compact('attributes'));
	}

	/**
	 * @return mixed
     */
	public function create()
	{
		return View::make('shop::admin.attribute.create');
	}

	/**
	 * @param $id
	 * @return mixed
     */
	public function edit($id)
	{
		$attribute = Attribute::findOrFail($id);

		return View::make('shop::admin.attribute.edit', compact('attribute'));
	}

	/**
	 * @param $id
	 * @return mixed
     */
	public function destroy($id)
	{
		$attribute = Attribute::findOrFail($id);
        $attribute->shopProductOptions()->delete();
        DB::table('shop_product_type_attributes')->where('shop_attribute_id',  $id)->delete();
		$attribute->options()->delete();
		$attribute->delete();
		event('shop.attribute.destroy');
		return redirect()->route('admin.shop.attribute.index')
            ->with('success', 'Attribute successfully deleted!');
	}

	/**
	 * @param Request $request
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function destroyOption(Request $request)
	{
        $attributeOption = AttributeOption::findOrFail($request->get('id'));
        DB::table('shop_product_options')->where('shop_product_option_id',  $attributeOption->id)->delete();
        $attributeOption->delete();

		return response()->json(['status' => 'SUCCESS']);;
	}

	/**
	 * @param Request $request
	 * @param $id
	 * @return \Illuminate\Http\RedirectResponse
     */
	public function update(Request $request, $id)
	{
		$attribute = Attribute::findOrFail($id);
		$attribute->fill($request->all());

		if ($request->get('options')) {
			foreach($request->get('options') as $option) {
			    $option['code'] = saveCode($option['code']);
				if (isset($option['id'])) {
					$attributeOption = AttributeOption::findOrFail($option['id']);
					$attributeOption->fill($option);
					$attributeOption->save();
				} else {
					$attribute->options()->save(new AttributeOption($option));
				}

			}
		}

		$attribute->save();

		return redirect()->back()->with('success', 'Attribute successfully updated');
	}

	/**
	 * @param AttributeRequest $request
	 * @return \Illuminate\Http\RedirectResponse
     */
	public function store(AttributeRequest $request)
	{
		$attribute =  Attribute::create($request->all());
		if ($request->get('options')) {
			foreach ($request->get('options') as $option) {
				$attribute->options()->save(new AttributeOption($option));
			}
		}

		return redirect()->back()->with('success', 'Attribute successfully added');
	}
	
	
}