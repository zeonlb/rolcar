<?php namespace Modules\Shop\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\View;
use Modules\Shop\Entities\Order;
use Modules\Shop\Entities\Product;
use Pingpong\Modules\Routing\Controller;

class OrderController extends Controller {

	protected  $status = [
		'new','in_progress','processed','send_to_client','delivered','sold', 'canceled'
	];
	/**
	 * @return mixed
     */
	public function index()
	{
		$orders = Order::orderBy('id', 'desc')->get();
		return View::make('shop::admin.order.list', compact('orders'));
	}

	/**
	 * @param $id
	 * @return mixed
     */
	public function show($id)
	{
		$order = Order::findOrFail($id);
        $order->products[0]->getThumb();
        $delivery = json_decode($order->delivery);
		$status = $this->status;
		return View::make('shop::admin.order.show', compact('order', 'status', 'delivery'));
	}

	/**
	 * @param Request $request
	 * @param $id
	 * @return \Illuminate\Http\RedirectResponse
     */
	public function update(Request $request, $id)
	{
		$order = Order::findOrFail($id);
		$order->fill($request->get('order'));
		$order->save();

		return redirect()->back()->with('success', 'Order successfully updated');
	}
}