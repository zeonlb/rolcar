<?php namespace Modules\Shop\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Modules\Shop\Entities\Configuration;
use Pingpong\Modules\Routing\Controller;

class ConfigController extends Controller
{

    public function getSettings()
    {
        $systemSettings = Configuration::all();
        $urlSave = url('admin/shop/config/settings');
        return view('admin::config.system', compact('systemSettings', 'urlSave'));
    }
    
    public function postSettings(Request $request)
    {
        $systemSettings = Configuration::whereIn('id', array_keys($request->get('config')))->get();
        foreach ( $systemSettings as $setting ) {
            $setting->value = $request->get('config')[$setting->id];
            $setting->save();
        }
        return redirect()->back()->with('success', 'Settings successfully saved!');
    }


}