<?php namespace Modules\Shop\Http\Controllers\Admin;


use App\Http\Requests\Request;
use App\Traits\SaveFileTrait;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Modules\Shop\Entities\CategoryJoinProduct;
use Modules\Shop\Http\Requests\CategoryRequest;
use Modules\Cms\Entities\Meta;
use Modules\Shop\Entities\Category;
use Illuminate\Support\Facades\File;
use Pingpong\Modules\Routing\Controller;

/**
 * @property string img
 */
class CategoryController extends Controller
{
    use SaveFileTrait;

    public function index()
    {
        $categories = Category::orderBy('parent_id', 'etc')->where('parent_id', null)->get();
        return View::make('shop::admin.category.list', compact('categories'));
    }

    public function create()
    {
        $categories = Category::all();

        return View::make('shop::admin.category.create', compact('categories'));
    }

    public function edit($id)
    {
        $categories = Category::all();
        $category = Category::with(['cjp', 'products'])->findOrFail($id);
        $category->cjp = $category->cjp->keyBy('shop_product_id');
        if (!$category->meta) {
            $category->meta = new Meta();
        }

        return View::make('shop::admin.category.edit', compact('category', 'categories'));
    }

    public function update(CategoryRequest $request, $id)
    {
        $postData = $request->all();

        if (!strlen($postData['parent_id'])) {
            unset($postData['parent_id']);
        }

        $category = Category::findOrFail($id);
        $category->fill($postData);
        if (!$category->meta) {
            $meta = Meta::create($postData);
            $category->meta()->associate($meta);
        } else {
            $category->meta->fill($postData);
            $category->meta->save();
        }

        if ($request->file('file')) {
            $category->img = $this->saveFile($request);
        }

        $category->save();

        if ($sort = $postData['category']['sort']??[]) {
            foreach ($sort as $product_id => $value) {
                \DB::table('shop_product_categories')
                    ->where(['shop_category_id' => $id, 'shop_product_id' => $product_id])
                    ->update(['sort' => $value]);
            }
        }

        return redirect()->back()->with('success', 'Category successfully updated!');
    }

    public function destroy($id)
    {
        $category = Category::findOrFail($id);
        Category::where('parent_id', $id)->delete();
        $category->delete();
        return redirect()->route('admin.shop.category.index')->with('success', 'Category successfully deleted!');
    }

    public function store(CategoryRequest $request)
    {
        $postData = $request->all();
        if (!isset($postData['parent_id']) || !strlen($postData['parent_id'])) {
            unset($postData['parent_id']);
        }
        $category = Category::create($postData);
        $meta = Meta::create($postData);
        $category->meta()->associate($meta);

        if ($request->file('file')) {
            $category->img = $this->saveFile($request);
        }

        $category->save();

        return redirect()->back()->with('success', 'Category successfully added');
    }


}
