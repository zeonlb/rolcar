<?php namespace Modules\Shop\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Modules\Cms\Entities\Meta;
use Modules\Shop\Entities\Brand;
use Modules\Shop\Http\Requests\BrandRequest;
use Pingpong\Modules\Routing\Controller;

class BrandController extends Controller {

	/**
	 * @return mixed
     */
	public function index()
	{
		$brands =  Brand::all();
		return View::make('shop::admin.brand.list', compact('brands'));
	}

	/**
	 * @return mixed
     */
	public function create()
	{
		return View::make('shop::admin.brand.create');
	}

	/**
	 * @param $id
	 * @return mixed
     */
	public function edit($id)
	{
		$brand = Brand::findOrFail($id);
		return View::make('shop::admin.brand.edit', compact('brand'));
	}

	/**
	 * @param $id
	 * @return mixed
     */
	public function destroy($id)
	{
		$brand = Brand::findOrFail($id);
		$brand->meta()->delete();
		$brand->delete();

		return redirect()->route('admin.shop.brand.index')->with('success', 'Brand successfully deleted!');
	}

	/**
	 * @param Request $request
	 * @param $id
	 * @return \Illuminate\Http\RedirectResponse
     */
	public function update(Request $request, $id)
	{
		$brand = Brand::findOrFail($id);
		$brand->fill($request->all());
		$brand->meta->fill($request->all());
		$brand->meta->save();
		$brand->save();

		return redirect()->back()->with('success', 'Brand successfully updated');
	}

	/**
	 * @param BrandRequest $request
	 * @return \Illuminate\Http\RedirectResponse
     */
	public function store(BrandRequest $request)
	{

		$brand = Brand::create($request->all());
		$meta = Meta::create($request->all());
		$brand->meta()->associate($meta);
		$brand->save();

		return redirect()->back()->with('success', 'Brand successfully added');
	}
	
	
}