<?php namespace Modules\Shop\Http\Controllers\Admin;

use Illuminate\Support\Facades\View;
use Pingpong\Modules\Routing\Controller;

class CurrencyController extends Controller {
	
	public function index()
	{
		return View::make('shop::admin.currency.list');
	}

	public function create()
	{

		return View::make('shop::admin.currency.create');
	}
}