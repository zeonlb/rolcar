<?php
namespace Modules\Shop\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Contracts\Validation\Validator;

class CreateProductStepOneRequest extends Request{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'productType' => 'required|max:255',
        ];
    }


    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'productType.required' => '"Product type" is required',
        ];
    }
}