<?php
namespace Modules\Shop\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Contracts\Validation\Validator;

class ProductCommentRequest extends Request{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'comment.name' => 'required|min:2|max:255',
            'comment.comment' => 'required|min:2|max:10000',
        ];
    }


    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'comment.name.required' => '"Имя" не может быть пустым',
            'comment.comment.required'  => '"Коментарий" не может быть пустым',
        ];
    }
}