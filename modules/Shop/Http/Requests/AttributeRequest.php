<?php
namespace Modules\Shop\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Support\Facades\Auth;

class AttributeRequest extends Request{

    public function authorize()
    {
        return Auth::check();
    }

    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'code' => 'required|max:255|unique:shop_attributes,code,'.$this->segment(4),
            'type' => 'required|max:255',
        ];
    }


    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => '"Name" is required',
            'code.required'  => '"Code" is required',
            'type.required'  => '"type" is required',
            'code.unique'  => '"Code" is unique',
        ];
    }
}