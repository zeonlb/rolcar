<?php
namespace Modules\Shop\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Support\Facades\Auth;

class ProductIconRequest extends Request{

    public function authorize()
    {
        return Auth::check();
    }

    public function rules()
    {
        return [
            'path' => 'required|max:255',
        ];
    }


    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'path.required' => '"Path" is required',
        ];
    }
}