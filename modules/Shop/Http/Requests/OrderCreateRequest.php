<?php
namespace Modules\Shop\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Support\Facades\Auth;

class OrderCreateRequest extends Request{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'first_name' => 'required|min:2|max:255',
            'payment_part_count' => 'integer|min:2|max:10',
            'phone' => 'required|min:3|max:255',
        ];
    }


    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'first_name.required' => 'поле "Имя" обязательное',
            'phone.required'  => 'поле "Телефон" обязательное',
            'email.required'  => 'поле "E-mail" обязательное',
        ];
    }
}