<?php
namespace Modules\Shop\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Contracts\Validation\Validator;

class ProductRequest extends Request{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'code' => 'required|max:255|unique:shop_product,code,'.$this->segment(4),
            'sku' => 'required|max:255|unique:shop_product,sku,'.$this->segment(4),
            'description' => 'required|max:5000',
            'short_description' => 'required|max:1200',
            'brand_id' => 'required|numeric',
            'count' => 'required|numeric',
            'price' => 'required|numeric',
            'currency' => 'required|numeric',
            'visible' => 'required|numeric',
//            'discount.value' => 'required_with:discount.date_start,discount.date_end,discount.type',
//            'discount.date_start' => 'required_with:discount.date_end,discount.value,discount.type',
//            'discount.date_end' => 'required_with:discount.date_start,discount.value,discount.type',
        ];
    }


    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => '"Name" is required',
            'code.required'  => '"Code" is required',
            'sku.required'  => '"Sku" is required',
            'sku.unique'  => '"Sku" is unique',
            'description.required'  => '"Description" is required',
            'short_description.required'  => '"Short description" is required',
            'count.required'  => '"Count" is required',
            'price.required'  => '"Price" is required',
            'currency.required'  => '"Currency" is required',
            'brand_id.required'  => '"Brand" is required',
            'visible.required'  => '"Visible" is required',
            'code.unique'  => '"Code" is unique',
            'discount.value.required_with'  => '"Amount sale" is required',
            'discount.date_start.required_with'  => '"Start date" is required',
            'discount.date_end.required_with'  => '"Start date" is required',
        ];
    }
}