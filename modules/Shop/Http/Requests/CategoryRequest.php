<?php
namespace Modules\Shop\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Contracts\Validation\Validator;

class CategoryRequest extends Request{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {

        return [
            'name' => 'required|max:255',
            'code' => 'required|max:255|unique:shop_category,code,'.$this->segment(4),
            'visible' => 'required',
        ];
    }


    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => '"Name" is required',
            'url.required'  => '"Url" is required',
        ];
    }
}