<?php

#Admin
Route::group(['prefix' => 'admin', 'middleware' => ['admin.auth', 'admin.menu.storing'], 'namespace' => 'Modules\Shop\Http\Controllers'], function () {
    Route::group(['prefix' => 'shop', 'namespace' => 'Admin'], function () {
        Route::resource('attribute', 'AttributeController');
        Route::post('attribute/destroyOption', 'AttributeController@destroyOption');
        Route::resource('category', 'CategoryController');
        Route::resource('product_type', 'ProductTypeController');
        Route::resource('product', 'ProductController');
        Route::resource('brand', 'BrandController');
        Route::resource('comment', 'CommentController');
        Route::post('/comment/{id}/answer', ['as' => 'admin.shop.comment.answer', 'uses' => 'CommentController@answer']);
        Route::resource('order', 'OrderController');
        Route::controller('config', 'ConfigController');
        Route::resource('banner', 'BannerController');
        Route::resource('icon', 'IconController');
        Route::get('product/add/{productType}', ['as' => 'admin.shop.product.add', 'uses' => 'ProductController@add']);
        Route::get('product/{id}/copy', ['as' => 'admin.shop.product.copy', 'uses' => 'ProductController@copy']);
        Route::get('product-export', ['as' => 'admin.shop.product.export', 'uses' => 'ProductController@export']);
        Route::post('product-import', ['as' => 'admin.shop.product.import', 'uses' => 'ProductController@import']);
        Route::delete('product/{id}/delete-icon', ['as' => 'admin.shop.product.delete_icon', 'uses' => 'ProductController@deleteIcon']);
    });
});


#Front
Route::group(['prefix' => 'products', 'namespace' => 'Modules\Shop\Http\Controllers\Front'], function () {
    Route::get('/', ['as' => 'product.main', 'uses' => 'ProductController@main']);
    Route::get('/{category}/{naznachenie?}/{tsokol?}/{voltage?}', ['as' => 'product.index', 'uses' => 'ProductController@index']);

});
Route::get('/product/{code}', ['as' => 'product.show', 'uses' => 'Modules\Shop\Http\Controllers\Front\ProductController@show']);
Route::post('/product/{code}', 'Modules\Shop\Http\Controllers\Front\ProductController@showLendingAjax');
Route::post('/product/{id}/comment/add', ['as' => 'product.comment.add', 'uses' => 'Modules\Shop\Http\Controllers\Front\CommentController@create']);
Route::get('/product-comments/{offset?}', ['as' => 'product.comment.list', 'uses' => 'Modules\Shop\Http\Controllers\Front\CommentController@index']);
Route::get('/shop/search', ['as' => 'shop.search', 'uses' => 'Modules\Shop\Http\Controllers\Front\SearchController@search']);
Route::post('/shop/checkout/cart/{id}/add', ['as' => 'shop.checkout.cart.add', 'uses' => 'Modules\Shop\Http\Controllers\Front\CheckoutController@addProductToCart']);
Route::get('/shop/checkout/cart/{id}/delete', ['as' => 'shop.checkout.cart.delete', 'uses' => 'Modules\Shop\Http\Controllers\Front\CheckoutController@deleteProductFromCart']);
Route::get('/shop/checkout/cart', ['as' => 'shop.checkout.list', 'uses' => 'Modules\Shop\Http\Controllers\Front\CheckoutController@index']);
Route::get('/shop/checkout/data', ['as' => 'shop.checkout.cart.data', 'uses' => 'Modules\Shop\Http\Controllers\Front\CheckoutController@cartModalBody']);
Route::get('/shop/checkout/countable/{key}/{type}/{count}', ['as' => 'shop.checkout.countable', 'uses' => 'Modules\Shop\Http\Controllers\Front\CheckoutController@changeProductCount']);
Route::post('/shop/order/create', ['as' => 'shop.order.create', 'uses' => 'Modules\Shop\Http\Controllers\Front\OrderController@create']);
Route::post('/shop/order/one_click', ['as' => 'shop.order.oneclick', 'uses' => 'Modules\Shop\Http\Controllers\Front\OrderController@oneClick']);

Route::get('/api/v1/hotline/products', ['as' => 'api.shop.ec.hotline.products', 'uses' => 'Modules\Shop\Http\Controllers\Api\ExportController@hotlineProducts']);
Route::get('/api/v1/rozetka/products', ['as' => 'api.shop.ec.rozetka.products', 'uses' => 'Modules\Shop\Http\Controllers\Api\ExportController@rozetkaProducts']);


Route::get('/api/v1/merchant/products', ['as' => 'api.shop.merchant.products', 'uses' => 'Modules\Shop\Http\Controllers\Api\ExportController@merchantProducts']);

//Payment routes
Route::get('/shop/order/payment/{code}', ['as' => 'shop.order.payment.liqpay', 'uses' => 'Modules\Shop\Http\Controllers\Front\OrderController@liqPayPayment']);
Route::post('/shop/order/privat24/{code}', ['as' => 'shop.order.payment.privat24', 'uses' => 'Modules\Shop\Http\Controllers\Front\OrderController@privat24Payment']);
Route::get('/shop/order/payment-info/{code}', ['as' => 'shop.order.payment.payment_info', 'uses' => 'Modules\Shop\Http\Controllers\Front\OrderController@paymentInfo']);
Route::post('/shop/order/payment-info/{code}', ['as' => 'shop.order.payment.payment_info.post', 'uses' => 'Modules\Shop\Http\Controllers\Front\OrderController@paymentInfo']);
Route::post('/shop/order/liqpay-status', ['as' => 'shop.order.payment.liqpay_status', 'uses' => 'Modules\Shop\Http\Controllers\Front\OrderController@liqpayStatus']);



Route::post('/api/v1/tilda/form', ['as' => 'api.shop.tilda.form', 'uses' => 'Modules\Shop\Http\Controllers\Api\FormRequestController@postForm']);
Route::get('/api/v1/np/city', ['as' => 'api.shop.np.city', 'uses' => 'Modules\Shop\Http\Controllers\Api\NpController@city']);
Route::get('/api/v1/np/warehouse', ['as' => 'api.shop.np.warhouse', 'uses' => 'Modules\Shop\Http\Controllers\Api\NpController@warhouse']);
Route::any('/api/v1/form-request', ['as' => 'api.shop.formrequest', 'uses' => 'Modules\Shop\Http\Controllers\Api\FormRequestController@postForm']);
Route::post('/api/v1/form/subscribe', ['as' => 'api.shop.subscribe', 'uses' => 'Modules\Shop\Http\Controllers\Api\FormRequestController@postSubscribe']);