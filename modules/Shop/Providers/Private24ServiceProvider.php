<?php


namespace Modules\Shop\Providers;

use App\Mail\ESputnik\ApiClient;
use Illuminate\Support\ServiceProvider;
use Modules\Shop\Services\Private24Service;

/**
 * Class Private24ServiceProvider
 * @package App\Providers
 */
class Private24ServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton(Private24Service::class, function ($app) {
            return new Private24Service(
                env('PRIVATE_24_SITE_ID'),
                env('PRIVATE_24_PASSWORD')
            );
        });

        $this->app->alias(Private24Service::class, 'private24');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
      //
    }
}
