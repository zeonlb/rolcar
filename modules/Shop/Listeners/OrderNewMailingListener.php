<?php

namespace Modules\Shop\Listeners;


use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\View;
use Modules\Cms\Managers\CmsConfigManager;
use Modules\Shop\Entities\Configuration;
use Modules\Shop\Entities\Order;

class OrderNewMailingListener
{
    /**
     *
     * @param Order $order
     */
    public function handle(Order $order)
    {
        $configManager = CmsConfigManager::getInstance();

        if ($configManager->get('mandrill_token') || $configManager->get('mail_smpt_host')) {
            Mail::send('shop::mail.order.new', ['order' => $order], function ($m) use ($order) {
                $m->to($order->email, $order->name)->subject('Заказ #'. $order->invoice);
            });
        }
    }
}