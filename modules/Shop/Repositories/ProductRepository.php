<?php namespace Modules\Shop\Repositories;


use App\Repository\Repository;
use Doctrine\DBAL\Query\QueryBuilder;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Modules\Shop\Entities\AttributeOption;
use Modules\Shop\Entities\Brand;
use Modules\Shop\Entities\Product;
use Modules\Shop\Entities\ProductType;
use Modules\Shop\Utils\Category\Manager\CategoryManager;
use Modules\Shop\Utils\Category\Manager\Manager;
use Modules\Shop\Utils\Category\Manager\CarCategoryManager;

class ProductRepository extends Repository
{

    public function model()
    {
        return Product::class;
    }

    public function getActiveProducts(
        $filters = [],
        $naznachenie,
        Manager $categoryManager = null,
        $orderBy = false,
        $destination = 'asc'
    )
    {
        $products = DB::table('shop_product as P')
            ->select('P.*', 'B.code as brand_code', 'CF.path as photo', 'SD.value as discount_amount',
                'SD.date_end as date_end')
            ->leftJoin('shop_brands as B', 'P.shop_brand_id', '=', 'B.id')
            ->leftJoin('shop_product_options as PO', 'P.id', '=', 'PO.shop_product_id')
            ->leftJoin('shop_attributes as SA', 'SA.id', '=', 'PO.shop_attribute_id')
            ->leftJoin('shop_attribute_options as AO', 'PO.shop_product_option_id', '=', 'AO.id')
            ->leftJoin('shop_product_photos as PPH', 'PPH.shop_product_id', '=', 'P.id')
            ->leftJoin('shop_product_categories as SHC', 'P.id', '=', 'SHC.shop_product_id')
            ->leftJoin('shop_category as SC', 'SC.id', '=', 'SHC.shop_category_id')
            ->leftJoin('shop_discounts as SD', 'P.id', '=', 'SD.shop_product_id')
            ->leftJoin('cms_files as CF', 'PPH.shop_photo_id', '=', 'CF.id')
            ->leftJoin(
                'lib_car_shop_product as LCSP',
                'LCSP.product_id', '=',
                'P.id')
            ->where('P.visible', true);
        if ($categoryManager) {
            switch (true) {
                case $categoryManager instanceof CarCategoryManager:

                    $products->where('LCSP.motor_id', $categoryManager->getId());
                    $products->where('SC.code', $naznachenie);

                    break;
            }
        }
        if ($filters) {

            foreach ($filters as $key => $fields) {
                switch ($key) {
                    case "brand" :
                        $firstCase = array_shift($fields);
                        if (count($fields)) {
                            $products->where(function ($q) use ($fields, $firstCase) {
                                $q->where('B.code', '=', $firstCase);
                                foreach ($fields as $field) {
                                    $q->orWhere('B.code', '=', $field);
                                }
                            });

                        } else {
                            $products->where('B.code', '=', $firstCase);
                        }
                        break;
                    default:
                        $firstCase = array_shift($fields);
//                        $products->where('SA.code', '=', $key);

                        $products->whereHas('options.productOptions', function ($q) use ($fields, $firstCase) {
                            $q->where('code', '=', $firstCase);
                            foreach ($fields as $field) {
                                $q->orWhere('code', '=', $field);
                            }
                        });
                        break;
                }
            }
        }

        if ($orderBy) {
            $products->orderBy('P.' . $orderBy, $destination);
        }
        $products->groupBy('P.id');

        return $products;
    }


    public function getActive(
        $filters = [],
        $category,
        Manager $categoryManager = null,
        $orderBy = false,
        $destination = 'asc',
        $tsokol = null,
        $voltage = null,
        $withOutPrice = true
    )
    {
        $products = Product::where('visible', true)->orderBy('count', 'DESC');


        $carOptions = [];
        if ($categoryManager instanceof CarCategoryManager) {
            $carOptions = json_decode($categoryManager->getMotor()->lampOptions->options, JSON_OBJECT_AS_ARRAY)[$category]??[];
        }
        if (null !== $tsokol || null !== $voltage) {
            if (null !== $tsokol && null !== $voltage) {
                $options = $carOptions[$tsokol]??[];
                if (count($options) === 0 || !in_array(strtoupper($voltage), array_map('strtoupper', $options))) {
                    abort(404);
                }
                $voltages = [12, 1224, 85];
                if ($categoryManager instanceof CarCategoryManager && $categoryManager->getMotor()->modification->model->transport->id == 3) {
                    $voltages = [1224, 24, 85];
                }
                $option = DB::table('shop_attribute_options')->select('id')->where('code', 'like', $tsokol)->first();

                $voltageOptions = (array)DB::table('shop_attribute_options')->select('id')->whereIn('code', $voltages)->get();
                if (!$option) {
                    abort(404);
                }
                $idProducts = DB::select(sprintf('
                    select PO.shop_product_id from shop_product_options as PO  
                    where PO.shop_product_option_id = %d AND 
                    (
                        select count(*) from shop_product_options as PO2 
                        where PO.shop_product_id = PO2.shop_product_id 
                        and PO2.shop_product_option_id IN (%s)
                    )',$option->id, implode(',', array_column($voltageOptions, 'id')))
                );

                $idProducts = array_column($idProducts, 'shop_product_id');
                if (!$idProducts) {
                    $idProducts = [-1];
                }
            } elseif (null !== $tsokol) {
                $idProducts = [];
                if (!array_key_exists($tsokol, $carOptions)) {
                    abort(404);
                }

                $voltages = [12, 1224, 85];
                if ($categoryManager instanceof CarCategoryManager && $categoryManager->getMotor()->modification->model->transport->id == 3) {
                    $voltages = [1224, 24, 85];
                }
                $option = DB::table('shop_attribute_options')->select('id')->where('code', 'like', $tsokol)->first();
                $voltageOptions = (array)DB::table('shop_attribute_options')->select('id')->whereIn('code', $voltages)->get();
                if (!$option) {
                    abort(404);
                }
                $idProductRes = DB::select(sprintf(
                    '
                    select PO.shop_product_id from shop_product_options as PO  
                    where PO.shop_product_option_id = %d AND 
                    (
                        select count(*) from shop_product_options as PO2 
                        where PO.shop_product_id = PO2.shop_product_id 
                        and PO2.shop_product_option_id IN ( %s )
                    )', $option->id, implode(',', array_column($voltageOptions, 'id'))
                )
                );
                $idProducts = array_merge($idProducts, array_column($idProductRes, 'shop_product_id'));
            }
            if (!$idProducts) {
                $idProducts = [-1];
            }
            if ($idProducts) {
                $products->whereIn('id', $idProducts);
            }
        } else if ($categoryManager) {
            $idProducts = [];
            foreach ($carOptions as $tsc => $voltage) {

                if (!$voltage) {
                    continue;
                }
                $voltage = current($voltage);
                $voltage = strtr(strtolower($voltage), ['v' => '', 'V' => '']);
                $option = DB::table('shop_attribute_options')->select('id')->where('code', 'like', $tsc)->first();
                $options2 = DB::table('shop_attribute_options')->select('id')->where('code', 'like', $voltage)->first();
                if (!$option || !$options2) {
                    abort(404);
                }
                $idProductRes = DB::select('
                    select PO.shop_product_id from shop_product_options as PO  
                    where PO.shop_product_option_id = ? AND 
                    (
                        select count(*) from shop_product_options as PO2 
                        where PO.shop_product_id = PO2.shop_product_id 
                        and PO2.shop_product_option_id = ?
                    )',
                    [$option->id, $options2->id]
                );
                $idProducts = array_merge($idProducts, array_column($idProductRes, 'shop_product_id'));
            }
            if ($idProducts) {
                $products->whereIn('id', $idProducts);
            }

        }
        // Do not sort by categories
        if (!preg_match('#(it|bt|ft)\d#', $category)) {
            $products->whereHas('categories', function ($q) use ($category) {
                $q->where('code', '=', $category);
            });
        }

        $products->with('photos', 'productType', 'options.productOptions', 'brand', 'icon');
        $products->with(['discount' => function ($q) {
            $q->where('active', '=', true);
        }]);

        if ($filters) {
            foreach ($filters as $key => $fields) {
                switch ($key) {
                    case "brand" :
                        $products->whereHas('brand', function ($q) use ($fields) {
                            $q->where(function ($q) use ($fields) {
                                foreach ($fields as $field) {
                                    $q->orWhere('code', '=', $field);
                                };
                            });
                        });
                        break;
                    case "pf" :
                        if (!$withOutPrice && $fields[0]??false) {
                            $products->where('price', '>=', $fields[0]);
                        }
                        break;
                    case "pt" :
                        if (!$withOutPrice && $fields[0]??false) {
                            $products->where('price', '<=', $fields[0]);
                        }
                        break;
                    default:
                        $products->whereHas('options.productOptions', function ($q) use ($fields) {
                            $q->where(function ($q) use ($fields) {
                                foreach ($fields as $field) {
                                    $q->orWhere('code', '=', $field);
                                };
                            });

                        });
                        break;
                }
            }
        }
        if ($orderBy) {
            if ($orderBy == 'discounts') {
                $products->whereHas('discount', function ($q) use ($categoryManager) {
                    $q->where('active', '=', true);
                });
            } else {
                $products->orderBy($orderBy, $destination);
            }
        } else {
            if ($categoryManager instanceof CategoryManager) {
                $products->orderBy(\DB::raw('(select shop_product_categories.sort from shop_product_categories where shop_product.id=shop_product_categories.shop_product_id and shop_category_id='.$categoryManager->category->id.')'));
            }
        }
        return $products;
    }

    public function getCachedActive(
        $filters = [],
        $naznachenie,
        $category,
        Manager $categoryManager = null,
        $orderBy = false,
        $destination = 'asc'
    )
    {
        $cacheKey = 'products.' . md5($naznachenie . $category);
        $products = Cache::remember($cacheKey, 10, function () use ($filters, $naznachenie, $category, $categoryManager, $orderBy, $destination) {
            $products = Product::where('visible', true);
            if ($categoryManager) {
                if ($categoryManager instanceof CarCategoryManager) {
                    $products->whereHas('motors', function ($q) use ($categoryManager, $naznachenie) {
                        $q->where('motor_id', '=', $categoryManager->getId());
                        $q->where('lamp_type', '=', $naznachenie);
                    });
                }
            }
            $products->with('photos', 'productType', 'options');
            $products->with(['discount' => function ($q) {
                $q->where('active', '=', true);
            }]);
            if ($filters) {
                foreach ($filters as $key => $fields) {
                    switch ($key) {
                        case "brand" :
                            $firstCase = array_shift($fields);
                            $products->whereHas('brand', function ($q) use ($fields, $firstCase) {
                                $q->where('code', '=', $firstCase);
                                foreach ($fields as $field) {
                                    $q->orWhere('code', '=', $field);
                                }
                            });
                            break;
                        default:
                            $firstCase = array_shift($fields);
//                        $products->where('SA.code', '=', $key);

                            $products->whereHas('options.productOptions', function ($q) use ($fields, $firstCase) {
                                $q->where('code', '=', $firstCase);
                                foreach ($fields as $field) {
                                    $q->orWhere('code', '=', $field);
                                }
                            });
                            break;
                    }
                }
            }

            if ($orderBy) {
                if ($orderBy == 'discounts') {
                    $products->whereHas('discount', function ($q) use ($categoryManager) {
                        $q->where('active', '=', true);
                    });
                } else {
                    $products->orderBy($orderBy, $destination);
                }
            }
            return $products->get();

        });


        return $products;
    }

    public function findActiveBrands()
    {

        return
            DB::table('shop_brands as b')
                ->select('b.*')
                ->join('shop_product as p', 'b.id', '=', 'p.shop_brand_id')
                ->groupBy('p.shop_brand_id')
                ->get();

    }

    public function getProductOptions($productId)
    {
        $query = DB::table('shop_product_options as PO')
            ->select('SO.name as aname', 'SO.code as acode', 'SAO.id', 'SAO.name', 'SAO.code', 'SO.required')
            ->join('shop_attributes as SO', 'SO.id', '=', 'PO.shop_attribute_id')
            ->join('shop_attribute_options as SAO', 'SAO.id', '=', 'PO.shop_product_option_id')
            ->where('SO.option', true)
            ->where('SO.visible', true)
            ->where('PO.shop_product_id', $productId)
            ->groupBy('SAO.id');
        $result = [];
        foreach ($query->get() as $key => $e) {
            $result [$e->acode]['name'] = $e->aname;
            $result [$e->acode]['code'] = $e->acode;
            $result [$e->acode]['required'] = $e->required;
            $result [$e->acode]['values'][] = $e;
        }
        return $result;
    }

    public function getPaginatedProducts($countOfList = 20)
    {
        return Product::where('visible', 1)->paginate($countOfList);
    }

    public function getProductByCode($code, $withActiveOptions = false)
    {
        $query = Product::where(['code' => $code, 'visible' => 1])
            ->with('photos', 'options', 'crossSales', 'crossSales.photos', 'crossSales.discount', 'currency', 'options.attribute');
        if ($withActiveOptions) {
            $query->with([
                'comments' =>
                    function ($q) {
                        $q->where('processed', true);
                    },
                'crossSales' =>
                    function ($q) {
                        $q->where('visible', true);
                    },
                'discount' => function ($q) {
                    $q->where('date_end', '>', new \DateTime('now'));
                }
            ]);

        }


        return $query->first();
    }

    public function getProductsWithOptionList()
    {
        $products = DB::table('shop_product as P')
            ->select('AO.code as attr', 'P.id as product_id', 'C.code as category')
            ->join('shop_product_options as PO', 'P.id', '=', 'PO.shop_product_id')
            ->join('shop_attribute_options as AO', 'AO.id', '=', 'PO.shop_product_option_id')
            ->join('shop_product_categories as PC', 'PC.shop_product_id', '=', 'P.id')
            ->join('shop_category as C', 'C.id', '=', 'PC.shop_category_id')
            ->where('P.visible', true)
            ->get();
        return $products;
    }


    public function getProductsFilterOptions()
    {
        /** @var QueryBuilder $result */
        $result = DB::table('shop_product as P')
            ->select(DB::raw('P.id, GROUP_CONCAT(DISTINCT AO.code SEPARATOR "_") as code'))
            ->join('shop_product_options as PO', 'P.id', '=', 'PO.shop_product_id')
            ->join('shop_attribute_options as AO', 'AO.id', '=', 'PO.shop_product_option_id')
            ->join('shop_attributes as A', 'A.id', '=', 'PO.shop_attribute_id')
            ->where('P.visible', true)
            ->whereIn('A.code', ['voltazh', 'tsokol'])
            ->groupBy('P.id')
            ->get();
        return array_unique(array_column($result, 'code'));
    }


    public function getProductsByIds(array $ids)
    {
        return Product::with('photos', 'productType', 'options.productOptions', 'brand')
            ->with(['discount' => function ($q) {
                $q->where('active', '=', true);
            }])
            ->where('visible', true)
            ->whereIn('id', $ids)
            ->take(20)
            ->get();
    }

    public function getTopProducts()
    {
        return Product::with('photos', 'productType', 'options.productOptions', 'brand')
            ->with(['discount' => function ($q) {
                $q->where('active', '=', true);
            }])
            ->where('visible', true)->take(20)->orderBy('created_at', 'desc')->get();
    }


}