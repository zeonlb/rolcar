<?php namespace Modules\Shop\Repositories;


use App\Repository\Repository;
use Modules\Shop\Entities\Attribute;

class AttributeRepository extends Repository
{
    public function model()
    {
        return Attribute::class;
    }

    public function getNotUsedAttributes(array $usedIds = [])
    {
        return $this->model->whereNotIn('id', $usedIds)->get();
    }

    public function hasOptionsAttribute($attributeId)
    {
        return $this->model->find($attributeId)->options->count();
    }
}