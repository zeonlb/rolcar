$(document).ready(function () {

    $('.add-new-option-btn').on('click', function (e) {
        var countOfOptions = ++$('.new-option-block').length;
        console.log(countOfOptions);
        e.preventDefault();
        var html = '<div class="new-option-block">' +
            '<span class="section">Option ' + countOfOptions + '</span>' +
            '<div class="item form-group">' +
            '<label class="control-label col-md-3 col-sm-3 col-xs-12" for="option_name">Code</label>' +
            '<div class="col-md-6 col-sm-6 col-xs-12">' +
            '<input id="option_name" class="form-control col-md-7 col-xs-12" name="options[' + countOfOptions + '][code]" placeholder="white" type="text" /> ' +
            '</div>' +
            '</div>' +
            '<div class="item form-group">' +
            '<label class="control-label col-md-3 col-sm-3 col-xs-12" for="option_value">Name</label>' +
            '<div class="col-md-6 col-sm-6 col-xs-12">' +
            '<input id="option_value" class="form-control col-md-7 col-xs-12" name="options[' + countOfOptions + '][name]" placeholder="#FFFFFF" type="text"> ' +
            '</div> ' +
            '</div> ' +
            '</div>';
        countOfOptions++;
        $('.option-blocks').append(html);
    });

    $('.delete-attr-option').on('click', function(e) {
        e.preventDefault();
        var deleteAttrBlock = $(this).parent().parent();
        $.ajax({
            url: '/admin/shop/attribute/destroyOption',
            type: 'post',
            data: {'id' : $(this).attr('eid')},
            success: function(result) {
                if (result.status == 'SUCCESS') {
                    deleteAttrBlock.remove();
                }
            },
            error: function(){

                new PNotify({
                    title: 'Error!',
                    text: 'Could not remove option! System error!',
                    type: 'error'
                });

            }
        });
    })
});