$(function() {
    $( "ul.droptrue" ).sortable({
        connectWith: "ul"
    });
    $( "#sortable1, #sortable2, #sortable3" ).disableSelection();

    $('form').submit(function (e) {
        e.preventDefault();

        if ($('.product-enable input').length == 0) {
            $('.product-enable li').each(function(){
                var input = "<input type='hidden' name='attributes[]' value='"+$(this).attr('eid')+"' />";
                $(this).after(input);
            });
        }

        var submit = true;
        // evaluate the form using generic validaing
        if (!validator.checkAll($(this))) {
            submit = false;
        }

        if (submit)
            this.submit();
        return false;
    });
});
