$('document').ready(function(){
    selectedMotors = {};
        $('input.tableflat').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });

    $('#tab_content2 li').on('click', function(e){
        e.preventDefault();
    });

    function onAddTag(tag) {
        alert("Added a tag: " + tag);
    }

    function onRemoveTag(tag) {
        alert("Removed a tag: " + tag);
    }

    function onChangeTag(input, tag) {
        alert("Changed a tag: " + tag);
    }

    $(function () {
        $('#tags_1').tagsInput({
            width: 'auto'
        });
    });

    //Transport changed
    $('select[name="transport"]').on('change', function(){
        if ($(this).val()) {
            $.ajax({
                type : "POST",
                url : "/api/car/mark/find-by-transport-id",
                data : {'transportId' : $(this).val()},
                success : function(response) {
                    fillCarOptions('mark', response);
                },
                error : function(){
                    new PNotify({
                        title: 'Error!',
                        text: 'Could not get marks',
                        type: 'error'
                    });
                }
            });
        }
    });

    //Marks changed
    $('select[name="mark"]').on('change', function(){
        if ($(this).val()) {
            $.ajax({
                type : "POST",
                url : "/api/car/model/find-by-mark-id",
                data : {'markId' : $(this).val(), 'transportId' : $('select[name="transport"]').val()},
                success : function(response) {
                    fillCarOptions('model', response);
                },
                error : function(){
                    new PNotify({
                        title: 'Error!',
                        text: 'Could not get marks',
                        type: 'error'
                    });
                }
            });
        }
    });
    //Model changed
    $('select[name="model"]').on('change', function(){
        if ($(this).val()) {
            $.ajax({
                type : "POST",
                url : "/api/car/modification/find-by-model-id",
                data : {'modelId' : $(this).val()},
                success : function(response) {
                    fillCarOptions('modification', response);
                },
                error : function(){
                    new PNotify({
                        title: 'Error!',
                        text: 'Could not get marks',
                        type: 'error'
                    });
                }
            });
        }
    });
    //modification changed
    $('select[name="modification"]').on('change', function(){
        if ($(this).val()) {
            $.ajax({
                type : "POST",
                url : "/api/car/motor/find-by-modification-id",
                data : {'modificationId' : $(this).val()},
                success : function(response) {
                    $('#modification-list').each(function () {
                        $(this).dataTable().fnClearTable();
                        $(this).dataTable().fnDestroy();
                    });
                   for (var i in response) {
                       var html = '' +
                           '<tr class="even pointer">' +
                           '<td class="a-center ">' +
                           '<input type="checkbox" class="tablecheckbox innercheck" name="selected_motors" value="'+response[i].id+'">' +
                           '</td>';
                       html += '<td>'+response[i].id+'</td>'
                       html += '<td>'+response[i].motor+'</td>'
                       html += '<td>'+response[i].power+'</td>'
                       html += '<td>'+response[i].year_start+'</td>'
                       html += '<td>'+response[i].year_end+'</td>'



                       html += '</tr>';
                       $('#modification-list tbody').append(html);
                   }
                   tableInit();

                },
                error : function(){
                    new PNotify({
                        title: 'Error!',
                        text: 'Could not get marks',
                        type: 'error'
                    });
                }
            });
        }
    });

    function fillCarOptions(name, response)
    {
        var select = $('select[name="'+name+'"]');
        select.html('<option></option>>');
        for(var i in response) {
            select.append('<option value="'+response[i].id+'">'+response[i].name+'</option>');
        }
    }


    function tableInit() {

        countChecked();

        for (var i in selectedMotors) {
            $('.innercheck[value="'+selectedMotors[i]+'"]').attr('checked', 1);
            $('.innercheck[value="'+selectedMotors[i]+'"]').parent().parent().addClass('selected');
        }
        $('#check-all').removeAttr('checked');


        var oTable = $('#modification-list').dataTable({
            "oLanguage": {
                "sSearch": "Search all columns:"
            },
            "aoColumnDefs": [
                {
                    'bSortable': false,
                    'aTargets': [0]
                } //disables sorting for column one
            ],
            'iDisplayLength': 12,
            "sPaginationType": "full_numbers",
            "dom": 'T<"clear">lfrtip',
            "tableTools": {
                "sSwfPath": "<?php echo base_url('assets2/js/Datatables/tools/swf/copy_csv_xls_pdf.swf'); ?>"
            }
        });
        $("tfoot input").keyup(function () {
            /* Filter on the column based on the index of this element's parent <th> */
            oTable.fnFilter(this.value, $("tfoot th").index($(this).parent()));
        });
        $("tfoot input").each(function (i) {
            asInitVals[i] = this.value;
        });
        $("tfoot input").focus(function () {
            if (this.className == "search_init") {
                this.className = "";
                this.value = "";
            }
        });
        $("tfoot input").blur(function (i) {
            if (this.value == "") {
                this.className = "search_init";
                this.value = asInitVals[$("tfoot input").index(this)];
            }
        });



    }

    $('body').on('click', '.innercheck', function () {
        if ($(this).is(':checked')) {
            $(this).parent().parent().addClass('selected');
        } else {
            $(this).parent().parent().removeClass('selected');
        }
        countChecked();
        calcSelected();
    });
    $('#check-all').on('click', function () {

        $(this).parent().find('input:checkbox').not(this).prop('checked', this.checked);
        countChecked();
        calcSelected();

    })
    function countChecked() {


        var n = $(".innercheck:checked").length;
        if (n > 0) {
            $('.column-title').hide();
            $('.bulk-actions').show();
            $('.action-cnt').html(n + ' Records Selected');
        } else {
            $('.column-title').show();
            $('.bulk-actions').hide();
        }
    }

    function calcSelected()
    {
        $(".innercheck").each(function(){
            var key = $(this).val();
            if ($(this).is(':checked')) {
                if (!$(this).parent().parent().hasClass('selected')) {
                    $(this).parent().parent().addClass('selected');
                }
                selectedMotors[key] = key;
            } else {
                $(this).parent().parent().removeClass('selected');
                delete selectedMotors[key];
            }

        })
    }


        // initialize the validator function
        validator.message['date'] = 'not a real date';

        // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
        $('form')
            .on('blur', 'input[required], input.optional, select.required', validator.checkField)
            .on('change', 'select.required', validator.checkField)
            .on('keypress', 'input[required][pattern]', validator.keypress);

        $('.multi.required')
            .on('keyup blur', 'input', function () {
                validator.checkField.apply($(this).siblings().last()[0]);
            });
        <!-- /form validation -->
        $('form').submit(function (e) {
            e.preventDefault();
            var submit = true;
            var someRequired = true;

            $(this).find('input[required="required"], select[required="required"]').each(function(key, el) {
                if (!$(this).val()) {
                    someRequired = false;
                }
            });

            // evaluate the form using generic validaing
            if (!validator.checkAll($(this)) || !someRequired) {
                submit = false;
                if (!someRequired) {
                    new PNotify({
                        title: 'Error!',
                        text: 'Some required inputs is empty',
                        type: 'error'
                    });
                }
            }
            if (submit) {
                for (var i in selectedMotors) {
                    $(this).append('<input type="hidden" name="motors['+selectedMotors[i]+']" value="'+selectedMotors[i]+'" />');
                }
                this.submit();
            }
            return false;
        });
});

