$(document).ready(function(){
    $('.count-manipulate').on('click', function(){
        var pType = $(this).attr('ptype');
        var pId = $(this).attr('pid');
        var count = $("#uniqcode-" + pId  + " .input-box-number").val();
        var pCount = 1;

        if ( pType == 'minus' && count == 1 )
        {
            $(this).hide();
        }
        else
        {
            $("#uniqcode-" + pId  + " .button-minus").show();
            $.ajax({
                url: '/shop/checkout/countable/'+pId+'/'+pType+'/'+pCount,
                type: 'get',
                success: function (result) {
                    $("#uniqcode-" + result['cartItem']['1'] + " .input-box-number").val(result['cartItem']['0']);
                    $("#uniqcode-" + result['cartItem']['1'] + " .cart-item-sum span").text(result['cartItem']['2']);
                    $(".total-price-sum span").text(result['totalSum']);
                }
            }); 

        }
    })

    $('.count-value').on('input', function(){
        var pType = 'manual';
        var pId = $(this).attr('pid');
        var pCount = $(this).val();
        var count = $("#uniqcode-" + pId  + " .input-box-number").val();

        if ( count >= 1 )
        {
            $.ajax({
                url: '/shop/checkout/countable/'+pId+'/'+pType+'/'+pCount,
                type: 'get',
                success: function (result) {
                    $("#uniqcode-" + result['cartItem']['1'] + " .input-box-number").val(result['cartItem']['0']);
                    $("#uniqcode-" + result['cartItem']['1'] + " .cart-item-sum span").text(result['cartItem']['2']);
                    $(".total-price-sum span").text(result['totalSum']);
                }
            });
        }
    })

    $('.delete-from-cart').on('click', function(){
        var pId = $(this).attr('pid');
        $("#uniqcode-" + pId).remove();
        $.ajax({
            url: '/shop/checkout/cart/'+pId+'/delete',
            type: 'get',
            success: function (result) {
                 $(".total-price-sum span").text(result['cartTotalSum']);
            }
        });
    })
});