//DISCOUNT TIMER
$('document').ready(function(){
    if ($('.action-timer').length) {
        setInterval(function(){
            $('.action-timer').each(function() {
                var date = $(this).attr('date-value');
                var lostTime =  getTimeRemaining(date);
                if (lostTime.total >= 0) {
                    if (lostTime.hours < 10) lostTime.hours = '0'+lostTime.hours;
                    if (lostTime.minutes < 10) lostTime.minutes = '0'+lostTime.minutes;
                    if (lostTime.seconds < 10) lostTime.seconds = '0'+lostTime.seconds;
                    $(this).text(''+lostTime.days+' дн '+lostTime.hours+':'+lostTime.minutes+':'+lostTime.seconds)
                } else {
                    $(this).removeClass('action-timer');
                    $(this).text('finished');
                }

            })
        }, 1000)
    }


    function getTimeRemaining(endtime){
        var t = Date.parse(endtime) - Date.parse(new Date());
        var seconds = Math.floor( (t/1000) % 60 );
        var minutes = Math.floor( (t/1000/60) % 60 );
        var hours = Math.floor( (t/(1000*60*60)) % 24 );
        var days = Math.floor( t/(1000*60*60*24) );
        return {
            'total': t,
            'days': days,
            'hours': hours,
            'minutes': minutes,
            'seconds': seconds
        };
    }
})