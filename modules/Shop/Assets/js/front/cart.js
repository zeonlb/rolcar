ShopCart = {
    "selector" :  "#shop-cart-modal",
    "init" :  function () {
        $('.shop-cart-modal-dialog').show();
        $(this.selector).show();
        $.get('/shop/checkout/data', function (response) {
            $('.cart-modal-content').html(response);
        })
    },
    "close" :  function () {
        $(this.selector).hide();
    }
};

$(document).ready(function(){
    $('body').on('click', '#shop-cart-modal .count-manipulate', function(){
        var pType = $(this).attr('ptype');
        var pId = $(this).attr('pid');
        var pCount = 1;
        var url = '/shop/checkout/countable/'+pId+'/'+pType+'/'+pCount;
        $.get(url, function () {
            ShopCart.init();
        });

    });
    $('body').on('change', '#payment', function(){
        if ($(this).val() === 'part_in_installments') {
            $('#payment_parts_block').show();
        } else {
            $('#payment_parts_block').hide();
        }
    });
    $('body').on('change', '#shop-cart-modal .count-value',function () {
        var pType = 'manual';
        var pId = $(this).attr('pid');
        var pCount = $(this).val();
        var url = '/shop/checkout/countable/'+pId+'/'+pType+'/'+pCount;
        $.get(url, function () {
            ShopCart.init();
        });
    });
    $('body').on('click', '#shop-cart-modal .delete-from-cart', function(){
        var pId = $(this).attr('pid');
        var url = '/shop/checkout/cart/'+pId+'/delete';
        $.get(url, function () {
            ShopCart.init();
        });
    });
    $('body').on('click', '#shop-cart-modal .continue-shopping', function(){
        ShopCart.close()
    });

    $('.basket').click(function() {
        ShopCart.init()
    })
});
