$.filterUrlGenerator = {
    productUrl: '',
    filterValue: '',

    init: function (productUrl, filterData) {
        $.filterUrlGenerator.productUrl = productUrl + '{filter?}';
        $.filterUrlGenerator.formInit(productUrl);
        $.filterUrlGenerator.filterValue = filterData;
        for (var i in filterData) {
            for (var j in filterData[i]) {
                $('input[data-filter="' + i + '"][value="' + filterData[i][j] + '"]').prop('checked', true);
            }

        }
        $('.checkbox-label a').on('click', function (e) {
            e.preventDefault();
            $(this).parent().parent().find('input').trigger('click');
        });

        $('.chosen-filters .btn').on('click', function () {
            var code = $(this).attr('oCode');
            $('.product-filter input[value="' + code + '"]').trigger('click');
        })
        $('.chosen-filters .chosen-filters-reset-all').on('click', function () {
            var filteredUrl = $.filterUrlGenerator.productUrl.replace("{filter?}", '');
            window.location.href = filteredUrl;
        })

        $('.filter-show-all.fshow').on('click', function () {

            $(this).addClass('hidden');
            $(this).parent().find('.fhide').removeClass('hidden');
            $(this).parent().find('li').each(function (v, el) {
                if ($(this).hasClass('hidden')) {
                    $(this).addClass('show');
                }
                $(this).removeClass('hidden');
            })
        })
        $('.filter-show-all.fhide').on('click', function () {
            $(this).addClass('hidden');
            $(this).parent().find('.fshow').removeClass('hidden');
            $(this).parent().find('li').each(function (v, el) {
                if ($(this).hasClass('show')) {
                    $(this).addClass('hidden');
                }
                $(this).removeClass('show');
            })
        })

    },

    formInit: function (productUrl) {
        $('.product-filter input[type="checkbox"], .product-mobile-filter input[type="checkbox"]').on('click', function () {

            $.filterUrlGenerator.clearFilterValues();
            if ($(this).is(':checked')) {
                $.filterUrlGenerator.filterValue = $.filterUrlGenerator.pushKeyValueToArray($.filterUrlGenerator.filterValue, $(this).attr('data-filter'), $(this).val())
            }
            $.filterUrlGenerator.submitForm();
        });

    },
    submitForm: function (productUrl) {
        var basicString = '?filter=';
        var filterString = '';
        for (var i in $.filterUrlGenerator.filterValue) {
            if ($.filterUrlGenerator.filterValue[i].length) {
                filterString += i + '=' + $.filterUrlGenerator.filterValue[i].join('+') + ';'
            }
        }
        if (filterString) {
            filterString = basicString + filterString.substring(0, filterString.length - 1);
        }

        var filteredUrl = $.filterUrlGenerator.productUrl.replace("{filter?}", filterString);
        window.location.href = filteredUrl;
    },
    clearFilterValues: function () {
        var unchecked = [];
        $('.product-filter input[type="checkbox"]').each(function () {
            var isChecked = $(this).is(':checked');
            var filter = $(this).attr('data-filter');
            var value = $(this).val();
            if (!isChecked) {
                unchecked.push(filter + value);
            }

        })
        var newValues = [];
        var ex = $.filterUrlGenerator.filterValue;
        for (var i in ex) {
            for (var j in ex[i]) {
                var key = i + ex[i][j];
                if (unchecked.indexOf(key) < 0) {
                    newValues = this.pushKeyValueToArray(newValues, i, ex[i][j]);
                }
            }
        }
        $.filterUrlGenerator.filterValue = newValues
    },

    pushKeyValueToArray: function (array, key, value, rewrite = false) {
        if (!array[key] || rewrite) {
            array[key] = new Array(value);
        } else {
            array[key].push(value);
        }
        return array;
    }
}