

$(document).ready(function(){
    $('.product-category-content-desc p').jTruncate({
        "moreText" : 'Еще',
        "lessText" : 'Скрыть'

    });
    $('.product-list-sort').hover(
        function(){
            $('.product-list-sort-menu').stop( true, true ).slideDown();
            $('.product-list-sort .btn-left-arrow').hide();
            $('.product-list-sort .btn-left-arrow-hover').show();
        },
        function(){
            $('.product-list-sort-menu').stop( true, true ).slideUp();
            $('.product-list-sort .btn-left-arrow-hover').hide();
            $('.product-list-sort .btn-left-arrow').show();
        }
    );

    $('.product').hover(
        function(){
            $(this).addClass('product-hovered');
            $(this).find('.hovered-description').show();
        },
        function(){
            $(this).removeClass('product-hovered');
            $(this).find('.hovered-description').hide();
        }
    );

});
