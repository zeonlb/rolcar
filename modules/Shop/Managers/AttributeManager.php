<?php namespace Modules\Shop\Mangers;


use Illuminate\Support\Collection;
use Modules\Shop\Entities\Attribute;
use Modules\Shop\Entities\AttributeOption;
use Modules\Shop\Entities\Brand;
use Modules\Shop\Entities\Product;
use Modules\Shop\Repositories\AttributeRepository;

class AttributeManager
{
    protected $attributeRepository;
    protected $activeBrands = [];
    protected $chosenObject = [];

    /**
     * @return array
     */
    public function getActiveBrands(): array
    {
        return $this->activeBrands;
    }

    function __construct(AttributeRepository $attributeRepository)
    {
        $this->attributeRepository = $attributeRepository;
    }

    public function getFilteredAttributes($products)
    {
        $attributeCollection = new Collection();
        foreach ($products as $product) {
            $this->activeBrands[$product->brand->code] = $product->brand;
            $product->productType->attributes->each(function($attribute) use ($attributeCollection){
                if (count($attribute->options) && !$attribute->option && $attribute->filter) {
                    $attributeCollection->push($attribute);
                }
            });
        }
        $attributeCollection = $attributeCollection->sortBy('sort');
        return $attributeCollection->unique('id');
    }


    public function parseFilterString($filterStr)
    {
        $result = [];

        if ($filterStr) {
            $filters = explode(';', $filterStr);
            if (is_array($filters)) {
                foreach ($filters as $flt) {
                    $filterData = explode('=', $flt);
                    $filterKey = array_shift($filterData);

                    $result[$filterKey] = explode(' ', value(array_shift($filterData)));
                }
            }
        }
        return $result;
    }

    public function getFilterObject($filterData)
    {
            $attributes = Attribute::all()->keyBy('code');
            $options = AttributeOption::all()->keyBy('code');
            $brands = Brand::all()->keyBy('code');
            $result = [];
            foreach ($filterData as $fCode => $data) {
                if ($fCode === 'brand' || $fCode === 'brands') {
                    $br = new \stdClass();
                    $br->name = 'Бренд';
                    $br->code = 'brand';
                    $result[$fCode]['attribute'] = $br;
                    foreach ($data as $chosen) {
                        $result[$fCode]['options'][] = $brands[$chosen];
                    }
                } else
                    if (isset($attributes[$fCode])) {
                        $result[$fCode]['attribute'] = $attributes[$fCode];
                        foreach ($data as $chosen) {
                            $result[$fCode]['options'][] = $options[$chosen];
                        }
                    }
                }

            return $result;
    }
}