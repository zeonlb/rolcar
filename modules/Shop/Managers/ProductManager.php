<?php namespace Modules\Shop\Mangers;




use App\Http\Requests\Request;
use App\Traits\SaveFileTrait;
use DateTime;
use DB;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Event;
use Modules\Cms\Entities\File;
use Modules\Cms\Entities\Meta;
use Modules\Shop\Entities\Attribute;
use Modules\Shop\Entities\Category;
use Modules\Shop\Entities\Product;
use Modules\Shop\Entities\ProductDiscount;
use Modules\Shop\Entities\ProductOptions;
use Modules\Shop\Entities\ProductVideo;
use Modules\Shop\Repositories\AttributeRepository;
use Modules\Shop\Utils\Price\Processor;

class ProductManager
{
    use SaveFileTrait;


    protected $request;
    protected $product;
    protected $attributeRepository;

    function __construct(AttributeRepository $attributeRepository)
    {
        $this->attributeRepository = $attributeRepository;
    }

    /**
     * Transaction to create new product
     *
     * @param Request $request
     * @return Product
     */
    public function createNewProduct(Request $request)
    {
        $this->request = $request;
        /**
         * Create new product
         */
        $this->product = new Product();
        $this->product->fill($this->request->all());
        $this->product->shop_currency_id = $this->request->get('currency');
        $this->product->shop_product_type_id = $this->request->get('productType');
        $this->product->shop_brand_id = $this->request->get('brand_id');
        $this->product->save();


        $this->addAttributes();
        $this->addCategories();
        $this->addMeta();
        $this->addFiles();
        $this->updateVideo();
        $this->updateCrossSales();
        $this->updateDiscounts();

        Event::fire('shop.product.new', [$this->product, $request]);

        return $this->product;
    }

    /**
     * Transaction to create new product
     *
     * @param Product $product
     * @param Request $request
     */
    public function update(Product $product, Request $request)
    {

        $this->request = $request;

        /**
         * Create new product
         */
        $this->product = $product;
        $this->product->fill($this->request->all());

        $this->product->shop_currency_id = $this->request->get('currency');
        $this->product->shop_product_type_id = $this->request->get('productType');
        $this->product->shop_brand_id = $this->request->get('brand_id');

        if (!$request->get('pre_order_date')) {
            $this->product->pre_order_date = null;
        }


        $this->clearAttributes();
        $this->addAttributes();
        $this->updateCategories();
        $this->updateMeta();
        $this->updateFiles();
        $this->updateRozetkaFiles();
        $this->updateVideo();
        $this->updateCrossSales();
        $this->updateDiscounts();
        $this->updateIcon();

        $this->product->save();

        Event::fire('shop.product.update', [$this->product, $request]);
    }

    protected function updateMeta()
    {
        if ($this->product->meta) {
            $this->product->meta->fill($this->request->all());
            return $this->product->meta->save();
        }
        $meta = Meta::create($this->request->all());

        $this->product->cms_meta_id = $meta->id;
    }

    protected function updateVideo()
    {
        $productVideo = $this->request->get('video');
        if (!empty($productVideo)) {
            if ($this->product->video) {
                $this->product->video->fill($productVideo);
                return $this->product->video->save();
            }
            $video = new ProductVideo();
            $video->fill($productVideo);
            $video->product()->associate($this->product);
            
            return $video->save();
        }

    }

    protected function addMeta()
    {
        $meta = Meta::create($this->request->all());
       
        $this->product->meta()->associate($meta);
        $this->product->meta->save();
    }

    protected function addAttributes()
    {
        if (is_array($this->request->get('attributes'))) {
            $productOptions = [];
            foreach ($this->request->get('attributes') as $attributeId => $attributeValue) {
                /**
                 * If has attribute drop box or multiple drop box
                 */
                if ($this->attributeRepository->hasOptionsAttribute($attributeId))
                {
                    if (is_array($attributeValue)) {

                        foreach ($attributeValue as $option) {
                            $productOption = new ProductOptions();
                            $productOption->shop_product_id =  $this->product->id;
                            $productOption->shop_attribute_id =  $attributeId;
                            $productOption->shop_product_option_id =  $option;
                            $productOption->value =  json_encode($attributeValue);
                            if ($attributeValue != "") {
                                $productOptions[] = $productOption;
                            }
                        }

                    } else {
                        $productOption = new ProductOptions();
                        $productOption->shop_product_id =  $this->product->id;
                        $productOption->shop_attribute_id =  $attributeId;
                        $productOption->shop_product_option_id =  $attributeValue;
                        $productOption->value =  json_encode($attributeValue);
                        if ($attributeValue != "" && $attributeValue) {
                            $productOptions[] = $productOption;
                        }
                    }
                } else {
                    $productOption = new ProductOptions();
                    $productOption->shop_product_id =  $this->product->id;
                    $productOption->shop_attribute_id =  $attributeId;
                    $productOption->value =  json_encode($attributeValue);
                    if ($attributeValue != "") {
                        $productOptions[] = $productOption;
                    }
                }

            }
            $this->product->options()->saveMany($productOptions);
        }
    }


    protected function addCategories()
    {
        if (is_array($this->request->get('categories'))) {
            $categories = Category::whereIn('id', $this->request->get('categories'))->get();
            if ($categories) {
                $categoriesArray = [];
                foreach ($categories as $category) {
                    $categoriesArray[] = $category;
                }
                $this->product->categories()->saveMany($categoriesArray);
            }
        }
    }

    protected function updateCategories()
    {
        $categories = is_array($this->request->get('categories')) ? $this->request->get('categories') : [];
        $this->product->categories()->sync($categories);
    }

    protected function updateCrossSales()
    {
        $crossSales = is_array($this->request->get('selected_cross_sales')) ? $this->request->get('selected_cross_sales') : [];
        $this->product->crossSales()->sync($crossSales);

    }

    protected function updateDiscounts()
    {
        DB::table('shop_discounts')->where('shop_product_id', $this->product->id)->delete();

        $dData = $this->request->get('discount');
        if (!$dData['type'] || !$dData['date_start'] || !$dData['value']) {
            return false;
        }
        $dData['date_start'] = new DateTime($dData['date_start']);
        $dData['date_end'] = new DateTime($dData['date_end']);
        $dData['date_end']->setTime(23, 59, 0);
        if ($dData['date_end'] > new DateTime()) {
            $dData['active'] = true;
        }
        if ($this->product->discount) {
            $this->product->discount->fill($dData);
            return $this->product->discount->save();
        }

        $discount = new ProductDiscount;
        $discount->fill($dData);

        $discount->product()->associate($this->product);
        $discount->save();
    }

    protected function addFiles()
    {
        if (is_array($this->request->get('files'))) {
            $files = File::whereIn('id', $this->request->get('files'))->get();
            if ($files) {
                $filesArray = [];
                foreach ($files as $file) {
                    $filesArray[] = $file;
                }
                $this->product->photos()->saveMany($filesArray);
            }
        }
    }
    protected function updateFiles()
    {
        $fielsList = is_array($this->request->get('files')) ? $this->request->get('files') : [];
        $this->product->photos()->sync($fielsList);
    }

    protected function clearAttributes()
    {
        $this->product->options()->delete();
    }


    public static function getMinMaxProductsPrices(Collection $products) {
        $prices = [];
        $processor = new Processor();

        /** @var Product $product */
        foreach ($products as $product) {
            $prices[] = $processor->calculate($product)->actualPrice();
        }
        sort($prices);
        return [array_shift($prices), array_pop($prices)];
    }

    private function updateIcon()
    {
        if ($idIcon = $this->request->get('icon')) {
            $this->product->icon()->sync([$idIcon]);
        }
    }

    private function updateRozetkaFiles()
    {
        $fielsList = is_array($this->request->get('rozetka_files')) ? $this->request->get('rozetka_files') : [];
        $fData = [];
        foreach ($fielsList as $fId) {
            $fData[$fId] = ['is_rozetka' => true];
        }
        $this->product->rozetkaPhotos()->sync($fData);
    }
}