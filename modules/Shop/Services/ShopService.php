<?php
/**
 * Created by PhpStorm.
 * User: dat
 * Date: 06.06.17
 * Time: 22:16
 */

namespace Modules\Shop\Services;


use Illuminate\Database\Eloquent\Collection;
use Modules\Shop\Entities\CartStorage;
use Modules\Shop\Entities\Category;
use Modules\Shop\Entities\Currency;
use Modules\Shop\Entities\Product;
use Modules\Shop\Entities\ProductComment;
use Modules\Shop\Repositories\ProductRepository;
use Pingpong\Themes\ThemeFacade as Theme;

class ShopService
{

    public function getCartData(): array
    {
        $cartData = session()->get('shop.cart', []);
        if (!$cartData && auth()->check() && null === auth()->user()->role_id) {
            $c = CartStorage::active()->where('user_id', auth()->user()->getAuthIdentifier())->first();
            if ($c) {
                $dbData = $c->cart_data;

                $cartData = $dbData;
            }
        }
        return $cartData;
    }
}