<?php
/**
 * Created by PhpStorm.
 * User: dat
 * Date: 04.07.18
 * Time: 16:20
 */

namespace Modules\Shop\Services;

use Exception;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use InvalidArgumentException;
use Modules\Shop\Entities\Order;
use Modules\Shop\Entities\Payment;

class Private24Service
{
    private $apiUrl = 'https://payparts2.privatbank.ua';
    protected $storeId;
    protected $storePassword;
    protected $client;

    /**
     * Constructor.
     *
     * @param string $storeId
     * @param string $storePassword
     *
     * @throws InvalidArgumentException
     */
    public function __construct($storeId, $storePassword)
    {
        if (empty($storeId)) {
            throw new InvalidArgumentException('public_key is empty');
        }

        if (empty($storePassword)) {
            throw new InvalidArgumentException('private_key is empty');
        }

        $this->storeId = $storeId;
        $this->storePassword = $storePassword;
        $this->client = new Client([
            'base_uri' => $this->apiUrl,
        ]);
    }

    /**
     * Call API
     *
     * @param string $path
     * @param array $params
     * @param int $timeout
     *
     * @return string
     */
    public function create(Order $order, $partsCount = 2): array
    {
        $data = [
            "storeId" => $this->storeId,
            "orderId" => $order->invoice,
            "amount" => number_format($order->total_price, 2, '.', ''),
            "partsCount" => $partsCount,
            "merchantType" => $order->paymentRel->payment_type === Payment::PAYMENT_TYPE_PART_INSTALLMENTS ? "PP" : 'II',
            "responseUrl" => route('shop.order.payment.privat24', ['code' => $order->paymentRel->code]),
            "redirectUrl" => $successUrl = route('shop.order.payment.payment_info', ['code' => $order->paymentRel->access_code])
        ];
        $data["products"] = [];
        foreach ($order->orderDetails()->get() as $od) {
            $data["products"][] = [
                "name" => $od->product->name,
                "count" => $od->count,
                "price" => number_format($od->price, 2, '.', '')
            ];
        }
        $signData = [
            $this->storePassword,
            $this->storeId,
            $data['orderId'],
            strtr($data['amount'], ['.' => '', ',' => '']),
            $data['partsCount'],
            $data['merchantType'],
            $data['responseUrl'],
            $data['redirectUrl'],
            implode('+', array_map(function ($item) {
                return $item['name'] . $item['count'] . strtr($item['price'], ['.' => '', ',' => '']);
            }, $data['products'])),
            $this->storePassword];
        $data['signature'] = base64_encode(sha1(implode('', $signData), true));

        try {
            $response = $this->client->post('/ipp/v2/payment/create', [
                'headers' => [
                    'Accept' => 'application/json; charset=utf-8'
                ],
                'json' => $data
            ]);
            return json_decode($response->getBody()->getContents(), JSON_OBJECT_AS_ARRAY);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            throw $e;
        }

    }

    /**
     * Return last api response http code
     *
     * @return string|null
     */
    public function getResponseCode()
    {
        return $this->_server_response_code;
    }


}