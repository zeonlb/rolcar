<?php namespace Modules\Storage\Entities;

/**
 * Created by PhpStorm.
 * User: dat
 * Date: 21.01.18
 * Time: 20:22
 */
class File extends \Modules\Cms\Entities\File
{

    const DOWNLOAD_LINK_SALT = 'xxxxxx';

    protected $fillable = [
        'name',
        'title',
        'path',
        'sort',
        'for_storage'
    ];

    public function getDownloadLink() {
        return md5($this->id.self::DOWNLOAD_LINK_SALT);
    }
}