<?php
#Admin
Route::group([
    'prefix' => 'admin',
    'middleware' => ['admin.auth', 'admin.menu.storing'],
    'namespace' => 'Modules\Storage\Http\Controllers'], function () {
    Route::group(['prefix' => 'storage', 'namespace' => 'Admin'], function () {
        Route::resource('storage', 'StorageController');
        Route::resource('upload', 'UploadController');
        Route::post('storage/mark', ['as' => 'admin.storage.storage.mark', 'uses' => 'StorageController@mark']);
    });
});

Route::get('storage/download/{code}', ['as' => 'front.storage.download', 'uses' => 'Modules\Storage\Http\Controllers\Front\StorageController@download']);