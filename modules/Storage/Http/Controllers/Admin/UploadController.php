<?php namespace Modules\Storage\Http\Controllers\Admin;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Modules\Cms\Entities\File as FileEntity;
use Pingpong\Modules\Routing\Controller;

class UploadController extends Controller
{

    /**
     * Upload images
     *
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
        $rules = array(
            'file' => 'max:5000',
        );

        $validation = Validator::make($request->all(), $rules);

        if ($validation->fails()) {
            return Response::make($validation->errors->first(), 400);
        }

        $destinationPath = strtolower(sprintf("uploads/%s/", str_random(8)));

        if (!File::exists($destinationPath)) {
            File::makeDirectory($destinationPath, 0775, true);
        }
        $extension = $request->file('file')->getClientOriginalExtension(); // getting file extension
        $fileName = md5(uniqid()) . '.' . $extension; // renameing image
        $upload_success = $request->file('file')->move($destinationPath, $fileName); // uploading file to given path

        if ($upload_success) {
            $file = FileEntity::create(
                [
                    'path' => $destinationPath . $fileName,
                    'name' => $request->file('file')->getClientOriginalName(),
                    'title' => $request->file('file')->getClientOriginalName()
                ]
            );
            return Response::json(['fileId' => $file->id], 200);
        } else {
            return Response::json('error', 400);
        }
    }


}