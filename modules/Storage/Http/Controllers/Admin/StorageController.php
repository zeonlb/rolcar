<?php namespace Modules\Storage\Http\Controllers\Admin;

use App\Traits\SaveFileTrait;
use Illuminate\Http\Request;
use Modules\Storage\Entities\File;
use Pingpong\Modules\Routing\Controller;
use Illuminate\Support\Facades\View;

class StorageController extends Controller
{

    use SaveFileTrait;

    /**
     * @return mixed
     */
    public function index()
    {
        $files = File::where(['from_storage' => 1])->orderBy('id', 'desc')->paginate(50);
        return View::make('storage::index', compact('files'));
    }

    /**
     * @return mixed
     */
    public function create()
    {
        return View::make('shop::admin.banner.create');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $banner = Banner::findOrFail($id);
        return View::make('shop::admin.banner.edit', compact('banner'));
    }

    /**
     * @param $id
     * @return mixed
     */
    public function mark(Request $request)
    {
        if (!$id = $request->get('id')) {
            return ['status' => 'fail'];
        }
        $file = File::findOrFail($id);
        $file->from_storage = true;
        $file->save();
        return ['status' => 'success'];
    }

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        $banner = Banner::findOrFail($id);
        $banner->delete();

        return redirect()->route('admin.shop.banner.index')->with('success', 'Banner successfully deleted!');
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $banner = Banner::findOrFail($id);
        $banner->fill($request->all());

        if ($request->file('file')) {
            $banner->img = $this->saveFile($request);
        }
        $banner->save();

        return redirect()->route('admin.shop.banner.index')->with('success', 'Banner successfully updated');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {

        $banner = Banner::create($request->all());

        if ($request->file('file')) {
            $banner->img = $this->saveFile($request);
        }
        $banner->save();

        return redirect()->route('admin.shop.banner.index')->with('success', 'Banner successfully added');
    }

}