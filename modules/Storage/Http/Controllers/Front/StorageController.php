<?php namespace Modules\Storage\Http\Controllers\Front;

use App\Traits\SaveFileTrait;
use Illuminate\Http\Request;
use Modules\Storage\Entities\File;
use Pingpong\Modules\Routing\Controller;
use Illuminate\Support\Facades\View;

class StorageController extends Controller
{

    public function download($code)
    {
        $salt = File::DOWNLOAD_LINK_SALT;
        $file = File::whereRaw("MD5(CONCAT(id,'{$salt}')) = '$code'")->firstOrFail();
        $fPath = base_path('public/'.$file->path);
        if (!file_exists($fPath) || !is_readable($fPath)) {
            abort(404);
        }
        return response()->download(
            $fPath,
            $file->name
        );
    }

}