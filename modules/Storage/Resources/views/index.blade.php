@extends('admin::layouts.master')

@section('content')
    <h1>Storage</h1>
    <div>
        <div class="col-md-12">
            <div class="col-md-10"></div>
            <div class="col-md-2">
                <button type="button" class="btn btn-success" data-toggle="modal" data-target=".bs-example-modal-lg">
                    Upload
                </button>
                @include('storage::modals.upload')
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>List files</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content col-md-12">
                        @foreach($files as $file)
                            <div class="col-md-3 storage-block">
                                @if (file_exists($file->path) && is_int(strpos(mime_content_type($file->path), 'image')))
                                 <img width="200" src="{{url($file->path)}}" alt="{{$file->title}}">
                                @else
                                 <img width="200" src="{{Theme::asset('image/storage_file.png')}}" alt="{{$file->title}}">
                                @endif
                                <div class="storage-img-nav">
                                    <button data-id="{{$file->id}}" class="cp-dwn-btn btn-xs btn-success">Copy download link</button>
                                    <button data-id="{{$file->id}}" class="cp-btn btn-xs  btn-info">Copy</button>
                                    <form class="entity-delete-form" method="POST"
                                          action="{{route('cms.image.destroy', ['id' => $file->id])}}">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                                        <button type="submit" class="entity-delete btn-xs  btn-danger">Delete
                                        </button>
                                    </form>
                                    <textarea  id="cp-btn-{{$file->id}}" class="cp-area">{{url($file->path)}}</textarea>
                                    <textarea  id="cp-dwn-btn-{{$file->id}}" class="cp-area">{{url('/storage/download/'.$file->getDownloadLink())}}</textarea>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <?php echo $files->render(); ?>

            </div>
        </div>
    @stop

    @section('javascript')
        <!-- chart js -->
            <script src="{{Module::asset('admin:js/chartjs/chart.min.js')}}"></script>
            <!-- bootstrap progress js -->
            <script src="{{Module::asset('admin:js/progressbar/bootstrap-progressbar.min.js')}}"></script>
            <script src="{{Module::asset('admin:js/nicescroll/jquery.nicescroll.min.js')}}"></script>
            <!-- dropzone -->
            <script src="{{Module::asset('admin:js/dropzone/dropzone.js')}}"></script>
            <script type="text/javascript">
                var uploadUrl = "{{ route('admin.storage.upload.store') }}";
                var token = "{{ csrf_token() }}";
                Dropzone.autoDiscover = false;
                var myDropzone = new Dropzone("div#myDrop", {
                    url: uploadUrl,
                    addRemoveLinks: true,
                    dictRemoveFile: 'Remove',
                    maxFilesize: 5,
                    params: {
                        _token: token,
                        from_storage: true
                    }
                });
                myDropzone.on("success", function (file, resp) {
                    if (resp.fileId) {
                        $.ajax({
                            type : 'post',
                            url: '{{route('admin.storage.storage.mark')}}',
                            data: {id: resp.fileId}
                        })

                    }
                });
                myDropzone.on("removedfile", function (file) {
                    $.ajax({
                        type: 'delete',
                        url: '{{route('cms.image.destroy')}}',
                        data: {id: file.uniqueId}
                    });

                });
                $('body').on('click', '.close, .modal-backdrop', function(){
                    window.location.reload()
                })
                $('.cp-btn').on('click', function (event) {
                    var id = $(this).attr('data-id');
                    console.log(id)
                    console.log('#cp-btn-' + id)
                    var copyTextarea = document.querySelector('#cp-btn-' + id);
                    copyTextarea.select();

                    try {
                        var successful = document.execCommand('copy');
                        var msg = successful ? 'successful' : 'unsuccessful';
                        console.log('Copying text command was ' + msg);
                    } catch (err) {
                        console.log('Oops, unable to copy');
                    }
                });
                $('.cp-dwn-btn ').on('click', function (event) {
                    var id = $(this).attr('data-id');
                    console.log(id)
                    console.log('#cp-dwn-btn-' + id)
                    var copyTextarea = document.querySelector('#cp-dwn-btn-' + id);
                    copyTextarea.select();

                    try {
                        var successful = document.execCommand('copy');
                        var msg = successful ? 'successful' : 'unsuccessful';
                        console.log('Copying text command was ' + msg);
                    } catch (err) {
                        console.log('Oops, unable to copy');
                    }
                });
            </script>

            <script>
                $('.storage-block').hover(
                    function () {
                        $(this).find('.storage-img-nav').show();
                    },
                    function () {
                        $(this).find('.storage-img-nav').hide();
                    }
                );
            </script>

        @stop
        @section('style')
            <link href="{{Module::asset('storage:css/style.css')}}" rel="stylesheet">
            <link href="{{Module::asset('admin:css/dropzone.css')}}" rel="stylesheet">
@stop

