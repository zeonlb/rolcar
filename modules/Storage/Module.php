<?php

namespace Modules\Storage;

use Modules\Cms\Seo\SiteMapGenerator;
use Pingpong\Menus\MenuFacade as Menu;
use System\Module\ModuleInterface;

class Module implements ModuleInterface
{
    public function getAdminNavigation()
    {
        $menu = Menu::instance('admin.navigation');
        $menu->url(route('admin.storage.storage.index'), 'Storage ', 5, ['icon' => 'fa fa-database']);
    }

    public function getFrontNavigation()
    {
    }

    public function getDescription()
    {
        // TODO: Implement getDescription() method.
    }

    public function getName()
    {
        // TODO: Implement getName() method.
    }

    public function setSettingFields()
    {
        // TODO: Implement setSettings() method.
    }

    public function getSiteMapData(): SiteMapGenerator
    {
        return new SiteMapGenerator();
    }
}