<?php

namespace Modules\Cms\Managers;


use Modules\Cms\Entities\Configuration;

class CmsConfigManager
{
    public static $instance;
    public static $configs;

    public static function getInstance()
    {
        if (!self::$instance) {
            self::$configs = Configuration::all()->keyBy('code');
            self::$instance = new self();
        }
        return self::$instance;
    }
    public function get($key)
    {
       if (isset(self::$configs[$key]) && self::$configs[$key]) {
           return self::$configs[$key]->value;
       }
       return null;
    }

    private function __clone(){}
    private function __construct(){}
}