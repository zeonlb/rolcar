<?php

namespace Modules\Cms\System;


use Illuminate\Mail\MailServiceProvider;
use Illuminate\Support\Facades\Config;
use Modules\Cms\Managers\CmsConfigManager;

class MailSender
{

    public static $instance;
    public static $configManager;

    public static function getInstance()
    {
        if (!self::$instance) {
            self::$configManager = CmsConfigManager::getInstance();
            self::$instance = new self;
        }
        return self::$instance;
    }

    public function init()
    {
        $envManager = new EnvironmentManager();
        $envManager->set('MAIL_FROM_ADDRESS', self::$configManager->get('default_email_from'));
        $envManager->set('MAIL_FROM_NAME', self::$configManager->get('default_email_title'));
        if (self::$configManager->get('mandrill_token')) {
            $envManager->set('MAIL_DRIVER', 'mandrill');
            $envManager->set('MANDRILL_SECRET', self::$configManager->get('mandrill_token'));

        }
        elseif (self::$configManager->get('mail_smpt_host') &&
            self::$configManager->get('mail_smpt_port') &&
            self::$configManager->get('mail_smpt_username') &&
            self::$configManager->get('mail_smpt_password'))
        {
            $envManager->set('MAIL_DRIVER', 'smtp');
            $envManager->set('MAIL_HOST', self::$configManager->get('mail_smpt_host'));
            $envManager->set('MAIL_PORT', self::$configManager->get('mail_smpt_port'));
            $envManager->set('MAIL_USERNAME', self::$configManager->get('mail_smpt_username'));
            $envManager->set('MAIL_PASSWORD', self::$configManager->get('mail_smpt_password'));
        }
        $envManager->save();


       return true;
    }


    private function __clone(){}
    private function __construct(){}
}