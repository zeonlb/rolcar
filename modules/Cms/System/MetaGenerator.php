<?php
/**
 * Created by PhpStorm.
 * User: rovel
 * Date: 12.02.17
 * Time: 12:14
 */

namespace Modules\Cms\System;


use Modules\Cms\Entities\Meta;
use Modules\Cms\Entities\PageSettings;

class MetaGenerator
{
    public static function make(Meta $meta = null, string $pageCode = null, array $vars = []) : Meta
    {
        $seoData = null;
        if (null === $meta) {
            $meta = new Meta();
        }
        if (null !== $pageCode) {
            $seoData = PageSettings::where('page_code', $pageCode)->first();
            if (!$seoData) {
                $seoData = PageSettings::where('page_code', 'general')->first();
            }
            $seoDataAttr = $seoData->attributesToArray();
            foreach ($meta->attributesToArray() as $key => $value) {
                if ($value && array_key_exists($key, $seoDataAttr)) {
                    $seoData->setAttribute($key, $value);
                }
            }
        }
        if ($seoData) {
            $m = new Meta();
            $m->meta_title = self::prepareText($meta->meta_title ?: $seoData->seo_title, $vars);
            $m->meta_description = self::prepareText($meta->meta_description ?: $seoData->seo_description, $vars);
            $m->seo_h1 = self::prepareText($meta->seo_h1 ?: $seoData->seo_h1, $vars);
            $m->seo_text = self::prepareText($meta->seo_text ?: $seoData->seo_text, $vars);
            $m->seo_text_2 = self::prepareText($meta->seo_text_2 ?: $seoData->seo_text_2, $vars);
            $m->seo_footer_text = self::prepareText($meta->seo_footer_text ?: $seoData->seo_footer_text, $vars);
            return $m;
        }
        return new Meta();
    }

    private static function checkMetaData(Meta $meta)
    {
        return (bool)$meta->meta_title;
    }

    private static function prepareText($text, array $vars = [])
    {
        if ($text === null) {
            return $text;
        }
        $regExp = '#%([\w|]+)%+#';
        $replace = [];
        preg_match_all($regExp, $text, $out);
        list($original, $parsed) = $out;
        if ($original && $parsed) {
            foreach ($parsed as $key => $code) {
                $command = null;
                if (is_numeric(strpos($code, '|'))) {
                    list($code, $command) = explode('|', $code);
                }
                if (array_key_exists($code, $vars)) {
                    $value =  $vars[$code];
                    if ($command === 'lower') {
                        $value = mb_strtolower(mb_substr($value, 0, 1)) . mb_substr($value, 1);
                    }
                    if ($command === 'upper') {
                        $value =  mb_strtoupper(mb_substr($value, 0, 1)) . mb_substr($value, 1);
                    }
                    if ($command === 'cyrillic') {
                        $value = cyrillic($value);
                    }
                    $replace[$original[$key]] = $value;
                }
            }
        }
        if ($replace) {
            $text = strtr($text, $replace);
        }
        $regExp = '#\s?%([\w|]{0,})%#';
        $text = preg_replace($regExp, '', $text);
        return $text;
    }
}