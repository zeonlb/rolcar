<?php

namespace Modules\Cms\System;


class EnvironmentManager
{
    private $file;
    private $config;
    public function __construct($file = '.env')
    {
        $this->file = $file;
        $this->config = file_get_contents(base_path($file));
    }

    public function set($key, $value)
    {
     
        $key = strtoupper($key);
        $regular = '/('.$key.'=)(.+)/';
        if ((bool)preg_match($regular, $this->config)) {
            $this->config =
                preg_replace_callback(
                    $regular,
                    function($match) use ($value){
                        return $match[1].$value;
                    },
                    $this->config
                );
        }else {
            $this->config .= $key .'='.$value."\n";
        }

        return $this;
    }

    public function save()
    {
        file_put_contents(base_path($this->file), $this->config);
    }
}