<?php namespace Modules\Cms\System\Navigation;


use Modules\Cms\Repositories\NavigationRepository;
use Pingpong\Menus\MenuFacade as Menu;

/**
 * Class NavigationGenerator
 * Generating CMS menu using settings from table 'navigation'
 *
 * @package Modules\Cms\System\Navigation
 */
class NavigationGenerator
{
    protected $navRepo;

    function __construct()
    {
        $this->navRepo = new NavigationRepository();
    }

    public function generate()
    {
        $navigationData = $this->navRepo->getSorted();

        foreach ($navigationData as $navKey => $navData) {
            Menu::create($navKey, function ($menu) use ($navKey, $navData){
                $menu->enableOrdering();
                $menu->style($navKey);
                foreach ($navData as $nav) {
                    $menu->url($nav->url, $nav->alias, ['icon' => $nav->icon])->order($nav->sort);
                }
            });
        }
        return true;
    }

}