<?php namespace Modules\Cms\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Modules\Cms\Job\FinishSiteMapJob;
use Modules\Cms\Job\ProductSiteMpaJob;
use Modules\Cms\Job\SiteMapJob;
use Modules\Car\Entities\Transport;
use Modules\Cms\Entities\Navigation;
use Modules\Cms\Seo\SiteMapGenerator;
use Pingpong\Modules\Facades\Module;
use Symfony\Component\Process\Process;
use System\Module\ModuleInterface;

class SitemapCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'cms:sitemap-generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate sitemap';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        @unlink(base_path('public').'/sitemap.xml');
        $generator = new SiteMapGenerator('');
        $generator->sitemapFileName = "sitemap-main.xml";
        /** @var ModuleInterface $module */

        foreach (Module::all() as $module) {
            if ($module->isStatus(true)) {
                $moduleName = "Modules\\".ucfirst($module->getName())."\\Module";
                if (is_callable($moduleName."::getSiteMapData")) {
                    $mod = new $moduleName;
                    $sg = $mod->getSiteMapData();
                    if (count($sg->getUrls()) > 0) {
                        $sg->createSitemap();
                        $sg->writeSitemap();
                    }
                }
            }

        }
        $this->generateForMainMenu($generator);
        $generator->createSitemap();
        $generator->writeSitemap();

        $this->generateForCatalog();
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
        ];
    }

    /**
     * @param $generator
     */
    private function generateForMainMenu($generator)
    {
        $navigation = Navigation::where('menu_name', 'cms.main.navigation')->orderBy('sort', 'desc')->get();

        foreach ($navigation as $nav) {
            $generator->addUrl(url($nav->url), date('c'), 'daily', 1);
        }
    }

    private function generateForCatalog()
    {

        $transports = Transport::all();
        $generator = new SiteMapGenerator('');
        $generator->sitemapFileName = "sitemap-catalog-transports.xml";
        foreach ($transports as $transport) {
            $generator->addUrl(route('catalog.marks', [$transport->id]), date('c'), 'daily', 0.8);
        }
        $generator->createSitemap();
        $generator->writeSitemap();

        $generator = new SiteMapGenerator('');
        $generator->sitemapFileName = "sitemap-catalog-models.xml";
        $models = DB::select('select lib_car_transport_id, lib_car_mark_id from lib_car_model');
        foreach ($models as $model) {
            $generator->addUrl(route('catalog.models',
                [$model->lib_car_mark_id, $model->lib_car_transport_id]
            ), date('c'), 'daily', 0.8);
        }
        $generator->createSitemap();
        $generator->writeSitemap();
        unset($models);

        $generator = new SiteMapGenerator('');
        $generator->sitemapFileName = "sitemap-catalog-modifications.xml";
        $modifications = DB::select(
            'select DISTINCT lib_car_model_id as id from lib_car_modification'
        );

        foreach ($modifications as $modification) {
            $generator->addUrl(route('catalog.modifications',
                [$modification->id]
            ), date('c'), 'daily', 0.8);
        }
        $generator->createSitemap();
        $generator->writeSitemap();
        unset($modifications);

        $generator = new SiteMapGenerator('');
        $generator->sitemapFileName = "sitemap-catalog-motors.xml";
        $motors = DB::select(
            'select DISTINCT lib_car_modification_id as id from lib_car_motor'
        );

        foreach ($motors as $motor) {
            $generator->addUrl(route('catalog.motors',
                [$motor->id]
            ), date('c'), 'daily', 0.7);
        }
        $generator->createSitemap();
        $generator->writeSitemap();


        unset($motors);

        $generator = new SiteMapGenerator('');
        $generator->sitemapFileName = "sitemap-catalog-lamps.xml";
        $lamps = DB::select(
            'select DISTINCT lib_car_motor_id as id from lib_car_motor_lamps WHERE options <> "[]"'
        );

        foreach ($lamps as $lamp) {
            $generator->addUrl(route('catalog.lamps',
                [$lamp->id]
            ), date('c'), 'daily', 0.6);
        }
        unset($lamps);
        $generator->createSitemap();
        $generator->writeSitemap();


        $limit = 1000;
        $offset = 0;
        while (true) {
            $query = "SELECT DISTINCT lib_car_motor_id as id, options  FROM lib_car_motor_lamps  WHERE options <> '[]' LIMIT $offset,$limit";
            echo $query.PHP_EOL;
            $lamps = DB::select($query);
            if (count($lamps) > 0) {
                echo $offset.PHP_EOL;
                dispatch(new SiteMapJob($query, $offset));
            } else {
                break;
            }
            $offset += $limit;
        }
        dispatch(new ProductSiteMpaJob());
        dispatch(new FinishSiteMapJob());
        return ;
    }



}
