<?php namespace Modules\Cms\Console;

use Illuminate\Console\Command;
use Modules\Cms\Entities\User;
use Pingpong\Themes\ThemeFacade as Theme;

class InitCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'cms:init';

    protected $signature = 'cms:init';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Init CMS';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->info('Generate application key');
        $this->call('key:generate');
        $this->info('Run migrations');
        $this->call('migrate');
        $this->call('module:migrate', ['module' => 'admin']);
        $this->call('module:migrate', ['module' => 'cms']);
        $this->call('module:migrate');
        $this->info('Make static files symbol links');
        if (strtoupper(substr(PHP_OS, 0, 3)) !== 'WIN') {
            $this->call('module:assets', ['type' => 'symbol']);
            $this->call('theme:assets', ['type' => 'symbol']);
        } else {
            $this->call('module:assets');
            $this->call('theme:assets');
        }

        $this->info('Create main layout');
        $this->makeMasterLayout();
        //Create admin user
        while (false == $this->createAdminUser()) {
        }

        $this->info('-------============  READY  ============-------');


    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
        ];
    }

    private function makeMasterLayout()
    {
        $customLayout = Theme::getPath() . '/' . Theme::getCurrent() . '/views/layouts/master_custom.blade.php';
        if (!file_exists($customLayout)) {
            $masterData = file_get_contents(Theme::getPath() . '/' . Theme::getCurrent() . '/views/layouts/master.blade.php');
            file_put_contents($customLayout, $masterData);
        }
    }

    private function createAdminUser()
    {
        if ('no' === $this->choice('Create admin user ?', ['yes', 'no'])) {
            return true;
        }


        $email = $this->ask('Admin email:');

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $this->error('Wrong email');
            return false;
        }

        $password = $this->secret('Admin password:');
        $repeatPassword = $this->secret('Admin repeat password:');

        if ($repeatPassword !== $password) {
            $this->error('Repeat password should be the equal password');
            return false;
        }

        User::create([
            'name' => 'admin',
            'email' => $email,
            'password' => bcrypt($password),
            'role_id' => 1
        ]);
        $this->info('You can login to admin panel (/admin)');
        return true;

    }

}
