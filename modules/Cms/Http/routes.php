<?php

Route::group(['prefix' => 'cms', 'namespace' => 'Modules\Cms\Http\Controllers'], function()
{
	Route::resource('image', 'ImageController');
	Route::get('image/thumb/{url}', 'ImageController@thumb');
});

if (Schema::hasTable('cms_page')) {
    $pageRepo = new \Modules\Cms\Repositories\PageRepository();
    foreach($pageRepo->findActive() as $redirection)
    {

        if (!$redirection->controller) {
            Route::get($redirection->url, 'Modules\Cms\Http\Controllers\CmsController@index');
        } else {
            Route::get($redirection->url, $redirection->controller);
        }
    }
}


#Admin
Route::group(['prefix' => 'admin', 'middleware' => ['admin.auth', 'admin.menu.storing'], 'namespace' => 'Modules\Cms\Http\Controllers'], function()
{
    Route::group(['prefix' => 'contact', 'namespace' => 'Admin'], function() {
        Route::resource('requests', 'ContactRequestController', ['only' => ['index', 'show', 'update', 'destroy']]);
    });
});

Route::post('/contact_request/create', ['as' => 'cms.contact_request.create', 'uses' => 'Modules\Cms\Http\Controllers\Front\ContactRequestController@create']);
Route::post('request/recall/create', ['as' => 'cms.request.recall.create', 'uses' => 'Modules\Cms\Http\Controllers\Front\ReCallRequestController@create']);
