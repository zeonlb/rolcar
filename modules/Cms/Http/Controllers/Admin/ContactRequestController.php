<?php namespace Modules\Cms\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Modules\Cms\Entities\ContactRequest;
use Modules\Cms\Repositories\PageRepository;
use Pingpong\Modules\Routing\Controller;
use Illuminate\Support\Facades\View;

class ContactRequestController extends Controller {

	function __construct(PageRepository $pageRepo)
	{
		$this->pageRepo = $pageRepo;
	}

	public function index(Request $request)
	{
		$contactRequests = ContactRequest::orderBy('id', 'desc')->get();

		return View::make('cms::admin.contact_requests.list',
			compact('contactRequests'));
	}

	public function show($id)
	{
		$contactRequest = ContactRequest::findOrFail($id);

		return View::make('cms::admin.contact_requests.show',
			compact('contactRequest'));
	}

	public function update($id)
	{
		$contactRequest = ContactRequest::findOrFail($id);
		$contactRequest->processed = true;
		$contactRequest->save();
		return redirect()->back()->with('success', 'Successfully updated!');
	}

	public function destroy($id)
	{
		$contactRequest = ContactRequest::findOrFail($id);
		$contactRequest->delete();
		return redirect()->back()->with('success', 'Successfully deleted!');
	}
	
}