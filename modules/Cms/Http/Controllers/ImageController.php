<?php namespace Modules\Cms\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Modules\Cms\Http\Requests\FileDeleteRequest;
use Pingpong\Modules\Routing\Controller;
use \Modules\Cms\Entities\File as FileEntity;

class ImageController extends Controller
{

    /**
     *
     * Upload images
     *
     * @return mixed
     */
    public function show($id)
    {
        $article = FileEntity::findOrFail($id);

        return $article;
    }


    /**
	 * Upload images
	 *
	 * @param Request $request
	 * @return mixed
	 */
	public function store(Request $request)
	{
		$rules = array(
			'file' => 'image|max:50000',
		);

		$validation = Validator::make($request->all(), $rules);

		if ($validation->fails()) {
			return Response::make($validation->errors->first(), 400);
		}

		$destinationPath = strtolower(sprintf("uploads/%s/", str_random(8)));

		if (!File::exists($destinationPath)) {
			File::makeDirectory($destinationPath, 0775, true);
		}
		$extension = $request->file('file')->getClientOriginalExtension(); // getting file extension
		$fileName = md5(uniqid()) . '.' . $extension; // renameing image
		$upload_success = $request->file('file')->move($destinationPath, $fileName); // uploading file to given path

		if ($upload_success) {
			$file = FileEntity::create(
				[
					'path' => $destinationPath.$fileName,
					'name' => $request->file('file')->getClientOriginalName(),
					'title' => $request->file('file')->getClientOriginalName()
				]
			);
			return Response::json(['fileId' => $file->id], 200);
		} else {
			return Response::json('error', 400);
		}
	}


    /**
     * @param $id
     * @param FileDeleteRequest $request
     * @return mixed
     */
    public function destroy($id = false, Request $request)
    {
        $id = $request->get('id', $id);
            if ($id) {
                    $file = FileEntity::findOrFail($id);
                    File::delete($file->path);
                    $file->delete();
        if ($request->ajax() || $request->isJson()) {
            return Response::json('SUCCESS', 200);
        }
        return redirect()->back()->with('success', 'Successfully removed!');
            }
    }

}