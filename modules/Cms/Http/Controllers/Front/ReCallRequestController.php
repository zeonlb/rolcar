<?php namespace Modules\Cms\Http\Controllers\Front;

use Modules\Cms\Entities\ContactRequest;
use Modules\Cms\Http\Requests\ReCallCreateRequest;
use Pingpong\Modules\Routing\Controller;

class ReCallRequestController extends Controller {


	public function create(ReCallCreateRequest $request)
	{
		$postData = $request->all();
		$postData['type'] = ContactRequest::TYPE_RECALL;
		$postData['message'] = "Client need to recall. Phone: ".$request->get('phone');
        if ($request->get('message')) {
            $postData['message'] .= '. With comment: '.$request->get('message');
        }
		ContactRequest::create($postData);
        $successMessage = 'Спасибо, за заявку. Мы обяательно с Вами свяжемся!';
        if ($request->ajax()) {
            $request->session()->flash('success', $successMessage);
            return $successMessage;
        }

		return redirect()->back()->with('success', $successMessage);
	}
	
}