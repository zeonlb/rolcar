<?php namespace Modules\Cms\Http\Controllers\Front;

use Modules\Cms\Http\Requests\ContactCreateRequest;
use Modules\Cms\Entities\ContactRequest;
use Pingpong\Modules\Routing\Controller;
use Pingpong\Themes\ThemeFacade as Theme;

class ContactRequestController extends Controller
{


    public function create(ContactCreateRequest $request)
    {
        ContactRequest::create($request->all());

        return redirect()->back()->with('success', 'Спасибо, за заявку. Мы обяательно с Вами свяжемся!');
    }

}