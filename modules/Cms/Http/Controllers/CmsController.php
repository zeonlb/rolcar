<?php namespace Modules\Cms\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Blade;
use Illuminate\View\Compilers\BladeCompiler;
use Modules\Cms\Entities\PageSettings;
use Modules\Cms\Repositories\PageRepository;
use Modules\Cms\System\Navigation\NavigationGenerator;
use Pingpong\Modules\Routing\Controller;
use Pingpong\Themes\ThemeFacade as Theme;

class CmsController extends Controller {

	function __construct(PageRepository $pageRepo)
	{
		$this->pageRepo = $pageRepo;
	}
	public function index(Request $request)
	{
		$navigationGenerator =  new NavigationGenerator();
		$navigationGenerator->generate();
		$routeName = $request->getPathInfo();
		$pageInfo = $this->pageRepo->getPageInfo($routeName);
		$breadCrumbs = [];
		if ($pageInfo->show_bread_crumbs) {
			$breadCrumbs = [
				'Главная' => url('/'),
				$pageInfo->name => '/'
			];
		}

		$pageInfo->html = Blade::compileString($pageInfo->html);
		return Theme::view('page', compact('pageInfo', 'breadCrumbs'));
	}
	
}