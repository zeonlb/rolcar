<?php
namespace Modules\Cms\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Contracts\Validation\Validator;

class SkuSearchRequest extends Request{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'search' => 'required',
        ];
    }


    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'search.required' => 'поле "Поиска" обязательное'
        ];
    }
}