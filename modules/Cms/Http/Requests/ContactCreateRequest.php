<?php
namespace Modules\Cms\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Contracts\Validation\Validator;

class ContactCreateRequest extends Request{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|min:2',
            'email' => 'required|email',
            'message' => 'required|min:2',
        ];
    }
    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => '"Имя" обязательное поле',
            'email.required' => '"Email" обязательное поле',
            'email.email' => 'Поле "Email" имеет не верный формат',
            'message.required' => '"Сообщение" обязательное поле',
        ];
    }
}