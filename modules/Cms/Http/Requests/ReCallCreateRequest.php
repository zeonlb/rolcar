<?php
namespace Modules\Cms\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Contracts\Validation\Validator;

class ReCallCreateRequest extends Request{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'phone' => 'required|min:5|max:20',
        ];
    }
    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'phone.required' => '"Телефон" обязательное поле',
        ];
    }
}