<?php

namespace Modules\Cms\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Cms\System\MetaGenerator;

class SliderFile extends Model
{
    protected $table = 'cms_slider_j_photo';

    protected $fillable = [
        'url',
        'title',
        'sort',
        'image_1920',
        'image_768',
        'image_480',
        'photo_url',
        'slider_id',
    ];
}
