<?php
namespace Modules\Cms\Entities;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cms_roles';

    /**
     * @var array
     */
    protected $fillable = ['name', 'options'];

    protected $casts = [
        'options' => 'json'
    ];

    public static function getUrls($roles = [])
    {
        $urls = [];
        if (is_array($roles)){
            foreach ($roles as $key => $item) {
                foreach ($item as $k => $i) {
                    if (is_string($k)) {
                        $urls[] = $k;
                    }
                }

            }
        }
        return $urls;

    }

}
