<?php

namespace Modules\Cms\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed meta_title
 * @property string meta_description
 * @property string meta_keywords
 * @property string seo_text
 * @property string seo_text_2
 * @property string seo_footer_text
 * @property string seo_h1
 */
class Meta extends Model
{
    protected $table = 'cms_meta';

    protected $fillable = [
        'meta_title',
        'meta_description',
        'meta_keywords',
        'meta_redirect_url',
        'meta_robot_index',
        'meta_robot_follow',
        'seo_h1',
        'seo_text',
        'seo_text_2',
        'seo_footer_text',
    ];
}
