<?php

namespace Modules\Cms\Entities;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $table = 'cms_files';

    protected $fillable = [
        'name',
        'title',
        'path',
        'sort',
    ];

}
