<?php namespace Modules\Cms\Entities;
   
use Illuminate\Database\Eloquent\Model;

class NavigationGroup extends Model {

    protected $table = 'cms_group_navigation';
    public $timestamps = false;

    protected $fillable = [
        'name',
        'title',
    ];

}