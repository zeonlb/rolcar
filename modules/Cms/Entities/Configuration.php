<?php

namespace Modules\Cms\Entities;

use Illuminate\Database\Eloquent\Model;

class Configuration extends Model
{
    protected $table = 'cms_system_configs';

    protected $fillable = [
        'name',
        'code',
        'value',
        'group',
        'type',
    ];
}
