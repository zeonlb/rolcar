<?php namespace Modules\Cms\Entities;
   
use Illuminate\Database\Eloquent\Model;

class ContactRequest extends Model {

    const TYPE_CONTACT = 'contact';
    const TYPE_RECALL = 'recall';
    const STATUS_PROCESSED = 1;
    const STATUS_UNPROCESSED = 0;

    protected $table = 'cms_contact_requests';

    protected $fillable = [
        'name',
        'email',
        'message',
        'processed',
        'type',
    ];

}