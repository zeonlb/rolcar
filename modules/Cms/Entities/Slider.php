<?php

namespace Modules\Cms\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Cms\System\MetaGenerator;

class Slider extends Model
{
    protected $table = 'cms_slider';

    protected $fillable = [
        'name',
        'code',
        'sort'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function files()
    {
        return $this->hasMany(SliderFile::class, 'slider_id');
    }

}
