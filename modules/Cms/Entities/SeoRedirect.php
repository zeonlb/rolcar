<?php

namespace Modules\Cms\Entities;

use Illuminate\Database\Eloquent\Model;

class SeoRedirect extends Model
{
    protected $table = 'cms_seo_redirects';

    protected $fillable = [
        'old_url',
        'new_url',
    ];

}
