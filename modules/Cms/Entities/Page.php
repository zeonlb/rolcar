<?php

namespace Modules\Cms\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Cms\System\MetaGenerator;

class Page extends Model
{
    const STATUS_VISIBLE = 1;
    const STATUS_HIDDEN = 0;
    protected $table = 'cms_page';

    protected $fillable = [
        'name',
        'url',
        'html',
        'visible',
        'sort',
        'controller',
        'show_bread_crumbs',
        'javascript',
        'style',
        'meta_id'
    ];

    public function meta()
    {
        return $this->belongsTo(Meta::class);
    }


}
