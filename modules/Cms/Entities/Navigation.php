<?php namespace Modules\Cms\Entities;
   
use Illuminate\Database\Eloquent\Model;

class Navigation extends Model {

    protected $table = 'cms_navigation';
    public $timestamps = false;

    protected $fillable = [
        'cms_group_navigation_id',
        'alias',
        'url',
        'icon',
        'sort',
        'parent_id',
        'live',
    ];

	public function parent() {

        return $this->hasOne(Navigation::class, 'id', 'parent_id')->orderBy('sort');

    }

    public function children() {

        return $this->hasMany(Navigation::class, 'parent_id', 'id')->orderBy('sort');

    }  

	public static function tree() {

		return static::with(implode('.', array_fill(0, 100, 'children')))->where('parent_id', '=', '0')->orderBy('sort')->get();

	}

}