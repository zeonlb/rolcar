<?php

namespace Modules\Cms\Entities;

use Illuminate\Database\Eloquent\Model;

class PageSettings extends Model
{
    const PAGES_LIST = [
        'general' => 'Basic for all pages',
        'product_list_light' => 'Products list with light',
        'product_details' => 'Product details',
        'catalog_modifications' => 'Catalog modifications',
        'catalog_lamps' => 'Catalog lamps',
        'catalog_motors' => 'Catalog motors',
        'catalog_model' => 'Catalog model',
        'catalog_transport' => 'Catalog transport'
    ];

    protected $table = 'cms_page_settings';

    protected $fillable = [
        'page_code',
        'seo_title',
        'seo_description',
        'seo_h1',
        'seo_footer_text',
        'seo_text',
        'seo_text_2',
    ];
}
