<?php namespace Modules\Cmss\Forms;

use Kris\LaravelFormBuilder\Form;
use Modules\Car\Entities\Mark;
use Modules\Car\Entities\Transport;


class SeoForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('meta_title', 'text', [
                'rules' => 'min:2',
                'label' => 'Meta title'
            ])
            ->add('meta_keywords', 'text',
                ['rules' => 'min:2', 'class' => 'tags form-control', 'id' => 'tag_1'])
            ->add('meta_redirect_url', 'text', [
                'rules' => 'min:2',
                'label' => 'Redirect url'
            ]);
    }
}