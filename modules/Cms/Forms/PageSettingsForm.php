<?php namespace Modules\Cms\Forms;

use Kris\LaravelFormBuilder\Form;
use Modules\Car\Entities\Mark;
use Modules\Car\Entities\Transport;
use Modules\Cms\Entities\PageSettings;


class PageSettingsForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('page_code', 'select', [
                'choices' => PageSettings::PAGES_LIST,
                'selected' => function () {
                    return $this->getData('page_code');
                },
                'rules' => 'required|unique:cms_page_settings,page_code,' . request()->segment(4),
                'empty_value' => 'Select page'
            ])
            ->add('seo_title', 'text', [
                'rules' => 'min:2|max:255',
                'label' => 'SEO title'
            ])
            ->add('seo_h1', 'text', [
                'rules' => 'min:2|max:255',
                'label' => 'SEO H1'
            ])
            ->add('seo_description', 'textarea', [
                'rules' => 'min:2|max:500',
                'label' => 'SEO description'
            ])
            ->add('seo_text', 'textarea', [
                'rules' => 'min:2',
                'label' => 'SEO text',
                'attr' => ['id' => 'html']
            ])
            ->add('seo_text_2', 'textarea', [
                'rules' => 'min:2',
                'label' => 'SEO text second',
                'attr' => ['id' => 'html2']
            ])
            ->add('seo_footer_text', 'textarea', [
                'rules' => 'min:2',
                'label' => 'SEO footer text second',
                'attr' => ['id' => 'html2']
            ])
            ->add('submit', 'submit',
                [
                    'label' => 'Save',
                    'template' => 'admin::vendor.laravel-form-builder.button_modal',
                    'attr' => ['class' => 'btn btn-success']
                ]
            );
    }
}