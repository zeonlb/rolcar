<?php namespace Modules\Cms\Repositories;

use Modules\Cms\Entities\Navigation;

class NavigationRepository {

    public function getNavigationInfo($routeName)
    {
        return  Navigation::where('url', $routeName)->cacheTags(['cms.menu'])->remember(10)->get();
    }
    public function getSorted()
    {
        $result = [];
        foreach (Navigation::orderBy('sort', 'desc')->get() as $key => $navigation)
        {
            $result[$navigation->menu_name][$key] = $navigation;
        }
        return $result;
    }
}