<?php namespace Modules\Cms\Repositories;

use Modules\Cms\Entities\Page;

class PageRepository {

    public function getPageInfo($routeName)
    {
        return  Page::where('url', $routeName)->firstOrFail();
    }
    public function findActive()
    {
        return Page::where('visible', Page::STATUS_VISIBLE)->get();
    }
    public function all()
    {
        return  Page::all();
    }
}