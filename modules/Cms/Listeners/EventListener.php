<?php namespace Modules\Cms\Listeners;


use Modules\Cms\Core\Images\Resizer;

class ImageResizer
{
    /**
     * @param array $imageList
     * @param int $width
     */
    public function handle(array $imageList, $width = 1024)
    {
        /**
         * Resize photos
         */
        foreach ($imageList as $image) {
            Resizer::process($image, $width);
        }
    }

}