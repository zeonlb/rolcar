<?php

namespace Modules\Cms;

use Modules\Cms\Entities\ContactRequest;
use Modules\Cms\Seo\SiteMapGenerator;
use System\Module\ModuleInterface;
use Pingpong\Menus\MenuFacade as Menu;

class Module implements ModuleInterface
{

    public function getAdminNavigation()
    {
        $countHtml = null;
        $menu = Menu::instance('admin.navigation');
        $count = ContactRequest::where('processed', false)->count();
        if ($count) {
            $countHtml = '&nbsp&nbsp<span class="badge bg-green">' . $count . '</span>';
        }

        $menu->url('admin/contact/requests', 'Contact requests ' . $countHtml, 2, ['icon' => 'fa fa-inbox']);
//        $menu->url('admin/uploads', 'Uploads', 3, ['icon' => 'fa fa-file']);

    }

    public function getFrontNavigation()
    {
        // TODO: Implement getFrontNavigation() method.
    }

    public function getDescription()
    {
        // TODO: Implement getDescription() method.
    }

    public function getName()
    {
        // TODO: Implement getName() method.
    }

    public function setSettingFields()
    {
        // TODO: Implement setSettings() method.
    }

    public function getSiteMapData(): SiteMapGenerator
    {
        return new SiteMapGenerator();
    }
}