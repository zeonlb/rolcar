<?php

namespace Modules\Cms\Services;


use Modules\Cms\Entities\Navigation;
use Modules\Cms\Entities\NavigationGroup;

class NavigationService
{
    public function getMenu(string $name)
    {
        $group = NavigationGroup::where('name', $name)->first();
        if (!$group) {
            return [];
        }
        $items = Navigation::where('cms_group_navigation_id', $group->id)
            ->where('parent_id', '=', '0')
            ->orderBy('sort')
            ->get();

        return $items;
    }
}
