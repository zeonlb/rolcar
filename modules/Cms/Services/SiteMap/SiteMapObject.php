<?php
/**
 * Created by PhpStorm.
 * User: rovel
 * Date: 11.02.18
 * Time: 22:27
 */

namespace Modules\Cms\Services\SiteMap;


class SiteMapObject
{
    /** @var string  */
    private $name;
    /** @var string  */
    private $link;
    /**
     * @var array key=> value [name => url]
     */
    private $siteMapData = [];

    /**
     * SiteMapObject constructor.
     * @param string $name
     * @param string $link
     */
    public function __construct(string $name, string $link = '/')
    {
        $this->name = $name;
        $this->link = $link;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return array
     */
    public function getSiteMapData(): array
    {
        return $this->siteMapData;
    }

    /**
     * @param array $siteMapData
     */
    public function setSiteMapData(array $siteMapData)
    {
        $this->siteMapData = $siteMapData;
    }

    /**
     * @param string $name
     * @param string $url
     */
    public function addSiteMapData(string $name, string $url)
    {
        $this->siteMapData[$name] = $url;
    }

    /**
     * @return string
     */
    public function getLink(): string
    {
        return $this->link;
    }

    /**
     * @param string $link
     */
    public function setLink(string $link)
    {
        $this->link = $link;
    }

}