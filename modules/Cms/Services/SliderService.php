<?php
/**
 * Created by PhpStorm.
 * User: dat
 * Date: 06.06.17
 * Time: 22:16
 */

namespace Modules\Cms\Services;


use Modules\Cms\Entities\Slider;

class SliderService
{
    public function getSlider(string $name)
    {
        $slider = Slider::where('code', $name)->with(['files' => function($q) {
            $q->orderBy('sort');
        }])->first();
        if (!$slider) {
            return [];
        }
        return $slider->files;
    }
}
