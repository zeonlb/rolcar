<?php namespace Modules\Cms\Core\Images;

use Intervention\Image\Exception\NotSupportedException;
use Intervention\Image\Exception\NotReadableException;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;

/**
 * Class ThumbBuilder
 * @package Modules\Cms\Core\Images
 */

class ThumbBuilder
{
    public static function make($url, $width = 'auto', $height = 'auto')
    {
        $fullImageUrl = self::getImageFullPath($url);

        $url = self::getThumbPath($fullImageUrl, $width, $height);

        $path = explode('/', $url);
        array_pop($path);
        $path = implode('/', $path);
        if (!File::exists($url)) {
            try {
                $img = Image::make($fullImageUrl);
                if ($height == 'auto') {
                    $img->resize($width, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                } elseif ($width == 'auto') {
                    $img->resize(null, $height, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                } else {
                    $img->resize($width, $height);
                }

                if (!File::exists($path)) {
                    file_put_contents('/tmp/sd',$path);
                    if (!File::makeDirectory($path, 0775, true)) {
                        throw new \Exception('Could not upload thumb');
                    }
                }
                $img->save($url);
            } catch (NotSupportedException $e) {
                echo $e->getMessage();
            } catch (NotReadableException $e) {
                echo $e->getMessage();
            }
        }
        return $url;
    }

    public static function resize($path, $width, $height)
    {
        $thumbPath =self::getThumbPath($path, $width, $height);
        if (File::exists($thumbPath)) {
            return url($thumbPath);
        } else {
            $url = self::make($path, $width, $height);
            return url(self::clearImageUrl($url));
        }
    }

    public static function getImageFullPath($url) {
        if (strpos($url, 'uploads')) {
            $fullImageUrl = public_path('uploads') .'/'. $url;
        } else {
            $fullImageUrl = public_path() .'/'. $url;
        }
        return  $fullImageUrl;

    }
    public static function getThumbPath($url, $width, $height) {
        $dir = md5($width.$height);
        return  strtr($url, ['uploads' => "uploads/thumb/{$dir}"]);
    }
    public static function clearImageUrl($url)
    {
        return strtr($url, [public_path() => '']);
    }
}