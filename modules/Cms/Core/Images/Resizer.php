<?php namespace Modules\Cms\Core\Images;


use Intervention\Image\Exception\NotSupportedException;
use Intervention\Image\Exception\NotReadableException;
use Intervention\Image\Facades\Image;


class Resizer
{
    /**
     * @param $url path after upload folder
     * @param string $width
     * @param string $height
     */
    public static function process($url, $width = 'auto', $height = 'auto')
    {
        $fullImageUrl = public_path('uploads') .'/'. $url;
        if (is_readable($fullImageUrl)) {
            try {
                $img = Image::make($fullImageUrl);

                if ($height == 'auto') {
                    $img->resize($width, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                } elseif ($width == 'auto') {
                    $img->resize(null, $height, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                } else {
                    $img->resize($width, $height);
                }
                $img->save();
            } catch (NotSupportedException $e) {
                echo $e->getMessage();
            } catch (NotReadableException $e) {
                echo $e->getMessage();
            }
        }
    }
}