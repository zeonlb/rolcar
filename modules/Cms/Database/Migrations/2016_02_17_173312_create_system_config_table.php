<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSystemConfigTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_system_configs', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('code')->unique();
            $table->string('name')->nullable();
            $table->text('value')->nullable();
            $table->text('group')->nullable();
            $table->enum('type', ['text','date', 'textarea', 'checkbox'])->default('text');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cms_system_configs');
    }

}
