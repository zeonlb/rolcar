<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSeoSettingsBlock extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cms_page_settings', function (Blueprint $table) {
            $table->string('page_code')->after('id');
            $table->string('seo_h1')->after('seo_title');
            $table->string('seo_description', 500)->after('seo_title');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cms_page_settings', function (Blueprint $table) {

        });
    }

}
