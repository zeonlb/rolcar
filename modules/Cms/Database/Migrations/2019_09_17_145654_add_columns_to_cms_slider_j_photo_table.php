<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToCmsSliderJPhotoTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cms_slider_j_photo', function(Blueprint $table)
        {
		$table->dropColumn('photo_url');
		$table->string('image_1920')->nullable();
		$table->string('image_768')->nullable();
		$table->string('image_480')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cms_slider_j_photo', function(Blueprint $table)
        {

        });
    }

}
