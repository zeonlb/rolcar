<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Modules\Cms\Entities\Navigation;
use  Modules\Cms\Entities\Page;

class AddSkuPageTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $pageData = [
            'name' => 'Поиск по артиклу',
            'code' => 'sku',
            'url' => '/sku',
            'visible' => true,
            'sort' => 0,
            'html' => file_get_contents(base_path('source/seed').'/sku'),
            'controller' => 'Modules\Car\Http\Controllers\Front\SkuController@index'

        ];

        Page::create($pageData);
        $navigation =  new Navigation();
//        $navigation->menu_name = 'cms.main.navigation';
        $navigation->alias = 'Поиск по артикулу';
        $navigation->url = '/sku';
        $navigation->sort = 1;
        $navigation->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       $page =  Page::where('code', 'sku')->first();
       $page->delete();
    }

}
