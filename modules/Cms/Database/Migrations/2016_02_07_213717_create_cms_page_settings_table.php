<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCmsPageSettingsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_page_settings', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('seo_title');
            $table->mediumText('footer_seo_text');
            $table->mediumText('footer_seo_text_2');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cms_page_settings');
    }

}
