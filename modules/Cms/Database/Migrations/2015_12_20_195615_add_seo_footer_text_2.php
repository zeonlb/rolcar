<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSeoFooterText2 extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cms_meta', function(Blueprint $table)
        {
            $table->mediumText('seo_text_2')->after('seo_text')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cms_meta', function(Blueprint $table)
        {
            $table->dropColumn('seo_text_2');
        });
    }

}
