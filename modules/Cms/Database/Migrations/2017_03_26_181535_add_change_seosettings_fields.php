<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddChangeSeosettingsFields extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cms_page_settings', function(Blueprint $table)
        {
            $table->renameColumn('footer_seo_text', 'seo_text');
            $table->renameColumn('footer_seo_text_2', 'seo_text_2');
            $table->string('seo_footer_text', 500)->after('seo_h1');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('', function(Blueprint $table)
        {

        });
    }

}
