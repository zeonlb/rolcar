<?php

use Illuminate\Database\Migrations\Migration;

class CreateAddBasicSystemConfigsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $config = new \Modules\Cms\Entities\Configuration();
        $config->code = 'default_email_from';
        $config->name = 'System from email';
        $config->value = 'info@ecm.com';
        $config->save();

        $config = new \Modules\Cms\Entities\Configuration();
        $config->code = 'default_email_title';
        $config->name = 'Default email title';
        $config->value = 'ECM SYSTEM';
        $config->save();
        ;


        $config = new \Modules\Cms\Entities\Configuration();
        $config->code = 'mail_smpt_host';
        $config->name = 'Mail host';
        $config->value = '';
        $config->save();

        $config = new \Modules\Cms\Entities\Configuration();
        $config->code = 'mail_smpt_port';
        $config->name = 'Mail port';
        $config->value = '';
        $config->save();

        $config = new \Modules\Cms\Entities\Configuration();
        $config->code = 'mail_smpt_username';
        $config->name = 'Mail username';
        $config->value = '';
        $config->save();

        $config = new \Modules\Cms\Entities\Configuration();
        $config->code = 'mail_smpt_password';
        $config->name = 'Mail password';
        $config->value = '';
        $config->save();


        $config = new \Modules\Cms\Entities\Configuration();
        $config->code = 'mandrill_token';
        $config->name = 'Mandrill token';
        $config->value = '';
        $config->save();

        $config = new \Modules\Cms\Entities\Configuration();
        $config->code = 'google_analitis_token';
        $config->name = 'Google analitics token';
        $config->value = '';
        $config->save();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }

}
