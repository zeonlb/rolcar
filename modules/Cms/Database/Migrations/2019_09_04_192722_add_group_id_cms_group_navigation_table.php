<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddGroupIdCmsGroupNavigationTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cms_navigation', function (Blueprint $table) {
            $table->integer('cms_group_navigation_id')->nullable();
//            $table->dropColumn('menu_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cms_navigation', function (Blueprint $table) {

        });
    }

}
