<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdditionalColumnsPageTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cms_page', function(Blueprint $table)
        {
            $table->boolean('show_bread_crumbs')->default(false)->after('sort');
            $table->mediumText('javascript')->nullable()->after('sort');
            $table->mediumText('style')->nullable()->after('sort');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cms_page', function(Blueprint $table)
        {
            $table->dropColumn('show_bread_crumbs');
            $table->dropColumn('javascript');
            $table->dropColumn('style');

        });
    }

}
