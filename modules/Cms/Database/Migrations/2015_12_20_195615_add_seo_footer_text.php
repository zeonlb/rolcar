<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSeoFooterText extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cms_meta', function(Blueprint $table)
        {
            $table->mediumText('seo_footer_text')->after('meta_robot_follow');
            $table->mediumText('seo_text')->after('meta_robot_follow');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cms_meta', function(Blueprint $table)
        {
            $table->dropColumn('seo_text');
            $table->dropColumn('seo_footer_text');
        });
    }

}
