<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCmsSliders extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_slider', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('code')->unique();
            $table->integer('sort')->unsigned();
            $table->timestamps();
        });

        Schema::create('cms_slider_j_photo', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('slider_id')->unsigned();
	    $table->string('photo_url');
            $table->string('url');
            $table->string('title');
            $table->integer('sort');
	    $table->boolean('show_only_for_auth')->default(false);
            $table->timestamps();

            $table->foreign('slider_id')->references('id')->on('cms_slider')->onDelete('cascade')->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('', function(Blueprint $table)
        {

        });
    }

}
