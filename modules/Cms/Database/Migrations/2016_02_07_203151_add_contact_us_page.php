<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddContactUsPage extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $html = file_get_contents(base_path('source/seed').'/contact_us');
        $js = file_get_contents(base_path('source/seed').'/contact_us_js');
        $page = new \Modules\Cms\Entities\Page();
        $page->name = 'Контактная информация';
        $page->url = '/contact_us';
        $page->html = $html;
        $page->visible = true;
        $page->javascript = $js;
        $page->show_bread_crumbs = true;

        $page->save();

        $navigation =  new \Modules\Cms\Entities\Navigation();
        $navigation->menu_name = 'cms.main.navigation';
        $navigation->alias = 'Контактная информация';
        $navigation->url = '/contact_us';
        $navigation->sort = 4;
        $navigation->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }

}
