<?php
/**
 * Created by PhpStorm.
 * User: rovel
 * Date: 11.03.17
 * Time: 17:27
 */

namespace Modules\Cms\Job;


use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Modules\Cms\Seo\SiteMapGenerator;
use Modules\Shop\Repositories\ProductRepository;

class FinishSiteMapJob implements ShouldQueue, SelfHandling
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $query;
    protected $limit;


    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        echo "Generate main sitemap".PHP_EOL;
        $sitemapIndexHeader = '<?xml version="1.0" encoding="UTF-8"?>
                                <sitemapindex
                                    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                                    xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9
                                    http://www.sitemaps.org/schemas/sitemap/0.9/siteindex.xsd"
                                    xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
                              </sitemapindex>';
        $xml = new \SimpleXMLElement($sitemapIndexHeader);

        foreach (glob(base_path('public').'/sitemap-*.xml') as $xmlFile) {
            $data = explode('/',$xmlFile);
            $xmlFile = $data[count($data) - 1];
            $row = $xml->addChild('sitemap');
            $row->addChild('loc', url() . '/' . $xmlFile);
        }

        $xml->asXML(base_path('public') . '/sitemap.xml');
        echo "Generate main sitemap finished".PHP_EOL;
    }
}