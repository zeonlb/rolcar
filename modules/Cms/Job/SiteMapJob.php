<?php
/**
 * Created by PhpStorm.
 * User: rovel
 * Date: 11.03.17
 * Time: 17:27
 */

namespace Modules\Cms\Job;


use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Modules\Cms\Seo\SiteMapGenerator;
use Modules\Shop\Repositories\ProductRepository;

class SiteMapJob implements  ShouldQueue, SelfHandling
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $query;
    protected $limit;

    /**
     * SiteMapJob constructor.
     * @param $query
     * @param $limit
     */
    public function __construct($query, $limit)
    {
        $this->limit = $limit;
        $this->query = $query;
    }


    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        echo $this->limit.PHP_EOL;
        $generator = new SiteMapGenerator('');
        $generator->sitemapFileName = "sitemap-catalog-products-{$this->limit}.xml";
        $lamps = DB::select($this->query);
        $productRepo = new ProductRepository();
        $enOptions = $productRepo->getProductsFilterOptions();
        foreach ($lamps as $lamp) {
            $options = json_decode($lamp->options, JSON_OBJECT_AS_ARRAY);
            $codeData = DB::select(
                'SELECT CONCAT_WS("-",MARK.code, MODIF.code, MOTOR.id) as code
                 FROM `lib_car_motor` AS MOTOR 
                 INNER JOIN `lib_car_modification` as MODIF ON MODIF.id = MOTOR.lib_car_modification_id 
                 INNER JOIN `lib_car_model` as MODEL ON MODEL.id = MODIF.lib_car_model_id 
                 INNER JOIN `lib_car_mark` as MARK ON MARK.id = MODEL.lib_car_mark_id 
                 WHERE MOTOR.id=MOTOR.parent_id AND MOTOR.id = '.$lamp->id
            );
            if (!is_array($options) || !$codeData) {
                continue;
            }
            $attributes = [
                'category' => $codeData[0]->code,
            ];
            foreach ($options as $category => $option) {
                if (!is_array($option)) {
                    echo "OPTION IS NOT ARRAY ".PHP_EOL;
                    echo var_dump($option).PHP_EOL;
                    continue;
                }
                foreach ($option as $lType => $voltages) {
                    if (!is_array($voltages)) {
                        if (is_string($voltages)) {
                            $code = strtolower($lType.'_'.$voltages);
                            echo "<pre>"; print_r($code); echo "</pre>"; die;
                            if (in_array($code, $enOptions)) {
                                $attributes['naznachenie'] = $category;
                                $url = route('product.index', $attributes);
                                $generator->addUrl($url,  date('c'), 'daily', 1);
                            }
                        } else {
                            echo var_dump($voltages).PHP_EOL;
                            continue;

                        }
                    } else {
                        foreach ($voltages as $voltage) {
                            $code = strtr(strtolower($lType.'_'.$voltage), ['v' => '']);
                            if (in_array($code, $enOptions)) {
                                $attributes['naznachenie'] = $category;
                                $url = route('product.index', $attributes);
                                $generator->addUrl($url,  date('c'), 'daily', 1);
                            }
                        }
                    }

                }
            }
        }
        if ($generator->getUrls()) {
            $generator->createSitemap();
            $generator->writeSitemap();
        } else {
            echo "There is not added urls".PHP_EOL;
        }
    }
}