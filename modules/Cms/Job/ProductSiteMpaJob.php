<?php
/**
 * Created by PhpStorm.
 * User: rovel
 * Date: 11.03.17
 * Time: 17:27
 */

namespace Modules\Cms\Job;


use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Modules\Cms\Seo\SiteMapGenerator;
use Modules\Shop\Repositories\ProductRepository;

class ProductSiteMpaJob implements ShouldQueue, SelfHandling
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $query;
    protected $limit;


    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        echo "Products preparing....".PHP_EOL;
        $products = DB::select('select code from shop_product where visible = 1');
        $generator = new SiteMapGenerator('');
        $generator->sitemapFileName = "sitemap-products.xml";

        foreach ($products as $product) {
            $url = route('product.show', ['code' => $product->code]);
            echo $url.PHP_EOL;
            $generator->addUrl($url, date('c'), 'daily', 0.7);

        }
        $generator->createSitemap();
        $generator->writeSitemap();
        echo "Products finished".PHP_EOL;
    }
}