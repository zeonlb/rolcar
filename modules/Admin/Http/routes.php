<?php

Route::group(['prefix' => 'admin', 'middleware' => ['admin.auth', 'admin.menu.storing'], 'namespace' => 'Modules\Admin\Http\Controllers'], function()
{
	Route::get('/', 'AdminController@index');
	Route::get('/403', 'AdminController@denied');
	Route::controller('config', 'ConfigController');
	#page
	Route::group(['prefix' => 'cms', 'namespace' => 'Cms'], function() {
		Route::controller('page', 'PageController');
		Route::controller('navigation', 'NavigationController');
		Route::controller('navigationGroup', 'NavigationGroupController');
		Route::controller('layout', 'LayoutController');
		Route::controller('slider', 'SliderController');
	});
});

Route::get('admin/login', 'Modules\Admin\Http\Controllers\UserController@login');
Route::post('admin/login', 'Modules\Admin\Http\Controllers\UserController@postLogin');
Route::get('admin/logout', 'Modules\Admin\Http\Controllers\UserController@getLogOut');
