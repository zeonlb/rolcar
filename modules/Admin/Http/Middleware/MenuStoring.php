<?php namespace Modules\Admin\Http\Middleware;

use Closure;
use Pingpong\Modules\Facades\Module;

/**
 * Register Main menu for Admin Panel
 * Class MenuStoring
 *
 * @package Modules\Admin\Http\Middleware
 */
class MenuStoring
{
    public function handle($request, Closure $next)
    {
        foreach(Module::all() as $name => $module) {
            if ($module->isStatus(true)) {
                $moduleName = "Modules\\".ucfirst($module->getName())."\\Module";
                if (is_callable($moduleName."::getAdminNavigation")) {
                    $mod = new $moduleName;
                    $mod->getAdminNavigation();
                }
            }
        }
        return $next($request);
    }

}
