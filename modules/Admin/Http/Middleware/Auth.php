<?php namespace Modules\Admin\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Modules\Cms\Entities\Role;

class Auth
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    protected $blockedUrls = [];

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {

        $this->auth = $auth;
        if (\Illuminate\Support\Facades\Auth::check() && \Illuminate\Support\Facades\Auth::user()->role) {
            $roles = \Illuminate\Support\Facades\Auth::user()->role->options;
            $this->blockedUrls = Role::getUrls($roles);
        }
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->auth->guest()) {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('admin/login');
            }
        }
        foreach ($this->blockedUrls as $url) {
            if (is_numeric(strpos($request->url().'#', $url.'#')) && !is_numeric(strpos($request->url(), 'admin/403'))) {
                return redirect()->to('admin/403');
            }
        }

        return $next($request);
    }
    
}
