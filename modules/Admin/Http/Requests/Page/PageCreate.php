<?php
namespace Modules\Admin\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Contracts\Validation\Validator;

class PageCreate extends Request{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'url' => 'required|max:255',
            'visible' => 'required',
        ];
    }


    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => '"Name" is required',
            'url.required'  => '"Url" is required',
        ];
    }
}