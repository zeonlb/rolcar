<?php
namespace Modules\Admin\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Contracts\Validation\Validator;

class NavigationCreate extends Request{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'alias' => 'required|max:255',
            'url' => 'required|max:255',
            'lang' => 'required'
        ];
    }


    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'menu_name.required' => '"Menu name" is required',
            'alias.required'  => '"Alias" is required',
            'url.required'  => '"Url" is required',
        ];
    }
}