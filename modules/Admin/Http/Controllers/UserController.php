<?php namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Pingpong\Modules\Routing\Controller;

class UserController extends Controller
{

    /**
     * Admin panel login GET
     *
     * @return \Illuminate\View\View
     */
    public function login()
    {
        return view('admin::user.login');
    }

    /**
     * Handle an authentication attempt.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postLogin(Request $request)
    {
        $this->validate($request, [
            'email' => 'required', 'password' => 'required',
        ]);
        if (Auth::attempt(['email' => $request->get('email'), 'password' => $request->get('password')])) {
            return redirect('admin');
        }
        return redirect()->back()->withErrors('Wrong login or password!');
    }

    /**
     * Handle an authentication attempt.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getLogOut(Request $request)
    {
        Auth::logout();
        return redirect('/');
    }

}