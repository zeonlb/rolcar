<?php namespace Modules\Admin\Http\Controllers\Cms;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Modules\Cms\Entities\Navigation;
use Modules\Cms\Entities\NavigationGroup;
use Modules\Admin\Http\Requests\NavigationCreate;
use Pingpong\Modules\Routing\Controller;
use \Illuminate\Http\Request;

class NavigationController extends Controller {
	
	public function getIndex()
	{
		$groups = NavigationGroup::orderBy('name', 'DESC')->get();
		$navMenuNames = [];
		foreach ($groups as  $nav) {
			$navMenuNames[$nav->id] = array(
				'title' => $nav->title,
				'name' => $nav->name,
				'items' => Navigation::where('cms_group_navigation_id', $nav->id)
							->where('parent_id', '=', '0')
							->orderBy('sort')
							->get(),
			);
		}
		return View::make('admin::navigation.main', compact('navigation', 'navMenuNames','categories'));
	}

	public function postCreate(NavigationCreate $request)
	{
		$request->merge(["parent_id"=>"0"]);
		Navigation::create($request->all());
		return redirect()->back()->with('success', $request->get('alias').' menu was created!');
	}

	public function putUpdate(Request $request, $id)
	{
		$navigation =  Navigation::findOrFail($id);
		$navigation->alias = $request->get('alias');
		$navigation->url = $request->get('url');
		$navigation->sort = $request->get('sort');
		$navigation->parent_id = $request->get('parent_id');
		$navigation->save();
		return redirect()->back()->with('success', $navigation->alias.' menu was updated!');
	}

	public function postDelete(Request $request)
	{
		$id  = $request->get('id');
		$navigation =  Navigation::findOrFail($id);
		$navigation->delete();
		Session::flash('success', 'Navigation successfully deleted!');
		return response()->json(['status' => 'SUCCESS']);;
	}
}