<?php namespace Modules\Admin\Http\Controllers\Cms;

use Illuminate\Support\Facades\View;
use Pingpong\Modules\Routing\Controller;
use \Illuminate\Http\Request;
use Pingpong\Themes\ThemeFacade as Theme;

class LayoutController extends Controller {
	
	public function getIndex()
	{
		$customLayout = Theme::getPath().'/'.Theme::getCurrent().'/views/layouts/master_custom.blade.php';
		if (file_exists($customLayout)) {
			$layout = file_get_contents($customLayout);
		} else {
			$layout = file_get_contents(Theme::getPath().'/'.Theme::getCurrent().'/views/layouts/master.blade.php');
		}
		return View::make('admin::cms.layout.main',compact('layout'));
	}
	public function postUpdate(Request $request)
	{

		file_put_contents(Theme::getPath().'/'.Theme::getCurrent().'/views/layouts/master_custom.blade.php', html_entity_decode($request->get('layout')));
		return redirect()->back()->with('message', "Successfully updated!");
	}
}