<?php namespace Modules\Admin\Http\Controllers\Cms;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Modules\Cms\Entities\NavigationGroup;
use Modules\Cms\Entities\Navigation;
use Modules\Admin\Http\Requests\NavigationGroupCreate;
use Pingpong\Modules\Routing\Controller;
use \Illuminate\Http\Request;

class NavigationGroupController extends Controller {
	

	public function postCreate(NavigationGroupCreate $request)
	{
		NavigationGroup::create($request->all());
		return redirect()->back()->with('success', $request->get('alias').' menu was created!');
	}

	public function postDelete(Request $request)
	{
		$id  = $request->get('id');
		$navigationItemsCount = Navigation::where('cms_group_navigation_id', $id)->count();
		if ($navigationItemsCount == 0 )
		{
			$navigation =  NavigationGroup::findOrFail($id);
			$navigation->delete();
			Session::flash('success', 'Navigation successfully deleted!');
			return response()->json(['status' => 'SUCCESS']);
		}
		else
		{
			return redirect()->back();
		}

	}
}