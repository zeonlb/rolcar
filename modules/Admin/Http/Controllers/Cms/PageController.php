<?php namespace Modules\Admin\Http\Controllers\Cms;

use Illuminate\Support\Facades\View;
use Modules\Cms\Entities\Meta;
use Modules\Cms\Entities\Page;
use Modules\Admin\Http\Requests\PageCreate;
use Pingpong\Modules\Routing\Controller;

class PageController extends Controller {
	
	public function getIndex()
	{
		$pages = Page::all();
		return View::make('admin::page.list', compact('pages'));
	}

	public function getView($id)
	{
		$page = Page::findOrFail($id);
		if (!$page->meta) $page->meta = new Meta();

		return View::make('admin::page.view', compact('page'));
	}

	public function getSitemap()
	{
//		echo shell_exec(env('PHP_BIN').' '.base_path().'/artisan queue:listen > /dev/null 2>&1 &');
		echo shell_exec(env('PHP_BIN').' '.base_path().'/artisan cms:sitemap-generate  > /dev/null 2>&1 &');
		return redirect()->back();
	}

	public function putUpdate($id, PageCreate $request)
	{
		$postData = $request->all();
		$page = Page::findOrFail($id);
		if ($page->meta) {
			$page->meta->fill($postData);
			$page->meta->save();
		} else {
			$meta = Meta::create($postData);
			$page->meta()->associate($meta);
		}
		$page->fill($request->all());
		$page->save();
		return redirect()->back()->with('success', $request->get('name').' page was successfully updated!');
	}

	public function postIndex(PageCreate $request)
	{
		$page =  new Page();
		$meta = Meta::create($request->all());
		$page->meta()->associate($meta);
		$page->fill($request->all());
		if (!is_int(strpos($page->html, 'class="container"'))) {
			$page->html = '<div class="container">'. $request->get('html'). '</div>';
		}

		$page->save();

		return redirect()->back()->with('success', $request->get('name').' page was added!');
	}
	public function getCreate()
	{
		return View::make('admin::page.create');
	}
	
}