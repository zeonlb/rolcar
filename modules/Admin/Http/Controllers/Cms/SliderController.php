<?php namespace Modules\Admin\Http\Controllers\Cms;

use Illuminate\Support\Facades\View;
use Kris\LaravelFormBuilder\FormBuilder;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Modules\Admin\Forms\SliderForm;
use Modules\Admin\Http\Requests\SliderCreate;
use Modules\Cms\Entities\Slider;
use Modules\Cms\Entities\SliderFile;
use Pingpong\Modules\Routing\Controller;

class SliderController extends Controller
{
    use FormBuilderTrait;

    /**
     * SliderController constructor.
     * @param FormBuilder $formBuilder
     */
    public function __construct(FormBuilder $formBuilder)
    {
        $this->formBuilder = $formBuilder;
    }

    public function getIndex()
    {
        $sliders = Slider::all();
        $form = $this->formBuilder->create(SliderForm::class, [
            'method' => 'POST',
            'url' => url('admin/cms/slider'),
            'novalidate' => '',
            'class' => 'form-horizontal form-label-left'
        ]);

        return View::make('admin::cms.slider.list', compact('sliders', 'form'));
    }

    public function getView($id)
    {
        $slider = Slider::where('id', $id)->with(['files' => function ($q) {
            $q->orderBy('sort');
        }])->first();
        return View::make('admin::cms.slider.view', compact('slider'));
    }

    public function putUpdate($id, SliderCreate $request)
    {
        #dd($request->all());
        $slider = Slider::findOrFail($id);
        $code = $slider->code;
        $files = $request->get('files', []);
        \DB::table('cms_slider_j_photo')->where('slider_id', $id)->delete();
        $insertData = [];

        foreach ($files as $file) {

            if ($file['image_1920'] ?? false) 
            {
                $f = new SliderFile();
                $insertData[] = $f->fill($file);
            }
        }
        if ($insertData) {
            $slider->files()->saveMany($insertData);
        }

        $slider->fill($request->all());
        $slider->code = $code;
        $slider->save();
        return redirect()->back()->with('success', $request->get('name') . ' banner was successfully updated!');
    }

    public
    function postIndex(SliderCreate $request)
    {

        $slider = new Slider();
        $slider->fill($request->all());
        $slider->save();
        return redirect()->to('admin/cms/slider/view/' . $slider->id)->with('success', $request->get('name') . ' slider was added!');

    }


}