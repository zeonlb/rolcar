<?php namespace Modules\Admin\Http\Controllers;

use Modules\Admin\Forms\RoleForm;
use Modules\Admin\Forms\SeoRedirectForm;
use Modules\Cms\Entities\Role;
use Modules\Cms\Entities\SeoRedirect;
use Modules\Cms\Entities\User;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilder;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Modules\Admin\Forms\UserForm;
use Modules\Cms\Entities\Configuration;
use Modules\Cms\Entities\PageSettings;
use Modules\Cms\Forms\PageSettingsForm;
use Modules\Cms\System\MailSender;
use Pingpong\Menus\MenuFacade;
use Pingpong\Modules\Routing\Controller;

class ConfigController extends Controller
{
    use FormBuilderTrait;

    protected $formBuilder;

    public function __construct(FormBuilder $formBuilder)
    {
        $this->formBuilder = $formBuilder;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getUser()
    {
        $form = $this->formBuilder->create(UserForm::class, [
            'method' => 'POST',
            'url' => url('admin/config/user'),
            'novalidate' => '',
            'class' => 'form-horizontal form-label-left'
        ]);

        $users =  User::with('role')->get();
        return view('admin::config.users', compact('users', 'form'));
    }

    public function postUser(Request $request)
    {
        $form = $this->form(UserForm::class);

        // It will automatically use current request, get the rules, and do the validation
        if (!$form->isValid()) {
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }
        User::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => bcrypt($request->get('password')),
            'role_id' => $request->get('role_id'),
        ]);
        return redirect()->back()->with('success', 'User successfully created!');
    }

    public function deleteUser($id)
    {
        $user = User::findOrFail($id);
        $user->delete();

        return redirect()->back()->with('success', 'User successfully deleted!');
    }

    public function getSeo()
    {
        $pageSettings = PageSettings::orderBy('id', 'desc')->get();

        $form = $this->formBuilder->create(PageSettingsForm::class, [
            'method' => 'POST',
            'url' => url('admin/config/seo'),
            'novalidate' => '',
            'class' => 'form-horizontal form-label-left'
        ]);

        return view('admin::config.seo', compact('pageSettings', 'form'));
    }


    public function getSeoEdit($id) {

        $pageSettings = PageSettings::findOrFail($id);
        $form = $this->formBuilder->create(PageSettingsForm::class, [
            'model' => $pageSettings,
            'method' => 'PUT',
            'url' => url('admin/config/seo/'.$pageSettings->id),
            'novalidate' => '',
            'class' => 'form-horizontal form-label-left'
        ], $pageSettings->toArray());
            return view('admin::config.seo_edit', compact('pageSettings', 'form'));
    }

    public function deleteSeo($id) {

        $pageSettings = PageSettings::findOrFail($id);
        $pageSettings->delete();
        return redirect()->back()->with('success', 'SEO config successfully deleted!');
    }

    public function putSeo(Request $request, $id)
    {
        /** @var PageSettings $pageSettings */
        $pageSettings = PageSettings::findOrFail($id);
        $form = $this->form(PageSettingsForm::class);
        // It will automatically use current request, get the rules, and do the validation
        if (!$form->isValid()) {
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }
        $pageSettings->fill($request->all());
        $pageSettings->save();
        return redirect()->back()->with('success', 'Page seo successfully saved!');
    }


    public function postSeo(Request $request)
    {
        $form = $this->form(PageSettingsForm::class);

        // It will automatically use current request, get the rules, and do the validation
        if (!$form->isValid()) {
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }
        PageSettings::create($request->all());

        return redirect()->back()->with('success', 'Page seo successfully saved!');
    }



    public function getSystem()
    {
        $systemSettings = Configuration::all();
        $urlSave = url('admin/config/system') ;
        return view('admin::config.system', compact('systemSettings', 'urlSave'));
    }
    
    public function postSystem(Request $request)
    {
        $systemSettings = Configuration::whereIn('id', array_keys($request->get('config')))->get();

        foreach ( $systemSettings as $setting ) {
            $setting->value = $request->get('config')[$setting->id];
            $setting->save();
        }
        MailSender::getInstance()->init();
        return redirect()->back()->with('success', 'Settings successfully saved!');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getRole()
    {
        $form = $this->formBuilder->create(RoleForm::class, [
            'method' => 'POST',
            'url' => url('admin/config/role'),
            'novalidate' => '',
            'class' => 'form-horizontal form-label-left'
        ]);
        $roles =  Role::all();
        return view('admin::config.roles', compact('roles', 'form'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function postRole(Request $request)
    {
        $role = new Role();
        $role->name  =  $request->get('name');
        $role->save();
        return redirect()->back()->with('success', 'Role successfully saved!');
    }
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function putRole(Request $request)
    {
        $role = Role::findOrFail($request->get('role_id'));
        $role->options  =  $request->get('options');
        $role->save();
        return redirect()->back()->with('success', 'Role successfully saved!');
    }
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getRoleEdit($id)
    {
        $menu  = MenuFacade::instance('admin.navigation');
        $menuItems = $menu->getItems();
        $role = Role::findOrFail($id);
        return view('admin::config.roles_edit', compact('role', 'menuItems'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getSeoRedirect()
    {
        $redirects = SeoRedirect::orderBy('id', 'desc')->get();
        $form = $this->formBuilder->create(SeoRedirectForm::class, [
            'method' => 'POST',
            'url' => url('admin/config/seo-redirect'),
            'novalidate' => '',
            'class' => 'form-horizontal form-label-left'
        ]);
        return view('admin::config.seo_redirects', compact('redirects', 'form'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getSeoRedirectEdit($id)
    {
        $redirect = SeoRedirect::findOrFail($id);
        $form = $this->formBuilder->create(SeoRedirectForm::class, [
            'model' => $redirect,
            'method' => 'PUT',
            'url' => url('admin/config/seo-redirect/'.$redirect->id),
            'novalidate' => '',
            'class' => 'form-horizontal form-label-left'
        ], $redirect->toArray());

        return view('admin::config.seo_redirects_edit', compact('form', 'redirect'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function deleteSeoRedirect($id)
    {
        $redirect = SeoRedirect::findOrFail($id);
        $redirect->delete();

        return redirect()->back()->with('success', 'Entity successfully deleted!');
    }

    /**
     * @param Request $request
     * @param $id
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function putSeoRedirect(Request $request, $id)
    {
        /** @var PageSettings $pageSettings */
        $redirect = SeoRedirect::findOrFail($id);
        $form = $this->form(SeoRedirectForm::class);
        if (!$form->isValid()) {
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }
        $redirect->fill($this->prepareRedirects($request));
        $redirect->save();
        return redirect()->back()->with('success', 'Entity successfully saved!');
    }

    private function prepareRedirects(Request $request) {
        $old_url = strtr($request->get('old_url'), [url() => '']);
        $new_url = strtr($request->get('new_url'), [url() => '']);
        return compact('old_url', 'new_url');
    }
    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function postSeoRedirect(Request $request)
    {
        $form = $this->form(SeoRedirectForm::class);

        // It will automatically use current request, get the rules, and do the validation
        if (!$form->isValid()) {
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }
        SeoRedirect::create($this->prepareRedirects($request));
        return redirect()->back()->with('success', 'Entity successfully created!');
    }




}