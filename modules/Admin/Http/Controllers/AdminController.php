<?php namespace Modules\Admin\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Modules\Car\Entities\CaseQuestion;
use Modules\Car\Entities\LampJoinQuestion;
use Modules\Car\Entities\Lamps;
use Modules\Car\Entities\Modification;
use Modules\Car\Entities\Motor;
use Modules\Car\Repositories\LampRepository;
use Modules\Cms\Entities\ContactRequest;
use Modules\Shop\Entities\Attribute;
use Modules\Shop\Entities\AttributeOption;
use Modules\Shop\Entities\Order;
use Modules\Shop\Entities\Product;
use Modules\Shop\Entities\ProductOptions;
use Pingpong\Modules\Routing\Controller;

class AdminController extends Controller
{



    public function index(LampRepository $lampRepository)
    {
        $countRequests = ContactRequest::all();
        $products = Product::all();
        $orders = Order::all();
        $lastTenOrders = Order::orderBy('id', 'desc')->limit(5)->get();
        $ordersInMonth = [];
        $motors = Motor::all();
        $ordersNotNew = Order::where('status', '!=', 'new')->count();
        foreach ($orders as $order) {
            $ordersInMonth[$order->created_at->format('Y-m-d')][] = 1;
        }
        $totalFilledPercent = $lampRepository->getTotalFilledPercent();
        return View::make('admin::index',
            compact(
                'countRequests',
                'products',
                'orders',
                'motors',
                'ordersNotNew',
                'lastTenOrders',
                'totalFilledPercent',
                'ordersInMonth'
            )
        );
    }


    public function denied()
    {
        return View::make('admin::config.denied');
    }

}