<?php

namespace Modules\Admin;

use Modules\Cms\Seo\SiteMapGenerator;
use System\Module\ModuleInterface;
use Pingpong\Menus\MenuFacade as Menu;

class Module implements ModuleInterface
{

    public function getAdminNavigation()
    {
        Menu::create('admin.navigation', function ($menu) {
            $menu->style('admin-navigation');
            $menu->url('admin', 'Home', 0, ['icon' => 'fa fa-home']);
            $menu->dropdown('CMS', function ($sub) {
                $sub->url('admin/cms/page', 'Pages');
                $sub->url('admin/cms/layout', 'Layout');
                $sub->url('admin/cms/navigation', 'Navigation');
		$sub->url('admin/cms/slider', 'Banners');
            }, 2, ['icon' => 'fa fa-desktop']);
            $menu->dropdown('Configuration', function ($sub) {
                $sub->url('admin/config/user', 'Users');
                $sub->url('admin/config/role', 'Roles');
                $sub->url('admin/config/seo', 'Seo');
                $sub->url('admin/config/seo-redirect', '301 Redirect');
                $sub->url('admin/config/system', 'System');
            }, 99, ['icon' => 'fa fa-cog']);
        });

    }

    public function getFrontNavigation()
    {
        // TODO: Implement getFrontNavigation() method.
    }

    public function getDescription()
    {
        // TODO: Implement getDescription() method.
    }

    public function getName()
    {
        // TODO: Implement getName() method.
    }

    public function setSettingFields()
    {
        // TODO: Implement setSettings() method.
    }

    public function getSiteMapData(): SiteMapGenerator
    {
        return new SiteMapGenerator();
    }
}
