<?php namespace Modules\Admin\Forms;

use Kris\LaravelFormBuilder\Form;


class RoleForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('name', 'text', [
                'rules' => 'required|min:2',
                'label' => 'Name'
            ])
            ->add('submit', 'submit',
                [
                    'label' => 'Create role',
                    'template' => 'admin::vendor.laravel-form-builder.button_modal',
                    'attr' => ['class' => 'btn btn-success']
                ]
            );
    }
}