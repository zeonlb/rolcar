<?php namespace Modules\Admin\Forms;

use Kris\LaravelFormBuilder\Form;
use Modules\Cms\Entities\Role;


class SliderForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('name', 'text', [
                'rules' => 'required|min:2',
                'label' => 'Name'
            ])
            ->add('code', 'text', [
                    'rules' => 'required',
                    'label' => 'Code'
                ])
            ->add('submit', 'submit',
                [
                    'label' => 'Create',
                    'template' => 'admin::vendor.laravel-form-builder.button_modal',
                    'attr' => ['class' => 'btn btn-success']]);
    }
}