<?php namespace Modules\Admin\Forms;

use Kris\LaravelFormBuilder\Form;
use Modules\Car\Entities\Mark;
use Modules\Car\Entities\Transport;
use Modules\Cms\Entities\Role;


class UserForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('name', 'text', [
                'rules' => 'required|min:2',
                'label' => 'Name'
            ])
            ->add('email', 'email', [
                    'rules' => 'required|email|unique:users,email',
                    'label' => 'Email'
                ])
            ->add('password', 'password', [
                    'rules' => 'required|min:7|confirmed',
                    'label' => 'Password'
                ])
            ->add('password_confirmation', 'password', [
                    'rules' => 'required|min:7',
                    'label' => 'Confirm password'
                ])
            ->add('role_id', 'entity', [
                'class' => Role::class,
                'label' => 'Role',
                'rules' => 'required',
                'property' => 'name',
                'query_builder' => function (Role $role) {
                    return $role->all();
                }
            ])
            ->add('submit', 'submit',
                [
                    'label' => 'Create user',
                    'template' => 'admin::vendor.laravel-form-builder.button_modal',
                    'attr' => ['class' => 'btn btn-success']]);
    }
}