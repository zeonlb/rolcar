<?php namespace Modules\Admin\Forms;

use Kris\LaravelFormBuilder\Form;


class SeoRedirectForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('old_url', 'text', [
                'rules' => 'required|min:2|max:255|unique:cms_seo_redirects,old_url,' . request()->segment(4),
                'label' => 'Old URL',
                'attr' => ['placeholder' => '/our-old-url']
            ])
            ->add('new_url', 'text', [
                'rules' => 'required|min:2|max:255',
                'label' => 'New URL',
                'attr' => ['placeholder' => '/our-new-url']
            ])
            ->add('submit', 'submit',
                [
                    'label' => 'Save',
                    'template' => 'admin::vendor.laravel-form-builder.button_modal',
                    'attr' => ['class' => 'btn btn-success']
                ]
            );
    }

}