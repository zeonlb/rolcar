<?php namespace Modules\Admin\Presenters;

use Illuminate\Support\Facades\Auth;
use Pingpong\Menus\MenuItem;
use Pingpong\Menus\Presenters\Presenter;

class MainNavigation extends Presenter
{
    private $roles = [];

    /**
     * MainNavigation constructor.
     */
    public function __construct()
    {
        $this->roles = (Auth::user()->role) ? Auth::user()->role->options : [];
        $this->roles = is_array($this->roles) ? $this->roles : [];
    }

    /**
     * {@inheritdoc }.
     */
    public function getOpenTagWrapper()
    {
        return PHP_EOL.'<ul class="nav side-menu">'.PHP_EOL;
    }

    /**
     * {@inheritdoc }.
     */
    public function getCloseTagWrapper()
    {
        return PHP_EOL.'</ul>'.PHP_EOL;
    }

    /**
     * {@inheritdoc }.
     */
    public function getMenuWithoutDropdownWrapper($item)
    {
        $title = clearMenuTags($item->title);
        if (array_key_exists($title, $this->roles)) {
            if (array_key_exists(0, $this->roles[$title]) ||
                array_key_exists($item->url, $this->roles[$title])
            ) {
                return false;
            }
        }
        return '<li'.$this->getActiveState($item).'><a href="'.$item->getUrl().'" '.$item->getAttributes().'>'.$item->getIcon().' '.$item->title.'</a></li>'.PHP_EOL;
    }

    /**
     * {@inheritdoc }.
     */
    public function getActiveState($item, $state = ' class="active"')
    {
        return $item->isActive() ? $state : null;
    }

    /**
     * Get active state on child items.
     *
     * @param $item
     * @param string $state
     *
     * @return null|string
     */
    public function getActiveStateOnChild($item, $state = 'active')
    {
        return $item->hasActiveOnChild() ? $state : null;
    }

    /**
     * {@inheritdoc }.
     */
    public function getDividerWrapper()
    {
        return '<li class="divider"></li>';
    }

    /**
     * {@inheritdoc }.
     */
    public function getHeaderWrapper($item)
    {
        return '<li><a><i class="fa fa-home"></i> '.$item->title.' <span class="fa fa-chevron-down"></span></a>';
    }

    /**
     * {@inheritdoc }.
     */
    public function getMenuWithDropDownWrapper($item)
    {
        $title = clearMenuTags($item->title);
        if (array_key_exists($title, $this->roles)) {
            if (array_key_exists(0, $this->roles[$title])) {
                return false;
            }
        }
        return '<li><a>'.$item->getIcon().' '.$item->title.' <span class="fa fa-chevron-down"></span></a>'.$this->getMultiLevelDropdownWrapper($item).'</li>';
    }

    /**
     * Get multilevel menu wrapper.
     *
     * @param \Pingpong\Menus\MenuItem $item
     *
     * @return string`
     */
    public function getMultiLevelDropdownWrapper($item)
    {
        $title = clearMenuTags($item->title);
        if (array_key_exists($title, $this->roles)) {
            if (array_key_exists(0, $this->roles[$title])) {
                return false;
            }
        }
        return '<ul class="nav child_menu '.$this->getActiveStateOnChild($item, ' active').
        '" style="">
        '.$this->getChildMenuItems($item).'</ul></li>'.PHP_EOL;

    }


    /**
     * Get child menu items.
     *
     * @param \Pingpong\Menus\MenuItem $item
     *
     * @return string
     */
    public function getChildMenuItems(MenuItem $item)
    {
        $results = '';
        foreach ($item->getChilds() as $child) {
            if ($child->hidden()) {
                continue;
            }
            if (isset($this->roles[clearMenuTags($item->title)][$child->url])) {
                continue;
            }


            if ($child->hasSubMenu()) {
                $results .= $this->getMultiLevelDropdownWrapper($child);
            } elseif ($child->isHeader()) {
                $results .= $this->getHeaderWrapper($child);
            } elseif ($child->isDivider()) {
                $results .= $this->getDividerWrapper();
            } else {
                $results .= $this->getMenuWithoutDropdownWrapper($child);
            }
        }

        return $results;
    }
}