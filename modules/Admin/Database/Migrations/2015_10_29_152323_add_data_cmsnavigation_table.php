<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDataCmsnavigationTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Modules\Cms\Entities\Navigation::create(
            [
                'menu_name' => 'cms.main.navigation',
                'alias' => 'Main',
                'url' => '/',
            ]
        );
        \Modules\Cms\Entities\Navigation::create(
            [
                'menu_name' => 'cms.footer.navigation',
                'alias' => 'Main',
                'url' => '/',
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }

}
