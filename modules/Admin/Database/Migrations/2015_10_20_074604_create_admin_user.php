<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Modules\Cms\Entities\User;

class CreateAdminUser extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         User::create([
            'name' => 'admin',
            'email' => 'admin@ecm.ua',
            'password' => bcrypt('root'),
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        User::where('name', 'admin')->delete();
    }

}
