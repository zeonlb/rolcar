@extends('admin::layouts.master')

@section('content')
    <!-- top tiles -->
    <div class="row tile_count">
        <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count">
            <div class="left"></div>
            <div class="right">
                <span class="count_top"><i class="fa fa-user"></i> Total requests</span>

                <div class="count">{{$countRequests->count()}}</div>
            </div>
        </div>
        <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count">
            <div class="left"></div>
            <div class="right">
                <span class="count_top"><i class="fa fa-user"></i> New requests</span>

                <div class="count">{{ $countRequests->where('processed', 0)->count() }}</div>
            </div>
        </div>
        <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count">
            <div class="left"></div>
            <div class="right">
                <span class="count_top"><i class="fa fa-dollar"></i> Total orders</span>

                <div class="count green">{{$orders->count()}}</div>
            </div>
        </div>
        <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count">
            <div class="left"></div>
            <div class="right">
                <span class="count_top"><i class="fa fa-dollar"></i> New orders</span>

                <div class="count">{{$orders->where('status', 'new')->count()}}</div>
            </div>
        </div>
        <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count">
            <div class="left"></div>
            <div class="right">
                <span class="count_top"><i class="fa fa-dollar"></i> Orders in progress</span>

                <div class="count">{{$orders->where('status', 'in_progress')->count()}}</div>
            </div>
        </div>
        <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count">
            <div class="left"></div>
            <div class="right">
                <span class="count_top"><i class="fa fa-shopping-cart"></i> Total products</span>

                <div class="count">{{$products->count()}}</div>
            </div>
        </div>

    </div>
    <!-- /top tiles -->

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="dashboard_graph">

                <div class="row x_title">
                    <div class="col-md-6">
                        <h3>Orders
                        </h3>
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div id="placeholder33" style="height: 260px; display: none" class="demo-placeholder"></div>
                    <div style="width: 100%;">
                        <div id="canvas_dahs" class="demo-placeholder" style="width: 100%; height:270px;"></div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

    </div>
    <br/>

    <div class="row">


        <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="x_panel tile fixed_height_320">
                <div class="x_title">
                    <h2>Work tracker</h2>

                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <h4>Total processed motors</h4>

                    <div class="widget_summary">
                        <div class="w_left w_25">
                            <span>{{number_format($totalFilledPercent, 2)}}%</span>
                        </div>
                        <div class="w_center w_55">
                            <div class="progress">
                                <div class="progress-bar bg-green" role="progressbar"
                                     aria-valuenow="{{ceil($totalFilledPercent)}}"
                                     aria-valuemin="0" aria-valuemax="100"
                                     style="width: {{ceil($totalFilledPercent)}}%;">
                                    <span class="sr-only">{{$totalFilledPercent}}% Complete</span>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <h4>Total processed orders</h4>

                    <div class="widget_summary">
                        <div class="w_left w_25">
                            @if ($orders->count() > 0)
                                <span>{{number_format( ($ordersNotNew * 100) / $orders->count(), 2)}}%</span>
                            @else
                                <span>0%</span>
                            @endif
                        </div>
                        <div class="w_center w_55">
                            <div class="progress">
                                <div class="progress-bar bg-green" role="progressbar"
                                     aria-valuenow="{{ceil($totalFilledPercent)}}"
                                     aria-valuemin="0" aria-valuemax="100"
                                     style="width: @if ($orders->count() > 0) {{ceil((($ordersNotNew * 100) / $orders->count()))}}% @else 0% @endif;">
                                    <span class="sr-only">@if ($orders->count() > 0) {{($ordersNotNew * 100) / $orders->count()}}
                                        % @else 0% @endif Complete</span>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <h4>Total processed requests</h4>
                    @if ($countReq = $countRequests->where('processed', 1)->count() > 0)
                    <div class="widget_summary">
                        <div class="w_left w_25">

                                <span>{{number_format( ($countReq * 100) / $countRequests->count(), 2)}}
                                    %</span>

                        </div>
                        <div class="w_center w_55">
                            <div class="progress">
                                <div class="progress-bar bg-green" role="progressbar"
                                     aria-valuenow="{{ceil($totalFilledPercent)}}"
                                     aria-valuemin="0" aria-valuemax="100"
                                     style="width: {{ceil(($countRequests->where('processed', 1)->count() * 100) / $countRequests->count())}}%;">
                                    <span class="sr-only">{{$totalFilledPercent}}% Complete</span>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>

                    </div>
                    @endif

                </div>
            </div>
        </div>


        <div class="col-md-8 col-sm-8 col-xs-12">
            <div class="x_panel tile fixed_height_320">
                <div class="x_title">
                    <h2>Last orders</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    <table class="table">
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Sum</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                        @foreach($lastTenOrders as $order)
                            <tr>
                                <td>{{$order->invoice}}</td>
                                <td>{{$order->first_name}}</td>
                                <td>{{$order->total_price}} uah</td>
                                <td>@if ($order->status == 'new')
                                        <lable class="text-primary">{{trans("shop::admin.".$order->status )}}</lable>
                                    @elseif ($order->status == 'sold')
                                        <lable class="text-success">{{trans("shop::admin.".$order->status )}}</lable>
                                    @else
                                        <lable class="text-warning">{{trans("shop::admin.".$order->status )}}</lable>
                                    @endif</td>
                                <td class=" last"><a href="{{route('admin.shop.order.show', ['id' => $order->id])}}"><i
                                                class="fa fa-eye"></i></a></td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>

    </div>


    <div class="row">
        <div class="col-md-8 col-sm-8 col-xs-12">
        @stop

        @section('javascript')
            <!-- gauge js -->
                <script type="text/javascript" src="{{Module::asset('admin:js/gauge/gauge.min.js')}}"></script>
                <script type="text/javascript" src="{{Module::asset('admin:js/gauge/gauge_demo.js')}}"></script>
                <!-- chart js -->
                <script src="{{Module::asset('admin:js/chartjs/chart.min.js')}}"></script>
                <!-- bootstrap progress js -->
                <script src="{{Module::asset('admin:js/progressbar/bootstrap-progressbar.min.js')}}"></script>
                <script src="{{Module::asset('admin:js/nicescroll/jquery.nicescroll.min.js')}}"></script>
                <!-- icheck -->
                <script src="{{Module::asset('admin:js/icheck/icheck.min.js')}}"></script>
                <!-- daterangepicker -->
                <script type="text/javascript" src="{{Module::asset('admin:js/moment.min.js')}}"></script>
                <script type="text/javascript"
                        src="{{Module::asset('admin:js/datepicker/daterangepicker.js')}}"></script>
            @yield('javascript')

            <!-- flot js -->
                <!--[if lte IE 8]>
                <script type="text/javascript" src="{{Module::asset('admin:js/excanvas.min.js')}}"></script><![endif]-->
                <script type="text/javascript" src="{{Module::asset('admin:js/flot/jquery.flot.js')}}"></script>
                <script type="text/javascript" src="{{Module::asset('admin:js/flot/jquery.flot.pie.js')}}"></script>
                <script type="text/javascript"
                        src="{{Module::asset('admin:js/flot/jquery.flot.orderBars.js')}}"></script>
                <script type="text/javascript"
                        src="{{Module::asset('admin:js/flot/jquery.flot.time.min.js')}}"></script>
                <script type="text/javascript" src="{{Module::asset('admin:js/flot/date.js')}}"></script>
                <script type="text/javascript" src="{{Module::asset('admin:js/flot/jquery.flot.spline.js')}}"></script>
                <script type="text/javascript" src="{{Module::asset('admin:js/flot/jquery.flot.stack.js')}}"></script>
                <script type="text/javascript" src="{{Module::asset('admin:js/flot/curvedLines.js')}}"></script>
                <script type="text/javascript" src="{{Module::asset('admin:js/flot/jquery.flot.resize.js')}}"></script>
                <script>
                    $(document).ready(function () {
                        // [17, 74, 6, 39, 20, 85, 7]
                        //[82, 23, 66, 9, 99, 6, 2]
                        var data3 = [];
                        @foreach($ordersInMonth as $date => $orders)
                                <?php $dt = explode('-', $date); ?>
                                data3.push([gd('{{$dt[0]}}', '{{$dt[1]}}', '{{$dt[2]}}'), {{count($orders)}}]);
                        @endforeach
                                console.log(data3)
                        var data1 = [[gd(2015, 1, 1), 17], [gd(2015, 1, 2), 74], [gd(2015, 1, 3), 6], [gd(2015, 1, 4), 39], [gd(2015, 1, 5), 20], [gd(2015, 1, 6), 85], [gd(2015, 1, 7), 7]];

                        var data2 = [[gd(2015, 1, 1), 82], [gd(2015, 1, 2), 23], [gd(2015, 1, 3), 66], [gd(2015, 1, 4), 9], [gd(2015, 1, 5), 119], [gd(2015, 1, 6), 6], [gd(2015, 1, 7), 9]];
                        $("#canvas_dahs").length && $.plot($("#canvas_dahs"), [
//                            data1, data2
                            data3
                        ], {
                            series: {
                                lines: {
                                    show: false,
                                    fill: true
                                },
                                splines: {
                                    show: true,
                                    tension: 0.4,
                                    lineWidth: 1,
                                    fill: 0.4
                                },
                                points: {
                                    radius: 0,
                                    show: true
                                },
                                shadowSize: 2
                            },
                            grid: {
                                verticalLines: true,
                                hoverable: true,
                                clickable: true,
                                tickColor: "#d5d5d5",
                                borderWidth: 1,
                                color: '#fff'
                            },
                            colors: ["rgba(38, 185, 154, 0.38)", "rgba(3, 88, 106, 0.38)"],
                            xaxis: {
                                tickColor: "rgba(51, 51, 51, 0.06)",
                                mode: "time",
                                tickSize: [1, "day"],
                                //tickLength: 10,
                                axisLabel: "Date",
                                axisLabelUseCanvas: true,
                                axisLabelFontSizePixels: 12,
                                axisLabelFontFamily: 'Verdana, Arial',
                                axisLabelPadding: 10
                                //mode: "time", timeformat: "%m/%d/%y", minTickSize: [1, "day"]
                            },
                            yaxis: {
                                ticks: 8,
                                tickColor: "rgba(51, 51, 51, 0.06)",
                            },
                            tooltip: false
                        });

                        function gd(year, month, day) {
                            return new Date(year, month - 1, day).getTime();
                        }
                    });
                </script>

                <!-- worldmap -->
                <script type="text/javascript"
                        src="{{Module::asset('admin:js/maps/jquery-jvectormap-2.0.1.min.js')}}"></script>
                <script type="text/javascript" src="{{Module::asset('admin:js/maps/gdp-data.js')}}"></script>
                <script type="text/javascript"
                        src="{{Module::asset('admin:js/maps/jquery-jvectormap-world-mill-en.js')}}"></script>
                <script type="text/javascript"
                        src="{{Module::asset('admin:js/maps/jquery-jvectormap-us-aea-en.js')}}"></script>
                <script>
                    $(function () {
                        $('#world-map-gdp').vectorMap({
                            map: 'world_mill_en',
                            backgroundColor: 'transparent',
                            zoomOnScroll: false,
                            series: {
                                regions: [{
                                    values: gdpData,
                                    scale: ['#E6F2F0', '#149B7E'],
                                    normalizeFunction: 'polynomial'
                                }]
                            },
                            onRegionTipShow: function (e, el, code) {
                                el.html(el.html() + ' (GDP - ' + gdpData[code] + ')');
                            }
                        });
                    });
                </script>
                <!-- skycons -->
                <script src="{{Module::asset('admin:js/skycons/skycons.js')}}"></script>
                <script>
                    var icons = new Skycons({
                            "color": "#73879C"
                        }),
                        list = [
                            "clear-day", "clear-night", "partly-cloudy-day",
                            "partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",
                            "fog"
                        ],
                        i;

                    for (i = list.length; i--;)
                        icons.set(list[i], list[i]);

                    icons.play();
                </script>

                <!-- dashbord linegraph -->
                <script>
                    var doughnutData = [
                        {
                            value: 30,
                            color: "#455C73"
                        },
                        {
                            value: 30,
                            color: "#9B59B6"
                        },
                        {
                            value: 60,
                            color: "#BDC3C7"
                        },
                        {
                            value: 100,
                            color: "#26B99A"
                        },
                        {
                            value: 120,
                            color: "#3498DB"
                        }
                    ];
                    var myDoughnut = new Chart(document.getElementById("canvas1").getContext("2d")).Doughnut(doughnutData);
                </script>
                <!-- /dashbord linegraph -->
                <!-- datepicker -->
                <script type="text/javascript">
                    $(document).ready(function () {

                        var cb = function (start, end, label) {
                            console.log(start.toISOString(), end.toISOString(), label);
                            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                            //alert("Callback has fired: [" + start.format('MMMM D, YYYY') + " to " + end.format('MMMM D, YYYY') + ", label = " + label + "]");
                        }

                        var optionSet1 = {
                            startDate: moment().subtract(29, 'days'),
                            endDate: moment(),
                            minDate: '01/01/2012',
                            maxDate: '12/31/2015',
                            dateLimit: {
                                days: 60
                            },
                            showDropdowns: true,
                            showWeekNumbers: true,
                            timePicker: false,
                            timePickerIncrement: 1,
                            timePicker12Hour: true,
                            ranges: {
                                'Today': [moment(), moment()],
                                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                                'This Month': [moment().startOf('month'), moment().endOf('month')],
                                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                            },
                            opens: 'left',
                            buttonClasses: ['btn btn-default'],
                            applyClass: 'btn-small btn-primary',
                            cancelClass: 'btn-small',
                            format: 'MM/DD/YYYY',
                            separator: ' to ',
                            locale: {
                                applyLabel: 'Submit',
                                cancelLabel: 'Clear',
                                fromLabel: 'From',
                                toLabel: 'To',
                                customRangeLabel: 'Custom',
                                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                                firstDay: 1
                            }
                        };
                        $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
                        $('#reportrange').daterangepicker(optionSet1, cb);
                        $('#reportrange').on('show.daterangepicker', function () {
                            console.log("show event fired");
                        });
                        $('#reportrange').on('hide.daterangepicker', function () {
                            console.log("hide event fired");
                        });
                        $('#reportrange').on('apply.daterangepicker', function (ev, picker) {
                            console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));
                        });
                        $('#reportrange').on('cancel.daterangepicker', function (ev, picker) {
                            console.log("cancel event fired");
                        });
                        $('#options1').click(function () {
                            $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
                        });
                        $('#options2').click(function () {
                            $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb);
                        });
                        $('#destroy').click(function () {
                            $('#reportrange').data('daterangepicker').remove();
                        });
                    });
                </script>
                <script>
                    NProgress.done();
                </script>
                <!-- /datepicker -->
@stop