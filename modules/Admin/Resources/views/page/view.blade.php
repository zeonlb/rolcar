@extends('admin::layouts.master')

@section('content')
    <h1>Page</h1>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-bars"></i> View Page</h2>

                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form name="add-new-page" action="{{url('admin/cms/page/update', ['id' => $page->id])}}" method="POST"
                      class="form-horizontal form-label-left" novalidate="">
                    <input type="hidden" name="_method" value="put" />
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="" role="tabpanel" data-example-id="togglable-tabs">
                    <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">
                                Page
                            </a>
                        </li>
                        <li role="presentation" class="">
                            <a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">
                                SEO
                            </a>
                        </li>
                        <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab"
                                                            data-toggle="tab" aria-expanded="false">
                                Additional settings
                            </a>
                        </li>
                    </ul>
                    <div id="myTabContent" class="tab-content">
                        <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Name <span
                                                class="required">*</span>
                                    </label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input id="name"  name="name" class="form-control col-md-7 col-xs-12"  name="name"
                                               placeholder="My own page" required="required" type="text" value="{{$page->name}}">
                                    </div>
                                </div>
                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="url">URL <span
                                                class="required">*</span>
                                    </label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input id="url" name="url" class="form-control col-md-7 col-xs-12"  value="{{$page->url}}"
                                               placeholder="/my_first_page" required="required" type="text">
                                    </div>
                                </div>


                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Visible</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div id="visible" class="btn-group" data-toggle="buttons">
                                        <label class="btn btn-default @if ($page->visible) active @endif" data-toggle-class="btn-default" data-toggle-passive-class="btn-default">
                                            <input @if ($page->visible) checked="" @endif type="radio" name="visible" value="1"> &nbsp; On &nbsp;
                                        </label>
                                        <label class="btn btn-danger @if (!$page->visible) active @endif" data-toggle-class="btn-default" data-toggle-passive-class="btn-default">
                                            <input @if (!$page->visible) checked="" @endif  type="radio" name="visible" value="0" > Off
                                        </label>
                                    </div>
                                </div>
                            </div>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <textarea name="html" id="html">{{html_entity_decode($page->html)}}</textarea>
                                    <br />
                                </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
                                <span class="section">Meta data</span>

                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="meta_title">Meta title</label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input id="meta_title" class="form-control col-md-7 col-xs-12"
                                               name="meta_title"
                                               placeholder="Title" type="text" value="{{$page->meta->meta_title}}">

                                    </div>
                                </div>

                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="meta_keywords">Meta keys</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input id="tags_1" name="meta_keywords" type="text" class="tags form-control" value="{{$page->meta->meta_keywords}}" />
                                        <div id="suggestions-container" style="position: relative; float: left; width: 250px; margin: 10px;"></div>
                                    </div>
                                </div>
                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="meta_redirect_url">Meta redirect url</label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input placeholder="/new/url" type="text" id="meta_redirect_url" value="{{$page->meta->meta_redirect_url}}" name="meta_redirect_url" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>

                                <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="meta_description">Meta description</label>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea placeholder="Your page description" id="meta_description" name="meta_description" class="form-control col-md-7 col-xs-12">{{$page->meta->meta_description}}</textarea>
                                    </div>
                                </div>
                            <span class="section">Seo data</span>

                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="meta_description">Seo text</label>

                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <textarea placeholder="Your seo text" id="seo_text" name="seo_text" class="form-control col-md-7 col-xs-12">{{$page->meta->seo_text}}</textarea>
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="meta_description">Seo footer text</label>

                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <textarea placeholder="Your seo footer text" id="seo_footer_text" name="seo_footer_text" class="form-control col-md-7 col-xs-12">{{$page->meta->seo_footer_text}}</textarea>
                                </div>
                            </div>
                                <div class="ln_solid"></div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
                            <span class="section">Additional settings</span>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Show bread crumbs</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div id="show_bread_crumbs" class="btn-group" data-toggle="buttons">
                                        <label class="btn btn-default @if ($page->show_bread_crumbs) active @endif" data-toggle-class="btn-default" data-toggle-passive-class="btn-default">
                                            <input @if ($page->show_bread_crumbs) checked="" @endif type="radio" name="show_bread_crumbs" value="1"> &nbsp; Show &nbsp;
                                        </label>
                                        <label class="btn btn-danger @if (!$page->show_bread_crumbs) active @endif" data-toggle-class="btn-default" data-toggle-passive-class="btn-default">
                                            <input @if (!$page->show_bread_crumbs) checked="" @endif  type="radio" name="show_bread_crumbs" value="0" > Hide
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="javascript">Javascript</label>

                                <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea rows="10" placeholder="<script>your code</script>" id="javascript"
                                                  name="javascript"
                                                  class="form-control col-md-7 col-xs-12">{{$page->javascript}}</textarea>
                                </div>
                            </div>

                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="style">Style</label>

                                <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea rows="10" placeholder="<style>your code</style>" id="style"
                                                  name="style"
                                                  class="form-control col-md-7 col-xs-12">{{$page->style}}</textarea>
                                </div>
                            </div>
                            <div class="ln_solid"></div>
                        </div>
                    </div>
                </div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                            <button id="send" type="submit" class="btn btn-success">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
@stop

@section('javascript')
    @include('admin::partials.js.form')

    <!-- input tags -->
    <script>
        function onAddTag(tag) {
            alert("Added a tag: " + tag);
        }

        function onRemoveTag(tag) {
            alert("Removed a tag: " + tag);
        }

        function onChangeTag(input, tag) {
            alert("Changed a tag: " + tag);
        }

        $(function () {
            $('#tags_1').tagsInput({
                width: 'auto'
            });
        });
    </script>
    <!-- /input tags -->
    <!-- form validation -->
    <script type="text/javascript">
        $(document).ready(function () {
            // initialize the validator function
            validator.message['date'] = 'not a real date';

            // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
            $('form')
                    .on('blur', 'input[required], input.optional, select.required', validator.checkField)
                    .on('change', 'select.required', validator.checkField)
                    .on('keypress', 'input[required][pattern]', validator.keypress);

            $('.multi.required')
                    .on('keyup blur', 'input', function () {
                        validator.checkField.apply($(this).siblings().last()[0]);
                    });
            $('form').submit(function (e) {
                e.preventDefault();
                var submit = true;
                // evaluate the form using generic validaing
                if (!validator.checkAll($(this))) {
                    submit = false;
                }

                if (submit)
                    this.submit();
                return false;
            });
        });
    </script>
    <!-- /form validation -->
    <!-- Text editor -->
    <script>
        CKEDITOR.replace( 'html' );
        CKEDITOR.config.htmlEncodeOutput = false;
        CKEDITOR.config.entities = false;
        CKEDITOR.config.basicEntities = false;
        CKEDITOR.config.configentities = false;
        CKEDITOR.config.forceSimpleAmpersand = true;
        CKEDITOR.config.allowedContent = true;
        CKEDITOR.config.skin = 'bootstrapck';
        CKEDITOR.config.extraPlugins = 'font';
    </script>
    <!-- /Text editor -->   <!-- Text editor -->
    <script>
        CKEDITOR.replace( 'seo_text' );
        CKEDITOR.config.htmlEncodeOutput = false;
        CKEDITOR.config.entities = false;
        CKEDITOR.config.basicEntities = false;
        CKEDITOR.config.configentities = false;
        CKEDITOR.config.forceSimpleAmpersand = true;
        CKEDITOR.config.allowedContent = true;
        CKEDITOR.config.skin = 'bootstrapck';
        CKEDITOR.config.extraPlugins = 'font';
    </script>
    <!-- /Text editor -->   <!-- Text editor -->
    <script>
        CKEDITOR.replace( 'seo_footer_text' );
        CKEDITOR.config.htmlEncodeOutput = false;
        CKEDITOR.config.entities = false;
        CKEDITOR.config.basicEntities = false;
        CKEDITOR.config.configentities = false;
        CKEDITOR.config.forceSimpleAmpersand = true;
        CKEDITOR.config.allowedContent = true;
        CKEDITOR.config.skin = 'bootstrapck';
        CKEDITOR.config.extraPlugins = 'font';
    </script>
    <!-- /Text editor -->
@stop

