@if ((count($category->children) > 0) AND ($category->parent_id > 0))

    <li>    
        <form class="sort-update" action="{{url('admin/cms/navigation/update', ['id' => $category->id])}}" method="POST">
            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
            <input type="hidden" name="_method" value="PUT"/>
             <span style="color: green; font-weight: bold;">{{$category->id}}</span>
            <span>Name: </span><input type="text" class="input-submit" name="alias" value="{{$category->alias}}"/>
            <span>URL: </span><input type="text" class="input-submit" name="url" value="{{$category->url}}"/>
            <span>Вес: </span><input class="sort-input input-submit" type="text" name="sort" value="{{$category->sort}}"/>
            <span>Родитель: </span><input class="sort-input input-submit" type="text" name="parent_id" placeholder="parent" value="{{$category->parent_id}}"/>
            <button type="button" nid="{{$category->id}}" class="btn btn-danger delete-navigation"><i class="fa fa-remove"></i></button>
        </form>

@else

    <form class="sort-update" action="{{url('admin/cms/navigation/update', ['id' => $category->id])}}" method="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
        <input type="hidden" name="_method" value="PUT"/>
         <span style="color: green; font-weight: bold;">{{$category->id}}</span>
        <span>Name: </span><input type="text" class="input-submit" name="alias" value="{{$category->alias}}"/>
        <span>URL: </span><input type="text" class="input-submit" name="url" value="{{$category->url}}"/>
        <span>Вес: </span><input class="sort-input input-submit" type="text" name="sort" value="{{$category->sort}}"/>
        <span>Родитель: </span><input class="sort-input input-submit" type="text" name="parent_id" placeholder="parent" value="{{$category->parent_id}}"/>
        <button type="button" nid="{{$category->id}}" class="btn btn-danger delete-navigation"><i class="fa fa-remove"></i></button>
    </form>

@endif

    @if (count($category->children) > 0)

        <ul>

        @foreach($category->children as $category)

            @include('admin::partials.navigation.categories', $category)

        @endforeach

        </ul>

    @endif

    </li>