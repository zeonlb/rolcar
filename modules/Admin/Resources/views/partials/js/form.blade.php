<!-- form validation -->
<script type="text/javascript" src="{{Module::asset('admin:js/parsley/parsley.min.js')}}"></script>
<!-- chart js -->
<script src="{{Module::asset('admin:js/chartjs/chart.min.js')}}"></script>
<!-- bootstrap progress js -->
<script src="{{Module::asset('admin:js/progressbar/bootstrap-progressbar.min.js')}}"></script>
<script src="{{Module::asset('admin:js/nicescroll/jquery.nicescroll.min.js')}}"></script>
<!-- icheck -->
<script src="{{Module::asset('admin:js/icheck/icheck.min.js')}}"></script>
<!-- tags -->
<script src="{{Module::asset('admin:js/tags/jquery.tagsinput.min.js')}}"></script>
<!-- switchery -->
<script src="{{Module::asset('admin:js/switchery/switchery.min.js')}}"></script>
<!-- daterangepicker -->
<script type="text/javascript" src="{{Module::asset('admin:js/moment.min2.js')}}"></script>
<script type="text/javascript" src="{{Module::asset('admin:js/datepicker/daterangepicker.js')}}"></script>
<script src="{{Module::asset('admin:js/ckeditor/ckeditor.js')}}"></script>
<!-- select2 -->
<script src="{{Module::asset('admin:js/select/select2.full.js')}}"></script>
<!-- Autocomplete -->
<script src="{{Module::asset('admin:js/autocomplete/jquery.autocomplete.js')}}"></script>

<script src="{{Module::asset('admin:js/validator/validator.js')}}"></script>

<!-- Ace Editor -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/ace/1.1.3/ace.js"></script>
<script src="{{Module::asset('admin:vendor/textarea-as-ace-editor-master/dist/textarea-as-ace-editor.js')}}"></script>



