<!-- Create navigation modal -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <form method="POST" class="form-horizontal form-label-left" action="{{url('admin/cms/navigation/create')}}" novalidate="">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Create Navigation</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Блок меню </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select name="cms_group_navigation_id" required="" class="form-control">
                                @foreach ($navMenuNames as $key => $groupItem)
                                    <option value="{{$key}}">{{$groupItem['title']}}</option>
                                @endforeach    
                            </select>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Название</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input id="alias" class="form-control col-md-7 col-xs-12"  name="alias"
                                   placeholder="Название" required="required" type="text">
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">URL </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input id="url" class="form-control col-md-7 col-xs-12"  name="url"
                                   placeholder="/our/url" required="required" type="text">
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Sort </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select name="sort" required="" class="form-control">
                                @for ($i = 0; $i < 10; $i++)
                                    <option value="{{$i}}">{{$i}}</option>
                                @endfor
                            </select>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Lang </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select name="lang" class="form-control">
                                <option value="ru">ru</option>
                            </select>
                        </div>
                    </div>
                </div>
                <br/>
                <br/>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button id="send" type="submit" class="btn btn-success">Save</button>
                </div>
            </div>
        </form>
    </div>
</div>