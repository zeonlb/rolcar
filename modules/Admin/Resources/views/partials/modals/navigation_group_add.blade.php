<!-- Create navigation modal -->
<div class="modal fade bs-group-modal-lg" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <form method="POST" class="form-horizontal form-label-left" action="{{url('admin/cms/navigationGroup/create')}}" novalidate="">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Новый блок меню</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Название</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input id="alias" class="form-control col-md-7 col-xs-12"  name="title"
                                   placeholder="Название" required="required" type="text">
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Програмное название (En)</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input id="alias" class="form-control col-md-7 col-xs-12"  name="name"
                                   placeholder="Програмное название (En)" required="required" type="text">
                        </div>
                    </div>
                </div>
                <br/>
                <br/>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button id="send" type="submit" class="btn btn-success">Save</button>
                </div>
            </div>
        </form>
    </div>
</div>