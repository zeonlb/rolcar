@if ($errors->any())
    <script type="text/javascript">
        $('document').ready(function(){})
        @foreach($errors->all() as $error)
            new PNotify({
                title: 'Error!',
                text: '{{$error}}',
                type: 'error'
            });
        @endforeach
    </script>
@endif

@if (session('success'))
    <script type="text/javascript">
        $('document').ready(function(){})
            new PNotify({
                title: 'Success!',
                text: '{{session('success')}}',
                type: 'success'
            });
    </script>
@endif
