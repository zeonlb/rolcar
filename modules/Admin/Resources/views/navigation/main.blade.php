@extends('admin::layouts.master')

@section('content')
                 <div class="col-md-8"><h1>Управление навигацией</h1></div>
                <div class="col-md-4">
                    <a href="#">
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target=".bs-example-modal-lg">
                            Добавить пункт меню
                        </button>
                        @include('admin::partials.modals.navigation_add')
                    </a>
                    <a href="#">
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target=".bs-group-modal-lg">
                            Добавить новый блок меню
                        </button>
                        @include('admin::partials.modals.navigation_group_add')
                    </a>
                </div> 

        <div class="x_panel">
            <div class="x_content">
                @foreach($navMenuNames as $key => $nav)

                    <div class="panel panel-default col-md-12">
                      <div class="panel-heading">
                            <button type="button" nid="{{$key}}" class="btn delete-navigationGroup">
                                <i class="fa fa-remove"></i>
                            </button>
                            {{$nav['title']}} ({{$nav['name']}})
                      </div>
                      <div class="panel-body">
                        <div class="mainnmenu">
                            <nav>
                                <ul class="main-nagivation">
                                    @each('admin::partials.navigation.categories', $nav['items'], 'category', 'admin::partials.navigation.categories_nothing')
                                </ul>
                            </nav>
                            <hr>
                        </div>
                      </div>
                    </div>
                @endforeach               
            </div>
        </div>

@stop

@section('javascript')
    <!-- bootstrap progress js -->
    <script src="{{Module::asset('admin:js/progressbar/bootstrap-progressbar.min.js')}}"></script>
    <script src="{{Module::asset('admin:js/nicescroll/jquery.nicescroll.min.js')}}"></script>

    <script src="{{Module::asset('admin:js/validator/validator.js')}}"></script>
    <!-- form validation -->
    <script type="text/javascript">
        $(document).ready(function () {
            // initialize the validator function
            validator.message['date'] = 'not a real date';

            // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
            $('form')
                    .on('blur', 'input[required], input.optional, select.required', validator.checkField)
                    .on('change', 'select.required', validator.checkField)
                    .on('keypress', 'input[required][pattern]', validator.keypress);

            $('.multi.required')
                    .on('keyup blur', 'input', function () {
                        validator.checkField.apply($(this).siblings().last()[0]);
                    });
            $('form').submit(function (e) {
                e.preventDefault();
                var submit = true;
                if (!validator.checkAll($(this))) {
                    submit = false;
                }
                if (submit)
                    this.submit();
                return false;
            });
        });
    </script>
    <!-- /form validation -->
    <script>
        $('document').ready(function(){
            $('.input-submit').on('change', function(){
                $(this).submit();
            });
            $('.sort-input').on('change', function(){
                $(this).submit();
            });
            $('.delete-navigation').on('click', function(){
                var id = $(this).attr('nid');
                $.ajax({
                    type: "POST",
                    dataType: "JSON",
                    url: "{{url('admin/cms/navigation/delete')}}",
                    data: {'id': id},
                    success: function(response, textStatus, xhr){
                        if (response.status == 'SUCCESS') {
                            window.location.reload();
                        }
                    },
                    error: function(){

                            new PNotify({
                                title: 'Error!',
                                text: 'Could not remove navigation! System error!',
                                type: 'error'
                            });

                    }
                });
            });
            $('.delete-navigationGroup').on('click', function(){
                var id = $(this).attr('nid');
                $.ajax({
                    type: "POST",
                    dataType: "JSON",
                    url: "{{url('admin/cms/navigationGroup/delete')}}",
                    data: {'id': id},
                    success: function(response, textStatus, xhr){
                        if (response.status == 'SUCCESS') {
                            window.location.reload();
                        }
                    },
                    error: function(){

                            new PNotify({
                                title: 'Error!',
                                text: 'В блоке есть пункты меню',
                                type: 'error'
                            });

                    }
                });
            });            

        });

    </script>
@stop

@section('style')
    <style>
        .sort-input {
            height: 33px;margin: 0;width: 35px; position: relative; top: -2px;text-align: center;
        }
    </style>
    @stop
