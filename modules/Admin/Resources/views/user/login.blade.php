@extends('admin::layouts.login')

@section('content')

    <div id="login" class="animate form">
        <section class="login_content">
            <form role="form" method="POST" action="{{url('admin/login')}}">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong>
                        There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <h1>Login Form</h1>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div>
                    <input type="email" name="email" class="form-control" placeholder="E-Mail" required="" />
                </div>
                <div>
                    <input type="password" name="password" class="form-control" placeholder="Password" required="" />
                </div>
                <div>
                    <button type="submit" class="btn btn-default submit">Log in</button>
                </div>
                <div class="clearfix"></div>

            </form>
            <!-- form -->
        </section>
        <!-- content -->
    </div>

@stop