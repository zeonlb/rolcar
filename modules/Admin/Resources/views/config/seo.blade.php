@extends('admin::layouts.master')

@section('content')
    <div >
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>List of roles</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="col-md-12">
                            <div class="col-md-10"></div>
                            <div class="col-md-2">
                                <a href="#">
                                    <button type="button" class="btn btn-success" data-toggle="modal" data-target=".bs-example-modal-lg">
                                        Create
                                    </button>
                                    @include('admin::partials.modals.create_entity')
                                </a>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="panel">
                                <p>Commands: cyrillic, lower, upper (example: model|upper)</p>
                                <table  class="table">
                                    <thead>
                                    <tr class="headings">
                                        <th>Page</th>
                                        <th>Vars</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="even pointer">
                                           <td>Catalog transport</td>
                                           <td>transport</td>
                                        </tr>
                                        <tr class="even pointer">
                                           <td>Catalog model</td>
                                           <td>transport,mark</td>
                                        </tr>
                                        <tr class="even pointer">
                                           <td>Catalog modifications</td>
                                           <td>transport,mark,model</td>
                                        </tr>
                                        <tr class="even pointer">
                                           <td>Catalog motors</td>
                                           <td>transport,mark,model,modification</td>
                                        </tr>
                                        <tr class="even pointer">
                                           <td>Catalog lamps</td>
                                           <td>transport,mark,model,modification,motor</td>
                                        </tr>
                                        <tr class="even pointer">
                                           <td>Products list with light</td>
                                           <td>brand,modelnyy_ryad,tsokol,tekhnologiya,moschnost,tsvetovaya_temperatura_lapmy,voltazh,transport,mark,model,modification,motor,light,product</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <table  class="table">
                            <thead>
                            <tr class="headings">
                                <th>ID </th>
                                <th>Page code </th>
                                <th>Title</th>
                                <th>H1</th>
                                <th>Created at</th>
                                <th class=" no-link last"><span class="nobr">Action</span>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($pageSettings as $ps)
                                <tr class="even pointer">
                                    <td>{{$ps->id}}</td>
                                    <td>{{\Modules\Cms\Entities\PageSettings::PAGES_LIST[$ps->page_code]}}</td>
                                    <td>{{$ps->seo_title}}</td>
                                    <td>{{$ps->seo_h1}}</td>
                                    <td>{{$ps->created_at}}</td>
                                    <td class=" last">
                                        <a href="{{url('admin/config/seo-edit', ['id' => $ps->id])}}"><button  type="submit" class="btn btn-warning btn-xs">edit</button></a>
                                        <form class="entity-delete-form" method="POST" action="{{url('admin/config/seo', ['id' => $ps->id])}}">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                                        <button  type="submit" class="entity-delete btn btn-danger btn-xs">delete</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <br />
            <br />
            <br />

        </div>
    </div>
@stop

@section('javascript')
    @include('admin::partials.js.form')
    <script>
        $(document).ready(function () {
            <!-- Text editor -->
            CKEDITOR.replace('html');
            CKEDITOR.config.htmlEncodeOutput = false;
            CKEDITOR.config.entities = false;
            CKEDITOR.config.basicEntities = false;
            CKEDITOR.config.configentities = false;
            CKEDITOR.config.forceSimpleAmpersand = true;
            CKEDITOR.config.allowedContent = true;
            CKEDITOR.config.skin = 'bootstrapck';
            CKEDITOR.config.extraPlugins = 'font';
            <!-- /Text editor -->
            <!-- Text editor -->
            CKEDITOR.replace('html2');
            CKEDITOR.config.htmlEncodeOutput = false;
            CKEDITOR.config.entities = false;
            CKEDITOR.config.basicEntities = false;
            CKEDITOR.config.configentities = false;
            CKEDITOR.config.forceSimpleAmpersand = true;
            CKEDITOR.config.allowedContent = true;
            CKEDITOR.config.skin = 'bootstrapck';
            CKEDITOR.config.extraPlugins = 'font';
            <!-- /Text editor -->
        });
    </script>
@stop
@section('style')
    <link href="{{Module::asset('admin:css/custom.css')}}" rel="stylesheet">
    <link href="{{Module::asset('admin:css/icheck/flat/green.css')}}" rel="stylesheet">
    <link href="{{Module::asset('admin:css/datatables/tools/css/dataTables.tableTools.css')}}" rel="stylesheet">
    @stop

