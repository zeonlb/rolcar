@extends('admin::layouts.master')

@section('content')
    <div >
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>List of roles</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        {!! form($form) !!}
                    </div>
                </div>
            </div>

            <br />
            <br />
            <br />

        </div>
    </div>
@stop

@section('javascript')
    @include('admin::partials.js.form')
    <script>
        $(document).ready(function () {
            <!-- Text editor -->
            CKEDITOR.replace('html');
            CKEDITOR.config.htmlEncodeOutput = false;
            CKEDITOR.config.entities = false;
            CKEDITOR.config.basicEntities = false;
            CKEDITOR.config.configentities = false;
            CKEDITOR.config.forceSimpleAmpersand = true;
            CKEDITOR.config.allowedContent = true;
            CKEDITOR.config.skin = 'bootstrapck';
            CKEDITOR.config.extraPlugins = 'font';
            <!-- /Text editor -->
            <!-- Text editor -->
            CKEDITOR.replace('html2');
            CKEDITOR.config.htmlEncodeOutput = false;
            CKEDITOR.config.entities = false;
            CKEDITOR.config.basicEntities = false;
            CKEDITOR.config.configentities = false;
            CKEDITOR.config.forceSimpleAmpersand = true;
            CKEDITOR.config.allowedContent = true;
            CKEDITOR.config.skin = 'bootstrapck';
            CKEDITOR.config.extraPlugins = 'font';
            <!-- /Text editor -->
        });
    </script>
@stop
@section('style')
    <link href="{{Module::asset('admin:css/custom.css')}}" rel="stylesheet">
    <link href="{{Module::asset('admin:css/icheck/flat/green.css')}}" rel="stylesheet">
    <link href="{{Module::asset('admin:css/datatables/tools/css/dataTables.tableTools.css')}}" rel="stylesheet">
    @stop

