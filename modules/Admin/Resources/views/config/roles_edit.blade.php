@extends('admin::layouts.master')

@section('content')
    <h1>Edit role</h1>
    <div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>{{$role->name}}</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <form method="POST" action="{{url('admin/config/role')}}">
                            <input type="hidden" name="_method" value="put">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <input type="hidden" name="role_id" value="{{$role->id}}">
                            <table class="table">
                                <thead>
                                <tr class="headings">
                                    <th>Name</th>
                                    <th>Url</th>
                                    <th class=" no-link last"><span class="nobr">Block</span>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($menuItems as $item)
                                    <tr class="even pointer">
                                        <?php
                                        $parentModule = clearMenuTags($item->title);
                                        ?>
                                        <td><b>{{$parentModule}}</b></td>
                                        <td>{{$item->url}}</td>
                                        <td class="last">
                                            <?php
                                            $parentChecked = false;
                                            if (is_array($role->options) && array_key_exists($parentModule, $role->options))
                                            {
                                                if (array_key_exists(0, $role->options[$parentModule]) ||
                                                    array_key_exists($item->url, $role->options[$parentModule])
                                                ){
                                                    $parentChecked = true;
                                                }

                                            }
                                            ?>
                                            <input @if($parentChecked) checked
                                                   @endif name="options[{{$parentModule}}][{{($item->url)}}][]"
                                                   value="1" type="checkbox"/>
                                        </td>
                                    </tr>
                                    @if($item->childs)
                                        @foreach($item->childs as $i)
                                            <?php
                                            $childModule = clearMenuTags($i->title);
                                            ?>
                                            <tr class="even pointer child-item">
                                                <td>&nbsp&nbsp&nbsp&nbsp{{$childModule}}</td>
                                                <td>{{$i->url}}</td>
                                                <td class=" last">
                                                    <?php
                                                    $childChecked = false;
                                                    if (is_array($role->options) && array_key_exists($parentModule, $role->options) &&
                                                        array_key_exists($i->url, $role->options[$parentModule])
                                                    ) {
                                                        $childChecked = true;
                                                    }
                                                    ?>
                                                    <input @if($childChecked || $parentChecked) checked
                                                           @endif name="options[{{$parentModule}}][{{($i->url)}}][]"
                                                           value="1" type="checkbox"/>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                @endforeach

                                </tbody>
                            </table>
                            <button type="submit" class="btn btn-success">Save</button>
                        </form>
                    </div>
                </div>
            </div>

            <br/>
            <br/>
            <br/>

        </div>
    </div>
@stop

@section('javascript')
    <!-- chart js -->
    <script src="{{Module::asset('admin:js/chartjs/chart.min.js')}}"></script>
    <!-- bootstrap progress js -->
    <script src="{{Module::asset('admin:js/progressbar/bootstrap-progressbar.min.js')}}"></script>
    <script src="{{Module::asset('admin:js/nicescroll/jquery.nicescroll.min.js')}}"></script>
    <!-- icheck -->
    <script src="{{Module::asset('admin:js/icheck/icheck.min.js')}}"></script>

    <!-- Datatables -->
    <script src="{{Module::asset('admin:js/datatables/js/jquery.dataTables.js')}}"></script>
    <script src="{{Module::asset('admin:js/datatables/tools/js/dataTables.tableTools.js')}}"></script>
    <script>
        $(document).ready(function () {
            $('input.tableflat').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });
        });

        var asInitVals = new Array();
        $(document).ready(function () {
            var oTable = $('#example').dataTable({
                "oLanguage": {
                    "sSearch": "Search all columns:"
                },
                "aoColumnDefs": [
                    {
                        'bSortable': false,
                        'aTargets': [0]
                    } //disables sorting for column one
                ],
                'iDisplayLength': 12,
                "sPaginationType": "full_numbers",
                "dom": 'T<"clear">lfrtip',
                "tableTools": {}
            });
            $("tfoot input").keyup(function () {
                /* Filter on the column based on the index of this element's parent <th> */
                oTable.fnFilter(this.value, $("tfoot th").index($(this).parent()));
            });
            $("tfoot input").each(function (i) {
                asInitVals[i] = this.value;
            });
            $("tfoot input").focus(function () {
                if (this.className == "search_init") {
                    this.className = "";
                    this.value = "";
                }
            });
            $("tfoot input").blur(function (i) {
                if (this.value == "") {
                    this.className = "search_init";
                    this.value = asInitVals[$("tfoot input").index(this)];
                }
            });
        });
    </script>
@stop
@section('style')
    <link href="{{Module::asset('admin:css/custom.css')}}" rel="stylesheet">
    <link href="{{Module::asset('admin:css/icheck/flat/green.css')}}" rel="stylesheet">
    <link href="{{Module::asset('admin:css/datatables/tools/css/dataTables.tableTools.css')}}" rel="stylesheet">
@stop

