@extends('admin::layouts.master')

@section('content')
    <h1>Users</h1>
    <div class="">
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>List of users</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="col-md-12">
                            <div class="col-md-10"></div>
                            <div class="col-md-2">
                                <a href="#">
                                    <button type="button" class="btn btn-success" data-toggle="modal" data-target=".bs-example-modal-lg">
                                        Create new user
                                    </button>
                                    @include('admin::partials.modals.create_entity')
                                </a>
                            </div>
                        </div>
                        <table  class="table">
                            <thead>
                            <tr class="headings">
                                <th>ID </th>
                                <th>Name </th>
                                <th>Email </th>
                                <th>Role </th>
                                <th>Created at</th>
                                <th class=" no-link last"><span class="nobr">Action</span>
                                </th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($users as $user)
                                <tr class="even pointer">
                                    <td class=" ">{{$user->id}}</td>
                                    <td class=" ">{{$user->name}}</td>
                                    <td class=" ">{{$user->email}}</td>
                                    @if($user->role)
                                        <td class=" ">{{$user->role->name}}</td>
                                    @else
                                        <td></td>
                                    @endif
                                    <td class=" ">{{$user->created_at}}</td>
                                    <td class=" last">
                                        <form class="entity-delete-form" method="POST" action="{{url('admin/config/user', ['id' => $user->id])}}">
                                            <input type="hidden" name="_method" value="DELETE">
                                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                                            <button  type="submit" class="entity-delete btn btn-danger btn-xs">delete</button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach

                            </tbody>

                        </table>
                    </div>
                </div>
            </div>

            <br />
            <br />
            <br />

        </div>
    </div>
@stop

@section('javascript')
    <!-- chart js -->
    <script src="{{Module::asset('admin:js/chartjs/chart.min.js')}}"></script>
    <!-- bootstrap progress js -->
    <script src="{{Module::asset('admin:js/progressbar/bootstrap-progressbar.min.js')}}"></script>
    <script src="{{Module::asset('admin:js/nicescroll/jquery.nicescroll.min.js')}}"></script>
    <!-- icheck -->
    <script src="{{Module::asset('admin:js/icheck/icheck.min.js')}}"></script>

    <!-- Datatables -->
    <script src="{{Module::asset('admin:js/datatables/js/jquery.dataTables.js')}}"></script>
    <script src="{{Module::asset('admin:js/datatables/tools/js/dataTables.tableTools.js')}}"></script>
    <script>
        $(document).ready(function () {
            $('input.tableflat').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });
        });

        var asInitVals = new Array();
        $(document).ready(function () {
            var oTable = $('#example').dataTable({
                "oLanguage": {
                    "sSearch": "Search all columns:"
                },
                "aoColumnDefs": [
                    {
                        'bSortable': false,
                        'aTargets': [0]
                    } //disables sorting for column one
                ],
                'iDisplayLength': 12,
                "sPaginationType": "full_numbers",
                "dom": 'T<"clear">lfrtip',
                "tableTools": {
                }
            });
            $("tfoot input").keyup(function () {
                /* Filter on the column based on the index of this element's parent <th> */
                oTable.fnFilter(this.value, $("tfoot th").index($(this).parent()));
            });
            $("tfoot input").each(function (i) {
                asInitVals[i] = this.value;
            });
            $("tfoot input").focus(function () {
                if (this.className == "search_init") {
                    this.className = "";
                    this.value = "";
                }
            });
            $("tfoot input").blur(function (i) {
                if (this.value == "") {
                    this.className = "search_init";
                    this.value = asInitVals[$("tfoot input").index(this)];
                }
            });
        });
    </script>
@stop
@section('style')
    <link href="{{Module::asset('admin:css/custom.css')}}" rel="stylesheet">
    <link href="{{Module::asset('admin:css/icheck/flat/green.css')}}" rel="stylesheet">
    <link href="{{Module::asset('admin:css/datatables/tools/css/dataTables.tableTools.css')}}" rel="stylesheet">
    @stop

