@extends('admin::layouts.master')

@section('content')
    <h1>System settings</h1>
    <div class="">
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Settings</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <form action="{{$urlSave}}" method="POST">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            @foreach($systemSettings as $setting)
                                <div class="item form-group col-xs-12">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12 required">{{$setting->name}}</label>
                                    @if ($setting->type == 'text')
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input class="form-control" value="{{$setting->value}}" name="config[{{$setting->id}}]" type="text">
                                        </div>
                                    @elseif($setting->type == 'textarea')
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <textarea class="form-control" name="config[{{$setting->id}}]">{{$setting->value}}</textarea>
                                        </div>
                                    @endif
                                </div>
                            @endforeach
                            <div class="col-md-offset-8 col-md-6 col-sm-6 col-xs-12">
                                <button class="btn btn-default">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <br />
            <br />
            <br />

        </div>
    </div>
@stop

@section('javascript')
    <!-- chart js -->
    <script src="{{Module::asset('admin:js/chartjs/chart.min.js')}}"></script>
    <!-- bootstrap progress js -->
    <script src="{{Module::asset('admin:js/progressbar/bootstrap-progressbar.min.js')}}"></script>
    <script src="{{Module::asset('admin:js/nicescroll/jquery.nicescroll.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ace/1.1.3/ace.js"></script>
    <script src="{{Module::asset('admin:vendor/textarea-as-ace-editor-master/dist/textarea-as-ace-editor.js')}}"></script>
    <script>
        $( document ).ready(function() {

            $("textarea").asAceEditor();
            var editor = $('textarea').data('ace-editor');
            $(".ace_editor").css({"max-width" : "100%"});
            editor.getSession().on('change', function(){
                $('#layout').val(editor.getSession().getValue());
            });
        });
    </script>

@stop
@section('style')
    <link href="{{Module::asset('admin:css/custom.css')}}" rel="stylesheet">
    <link href="{{Module::asset('admin:css/icheck/flat/green.css')}}" rel="stylesheet">
    <link href="{{Module::asset('admin:css/datatables/tools/css/dataTables.tableTools.css')}}" rel="stylesheet">
    @stop

