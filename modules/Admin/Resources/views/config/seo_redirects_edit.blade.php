@extends('admin::layouts.master')

@section('content')
    <div >
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>List of roles</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        {!! form($form) !!}
                    </div>
                </div>
            </div>

            <br />
            <br />
            <br />

        </div>
    </div>
@stop

@section('javascript')
    @include('admin::partials.js.form')
@stop
@section('style')
    <link href="{{Module::asset('admin:css/custom.css')}}" rel="stylesheet">
    <link href="{{Module::asset('admin:css/icheck/flat/green.css')}}" rel="stylesheet">
    <link href="{{Module::asset('admin:css/datatables/tools/css/dataTables.tableTools.css')}}" rel="stylesheet">
@stop

