@extends('admin::layouts.master')

@section('content')
    <div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Edit banner</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <form method="POST" class="form-horizontal form-label-left"
                              action="{{url('admin/cms/slider/update', ['id' => $slider->id])}}" novalidate="">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                            <input type="hidden" name="_method" value="put"/>

                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Name</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input id="name" value="{{$slider->name}}" class="form-control col-md-7 col-xs-12"
                                           name="name"
                                           placeholder="Name" required="required" type="text">
                                    <input id="code" class="form-control col-md-7 col-xs-12" value="{{$slider->code}}"
                                           name="code"
                                           placeholder="code" required="required" type="hidden">
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Code </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input disabled id="code" class="form-control col-md-7 col-xs-12" value="{{$slider->code}}"
                                           name="code"
                                           placeholder="code" required="required" type="text">
                                </div>
                            </div>
                            <br />
                            <br />
                            <hr />
                            @foreach($slider->files as $key => $file)
                                <div class="panel panel-default file-block">

                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12"> </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <img src="{{$file->image_1920}}" alt="" width="400px">
                                            <img src="{{$file->image_768}}" alt="" width="200px">
                                            <img src="{{$file->image_480}}" alt="" width="100px">
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Image 1920 </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input id="code" class="form-control col-md-7 col-xs-12"
                                                   value="{{$file->image_1920}}" name="files[{{$key}}][image_1920]" required
                                                   placeholder="file" type="text">

                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Image 768 </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input id="code" class="form-control col-md-7 col-xs-12"
                                                   value="{{$file->image_768}}" name="files[{{$key}}][image_768]" required
                                                   placeholder="file" type="text">

                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Image 480 </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input id="code" class="form-control col-md-7 col-xs-12"
                                                   value="{{$file->image_480}}" name="files[{{$key}}][image_480]" required
                                                   placeholder="file" type="text">

                                        </div>
                                    </div>                                                                        
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Title </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input id="title" class="form-control col-md-7 col-xs-12" value="{{$file->title}}"
                                                   name="files[{{$key}}][title]" required
                                                   placeholder="title" type="text">
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Url </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input id="code" class="form-control col-md-7 col-xs-12" value="{{$file->url}}"
                                                   required
                                                   name="files[{{$key}}][url]"
                                                   placeholder="url" type="text">
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Sort </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input id="code" class="form-control col-md-7 col-xs-12" value="{{$file->sort}}"
                                                   name="files[{{$key}}][sort]"
                                                   placeholder="file" type="text">
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <button class="delete-file btn btn-danger btn-xs">delete</button>
                                        </div>
                                    </div>

                                </div>
                            @endforeach
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12"> </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <button id="slider-add-img" class="btn btn-info">Add image</button>
                                </div>
                            </div>

                            <div class="slider-imgages">
                            </div>
                            <br/>
                            <br/>

                            <div class="modal-footer">
                                <button id="send" type="submit" class="btn btn-success">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    @include('admin::partials.js.form')
    <script>
        $(document).ready(function () {
            $('#slider-add-img').on('click', function (e) {
                e.preventDefault();
                var key = $('.file-block').length + 1;
                var html = '<div class="panel  file-block"> ' +
                    '                    <div class="item form-group"> ' +
                    '<label class="control-label col-md-3 col-sm-3 col-xs-12">Image 1920 </label> ' +
                    '<div class="col-md-6 col-sm-6 col-xs-12"> ' +
                    '<input  required id="code" class="form-control col-md-7 col-xs-12"' +
                    'name="files['+key+'][image_1920]" placeholder="file" type="text"></div> ' +
                    '</div> ' +
                    '                    <div class="item form-group"> ' +
                    '<label class="control-label col-md-3 col-sm-3 col-xs-12">Image 768 </label> ' +
                    '<div class="col-md-6 col-sm-6 col-xs-12"> ' +
                    '<input  required id="code" class="form-control col-md-7 col-xs-12"' +
                    'name="files['+key+'][image_768]" placeholder="file" type="text"></div> ' +
                    '</div> ' +
                    '                    <div class="item form-group"> ' +
                    '<label class="control-label col-md-3 col-sm-3 col-xs-12">Image 480 </label> ' +
                    '<div class="col-md-6 col-sm-6 col-xs-12"> ' +
                    '<input  required id="code" class="form-control col-md-7 col-xs-12"' +
                    'name="files['+key+'][image_480]" placeholder="file" type="text"></div> ' +
                    '</div> ' +                                        
                    '<div class="item form-group"> ' +
                    '<label class="control-label col-md-3 col-sm-3 col-xs-12">Title </label> ' +
                    '<div class="col-md-6 col-sm-6 col-xs-12"> ' +
                    '<input  required id="title" class="form-control col-md-7 col-xs-12" ' +
                    'name="files['+key+'][title]" placeholder="title" type="text"> </div> </div> ' +
                    '<div class="item form-group"> ' +
                    '<label class="control-label col-md-3 col-sm-3 col-xs-12">Url </label> ' +
                    '<div class="col-md-6 col-sm-6 col-xs-12">' +
                    '<input  required id="code" class="form-control col-md-7 col-xs-12" value=""' +
                    'name="files['+key+'][url]" placeholder="url" type="text"> </div> </div> ' +
                    '<div class="item form-group"> ' +
                    '<label class="control-label col-md-3 col-sm-3 col-xs-12">Sort </label> ' +
                    '<div class="col-md-6 col-sm-6 col-xs-12"> ' +
                    '<input id="code" class="form-control col-md-7 col-xs-12" value="" name="files['+key+'][sort]" placeholder="sort" type="text"> ' +
                    '</div> </div> ' +
                    '<div class="item form-group"> <label class="control-label col-md-3 col-sm-3 col-xs-12"></label> ' +
                    '<div class="col-md-6 col-sm-6 col-xs-12"> ' +
                    '<button class="delete-file btn btn-danger btn-xs">delete</button> </div> </div> </div>';
                $('.form-group:last').after(html)
            })
            $('.x_panel').on('click', '.delete-file', function (e) {
                e.preventDefault();
                $(this).parent().parent().parent().remove();
            })
        });
    </script>
@stop
@section('style')
    <link href="{{Module::asset('admin:css/custom.css')}}" rel="stylesheet">
    <link href="{{Module::asset('admin:css/icheck/flat/green.css')}}" rel="stylesheet">
    <link href="{{Module::asset('admin:css/datatables/tools/css/dataTables.tableTools.css')}}" rel="stylesheet">
@stop

