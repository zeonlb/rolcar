@extends('admin::layouts.master')

@section('content')
    <h1>Layout</h1>
    <form method="POST" action="{{url('admin/cms/layout/update')}}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <textarea class="form-control" name="layout" id="layout" cols="30" rows="10">{!! html_entity_decode($layout) !!}</textarea>
        <button type="submit" class="btn-success btn">Save</button>
    </form>
@stop

@section('javascript')
    <script src="{{Module::asset('admin:js/chartjs/chart.min.js')}}"></script>
    <!-- bootstrap progress js -->
    <script src="{{Module::asset('admin:js/progressbar/bootstrap-progressbar.min.js')}}"></script>
    <script src="{{Module::asset('admin:js/nicescroll/jquery.nicescroll.min.js')}}"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.1/js/bootstrap.min.js"></script>

    <script src="//cdnjs.cloudflare.com/ajax/libs/ace/1.1.3/ace.js"></script>
    <script src="{{Module::asset('admin:vendor/textarea-as-ace-editor-master/dist/textarea-as-ace-editor.js')}}"></script>
    <script>
        $( document ).ready(function() {
            $("#layout").asAceEditor();
            var editor = $('#layout').data('ace-editor');
            $(".ace_editor").css({"max-width" : "100%"});
            editor.getSession().on('change', function(){
                $('#layout').val(editor.getSession().getValue());
            });
        });
    </script>

@stop

@section('style')
    <link href="{{Module::asset('admin:css/editor/index.css')}}" rel="stylesheet">
@stop
